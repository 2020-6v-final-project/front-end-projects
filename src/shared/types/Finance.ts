export type Finance = {
    id: number;
    currentbalance: number;
    branchid:number;
    bankname:string;
    statusid:number;
    statuscode:string;
    currencyid:number;
    isbankaccount:boolean;
    accountnumber:string;
}