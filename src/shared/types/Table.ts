export type Table = {
    id: number;
    branchid: number;
    slots: number;
    statusid: number;
    statuscode: string;
    floor: string;
};