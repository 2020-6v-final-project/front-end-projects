export interface BranchItem {
    branchid: number;
    itemid: number;
    currentquantity: number;
    statusid: number;
    statuscode: string;
}