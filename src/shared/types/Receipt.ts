export type Receipt = {
    id: number;
    branchid: number;
    repositoryid: number;
    createdby: number;
    createdat: Date;
    total: number;
    statusid: number;
    statuscode: string;
    note:string;
    billdata:string;
    referenceid:number;
    receipttypeid:number;
    approvedrequestid:number;
}