export type PromotionCondition = {
    id: number;
    promotion_id: number;
    type: number;
    itemid: number;
    item_quantity: number;
    item_type_id: number;
    order_total: number;
    statusid: number;
    statuscode: string;
} | {
    type: number;
    itemid: number;
    item_quantity: number;
} | {
    type: number;
    order_total: number;
} | {
    type: number;
    item_type_id: number;
    item_quantity: number;
} | {
    type: number;
    item_quantity: number;
}
