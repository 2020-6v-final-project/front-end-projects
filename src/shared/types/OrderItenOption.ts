export interface OrderItemOption {
    line: number;
    orderid: number;
    itemid: number;
    optionid: number;
    optionprice: number;
    optionquantity: number;
    optiontotal: number;
}
  