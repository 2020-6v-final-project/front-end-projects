export type Repository = {
    id: number;
    address: string;
    name: string;
    statusid: number;
    statuscode: string;
}