export type ItemIngredient = {
    itemid: number;
    ingredientid: number;
    quantity: number;
}