import { Ingredient } from "./Ingredient";
import { ItemOptionIngredient } from "./ItemOptionIngredient";

export interface ItemOption {
    itemid: number;
    optionid: number;
    note: string;
    price: number;
    ingredients: (ItemOptionIngredient & Ingredient)[];
}