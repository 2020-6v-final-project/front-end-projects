export type MenuItem = {
    menuid: number;
    itemid: number;
    price: number;
}