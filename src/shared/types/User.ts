export interface User {
	id: number;
	username: string;
	hash: string;
	salt: string;
	firstname: string;
	middlename: string;
	lastname: string;
	email: string;
	dateofbirth: Date;
	joinedat: Date;
	token: string;
	tokenexpiredat: Date;
	statusid: number;
	statuscode: string;
	imgpath: string;
	phone: string;
	address: string;
  	gender: string;
	hasroles: {
		id: number,
		branchids: number[],
	}[];
}
