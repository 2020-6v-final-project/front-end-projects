export type RepositoryIngredient = {
    line: number;
    receiptid: number;
    ingredientid: string;
    startquantity: number;
    receivedat: Date;
    factorydate: Date;
    expiredat: Date;
    currentquantity: number;
    statusid: number;
    statuscode: string;
    repositoryid: number;
}