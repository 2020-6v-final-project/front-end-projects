export type Check = {
    id: number,
    accountid:number,
    oldbalance: number,
    debitamount: number,
    fee: number,
    newbalance: number,
    note: string,
    createdat: Date,
    receiptid: number,
    approvedrequestid: number, 
    statusid:number,
    statuscode:string,
}