export type Vendor = {
    id: number;
    name: string;
    phonenumber: string;
    address: string;
    ingredients: any[];
}