import { Item } from "./Item";
import { ItemIngredient } from "./ItemIngredient";

export type Ingredient = {
    id: number;
    name: string;
    unit: string;
    statusid: number;
    statuscode: string;
    imgpath: string;
    createdat: Date;
    typeid: number;
    items: (Item & ItemIngredient)[];
    lastmodified: Date;
}