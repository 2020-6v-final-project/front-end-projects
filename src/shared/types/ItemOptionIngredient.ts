export interface ItemOptionIngredient {
    itemid: number;
    optionid: number;
    ingredientid: number;
    ingredientquantity: number;
}
  