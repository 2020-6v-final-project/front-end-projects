export interface Order {
    id: number;
    branchid: number;
    createdby: number;
    createdat: Date;
    statusid: number;
    statuscode: string;
    note: string;
    ordertotal: number;
    total: number;
    ordertypeid: number;
    tableid: number;
    ordernumber: number;
    paymenttypeid: number;
    paymenttypecode: string;
}