export interface PromotionBenefitType {
    id: number;
    description: string;
}
