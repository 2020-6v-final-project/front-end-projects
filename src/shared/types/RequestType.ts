export type RequestType = {
    id: number;
    code: string;
}