export type Menu = {
    id: number;
    name: string;
    description: string;
    statusid: number;
    statuscode: string;
    createdat: Date;
    createdby: number;
    imgpath: string;
    approvedrequestid: number;
    itemList: {
        menuid: number,
        itemid: number,
        price: number,
    }[];
}