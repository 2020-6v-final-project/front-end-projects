import { PromotionBenefit } from "./PromotionBenefit";
import { PromotionCondition } from "./PromotionCondition";

export interface Promotion {
    id: number;
    code: string;
    allow_apply_with_other: boolean;
    name: string;
    statusid: number;
    statuscode: string;
    description: string;
    conditions: PromotionCondition[];
    benefits: PromotionBenefit[];
}
