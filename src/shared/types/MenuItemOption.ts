export interface MenuItemOption {
	menuid: number;
	itemid: number;
	optionid: number;
	price: number;
}