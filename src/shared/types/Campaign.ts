import { CampaignBranch } from "./CampaignBranch";
import { Promotion } from "./Promotion";

export interface Campaign {
    id: number;
    name: string;
    imgpath: string;
    description: string;
    start_after: Date;
    end_before: Date;
    start_hour: number;
    end_hour: number;
    weekday: string;
    created_at: Date;
    updated_at: Date;
    applied_cities: string;
    applied_customertype: number;
    statusid: number;
    statuscode: string;
    promotions: (Promotion & {
        current_quantity: number;
        max_quantity: number | null;
    })[];
    branches: CampaignBranch[];
}
