export type PromotionBenefit = {
    id: number;
    promotion_id: number;
    type: number;
    percent: number;
    value: number;
    max_value: number;
    itemid: number;
    item_quantity: number;
    statusid: number;
    statuscode: string;
} | {
    type: number;
    percent: number;
    max_value: number;
} | {
    type: number;
    value: number;
} | {
    type: number;
    itemid: number;
    item_quantity: number;
} | {
    type: number;
    itemid: number;
    value: number;
} | {
    type: number;
    itemid: number;
    percent: number;
    max_value: number;
}
