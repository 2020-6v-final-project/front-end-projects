export type BranchError = {
  code: string | boolean;
  name: string | boolean;
  address: string | boolean;
  manager: string | boolean;
  currencyid: string | boolean;
  [key: string]: string | boolean;
};
