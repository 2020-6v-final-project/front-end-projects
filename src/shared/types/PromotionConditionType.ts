export interface PromotionConditionType {
    id: number;
    description: string;
}