import { Ingredient } from "./Ingredient";

export interface IngredientType {
    id: number;
    name: string;
    ingredients: Ingredient[];
}
  