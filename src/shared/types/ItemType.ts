import { Item } from "./Item";

export interface ItemType {
    id: number;
    name: string;
    items: Item[];
}
  