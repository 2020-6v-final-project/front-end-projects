export type Request = {
    id: number;
    requesttypeid: number;
    createdby: number;
    referenceid:number;
    createdat: Date;
    approvedat: Date;
    approvedby:number;
    note:string;
    statusid: number;
    statuscode: string;
}