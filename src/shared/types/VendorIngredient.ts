export type VendorIngredient = {
    itemid: number;
    ingredientid: number;
}