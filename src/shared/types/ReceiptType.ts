export type ReceiptType = {
    id: number;
    code: string;
    name: string;
}