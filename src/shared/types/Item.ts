import { ItemIngredient } from './ItemIngredient';
export type Item = {
    id: number;
    name: string;
    unit: string;
    price: number;
    lastmodified: Date;
    statusid: number;
    statuscode: string;
    recipe: string;
    description: string;
    imgpath: string;
    typeid: number;
    ingredients: ItemIngredient[];
    options: any[];
}