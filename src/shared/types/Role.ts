import { Permission } from "./Permission";

export type Role = {
    id: number;
    code: string;
    name: string;
    description: string;
    haspermissions: Permission[],
    nothavepermissions: Permission[],
    isprimary: boolean,
    isall: boolean,
    issystem: boolean,
    branchids: number[],
}