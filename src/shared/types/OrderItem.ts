export interface OrderItem {
    orderid: number;
    line: number;
    itemid: number;
    itemprice: number;
    optionsprice: number;
    itemquantity: number;
    total: number;
    note: string;
    lineupnumber: number;
}
