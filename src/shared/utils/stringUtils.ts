import {formatOptions, shortFormatOptions} from '@shared/components';

export function numberToStringWithCommas(number: number | undefined) {
    if (!number) return undefined;
    return Intl.NumberFormat('vi-VN').format(number);
}
export function whiteSpaceString(length: number) {
    let result = '';
    for(let i = 0; i < length ; i++) result += '\xa0';
    return result;
}

export function shortDateFormat(date: Date | undefined) : (string | undefined) {
    if(!date) return undefined;
    return Intl.DateTimeFormat("vi-VN", shortFormatOptions)
    .format(new Date(date))
    .toString();
}

export function fullDateFormat(date: Date | undefined) : (string | undefined) {
    if(!date) return undefined;
    return Intl.DateTimeFormat("vi-VN", formatOptions)
    .format(new Date(date))
    .toString();
}

export function currencyFormat(number: number | undefined) {
    if (number !== 0 && !number) return undefined;
    return Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(number);
}