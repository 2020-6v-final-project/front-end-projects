import axios, { Method } from "axios";
import settings from "../api/settings";

export const axiosCaller = async function (
  params: any,
  targetUrl: any,
  method: Method,
  callback: any,
  responseType: any,
  data: any,
  contentType: any
) {
  if (targetUrl.startsWith("/")) {
    targetUrl = settings.baseURL + targetUrl;
  }
  if (!responseType) {
    responseType = "json";
  }
  if (!data) {
    data = "";
  }
  if (!contentType) {
    contentType = "application/json";
  }
  try {
    const result = await axios.request({
      url: targetUrl,
      method,
      responseType,
      params,
      data: JSON.stringify({ ...data }),
      headers: {
        "Content-Type": contentType,
        Authorization: "bearer " + localStorage.getItem("token"),
      },
    });
    return callback(result.data);
  } catch (e) {
    throw e;
  }
};
