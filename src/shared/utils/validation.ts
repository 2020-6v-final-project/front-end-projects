import * as yup from "yup";

export const UserSchema = yup.object().shape({
  firstname: yup.string().required().min(2).max(20),
  middlename: yup.string().min(0).max(20),
  lastname: yup.string().required().min(2).max(20),
  phone: yup.string().required().min(10).max(12),
  dateofbirth: yup.string().required(),
  address: yup.string().required().min(5).max(100),
  ismale: yup.string().required(),
  email: yup.string().email(),

});

export const BranchSchema = yup.object().shape({
  code: yup.string().required(),
  name: yup.string().required(),
  address: yup.string().required(),
  manager: yup.number().required(),
  mainmenu: yup.number().nullable(),
  currencyid: yup.string().required(),
});
