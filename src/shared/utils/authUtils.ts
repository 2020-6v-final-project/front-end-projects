import { Role } from "@custom-types/Role";

export function authUtils() {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('user') as any);

    if (user && user.token) {
        return { 'Authorization': 'Bearer ' + user.token };
    } else {
        return {};
    }
}

export const checkPermission = (userRoles: Role[], targetPermissions: Array<any> | []): boolean => {
    if (targetPermissions.length < 1) {
        return true;
    }
    else if (userRoles.length < 1) {
        return false;
    }
    else {
        let hasPermissions: Array<number> = [];
        for (let role of userRoles) {
            hasPermissions = hasPermissions.concat(role.haspermissions.map(item => item.id));
        }

        let requiredPermissions: Array<number> = [];
        for (let permission of targetPermissions) {
            requiredPermissions = requiredPermissions.concat(permission.id);
        }
        return hasPermissions?.some(permissionId => requiredPermissions.includes(permissionId)) || false;

    }
}