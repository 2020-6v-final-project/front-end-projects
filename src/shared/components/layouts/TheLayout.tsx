import AccessProvider from "@features/AccessProvider";
import { loginSuccess } from "@features/LoginPage/LoginPage.slice";
import React, { useEffect } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import { RootState } from "src/store";
import { TheContent, TheSidebar, TheHeader } from "./index";

type TheLayoutProps = {
  token: string;
  loginSuccess: Function;
};

const TheLayout: React.FC<TheLayoutProps> = (props) => {
  let history = useHistory();
  const { token, loginSuccess } = props;

  useEffect(() => {
    if (!token) {
      let tempToken = localStorage.getItem("token");
      if (!tempToken) {
        history.push("/login");
      }
      let tempRole = localStorage.getItem("roles");
      let tempBranches = localStorage.getItem("branches");
      let tempUser = localStorage.getItem("user");
      if (tempRole && tempToken && tempBranches && tempUser) {
        loginSuccess({
          token: tempToken,
          roles: JSON.parse(tempRole),
          branches: JSON.parse(tempBranches),
          user: JSON.parse(tempUser),
        });
      }
    }
  }, [token, history, loginSuccess]);

  return (
    <AccessProvider>
      <div className="c-app c-default-layout">
        <TheSidebar history={history} />
        <div className="c-wrapper">
          <TheHeader history={history} />
          <div className="c-body ">
            <TheContent />
          </div>
        </div>
      </div>
    </AccessProvider>
  );
};

const mapState = (state: RootState) => {
  return {
    token: state.auth.token,
  };
};
const mapDispatch = { loginSuccess };

export default connect(mapState, mapDispatch)(TheLayout);
