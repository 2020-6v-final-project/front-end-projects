import React, { useEffect } from "react";
import { PageHeader, Typography, Avatar } from "antd";
import { connect } from "react-redux";
import { RootState } from "src/store";
import { updateHeader } from "./TheCustomHeader.slice";
import { ArrowLeftOutlined } from "@ant-design/icons";
import nav from "../_nav";

type HeaderProps = {
  title?: string;
  back?: boolean;
  prefix?: string;
  updateHeader: Function;
  history: any;
};

const TheCustomHeader: React.FC<HeaderProps> = (props) => {
  const { title, updateHeader, history, prefix } = props;

  useEffect(() => {
    nav.forEach((item) => {
      if (item._children) {
        item._children.forEach((item: any) => {
          if (window.location.pathname.includes(item.to)) {
            if (item.type === "management") {
              window.document.title = item.name
                ? "Cookit - Quản lý " + item.name
                : "Cookit";
              updateHeader({
                prefix: "Quản lý",
                title: item.name,
              });
            } else {
              window.document.title = item.name
                ? "Cookit - " + item.name
                : "Cookit";
              updateHeader({
                title: item.name,
              });
            }
          }
        });
      } else {
        if (window.location.pathname.includes(item.to)) {
          if (item.type === "management") {
            window.document.title = item.name
              ? "Cookit - Quản lý " + item.name
              : "Cookit";
            updateHeader({
              prefix: "Quản lý",
              title: item.name,
            });
          } else {
            window.document.title = item.name
              ? "Cookit - " + item.name
              : "Cookit";
            updateHeader({
              title: item.name,
            });
          }
        }
      }
    });
  }, [updateHeader]);

  useEffect(() => {
    history.listen((location: any) => {
      nav.forEach((item) => {
        if (item._children) {
          item._children.forEach((item: any) => {
            if (location.pathname.includes(item.to)) {
              if (item.type === "management") {
                window.document.title = item.name
                  ? "Cookit - Quản lý " + item.name
                  : "Cookit";
                updateHeader({
                  prefix: "Quản lý",
                  title: item.name,
                });
              } else {
                window.document.title = item.name
                  ? "Cookit - " + item.name
                  : "Cookit";
                updateHeader({
                  title: item.name,
                });
              }
            }
          });
        } else {
          if (location.pathname.includes(item.to)) {
            if (item.type === "management") {
              window.document.title = item.name
                ? "Cookit - Quản lý " + item.name
                : "Cookit";
              updateHeader({
                prefix: "Quản lý",
                title: item.name,
              });
            } else {
              window.document.title = item.name
                ? "Cookit - " + item.name
                : "Cookit";
              updateHeader({
                title: item.name,
              });
            }
          }
        }
      });
    });
  }, [history, updateHeader]);
  return (
    <PageHeader
      className="mb-0 pb-0"
      onBack={() => {
        window.history.back();
      }}
      title={
        <Typography.Title level={2}>
          <span style={{ fontWeight: 200 }}>{prefix} </span>
          {title}
        </Typography.Title>
      }
      extra={[<Avatar key="1">H</Avatar>]}
      backIcon={<ArrowLeftOutlined className="mb-3" />}
    ></PageHeader>
  );
};

const mapState = (state: RootState) => {
  return {
    title: state.header.title,
    back: state.header.back,
    prefix: state.header.prefix,
  };
};

const mapDispatch = { updateHeader };

export default connect(mapState, mapDispatch)(TheCustomHeader);
