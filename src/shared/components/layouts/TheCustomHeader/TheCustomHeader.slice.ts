import {createSlice, PayloadAction} from '@reduxjs/toolkit';

type HeaderState = {
    title?: string,
    back?: boolean,
    prefix?: string,
}

const initialState: HeaderState = {
    title: "",
    back: false,
    prefix: "",
}

const HeaderSlice = createSlice({
    name: 'customHeader',
    initialState,
    reducers: {
        updateHeader: (state, action: PayloadAction<HeaderState>) => {
            state.title = action.payload.title? action.payload.title : "";
            state.prefix= action.payload.prefix? action.payload.prefix: "";
            state.back = action.payload.back? true: false;
            return state;
        }
    }
})

export const {
updateHeader
} = HeaderSlice.actions;

export default HeaderSlice.reducer;