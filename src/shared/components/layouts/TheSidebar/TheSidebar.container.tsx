import logo_minimized from "@assets/login_img.png";
import logo from "@assets/logo_trans.png";
import { routesConstants } from "@constants";
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarMinimizer,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavDropdown,
  CSidebarNavItem,
  CSidebarNavTitle,
} from "@coreui/react";
import { Permission, Role } from "@custom-types";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { RootState } from "src/store";
// sidebar nav config
import navigation from "../_nav";
import { set } from "./TheSidebar.slice";

type TheSidebarState = {
  show: boolean;
  roles: Role[];
  history: any;
};

type TheSidebarDispatch = {
  set: Function;
};

type TheSidebarDefaultProps = TheSidebarState & TheSidebarDispatch;

const TheSidebarContainer: React.FC<TheSidebarDefaultProps> = React.memo(
  (props) => {
    const { show, set, roles, history } = props;
    const [finalNav, setFinalNav] = useState<any>([]);
    const [previousPath, setPreviousPath] = useState(history.location.pathname);

    useEffect(() => {
      let userPermission = [] as Permission[];
      let acceptedNav = [];

      roles.forEach((role) => {
        role.haspermissions.forEach((permission: Permission) => {
          userPermission.push(permission);
        });
      });

      for (let a = 0; a < navigation.length; a++) {
        let permissionNeed = [] as any;
        let permissionChecked = false as boolean;
        if (navigation[a].hasOwnProperty("to")) {
          for (let b = 0; b < routesConstants.length; b++) {
            if (routesConstants[b].path === navigation[a].to) {
              routesConstants[b].allowedPermissions.forEach((per) =>
                permissionNeed.push(per)
              );
            }
          }
          if (permissionNeed.length > 0) {
            for (let c = 0; c < permissionNeed.length; c++) {
              for (let d = 0; d < userPermission.length; d++) {
                if (userPermission[d].id === permissionNeed[c].id) {
                  permissionChecked = true;
                }
              }
            }
          } else permissionChecked = true;
          if (permissionChecked) acceptedNav.push(navigation[a]);
        }
      }
      setFinalNav(acceptedNav);
    }, [roles]);

    useEffect(() => {
      if (history.location.pathname !== previousPath) {
        setPreviousPath(history.location.pathname);
        let temp = [...finalNav];
        temp.forEach((item: any) => {
          if (history.location.pathname.includes(item.to)) {
            item.to = history.location.pathname;
          }
        });
        setFinalNav(temp);
      }
    }, [history, finalNav, previousPath]);

    return (
      <CSidebar
        show={show}
        colorScheme="light"
        minimize
        onShowChange={(val: any) => set(val)}
      >
        <CSidebarBrand
          className="d-md-down-none py-3"
          style={{ backgroundColor: "white" }}
          to="/"
        >
          <img
            className="c-sidebar-brand-full"
            alt="logo"
            src={logo}
            style={{ height: 35 }}
          />
          <img
            className="c-sidebar-brand-minimized"
            alt="logo"
            src={logo_minimized}
            style={{ height: 35 }}
          />
        </CSidebarBrand>
        <CSidebarNav>
          <CCreateElement
            items={finalNav}
            components={{
              CSidebarNavDivider,
              CSidebarNavDropdown,
              CSidebarNavItem,
              CSidebarNavTitle,
            }}
          />
        </CSidebarNav>
        <CSidebarMinimizer className="c-d-md-down-none" />
      </CSidebar>
    );
  }
);

const mapState = (state: RootState) => {
  return {
    show: state.sidebar.show,
    roles: state.auth.roles,
  };
};
const mapDispatch = { set };

export default React.memo(connect(mapState, mapDispatch)(TheSidebarContainer));
