import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type TheSidebarState = {
    show: boolean,
}

const initialState: TheSidebarState = {
    show: true,
}

const TheSidebarSlice = createSlice({
    name: 'sidebar',
    initialState,
    reducers: {
        set: (state: TheSidebarState, action: PayloadAction<boolean>) => {
            state.show = action.payload;
        }
    }
})

const { set } = TheSidebarSlice.actions;

export {
    set,
};

export default TheSidebarSlice.reducer;