const SACNavigation = [
  {
    _tag: "CSidebarNavTitle",
    _children: ["Chủ công ty"],
    to: "Chủ công ty",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Tổng quan",
    to: "/dashboard",
    icon: "cil-speedometer",
    type: "statistics",
  },
  {
    _tag: "CSidebarNavDropdown",
    name: "Công ty",
    to: "",
    icon: "cil-factory",
    _children: [
      {
        _tag: "CSidebarNavItem",
        name: "Chi nhánh",
        to: "/branch-management",
        icon: "cil-building",
        type: "management",
      },
      {
        _tag: "CSidebarNavItem",
        name: "Nhân viên",
        to: "/user-management",
        icon: "cil-user-plus",
        type: "management",
      },
      {
        _tag: "CSidebarNavItem",
        name: "Kho",
        to: "/repository-management",
        icon: "cil-storage",
        type: "management",
      },
      {
        _tag: "CSidebarNavItem",
        name: "Yêu cầu",
        to: "/company-request-management",
        icon: "cil-library",
        type: "management",
      },
      {
        _tag: "CSidebarNavItem",
        name: "Nhà cung cấp",
        to: "/vendor-management",
        icon: "cil-factory",
        type: "management",
      },
    ],
  },

  {
    _tag: "CSidebarNavDropdown",
    name: "Sản phẩm",
    to: "",
    icon: "cil-color-fill",
    _children: [
      {
        _tag: "CSidebarNavItem",
        name: "Món ăn",
        to: "/item-management",
        icon: "cil-fastfood",
        type: "management",
      },
      {
        _tag: "CSidebarNavItem",
        name: "Phân loại",
        to: "/category",
        icon: "cil-basket",
        type: "management",
      },
      {
        _tag: "CSidebarNavItem",
        name: "Thực đơn",
        to: "/menu-management",
        icon: "cil-restaurant",
        type: "management",
      },
      {
        _tag: "CSidebarNavItem",
        name: "Nguyên liệu",
        to: "/ingredient-management",
        icon: "cil-balance-scale",
        type: "management",
      },
    ],
  },

  {
    _tag: "CSidebarNavDropdown",
    name: "Khuyến mãi",
    to: "",
    icon: "cil-mood-very-good",
    _children: [
      {
        _tag: "CSidebarNavItem",
        name: "Chương trình",
        icon: "cil-bullhorn",
        to: "/campaign-management",
        type: "management",
      },
      {
        _tag: "CSidebarNavItem",
        name: "Ưu đãi",
        icon: "cil-gift",
        to: "/promotion-management",
        type: "management",
      },
    ],
  },
];

const ReceptionistNavigation = [
  {
    _tag: "CSidebarNavTitle",
    _children: ["Thu ngân"],
    to: "Thu ngân",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Đơn hàng",
    to: "/order-management",
    icon: "cil-money",
    type: "management",
  },
];

const SMNavigation = [
  {
    _tag: "CSidebarNavTitle",
    _children: ["Quản lý"],
    to: "Quản lý",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Yêu cầu",
    to: "/manager-request-management",
    icon: "cil-library",
    type: "management",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Tài chính",
    to: "/finance-management",
    icon: "cil-wallet",
    type: "management",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Giao dịch",
    to: "/transaction-management",
    icon: "cil-dollar",
    type: "management",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Kho",
    to: "/storage-management",
    icon: "cil-storage",
    type: "management",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Bàn",
    to: "/table-management",
    icon: "cil-view-module",
    type: "management",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Thực đơn",
    to: "/menu-branch-management",
    icon: "cil-restaurant",
    type: "management",
  },
];

const navigation = [
  ...SACNavigation,
  ...ReceptionistNavigation,
  ...SMNavigation,
];

export default navigation;
