import { Col, Input, Row, Select } from "antd";
import React from "react";

type SidePanelProps = {
  handleSearch: Function;
  searchCriteria: Array<{
    name: string;
    label: string;
  }>;
  handleSort: Function;
  searchLabel: string;
};

const SidePanel: React.FC<SidePanelProps> = React.memo((props) => {
  const { handleSearch, searchLabel } = props;
  return (
    <Col span={7} style={{ height: "90vh" }} className="side-menu">
      <Row justify="space-between" align="middle">
        <Input
          onChange={(e) => {
            handleSearch(e.target.value);
          }}
          className="mb-3"
          placeholder={searchLabel}
        ></Input>
      </Row>

      <Row justify="center" className="mb-3">
        <Select
          placeholder="Sắp xếp"
          defaultValue={"name_asc"}
          defaultActiveFirstOption
        >
          <Select.Option value="name_asc" key={1}>
            Tên tăng dần
          </Select.Option>
          <Select.Option value="name_desc" key={2}>
            Tên giảm dần
          </Select.Option>
          <Select.Option value="baseprice_asc" key={3}>
            Giá tăng dần
          </Select.Option>
          <Select.Option value="baseprice_desc" key={4}>
            Giá giảm dần
          </Select.Option>
        </Select>
      </Row>
    </Col>
  );
});

export default SidePanel;
