import TheContent from "./TheContent";
import TheLayout from "./TheLayout";
import TheSidebar from "./TheSidebar/TheSidebar.container";
import TheHeader from "./TheCustomHeader/TheCustomHeader.container";
export * from './TheSidebar';

export {
  TheContent,
  TheLayout,
  TheSidebar,
  TheHeader
};
