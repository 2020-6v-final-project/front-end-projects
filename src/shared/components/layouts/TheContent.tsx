import { CContainer } from "@coreui/react";
import React, { Suspense } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
// routes config
import routes from "../../../routes";

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
);

const TheContent: React.FC = (props: any) => {
  return (
    <main className="c-main pt-2">
      <CContainer fluid>
        <Suspense fallback={loading}>
          <Switch>
            {routes.map((route: any, idx: any) => {
              return (
                route.component && (
                  <Route
                    key={idx}
                    path={route.path}
                    exact={route.exact}
                    component={route.component}
                  />
                )
              );
            })}
            <Redirect from="/" to="/login" />
          </Switch>
        </Suspense>
      </CContainer>
    </main>
  );
};

export default React.memo(TheContent);
