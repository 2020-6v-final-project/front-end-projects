import React from "react";
import { Typography } from "antd";

type DateTimeTextProps = {
  className?: string | "";
  style?: any | {};
  dateTimeFormatOptions?: Intl.DateTimeFormatOptions;
};

export const formatOptions: Intl.DateTimeFormatOptions = {
  year: "numeric",
  month: "long",
  day: "numeric",
  hour: "numeric",
  minute: "numeric",
};

export const shortFormatOptions: Intl.DateTimeFormatOptions = {
  year: "numeric",
  month: "long",
  day: "numeric",
};

const DateTimeText: React.FC<DateTimeTextProps> = ({
  dateTimeFormatOptions,
  children,
  className,
  style,
}) => {
  let formatted;
  const value = children as Date;
  if (!dateTimeFormatOptions) {
    formatted = new Intl.DateTimeFormat("vi-VN", formatOptions).format(value);
  } else {
    formatted = new Intl.DateTimeFormat("vi-VN", dateTimeFormatOptions).format(
      value
    );
  }

  return (
    <Typography.Text className={className} style={{ ...style }}>
      {formatted}
    </Typography.Text>
  );
};

export default DateTimeText;
