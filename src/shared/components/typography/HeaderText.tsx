import React from 'react';
import { Typography } from 'antd';

type HeaderTextProps = {

}

const HeaderText: React.FC<HeaderTextProps> = ({ children }) => {
    return (<Typography.Title className="mb-3" level={1}>{children}</Typography.Title>);
}

export default HeaderText;