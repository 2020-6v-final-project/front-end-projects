import React, { CSSProperties } from 'react';
import { Typography } from 'antd';

type SubHeaderTextProps = {
    style?: CSSProperties,
    className?: string,
}

const SubHeaderText: React.FC<SubHeaderTextProps> = ({ children, style, className }) => {
    return (<Typography.Title className={className} level={3} style={{ fontWeight: 400, ...style }}>{children}</Typography.Title>);
}

export default SubHeaderText;