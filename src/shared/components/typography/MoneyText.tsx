import React from 'react';
import { Typography } from 'antd';

type MoneyTextProps = {
    className?: string | "",
    level?: any | 5,
    style?: any | {},
}

const MoneyText: React.FC<MoneyTextProps> = ({ children, className, level, style }) => {
    const value = parseFloat(children as string);
    const formatted = Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(value);
    return (<Typography.Title style={{ ...style }} className={className} level={level}>{formatted}</Typography.Title>);
}

export default MoneyText;