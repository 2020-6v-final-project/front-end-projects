import { CModal, CCard, CCardBody, CButton, CSpinner, CRow, CCol } from '@coreui/react';
import { Row, Typography } from 'antd';
import React, { ReactNode, useState } from 'react';
import './modal.scss';

type ModalType = {
    content: ReactNode | string;
    show: boolean;
    title?: string;
    confirmButtonName?: string;
    size?: "" | "sm" | "lg" | "xl";
    height?: string | number;
    width?: string | number;
    buttonHidden?: boolean;
    onChoose?: ((result: boolean| undefined) => void);
    onChooseAsync?: ((result: boolean | undefined) => Promise<void>);
    onClose: Function;
}

const Modal: React.FC<ModalType> = React.memo((props) => {
    const {
        content,
        title,
        confirmButtonName,
        size,
        show,
        height,
        width,
        buttonHidden,
        onChoose,
        onChooseAsync,
        onClose,
    } = props;

    const [loading, setLoading] = useState<boolean>(false);

    const onChooseHandle = async () => {
        setLoading(true);
        if (onChooseAsync) {
            await onChooseAsync(true);
        } else if (onChoose) onChoose(true);
        onClose();
        setLoading(false);
    }

    const onCancelHandle = () => {
        if (onChooseAsync) onChooseAsync(false);
        else if (onChoose) onChoose(false);
        onClose();
    }

    return (
        <CModal
            show={show}
            backdrop
            centered
            onClose={onClose}
            size={size}
            height={height}
            width={width}
            className="ConfirmModal-styles"
        >
            <CCard className="align-middle justify-content-center jus ConfirmModal-card-styles">
                <CCardBody className="ConfirmModal-cardBody-styles">
                    <h4 className="ConfirmModal-h4-styles">{title ? title : "Lưu ý"}</h4>
                    {content instanceof String ?
                        <Row justify="center">
                            <Typography.Title level={5}>
                                Bạn có muốn xóa món ăn này ra khỏi đơn hàng ?
                            </Typography.Title>
                        </Row>
                    : content}
                    
                    {!buttonHidden ?
                    <CRow style={{ marginTop: 20 }}>
                        <CCol span={6}>
                            <CButton
                                color="primary"
                                className="ConfirmModal-confirm-button-styles btn my-3"
                                disabled={loading}
                                onClick={onChooseHandle}
                            >
                                {loading ? <CSpinner size="sm" color="#ffffff"/> : ""} {confirmButtonName ? confirmButtonName : "Xác nhận"}
                            </CButton>
                        </CCol>
                        <CCol 
                            span={6}
                            className={loading ? "Display-none" : ""}
                        >
                            <CButton
                                className="ConfirmModal-cancel-button-styles btn my-3"
                                onClick={onCancelHandle}
                            >
                                Hủy
                            </CButton>
                        </CCol>
                    </CRow>
                    :
                    <></>}
                </CCardBody>
            </CCard>
        </CModal>
    );
})

export default Modal;