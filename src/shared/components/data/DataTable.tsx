import {
  CDataTable
} from "@coreui/react";
import React from "react";

type DataTableProps = {
  fields: Array<any>,
  items: Array<any> | undefined,
  scopedSlots?: {},
  onRowClick: Function,
  loading?: boolean,
  itemsMapper: (item: any, index: number) => any,
}

const DataTable: React.FC<DataTableProps> = React.memo((props) => {

  const { fields, items, scopedSlots, onRowClick, itemsMapper, loading } = props;

  return (
    <CDataTable
      fields={fields}
      hover
      sorter
      tableFilter
      itemsPerPageSelect
      border={false}
      outlined={false}
      striped
      itemsPerPage={10}
      footer={false}
      activePage={0}
      pagination
      clickableRows
      items={items?.map(itemsMapper)}
      scopedSlots={scopedSlots}
      onRowClick={onRowClick}
      loading={loading}
    />
  );
});

export default DataTable;
