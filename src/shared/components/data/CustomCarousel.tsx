import React, { CSSProperties } from 'react'
import { Carousel, Empty, Row, Col } from 'antd'
import { ArrowRightOutlined, ArrowLeftOutlined } from '@ant-design/icons';
import "./index.scss";

type CustomCarouselProps = {
    items: Array<any>,
    className?: string,
}

const carouselStyles: CSSProperties = {
    margin: '0px 32px',
    height: '210px',
}

const breakpoints = [
    {
        breakpoint: 1920,
        settings: {
            slidesPerRow: 6,
        }
    },
    {
        breakpoint: 1600,
        settings: {
            slidesPerRow: 5,
        }
    },

    {
        breakpoint: 1200,
        settings: {
            slidesPerRow: 4,
        }
    },
    {
        breakpoint: 962,
        settings: {
            slidesPerRow: 3,
        }
    },
    {
        breakpoint: 768,
        settings: {
            slidesPerRow: 2,
        }
    },
    {
        breakpoint: 576,
        settings: {
            slidesPerRow: 1,
        }
    }
]

const CustomCarousel: React.FC<CustomCarouselProps> = ({ children, items, className }) => {
    const hasItems = items.length > 0;
    return (
        <Carousel className={className} style={{ ...carouselStyles }} dots={false} slidesPerRow={hasItems ? undefined : 1}
            responsive={breakpoints} arrows nextArrow={<ArrowRightOutlined />} prevArrow={<ArrowLeftOutlined />} >
            {hasItems ? items.map((item, index) => <div key={index}>{item}</div>) :
                <Row className="carousel-empty-row" justify="center" align="middle">
                    <Col>
                        <Empty style={{ display: hasItems ? "none" : "block" }} description="Không có gì để hiển thị" />
                    </Col>
                </Row>}
        </Carousel>
    )
}

export default CustomCarousel;