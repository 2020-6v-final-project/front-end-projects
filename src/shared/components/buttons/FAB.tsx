import React, { CSSProperties } from "react";
import { PlusOutlined } from "@ant-design/icons";
import { Button } from "antd";

type FABProps = {
  callback: Function;
  className?: string;
  style?: CSSProperties;
};

const FABStyle: CSSProperties = {
  position: "fixed",
  bottom: 50,
  right: 50,
  width: 45,
  height: 45,
  boxShadow: "0px 6px 16px #e1b48b",
  backgroundColor: "#ff9b40",
  borderColor: "#ff9b40",
  opacity: 1,
  transitionDuration: "200ms",
};

const FAB: React.FC<FABProps> = ({ callback, className, style }) => {
  return (
    <Button
      icon={<PlusOutlined style={{ marginBottom: 8, marginLeft: 1 }} />}
      type="primary"
      shape="circle"
      className={className + " fab"}
      onClick={(e) => callback(e)}
      style={{ ...FABStyle, ...style }}
    ></Button>
  );
};

export default FAB;
