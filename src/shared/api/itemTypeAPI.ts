import { axiosCaller } from "../utils/axiosCaller";

export async function getItemTypeList(callback: Function) {
    try {
        await axiosCaller(
            {},
            "/itemtypes?withitems",
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function addItemType(itemType: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/itemtypes",
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            itemType,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function updateItemType(itemType: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/itemtypes/${itemType.id}?withitems`,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            itemType,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function deleteItemType(itemTypeId: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/itemtypes/${itemTypeId}`,
            "DELETE",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}