import { axiosCaller } from "../utils/axiosCaller";

export async function getRequestsList(callback: Function) {
    try {
        await axiosCaller(
            {},
            "/requests",
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function addRequest(Request: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/requests",
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            Request,
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function updateRequest(menu: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/menus/" + menu.id,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            menu,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function removeRequest(menuid: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/menus/remove/" + menuid,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            menuid,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function getRequestTypesList(callback: Function) {
    try {
        await axiosCaller(
            {},
            "/requesttypes",
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function approveRequest(req:any,callback: Function) {
    try {
        await axiosCaller(
            {},
            `/requests/${req.id}/approved`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            req,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function rejectRequest(req:any,callback: Function) {
    try {
        await axiosCaller(
            {},
            `/requests/${req.id}/rejected`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            req,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function getBranchRequestsList(branchid:number,callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${branchid}/requests`,
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function addBranchRequest(Request: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${Request.branchid}/requests`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            Request,
            ""
        );
    } catch (e) {
        throw e;
    }

}


export async function approveBranchRequest(req:any,callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${req.branchid}/requests/${req.id}/approved`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            req,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function rejectBranchRequest(req:any,callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${req.branchid}/requests/${req.id}/rejected`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            req,
            ""
        );
    } catch (e) {
        throw e;
    }
}