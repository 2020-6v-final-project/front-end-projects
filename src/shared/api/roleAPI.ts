import { axiosCaller } from "../utils/axiosCaller";
// import { Role } from "@custom-types";

export async function getRoleList(callback: Function) {
  try {
    await axiosCaller(
      {},
      "/admin/roles/",
      "GET",
      (response: any) => {
        callback(response);
      },
      "",
      "",
      ""
    );
  } catch (e) {
    throw e;
  }
}

