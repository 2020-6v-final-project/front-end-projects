import { axiosCaller } from "../utils/axiosCaller";

export async function getMenuList(callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/menus",
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function addMenu(menu: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/menus",
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            menu,
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function updateMenu(menu: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/menus/" + menu.id,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            menu,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function deactivateMenu(menuid: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/menus/deactivate/" + menuid,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            menuid,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function reactivateMenu(menuid: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/menus/reactivate/" + menuid,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            menuid,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function removeMenu(menuid: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/menus/" + menuid,
            "DELETE",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function requestNewMenu(menuId: number, data: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/admin/menus/${menuId}/requestchangeprice`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            data,
            ""
        );
    } catch (e) {
        throw e;
    }
}