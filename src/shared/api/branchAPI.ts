import { axiosCaller } from "../utils/axiosCaller";

export async function getBranchList(callback: Function) {
  try {
    await axiosCaller(
      {},
      "/branches",
      "GET",
      (response: any) => {
        callback(response);
      },
      "",
      "",
      ""
    );
  } catch (e) {
    throw e;
  }
}

export async function addBranch(branch: any, callback: Function) {
  try {
    await axiosCaller(
      {},
      "/branches",
      "POST",
      (response: any) => {
        callback(response);
      },
      "",
      branch,
      ""
    );
  } catch (e) {
    throw e;
  }
}

export async function updateBranch(branch: any, callback: Function) {
  try {
    await axiosCaller(
      {},
      "/branches/" + branch.id,
      "PUT",
      (response: any) => {
        callback(response);
      },
      "",
      branch,
      ""
    );
  } catch (e) {
    throw e;
  }
}

export async function deactivateBranch(branchId: number, callback: Function) {
  try {
    await axiosCaller(
      {},
      `/branches/${branchId}/disabled`,
      "POST",
      (response: any) => {
        callback(response);
      },
      "",
      "",
      ""
    );
  } catch (e) {
    throw e;
  }
}

export async function reactivateBranch(branchId: number, callback: Function) {
  try {
    await axiosCaller(
      {},
      `/branches/${branchId}/available`,
      "POST",
      (response: any) => {
        callback(response);
      },
      "",
      "",
      ""
    );
  } catch (e) {
    throw e;
  }
}
