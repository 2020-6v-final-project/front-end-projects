import { axiosCaller } from "../utils/axiosCaller";

export async function getCampaignList(callback: Function) {
    try {
        await axiosCaller(
            {},
            `/admin/campaigns`,
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (err) {
        throw err;
    }
}

export async function addCampaign(campaign: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/admin/campaigns`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            campaign,
            ""
        );
    } catch (err) {
        throw err;
    }
}

export async function updateCampaign(campaignId: number, campaign: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/admin/campaigns/${campaignId}`,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            campaign,
            ""
        );
    } catch (err) {
        throw err;
    }
}

export async function deactivateCampaign(campaignId: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/admin/campaigns/deactivate/${campaignId}`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (err) {
        throw err;
    }
}

export async function reactivateCampaign(campaignId: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/admin/campaigns/reactivate/${campaignId}`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (err) {
        throw err;
    }
}