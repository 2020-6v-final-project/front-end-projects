import { axiosCaller } from "../utils/axiosCaller";

export async function getFinanceList(callback: Function) {
    try {
        await axiosCaller(
            {},
            "/financeaccounts",
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function getBranchFinancesList(branchid:number, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${branchid}/financeaccounts`,
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function addFinance(finance: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/finances?withingredients",
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            finance,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function updateFinance(finance: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/finances/${finance.id}?withingredients`,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            finance,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function deactivateFinance(financeId: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/finances/deactivate/" + financeId,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function reactivateFinance(financeId: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/finances/reactivate/" + financeId,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}