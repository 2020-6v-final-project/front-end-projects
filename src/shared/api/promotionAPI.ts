import { axiosCaller } from "../utils/axiosCaller";

export async function getPromotionList(callback: Function) {
    try {
        await axiosCaller(
            {},
            `/admin/promotions`,
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (err) {
        throw err;
    }
}

export async function addPromotion(promotion: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/admin/promotions`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            promotion,
            ""
        );
    } catch (err) {
        throw err;
    }
}

export async function updatePromotion(promotionId: number, promotion: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/admin/promotions/${promotionId}`,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            promotion,
            ""
        );
    } catch (err) {
        throw err;
    }
}

export async function deactivatePromotion(promotionId: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/admin/promotions/deactivate/${promotionId}`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (err) {
        throw err;
    }
}

export async function reactivatePromotion(promotionId: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/admin/promotions/reactivate/${promotionId}`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (err) {
        throw err;
    }
}