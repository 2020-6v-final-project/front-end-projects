import { axiosCaller } from "../utils/axiosCaller";

export async function getVendorList(callback: Function) {
    try {
        await axiosCaller(
            {},
            "/vendors?withingredients",
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function addVendor(vendor: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/vendors?withingredients",
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            vendor,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function updateVendor(vendor: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/vendors/${vendor.id}?withingredients`,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            vendor,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function deactivateVendor(vendorId: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/vendors/deactivate/" + vendorId,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function reactivateVendor(vendorId: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/vendors/reactivate/" + vendorId,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}