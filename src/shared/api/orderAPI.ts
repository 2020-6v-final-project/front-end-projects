import { axiosCaller } from "../utils/axiosCaller";

export async function getMainMenu(branchId: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${branchId}/mainmenu`,
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (err) {
        throw err;
    }
}

export async function getOrderList(branchId: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${branchId}/orders`,
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (err) {
        throw err;
    }
}

export async function addOrder(branchId: number, order: any, callback: Function) {
    try {    
        await axiosCaller(
            {},
            `/branches/${branchId}/orders`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            order,
            ""
        );
    } catch (err) {
        throw err;
    }
}

export async function updateOrder(branchId: number, orderId: number, order: any, callback: Function) {
    try {    
        await axiosCaller(
            {},
            `/branches/${branchId}/orders/${orderId}`,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            order,
            ""
        );
    } catch (err) {
        throw err;
    }
}

export async function deleteOrder(branchId: number, orderId: number, callback: Function) {
    try {    
        await axiosCaller(
            {},
            `/branches/${branchId}/orders/${orderId}`,
            "DELETE",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (err) {
        throw err;
    }
}

export async function getCurrentOrder(branchId: number, orderId: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${branchId}/orders/${orderId}`,
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (err) {
        throw err;
    }
}

export async function addLineToCurrentOrder(branchId: number, orderId: number, line: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${branchId}/orders/${orderId}/lines`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            line,
            ""
        );
    } catch (err) {
        throw err;
    }
}

export async function updateLineOfCurrentOrder(branchId: number, orderId: number, line: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${branchId}/orders/${orderId}/lines/${line.line}`,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            line,
            ""
        );
    } catch (err) {
        throw err;
    }
}

export async function deleteLineOfCurrentOrder(branchId: number, orderId: number, lineId: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${branchId}/orders/${orderId}/lines/${lineId}`,
            "DELETE",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (err) {
        throw err;
    }
}
