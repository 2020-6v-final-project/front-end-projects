import { axiosCaller } from "../utils/axiosCaller";

export async function getIngredientTypeList(callback: Function) {
    try {
        await axiosCaller(
            {},
            "/ingredienttypes?withingredients",
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function addIngredientType(ingredientType: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/ingredienttypes",
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            ingredientType,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function updateIngredientType(ingredientType: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/ingredienttypes/${ingredientType.id}?withingredients`,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            ingredientType,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function deleteIngredientType(ingredientTypeId: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/ingredienttypes/${ingredientTypeId}`,
            "DELETE",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}