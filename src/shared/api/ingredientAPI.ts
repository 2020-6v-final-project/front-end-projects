import { axiosCaller } from "../utils/axiosCaller";
import { Ingredient } from "../types/Ingredient";

export async function getIngredientList(callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/ingredients?withitems",
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function addIngredient(ingredient: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/ingredients?withitems",
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            ingredient,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function updateIngredient(ingredient: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/admin/ingredients/${ingredient.id}?withitems`,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            ingredient,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export function deactivateIngredient(ingredientId: number, callback: Function) {
    try {
        axiosCaller(
            {},
            `/admin/ingredients/deactivate/${ingredientId}?withitems`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}

export function reactivateIngredient(ingredientId: number, callback: Function) {
    try {
        axiosCaller(
            {},
            `/admin/ingredients/reactivate/${ingredientId}?withitems`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}

//-------------------------------------------------------
export async function addIngredientToStorage(ingredient: Ingredient, callback: Function) {
    try {
        axiosCaller(
            {},
            "/admin/ingredients/deactivate/",
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function updateIngredientQuantity(ingredient: Ingredient, callback: Function) {
    try {
        axiosCaller(
            {},
            "/admin/ingredients/deactivate/",
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function removeIngredientFromStorage(ingredient: Ingredient, callback: Function) {
    try {
        axiosCaller(
            {},
            "/admin/ingredients/deactivate/",
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}
