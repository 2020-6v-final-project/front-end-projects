import { axiosCaller } from "../utils/axiosCaller";

export async function getReceiptsList(branchid:number,callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${branchid}/repositories/receipts`,
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            branchid,
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function getReceiptById(branchid:number,receiptid:number,callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${branchid}/receipts/${receiptid}`,
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            branchid,
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function addReceipt(receipt: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${receipt.branchid}/repositories/receipts/`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            receipt,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function updateReceipt(receiptbody: any,branchid:number,receiptid:number, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${branchid}/receipts/${receiptid}`,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            receiptbody,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function deactivateReceipt(menuid: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/menus/deactivate/" + menuid,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            menuid,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function reactivateReceipt(menuid: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/menus/reactivate/" + menuid,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            menuid,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function removeReceipt(receipt: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${receipt.branchid}/repositories/receipts/`,
            "DELETE",
            (response: any) => {
                callback(response);
            },
            "",
            receipt,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function getReceiptTypesList(callback: Function) {
    try {
        await axiosCaller(
            {},
            "/receipttypes",
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function addIngredientToReceipt(ingre: any,branchid:number, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${branchid}/repositories/receipts/ingredient/` + ingre.receiptid ,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            ingre,
            ""
        );
    } catch (e) {
        throw e;
    }

}


export async function addIngredientToExportReceipt(ingre: any, branchid:number, receiptid:number, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${branchid}/repositories/exportreceipts/ingredients/${receiptid}`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            ingre,
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function removeIngredientFromReceipt(ingre: any,branchid:number, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${branchid}/repositories/receipts/ingredient/` + ingre.receiptid ,
            "DELETE",
            (response: any) => {
                callback(response);
            },
            "",
            ingre,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function createReceiptRequest(req: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${req.branchid}/receipts/${req.referenceid}/request/`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            req,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function receiveReceipt(req: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${req.branchid}/receipts/${req.receiptid}/received/`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            req,
            ""
        );
    } catch (e) {
        throw e;
    }
}
