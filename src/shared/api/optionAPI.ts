import { axiosCaller } from "../utils/axiosCaller";

export async function getOptionList(callback: Function) {
    try {
        await axiosCaller(
            {},
            "/options",
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function addOptionToItem(itemId: number, option: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/admin/items/${itemId}/options`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            option,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function updateOptionOfItem(itemId: number, option: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/admin/items/${itemId}/options/${option.id}`,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            option,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function deleteOptionFromItem(itemId: number, optionId: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/admin/items/${itemId}/options/${optionId}`,
            "DELETE",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}