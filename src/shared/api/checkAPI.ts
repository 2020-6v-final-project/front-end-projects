import { axiosCaller } from "../utils/axiosCaller";

export async function getChecksList(callback: Function) {
    try {
        await axiosCaller(
            {},
            "/checks",
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function addCheck(Check: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/checks",
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            Check,
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function updateCheck(check: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/checks/" + check.id,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            check,
            ""
        );
    } catch (e) {
        throw e;
    }
}


export async function approveCheck(req:any,callback: Function) {
    try {
        await axiosCaller(
            {},
            `/checks/${req.id}/approved`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            req,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function rejectCheck(req:any,callback: Function) {
    try {
        await axiosCaller(
            {},
            `/checks/${req.id}/rejected`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            req,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function getBranchChecksList(branchid:number,callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${branchid}/checks`,
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function addBranchCheck(branchid:number,Check: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${branchid}/checks`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            Check,
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function updateBranchCheck(branchid:number,check: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${branchid}/checks/${check.id}`,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            check,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function createCheckRequest(req: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            `/branches/${req.branchid}/checks/${req.referenceid}/request/`,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            req,
            ""
        );
    } catch (e) {
        throw e;
    }
}