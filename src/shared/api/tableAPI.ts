import { axiosCaller } from "../utils/axiosCaller";

export async function getTableList(callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/tables",
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function addTable(table: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/tables",
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            table,
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function updateTable(table: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/tables/" + table.id,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            table,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function deactivateTable(table: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/tables/deactivate/" + table.id,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            table,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function reactivateTable(table: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/tables/reactivate/" + table.id,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            table,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function removeTable(table: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/tables/" + table.id,
            "DELETE",
            (response: any) => {
                callback(response);
            },
            "",
            table,
            ""
        );
    } catch (e) {
        throw e;
    }
}