import { axiosCaller } from "../utils/axiosCaller";

export async function getCurrencyList(callback: Function) {
  try {
    await axiosCaller(
      {},
      "/currencies",
      "GET",
      (response: any) => {
        callback(response);
      },
      "",
      "",
      ""
    );
  } catch (e) {
    throw e;
  }
}