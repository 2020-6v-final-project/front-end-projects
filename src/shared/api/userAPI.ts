import { axiosCaller } from "../utils/axiosCaller";

export const logIn = async (username: string, password: string, callback: Function) => {
  try {
    await axiosCaller(
      {},
      "/auth/login",
      "POST",
      (response: any) => {
        callback(response);
      },
      "",
      {
        username,
        password
      },
      ""
    );
  } catch (e) {
    throw e;
  }
}

export async function getUserList(callback: Function) {
  try {
    await axiosCaller(
      {},
      "/admin/users?withroles",
      "GET",
      (response: any) => {
        callback(response);
      },
      "",
      "",
      ""
    );
  } catch (e) {
    throw e;
  }
}

export async function addUser(user: any, callback: Function) {
  try {
    await axiosCaller(
      {},
      "/admin/users",
      "POST",
      (response: any) => {
        callback(response);
      },
      "",
      user,
      ""
    );
  } catch (e) {
    throw e;
  }
}

export async function updateUser(user: any, callback: Function) {
  try {
    await axiosCaller(
      {},
      "/admin/users/" + user.id,
      "PUT",
      (response: any) => {
        callback(response);
      },
      "",
      user,
      ""
    );
  } catch (e) {
    throw e;
  }
}

export async function deactivateUser(userId: number, callback: Function) {
  try {
    await axiosCaller(
      {},
      `/admin/users/${userId}/disabled`,
      "POST",
      (response: any) => {
        callback(response);
      },
      "",
      "",
      ""
    );
  } catch (e) {
    throw e;
  }
}

export async function reactivateUser(userId: number, callback: Function) {
  try {
    await axiosCaller(
      {},
      `/admin/users/${userId}/available`,
      "POST",
      (response: any) => {
        callback(response);
      },
      "",
      "",
      ""
    );
  } catch (e) {
    throw e;
  }
}
