import { axiosCaller } from "../utils/axiosCaller";

export async function getItemList(callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/items/?withingredients&withoptions",
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function addItem(item: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/items",
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            item,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function updateItem(item: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/items/" + item.id,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            item,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function deactivateItem(itemId: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/items/deactivate/" + itemId,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function reactivateItem(itemId: number, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/items/reactivate/" + itemId,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}