import { axiosCaller } from "../utils/axiosCaller";

export async function getRepoIngredientsList(repoId:number,callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/repositories/ingredients/"+repoId,
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            repoId,
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function addIngredientToRepo(repo: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/repositories/ingredients/" + repo.repositoryid,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            repo,
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function updateRepoIngredients(repo: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/repositories/ingredients/" + repo.repositoryid,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            repo,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function deactivateRepoIngredient(repo: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/repositories/ingredients/deactivate/" + repo.repositoryid,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            repo,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function reactivateRepoIngredient(repo: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/repositories/ingredients/reactivate/" + repo.repositoryid,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            repo,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function removeRepoIngredient(repo: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/repositories/ingredients/" + repo.repositoryid,
            "DELETE",
            (response: any) => {
                callback(response);
            },
            "",
            repo,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function getRepositoriesList(callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/repositories/",
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            "",
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function getRepositoryById(repoId: number,callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/repositories/"+repoId,
            "GET",
            (response: any) => {
                callback(response);
            },
            "",
            repoId,
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function addRepository(repo: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/repositories/",
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            repo,
            ""
        );
    } catch (e) {
        throw e;
    }

}

export async function updateRepository(repo: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/repositories/" + repo.id,
            "PUT",
            (response: any) => {
                callback(response);
            },
            "",
            repo,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function deactivateRepository(repo: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/repositories/deactivate/" + repo.id,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            repo,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function reactivateRepository(repo: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/repositories/reactivate/" + repo.id,
            "POST",
            (response: any) => {
                callback(response);
            },
            "",
            repo,
            ""
        );
    } catch (e) {
        throw e;
    }
}

export async function removeRepository(repo: any, callback: Function) {
    try {
        await axiosCaller(
            {},
            "/admin/repositories/" + repo.id,
            "DELETE",
            (response: any) => {
                callback(response);
            },
            "",
            repo,
            ""
        );
    } catch (e) {
        throw e;
    }
}