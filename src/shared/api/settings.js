const settings = {
  baseURL:
    process.env.NODE_ENV === "development"
      ? "http://localhost:5100/api"
      : "https://staging-server-6v.herokuapp.com/api",
};

export default settings;
