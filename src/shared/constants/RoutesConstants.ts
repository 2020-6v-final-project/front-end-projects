export const routesConstants = [
    {
        path: "/",
        pathName: "Home",
        allowedPermissions: []
    },
    {
        path: "/dashboard",
        pathName: "Dashboard",
        allowedPermissions: []
    },
    {
        path: "Chủ công ty",
        pathName: "Chủ công ty",
        allowedPermissions: []
    },
    {
        path: "Thu ngân",
        pathName: "Thu ngân",
        allowedPermissions: [
            // {
            //     id: 2,
            //     name: "DEVELOP API"
            // },
            // {
            //     id: 3,
            //     name: "DELETE API"
            // }
        ]
    },
    {
        path: "/user-management",
        pathName: "User Management",
        allowedPermissions: [
            // {
            //     id: 2,
            //     name: "DEVELOP API"
            // },
            // {
            //     id: 3,
            //     name: "DELETE API"
            // }
        ]
    },
    {
        path: "/branch-management",
        pathName: "Branch Management",
        allowedPermissions: [
            // {
            //     id: 2,
            //     name: "DEVELOP API"
            // },
            // {
            //     id: 3,
            //     name: "DELETE API"
            // }
        ]
    },
    {
        path: "/bill-management",
        pathName: "Bill Management",
        allowedPermissions: [
            // {
            //     id: 2,
            //     name: "DEVELOP API"
            // },
            // {
            //     id: 3,
            //     name: "DELETE API"
            // }
        ]
    },
    {
        path: "/login",
        pathName: "Login",
        allowedPermissions: [
            {
                id: 1,
                name: "CORE LOGIN"
            }
        ]
    },
    {
        path: "/item-management",
        pathName: "Item Management",
        allowedPermissions: [
            // {
            //     id: 2,
            //     name: "DEVELOP API"
            // },
            // {
            //     id: 3,
            //     name: "DELETE API"
            // }
        ]
    },
    {
        path: "/ingredient-management",
        pathName: "Ingredient Management",
        allowedPermissions: [
            // {
            //     id: 2,
            //     name: "DEVELOP API"
            // },
            // {
            //     id: 3,
            //     name: "DELETE API"
            // }
        ]
    },
    {
        path: "/menu-management",
        pathName: "Menu Management",
        allowedPermissions: [
            // {
            //     id: 2,
            //     name: "DEVELOP API"
            // },
            // {
            //     id: 3,
            //     name: "DELETE API"
            // }
        ]
    },
    {
        path: "/repository-management",
        pathName: "Repository Management",
        allowedPermissions: [
            // {
            //     id: 2,
            //     name: "DEVELOP API"
            // },
            // {
            //     id: 3,
            //     name: "DELETE API"
            // }
        ]
    },
    {
        path: "/table-management",
        pathName: "Table Management",
        allowedPermissions: [
            // {
            //     id: 2,
            //     name: "DEVELOP API"
            // },
            // {
            //     id: 3,
            //     name: "DELETE API"
            // }
        ]
    },
    
]