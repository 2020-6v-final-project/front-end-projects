import { AxiosError } from "axios";

const defaultErrors = [
    {
        status: 401,
        defaultMessage: "Tài khoản của bạn không được phép",
        errorMessage: [],
    },
    {
        status: 500,
        defaultMessage: "Lỗi không xác định",
        errorMessage: [],
    }
]

export const ErrorConstants = [
    {
        from: "LOGIN",
        title: "Đăng nhập thất bại",
        errors: [
            defaultErrors[1],
            {
                status: 401,
                defaultMessage: "Không tìm thấy thông tin",
                errorMessage: [
                    {
                        regrex: /Company has code (\w+| ).*business contract.*/g,
                        message: "Hợp đồng của bạn đã hết hạn hoặc xảy ra lỗi",
                    },
                    {
                        regrex: /User.\w+.*not found.*|^Incorrect|(Company has code (\w+ | )not found.*)/g,
                        message: "Tài khoản hoặc mật khẩu không chính xác",
                    },
                ],
            },
        ],
    },
    {
        from: "ORDER_MANAGEMENT",
        title: "Thất bại",
        errors: [
            ...defaultErrors,
            {
                status: 404,
                defaultMessage: "Không tìm thấy thông tin",
                errorMessage: [],
            },
        ]
    },
    {
        from: "PROMOTION_MANAGEMENT",
        title: "Thất bại",
        errors: [
            ...defaultErrors,
            {
                status: 404,
                defaultMessage: "Không tìm thấy thông tin",
                errorMessage: [
                    {
                        regrex: /Promotion \(id: \d+\) not found!/g,
                        message: "Không tìm thấy ưu đãi",
                    },
                ],
            },
            {
                status: 400,
                defaultMessage: "Không phù hợp",
                errorMessage: [
                    {
                        regrex: /Condition's field not compatibility with it's type/g,
                        message: "Điều kiện của ưu đãi không hợp lệ",
                    }, 
                    {
                        regrex: /This type has been existed/g,
                        message: "Điều kiện hoặc ưu đãi trùng loại",
                    }
                ],
            }
        ]
    },
    {
        from: "CAMPAIGN_MANAGEMENT",
        title: "Thất bại",
        errors: [
            ...defaultErrors,
            {
                status: 404,
                defaultMessage: "Không tìm thấy thông tin",
                errorMessage: [
                    {
                        regrex: /Campaign \(id: \d+\) not found!/g,
                        message: "Không tìm thấy chương trình",
                    }
                ],
            },
            {
                status: 400,
                defaultMessage: "Không phù hợp",
                errorMessage: [],
            }
        ]
    }
]

type From = 
    "LOGIN" 
    | "USER_MANAGEMENT" 
    | "BRANCH_MANAGEMENT" 
    | "INGREDIENT_MANAGEMENT" 
    | "ITEM_MANAGEMENT" 
    | "MENU_MANAGEMENT"
    | "ORDER"
    | "PROMOTION_MANAGEMENT"
    | "CAMPAIGN_MANAGEMENT"

export const errorMessage = (errRes: AxiosError, from: From, title?: string): {
    title: string,
    description: string,
} => {
    const errorBlock = ErrorConstants.find(block => block.from === from);
    if (errorBlock) {
        const error = errorBlock.errors.find(error => error.status === errRes.response?.status);
        if (error) {
            for (const errorMessage of error.errorMessage) {
                if(errRes.response?.data.message?.match(errorMessage.regrex)) {
                    return {
                        title: title ? title : errorBlock.title,
                        description: errorMessage.message,
                    }
                }
            }
            return {
                title: title ? title : errorBlock.title,
                description: error.defaultMessage,
            }
        }
    }
    return {
        title: title ? title : "Thất bại",
        description: "Không xác định",
    };
}