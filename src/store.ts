import { Action, combineReducers, configureStore } from "@reduxjs/toolkit";
import { ThunkAction } from "redux-thunk";
import HeaderSlice from '@components/layouts/TheCustomHeader/TheCustomHeader.slice';
import UserManagementSlice from "./features/SAC/UserManagement/UserManagement.slice";
import BranchManagementSlice from "@features/SAC/BranchManagement/BranchManagement.slice";
import OrderManagementSlice from "@features/Receptionist/OrderManagement/OrderManagement.slice";
import LoginPageSlice from '@features/LoginPage/LoginPage.slice';
import ItemManagementSlice from '@features/SAC/ItemManagement/ItemManagement.slice';
import TheSidebarSlice from '@components/layouts/TheSidebar/TheSidebar.slice';
import IngredientManagementSlice from '@features/SAC/IngredientManagement/IngredientManagement.slice';
import MenuManagementSlice from '@features/SAC/MenuManagement/MenuManagement.slice';
import RoleManagementSlice from '@features/SAC/RoleManagement/RoleManagement.slice';
import TableManagementSlice from '@features/Manager/TableManagement/TableManagement.slice';
import CurrencyManagement from '@features/SAC/CurrencyManagement/CurrencyManagement.slice';
import RepositoryIngredientsManagementSlice from '@features/Manager/StorageManagement/StorageManagement.slice';
import RepositoryManagementSlice from "@features/SAC/RepositoryManagement/RepositoryManagement.slice";
import AzureManagementSlice from "@features/Azure/AzureManagement.slice";
import ItemTypeIngredientTypeManagementSlice from '@features/SAC/ItemIngredientTypeManagement/ItemIngredientTypeManagement.slice';
import ReceiptManagementSlice from '@features/Manager/StorageManagement/ReceiptManagement/ReceiptManagement.slice'
import ManagerRequestManagementSlice from "@features/Manager/ManagerRequestManagement/ManagerRequestManagement.slice";
import CompanyRequestManagementSlice from "@features/SAC/CompanyRequestManagement/CompanyRequestManagement.slice";
import VendorManagementSlice from '@features/SAC/VendorManagement/VendorManagement.slice';
import TransactionManagementSlice from '@features/Manager/TransactionManagement/TransactionManagement.slice';
import FinanceManagementSlice from '@features/Manager/FinanceManagement/FinanceManagement.slice';
import PromotionManagementSlice from '@features/SAC/PromotionManagement/PromotionManagement.slice';
import CampaignManagementSlice from '@features/SAC/CampaignManagement/CampaignManagement.slice';

const rootReducer = combineReducers({
  header: HeaderSlice,
  user: UserManagementSlice,
  auth: LoginPageSlice,
  branch: BranchManagementSlice,
  order: OrderManagementSlice,
  sidebar: TheSidebarSlice,
  item: ItemManagementSlice,
  ingredient: IngredientManagementSlice,
  menu: MenuManagementSlice,
  role: RoleManagementSlice,
  table: TableManagementSlice,
  currency: CurrencyManagement,
  repositoryIngredients: RepositoryIngredientsManagementSlice,
  repository: RepositoryManagementSlice,
  azure: AzureManagementSlice,
  itemTypeIngredientType: ItemTypeIngredientTypeManagementSlice,
  receipt: ReceiptManagementSlice,
  managerRequest: ManagerRequestManagementSlice,
  companyRequest: CompanyRequestManagementSlice,
  vendor: VendorManagementSlice,
  transaction:TransactionManagementSlice,
  finance: FinanceManagementSlice,
  promotion: PromotionManagementSlice,
  campaign: CampaignManagementSlice,
});

const store = configureStore({ reducer: rootReducer });

export type RootState = ReturnType<typeof rootReducer>;
export type AppThunk = ThunkAction<void, RootState, unknown, Action<string>>;
export type AppDispatch = typeof store.dispatch;

export default store;
