import React from "react";
import { Route, Switch } from "react-router-dom";
import TheLayout from "./shared/components/layouts/TheLayout";
import LoginPageContainer from "./features/LoginPage/LoginPage.container";

import "./assets/scss/style.scss";
import 'react-perfect-scrollbar/dist/css/styles.css';
import './App.less';

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
);

const App: React.FC = (props: any) => {
  return (
    <React.Suspense fallback={loading}>
      <Route>
        <Switch>
          <Route path="/login" component={LoginPageContainer} />
          <Route path="/" component={TheLayout} />
        </Switch>
      </Route>
    </React.Suspense>
  );
};

export default App;
