import { Branch,Role } from '@custom-types';
import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectBranchOfBranchManagementUserList = createSelector(
    [
        (state: RootState) => state.auth.branches, 
        (state: RootState) => state.auth.roles,
    ],
    (branchList, roles) => {
        const smRole = roles.find(role => role.id === 5);
        if (smRole) {
            let branchListOfManager = [] as Branch[];
            smRole.branchids.forEach(id => {
                const branch = branchList.find(branch => branch.id === id);
                if (branch) branchListOfManager.push(branch);
            });
            return branchListOfManager;
        } 
        return [];
    }
)

export const selectBranchOfReceptionistUserList = createSelector(
    [
        (state: RootState) => state.auth.branches, 
        (state: RootState) => state.auth.roles,
    ],
    (branchList, roles) => {
        const receptionistRole = roles.find(role => role.id === 6);
        if (receptionistRole) {
            let branchListOfReceptionist = [] as Branch[];
            receptionistRole.branchids.forEach(id => {
                const branch = branchList.find(branch => branch.id === id);
                if (branch) branchListOfReceptionist.push(branch);
            });
            return branchListOfReceptionist;
        } 
        return [];
    }
)

export const selectBranchOfUserForStorageList = createSelector(
    [
        (state: RootState) => state.auth.branches, 
        (state: RootState) => state.auth.user, 
        (state: RootState) => state.auth.roles,
    ],
    (branchList, user, roles) => {
        let stoRoles = [] as Role[];
        for (let a=0; a < roles.length;a++)
        {
            if(roles[a].haspermissions.find(per => per.id === 28) !== undefined)
            {
                stoRoles.push(roles[a]);
                continue;
            }
        }
        if (stoRoles) {
            let branchListOfStorage = [] as Branch[];
            stoRoles.forEach(role => {
                if(role.branchids!==undefined)
                for (let a=0; a < role.branchids.length;a++)
                {
                    const branch = branchList.find(branch => branch.id === role.branchids[a]);

                    if (branch && branchListOfStorage.findIndex(br => branch.id === br.id) < 0) branchListOfStorage.push(branch);    
                }
            });
            return branchListOfStorage;
        }
        return [];
    }
)
