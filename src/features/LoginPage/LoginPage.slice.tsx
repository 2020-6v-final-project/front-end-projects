import { Role, Branch, User } from "@custom-types";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AppThunk } from "src/store";
import { logIn } from "@shared";
import { history } from "@components/history";
import swal from 'sweetalert';
import { errorMessage } from "@shared/constants/ErrorConstants";

type UserState = {
  token: string;
  roles: Role[];
  branches: Branch[];
  user: User | undefined;
};

const initialState: UserState = {
  token: "",
  roles: [],
  branches: [],
  user: undefined,
};

const UserSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    loginSuccess: (state: UserState, action: PayloadAction<UserState>) => {
      localStorage.setItem("token", action.payload.token);
      localStorage.setItem("roles", JSON.stringify(action.payload.roles));
      localStorage.setItem("branches", JSON.stringify(action.payload.branches));
      localStorage.setItem("user", JSON.stringify(action.payload.user));
      state.token = action.payload.token;
      state.roles = action.payload.roles;
      state.branches = action.payload.branches;
      state.user = action.payload.user;
      return state;
    },
    loginFailed: (state: UserState) => {
      return state;
    },
    logout: (state: UserState) => {
      state.roles = [];
      state.token = "";
      state.branches = [];
      state.user = undefined;
      localStorage.removeItem("token");
      localStorage.removeItem("roles");
      localStorage.removeItem("branches");
      localStorage.removeItem("user");
    },
    getBranchOfUserSuccess: (
      state: UserState,
      action: PayloadAction<Branch[]>
    ) => {
      const branchIdList = state.branches?.map((branch) => branch.id);
      let newBranches = [] as Branch[];
      action.payload?.forEach((branch) => {
        if (branchIdList.includes(branch.id)) {
          newBranches.push(branch);
        }
      });
      state.branches = newBranches;
      return state;
    },
    getBranchOfUserFailed: (state: UserState) => {
      return state;
    },
    updateBranchOfUserSuccess: (
      state: UserState,
      action: PayloadAction<Branch>
    ) => {
      const index = state.branches?.findIndex(
        (branch) => branch.id === action.payload.id
      );
      if (index > -1) state.branches?.splice(index, 1, action.payload);
      return state;
    },
    updateBranchOfUserFailed: (state: UserState) => {
      return state;
    },
  },
});

export const loginAsync = (
  username: string,
  password: string
): AppThunk => async (dispatch) => {
  try {
    await logIn(username, password, (data: UserState) => {
      dispatch(loginSuccess(data));
      history.push("/dashboard");
    });
  } catch (e) {
    const errorBundle = errorMessage(e, "LOGIN");
    swal(errorBundle.title, errorBundle.description, "error");
    dispatch(loginFailed());
  }
};

const {
  loginSuccess,
  loginFailed,
  logout,
  updateBranchOfUserSuccess,
  updateBranchOfUserFailed,
  getBranchOfUserSuccess,
  getBranchOfUserFailed,
} = UserSlice.actions;

export {
  loginSuccess,
  loginFailed,
  logout,
  updateBranchOfUserSuccess,
  updateBranchOfUserFailed,
  getBranchOfUserSuccess,
  getBranchOfUserFailed,
};

export default UserSlice.reducer;
