import { CFade } from '@coreui/react';
import React, { useEffect,useState  } from 'react';
import {  Row, Col, Table, Tag, Typography} from 'antd';
import { ColumnsType } from "antd/lib/table";
import { connect } from 'react-redux';
import { RootState } from 'src/store';
import {
  currencyFormat,
  shortDateFormat,
} from "@shared/utils/";
import { Scrollbars } from "react-custom-scrollbars";
import { getReceiptByIdAsync } from '../../Manager/StorageManagement/ReceiptManagement/ReceiptManagement.slice'
import { selectReceiptTypesList,selectReceiptsList } from "../../Manager/StorageManagement/ReceiptManagement/ReceiptManagement.selector";
import { getReceiptTypesAsync } from "../../Manager/StorageManagement/ReceiptManagement/ReceiptManagement.slice";
import { selectVendorList } from "../../SAC/VendorManagement/VendorManagement.selector";
import {  getVendorAsync } from "../../SAC/VendorManagement/VendorManagement.slice";
import { getIngredientAsync } from "../../SAC/IngredientManagement/IngredientManagement.slice";
import { selectIngredientList } from "../../SAC/IngredientManagement/IngredientManagement.selector";
import { selectRepositoriesList } from "../../SAC/RepositoryManagement/RepositoryManagement.selector";
import { fetchBranchListAsync } from "../../SAC/BranchManagement/BranchManagement.slice";
import { branchListSelector } from "../../SAC/BranchManagement/BranchManagement.selector";
import { getRepositoriesAsync } from "../../SAC/RepositoryManagement/RepositoryManagement.slice";

import "./ReceiptDetail.scss"

type ReceiptDetailState = {
    match: any;
    receiptList: any[];
    vendorList: any[];
    receiptTypes: any[];
    ingredients: any[];
    repositories: any[];
    branches: any[];
    receiptTypesList: any[];
}

type ReceiptDetailDispatch = {
  getReceiptByIdAsync: Function,
  fetchBranchListAsync: Function,
  getIngredientAsync: Function,
  getReceiptTypesAsync: Function,
  getVendorAsync: Function,
  getRepositoriesAsync: Function,
}

type ReceiptDetailDefaultProps = ReceiptDetailState & ReceiptDetailDispatch;

const ReceiptDetailContainer: React.FC<ReceiptDetailDefaultProps> = React.memo((props) => {
  const { match, receiptList, receiptTypesList, vendorList, ingredients, repositories, branches,
    getRepositoriesAsync, getReceiptByIdAsync, fetchBranchListAsync, getIngredientAsync, getReceiptTypesAsync, getVendorAsync
    } = props;
  const [tableData, setTableData] = useState<any[]>([]);
  useEffect(() => {
    const fetchData = async () => {
      if(match?.params.branchid && match?.params.receiptid) 
      {
        await getReceiptByIdAsync(match?.params.branchid,match?.params.receiptid);
        await fetchBranchListAsync();
        await getIngredientAsync();
        await getReceiptTypesAsync();
        await getVendorAsync();
        await getRepositoriesAsync();
      }
    }
    fetchData();
  },[getRepositoriesAsync, match?.params.receiptid, match?.params.branchid, getReceiptByIdAsync,fetchBranchListAsync,getIngredientAsync, getReceiptTypesAsync, getVendorAsync]);

  useEffect(() => {
    if(receiptList)
      setTableData(receiptList[0]?.receiptingredient);
  },[receiptList]);

  const columns: ColumnsType = [
    {
      align: "center",
      title: "Tên nguyên liệu",
      dataIndex: "ingredientid",
      key: "ingredientid",
      render: (text: string) => <Typography>{ingredients.find((ingre)=>ingre.id===Number(text))?.name}</Typography>,
    },
    {
      align: "center",
      title: 'Nhà cung cấp',
      dataIndex: 'vendorid',
      key: 'vendorid',
      render: (text: string) => <Typography>{vendorList?.find(vendor => vendor.id === text)?.name}</Typography>,
    },
    {
      align: "center",
      title: "Giá",
      dataIndex: "price",
      key: "price",
      render: (text: string) => <Typography>{currencyFormat(Number(text))}</Typography>,
    },
    {
      align: "center",
      title: "Số lượng",
      dataIndex: "quantity",
      key: "quantity",
    },
    {
      align: "center",
      title: "Tổng tiền",
      dataIndex: "total",
      key: "total",
      render: (text: string) => <Typography>{currencyFormat(Number(text))}</Typography>,
    },
    {
      align: "center",
      title: "Ghi chú",
      dataIndex: "note",
      key: "note",
      render: (text:string) => <Typography.Text editable={receiptList[0]?.statuscode !== "EDITING" ? false : true} > {text} </Typography.Text>
    }
  ];

  const renderSwitchTag = (param:any) => {
    switch(param) {
      case "EDITING":
        return <Tag color="magenta">Chỉnh sửa</Tag>;
      case "PENDINGAPPROVED":
        return <Tag color="gold">Đang chờ duyệt</Tag>;
      case "REJECTED":
        return <Tag color="red">Bị từ chối</Tag>;
      case "APPROVED":
        return <Tag color="lime">Đã duyệt</Tag>;
      case "PENDINGPAYMENT":
        return <Tag color="geekblue">Đang chờ chi</Tag>;
      case "PENDINGRECEIVED":
        return <Tag color="cyan">Đang nhận hàng</Tag>;
      default:
        return <Tag color="success">Hoàn thành</Tag>;
    }
  }



    return (
      <CFade>
        <Scrollbars
              autoHeightMin={0}
              autoHeightMax={800}
              autoHeight
              >
        <Row justify="center" gutter={24}>
          <Col span={6} className="ReceiptInDetailModal-row-title">
            <Typography.Text >Chi tiết đơn hàng</Typography.Text>
          </Col>
        </Row>
        <Row  justify="center" gutter={24}>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Chi Nhánh</h6>
            <h5>{branches.find((branch) => receiptList[0]?.branchid === branch.id)?.name}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Kho</h6>
            <h5>{repositories.find((repo) => receiptList[0]?.repositoryid === repo.id)?.name}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Ngày tạo</h6>
            <h5>{shortDateFormat(receiptList[0]?.createdat)}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Loại phiếu</h6>
            <h5>{receiptTypesList.find((rec)=>rec.id===receiptList[0]?.receipttypeid)?.name}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Trạng thái</h6>
            {renderSwitchTag(receiptList[0]?.statuscode)}  
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="ReceiptInDetailModal-row-sub-title">
            <Typography.Text >Ghi chú:</Typography.Text>
          </Col>
          <Col span={18} className="ReceiptInDetailModal-row-text">
            <Typography.Text editable={receiptList[0]?.statuscode !== "EDITING" ? false : true}>{receiptList[0]?.note}</Typography.Text>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={24} className="ReceiptInDetailModal-row-title">
            <Typography.Text >Danh sách nguyên liệu:</Typography.Text>
          </Col>
        </Row>
        <Row>
          <Table
            bordered
            size="small"
            className="ReceiptInDetailModal-table"
            columns={columns}
            pagination={{ position: ["bottomCenter"] }}
            dataSource={tableData}
            scroll={{ y: "55vh" }}
          />
      </Row>
      </Scrollbars>
      </CFade>
    )
});

const mapState = (state: RootState) => {
    return {
      receiptList: selectReceiptsList(state),
      vendorList: selectVendorList(state),
      receiptTypesList: selectReceiptTypesList(state),
      ingredients: selectIngredientList(state),
      repositories: selectRepositoriesList(state),
      branches: branchListSelector(state),
    }
};

const mapDispatch = {  
  getReceiptByIdAsync,
  fetchBranchListAsync, 
  getIngredientAsync,
  getReceiptTypesAsync,
  getVendorAsync,
  getRepositoriesAsync,
};

export default connect(mapState, mapDispatch)(ReceiptDetailContainer);