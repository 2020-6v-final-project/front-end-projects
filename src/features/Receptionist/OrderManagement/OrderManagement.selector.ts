import { RootState } from "src/store";
import { createSelector } from "reselect";

export const orderListSelector = createSelector(
  [(state: RootState) => state.order.orderList],
  (orderList) => orderList
);

export const currenOrderSelector = createSelector(
  [(state: RootState) => state.order.currentOrder],
  (currentOrder) => currentOrder
);

export const mainMenuSelector = createSelector(
  [(state: RootState) => state.order.mainMenu],
  (mainMenu) => mainMenu
);