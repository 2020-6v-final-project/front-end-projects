import {
  AutoComplete,
  Badge,
  Button,
  Col,
  DatePicker,
  Divider,
  Empty,
  Input,
  List,
  Row,
  Select,
  Switch,
  Typography,
  Space,
  Form,
} from "antd";
import React, { useEffect, useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import PerfectScrollbar from "react-perfect-scrollbar";
import OrderListItem from "../OrderListItem/OrderListItem";
import BottomBar from "../BottomBar/BottomBar";
import SavedOrder from "../SavedOrder/SavedOrder";
import "./MainOrderPanel.scss";
import { Order } from "@custom-types";
import { ResponseOrder, ResponseMenu } from "../../OrderManagement.slice";
import AddNewOrderModal from "../AddNewOrderModal/AddNewOrderModal";
import ConfirmDeleteOrderModal from "@shared/components/dialog/modal/modal"; 
import OrderListModal from "@shared/components/dialog/modal/modal";
import UpdateOrderModal from "@shared/components/dialog/modal/modal";
import { tempOrderTypeData } from "../AddNewOrderModal/AddNewOrderModal";
import { Moment } from "moment";

type SideOrderPanelProps = {
  changeCurrentOrderLoading: boolean;
  orderList: Order[];
  mainMenu: ResponseMenu | undefined;
  currentOrder: ResponseOrder | undefined;
};

type SideOrderPanelDispatch = {
  handleChangeCurrentOrder: Function;
  handleAddOrder: Function;
  handleDeleteLineOfCurrentOrder: Function;
  handleUpdateLineToOfCurrentOrder: Function;
  handleDeleteOrder: Function;
  handleUpdateOrder: Function;
};

type SideOrderPanelDefaultProps = SideOrderPanelProps & SideOrderPanelDispatch;

type timeType =  "date" | "week" | "month" | "quarter" | "year";

const PickerWithType = ({ type, onChange }: { type: timeType, onChange: (value: Moment | null, dateString: string) => void }) => {
  return <DatePicker picker={type} onChange={onChange} />;
}

const filterByOrderType = (orderType: string, orderList: Order[]) => {
  switch (orderType) {
    case "all": 
      return orderList;
    case "take_away":
      return orderList.filter(order => order.ordertypeid === 1);
    case "in_place":
      return orderList.filter(order => order.ordertypeid === 2);
    default:
      return orderList;
  }
}

const filterByOrderStatus = (status: string, orderList: Order[]) => {
  switch (status) {
    case "all": 
      return orderList;
    case "editing":
      return orderList.filter(order => order.statusid === 4);
    case "paid":
      return orderList.filter(order => order.statusid === 8);
    default:
      return orderList;
  }
}

const filterByOrderDateCreate = (type: string, date: Moment | null, orderList: Order[]) => {
  if (date) {
    switch (type) {
      case "date":
        return orderList.filter(order => (new Date(order.createdat)).getDate() === date.toDate().getDate());
      case "week":
        return orderList.filter(order => {
          const createDate = new Date(order.createdat);
          if (createDate.getTime() >= date?.weekday(0).toDate().getTime()
            && createDate.getTime() <= date?.weekday(6).toDate().getTime()) 
            return order;
            else 
            return undefined;
        });
      case "month":
        return orderList.filter(order => (new Date(order.createdat)).getMonth() === date.toDate().getMonth());;
      case "quarter":
        const endOfQuarter = new Date(date.toDate().setMonth(date.toDate().getMonth() + 2));
        return orderList.filter(order => {
          const createDate = new Date(order.createdat);
          if (createDate.getTime() >= date.toDate().getTime()
          && createDate.getTime() <= endOfQuarter.getTime()) 
            return order;
            else 
            return undefined;
        });
      case "year":
        return orderList.filter(order => (new Date(order.createdat)).getFullYear() === date.toDate().getFullYear());
      default: 
        return orderList;
    }
  } else {
    return orderList;
  }
}

const MainOrderPanel: React.FC<SideOrderPanelDefaultProps> = React.memo((props) => {
    const { changeCurrentOrderLoading, orderList, currentOrder, mainMenu } = props;
    const {
      handleDeleteLineOfCurrentOrder,
      handleAddOrder,
      handleChangeCurrentOrder,
      handleUpdateLineToOfCurrentOrder,
      handleDeleteOrder,
      handleUpdateOrder,
    } = props;

    const [activeKey, setActiveKey] = useState<string | undefined>("1");
    const [openOrderListModal, setOpenOrderListModal] = useState<boolean>(false);
    const [openAddOrderModal, setOpenAddOrderModal] = useState<boolean>(false);

    const [selectedOrderId, setSelectedOrderId] = useState<number | undefined>(undefined);
    const [openConfirmDeleteOrderModal, setOpenConfirmDeleteOrderModal] = useState<boolean>(false);
    const [openUpdateOrderModal, setOpenUpdateOrderModal] = useState<boolean>(false);

    const [type, setType] = useState<timeType>('date');
    const [orderTypeValue, setOrderTypeValue] = useState<string>("all");
    const [orderStatusValue, setOrderStatusValue] = useState<string>("all");
    const [orderCreateDateValue, setOrderCreateDateValue] = useState<Moment | null>(null);
    const [filtered, setFiltered] = useState<Order[]>(orderList);

    const [form] = Form.useForm();

    useEffect(() => {
      setFiltered(
        filterByOrderType(orderTypeValue, 
          filterByOrderStatus(orderStatusValue,
            filterByOrderDateCreate(type, orderCreateDateValue,
              orderList))));
    }, [orderList, orderTypeValue, orderStatusValue, type, orderCreateDateValue]);

    const confirmDialogContent = (
      <Row justify="center">
        <Typography.Title level={5}>
          Bạn có muốn xóa đơn hàng này ?
        </Typography.Title>
      </Row>
    )

    const handleUpdateOrderFromMainOrder = async (value: any) => {
      await handleUpdateOrder(selectedOrderId, value);
    }
    
    return (
      <Row
        justify="space-between"
        style={{ position: "relative", height: "88vh", width: "100%" }}
      >
        <OrderListModal
          show={openOrderListModal}
          onClose={() => setOpenOrderListModal(false)}
          buttonHidden={true}
          size="xl"
          title="Danh sách đơn hàng"
          content={
            <Row style={{ height: "100%" }}>
              <Col span={24}>
                <Row justify="center" gutter={8} className="mb-5 mt-3">
                  <Col span={8}>
                    <Row justify="start" gutter={8}>
                      <Col>
                        <Row align="middle" style={{ height: "100%" }}>
                          <Typography.Title level={5} style={{ marginBottom: 0 }}>Loại đơn hàng</Typography.Title>
                        </Row>
                      </Col>
                      <Col>
                        <Select 
                          value={orderTypeValue} 
                          style={{ minWidth: "100px" }}
                          onChange={(value) => {
                            setOrderTypeValue(value);
                          }}
                        >
                          <Select.Option value="all">Tất cả</Select.Option>
                          <Select.Option value="take_away">Mang đi</Select.Option>
                          <Select.Option value="in_place">Tại quán</Select.Option>
                        </Select>
                      </Col>
                    </Row>
                  </Col>
                  <Col span={8}>
                    <Row justify="start" gutter={8}>
                      <Col>
                        <Row align="middle" style={{ height: "100%" }}>
                          <Typography.Title level={5} style={{ marginBottom: 0 }}>Trạng thái</Typography.Title>
                        </Row>
                      </Col>
                      <Col>
                        <Select 
                          value={orderStatusValue} 
                          style={{ minWidth: "200px" }}
                          onChange={(value) => {
                            setOrderStatusValue(value);
                          }}  
                        >
                          <Select.Option value="all">Tất cả</Select.Option>
                          <Select.Option value="editing">Đang chỉnh sửa</Select.Option>
                          <Select.Option value="paid">Đã thanh toán</Select.Option>
                        </Select>
                      </Col>
                    </Row>
                  </Col>
                  <Col span={8}>
                    <Row justify="start" gutter={8}>
                      <Col>
                        <Row align="middle" style={{ height: "100%" }}>
                          <Typography.Title level={5} style={{ marginBottom: 0 }}>Ngày tạo</Typography.Title>
                        </Row>
                      </Col>
                      <Col>
                      <Space>
                        <Select value={type} onChange={setType}>
                          <Select.Option value="date">Ngày</Select.Option>
                          <Select.Option value="week">Tuần</Select.Option>
                          <Select.Option value="month">Tháng</Select.Option>
                          <Select.Option value="quarter">Quý</Select.Option>
                          <Select.Option value="year">Năm</Select.Option>
                        </Select>
                        <PickerWithType 
                          type={type} 
                          onChange={(value) => {
                            setOrderCreateDateValue(value);
                          }} 
                        />
                      </Space>
                      </Col>
                    </Row>
                  </Col>
                </Row>
                <Divider>Đơn hàng</Divider>
                <Row className="mt-4">
                  <Col span={24}>
                    <Row style={{ marginRight: "100px", padding: "0 5px 0 5px" }}>
                      <Col span={24}>
                        <Row justify="space-between">
                          <Col span={7}>
                            <Typography.Title level={5}>Thông tin</Typography.Title>
                          </Col>
                          <Col span={7}>
                            <Typography.Title level={5}>Ngày tạo</Typography.Title>
                          </Col>
                          <Col span={6}>
                            <Typography.Title level={5}>Tổng cộng</Typography.Title>
                          </Col>
                          <Col span={4}>
                            <Typography.Title level={5}>Trạng thái</Typography.Title>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                    <PerfectScrollbar style={{ maxHeight: 340 }}>
                      <List>
                        {filtered.length !== 0 ? filtered.map((order, index) => 
                          <SavedOrder 
                            isSelected={order.id === currentOrder?.id}
                            order={order} 
                            key={index}
                            handleChangeCurrentOrder={(orderId: number) => {
                              setOpenOrderListModal(false);
                              handleChangeCurrentOrder(orderId);
                            }}
                            handleSetDeleteOrderId={(id: number | undefined) => {
                              setSelectedOrderId(id);
                              setOpenConfirmDeleteOrderModal(true);
                            }}
                            handleSetUpdateOrderId={(order: Order) => {
                              setSelectedOrderId(order.id);
                              form.setFieldsValue({
                                ordertypeid: order.ordertypeid,
                                note: order.note,
                              })
                              setOpenUpdateOrderModal(true);
                            }}
                          />
                        ): undefined}
                      </List>

                      <UpdateOrderModal
                        title="Cập nhập đơn hàng"
                        show={openUpdateOrderModal}
                        onClose={() => setOpenUpdateOrderModal(false)}
                        onChooseAsync={async (result) => {
                          if (result) await handleUpdateOrderFromMainOrder(form.getFieldsValue());
                        }}
                        content={
                          <Form form={form} onFinish={handleUpdateOrderFromMainOrder} className="mt-5">
                            <Row>
                              <Col span={24}>
                                <Form.Item
                                    name="ordertypeid"
                                    initialValue={tempOrderTypeData[0].id}
                                >
                                  <Select>
                                      {tempOrderTypeData.map((orderType, index) => 
                                          <Select.Option key={index} value={orderType.id}>{orderType.text}</Select.Option>
                                      )}
                                  </Select>
                                </Form.Item>
                              </Col>
                            </Row>
                            <Row>
                                <Col span={24}>
                                    <Form.Item
                                        name="note"
                                    >
                                        <Input.TextArea
                                            autoSize={{ minRows: 3, maxRows: 3 }}
                                            maxLength={100}
                                            placeholder="Ghi chú"
                                        />
                                    </Form.Item>
                                </Col>
                            </Row>
                          </Form>
                        }
                      />
                    </PerfectScrollbar>
                  </Col>
                </Row>
              </Col>
            </Row>
          }
        />

        <ConfirmDeleteOrderModal
          content={confirmDialogContent}
          title="Xác nhận xóa"
          show={openConfirmDeleteOrderModal}
          onClose={() => setOpenConfirmDeleteOrderModal(false)}
          onChooseAsync={async (result) => {
            if (result) {
              if (selectedOrderId === currentOrder?.id)
                await handleDeleteOrder(undefined);
              else
                await handleDeleteOrder(selectedOrderId);
            }
          }}
        />

        <AddNewOrderModal
          openAddOrderModal={openAddOrderModal}
          handleCloseAddOrderModal={() => setOpenAddOrderModal(false)}
          handleAddOrder={handleAddOrder}
        />

        <Col span={24}>
          <Row justify="space-between" gutter={12}>
            <Col span={12}>
              <Typography.Title level={4}>Chi tiết đơn hàng</Typography.Title>
            </Col>

            <Col span={6}>
              <Row justify="end" >
                <Badge count={orderList.length} overflowCount={99}>
                  <Button 
                    onClick={() => setOpenOrderListModal(true)}
                    loading={changeCurrentOrderLoading}
                  >
                    Đơn đã lưu
                  </Button>
                </Badge>
              </Row>
            </Col>

            <Col span={4}>
              <Row justify="end">
                <Button 
                  type="primary"
                  onClick={() => setOpenAddOrderModal(true)}
                >
                  Tạo đơn mới
                </Button>
              </Row>
            </Col>
          </Row>
          <Row align="middle" className="pt-3 " gutter={24}>
            <Col span={10}>
              <AutoComplete
                style={{ width: "100%" }}
                placeholder="Chọn bàn"
              />
            </Col>

            <Col span={10}>
              <Input
                style={{ width: "100%" }}
                placeholder="Mã khuyến mãi đơn"
              />
            </Col>

            <Col span={4}>
              <Switch 
                checked={activeKey ? true : false}
                checkedChildren={"Tùy chọn"}
                unCheckedChildren={"Tùy chọn"}
                onClick={() => {
                  if (activeKey === "1") setActiveKey(undefined);
                  else setActiveKey("1");
                }}
              />
            </Col>
          </Row>
          <Scrollbars style={{ marginTop: 15 }}>
            <Row style={{ paddingBottom: "170px" }}>
              <Col span={24}>
                {currentOrder ?
                  <List size="large">
                    {currentOrder.lines?.length > 0 ?currentOrder?.lines?.map((line, index) =>
                      <OrderListItem 
                        activeKey={activeKey}
                        key={index}
                        line={line}
                        itemBundle={mainMenu?.items.find(itemBundle => itemBundle?.itemid === line.itemid)}
                        handleUpdateLineOfCurrentOrder={handleUpdateLineToOfCurrentOrder}
                        handleDeleteLineOfCurrentOrder={handleDeleteLineOfCurrentOrder}
                      />
                    )
                    :
                    <Empty 
                      className="mt-5"
                      description="Chưa có món ăn"
                    />}
                  </List>
                  :
                  <Empty 
                    className="mt-5"
                    image={Empty.PRESENTED_IMAGE_SIMPLE}
                    description="Chưa có đơn hàng nào được chọn"
                  />}
              </Col>
            </Row>
          </Scrollbars>
        </Col>
        {currentOrder && currentOrder.lines?.length > 0 ?
        <BottomBar 
          currentOrder={currentOrder}
          handleDeleteOrder={(id: number | undefined) => {
            setSelectedOrderId(id);
            setOpenConfirmDeleteOrderModal(true);
          }}
        /> : <></>}
      </Row>
    );
  }
);

export default MainOrderPanel;
