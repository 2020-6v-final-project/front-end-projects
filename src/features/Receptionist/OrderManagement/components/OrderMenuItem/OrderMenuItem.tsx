import { CloseOutlined } from "@ant-design/icons";
import {
  Avatar,
  Button,
  Card,
  Col,
  Form,
  Input,
  InputNumber,
  Popover,
  Radio,
  Row,
  Tabs,
  List,
  Typography,
  Empty,
  Tag,
} from "antd";
import React, { CSSProperties, useEffect, useState } from "react";
import { ResponseItem } from "../../OrderManagement.slice";
import { currencyFormat } from '../../../../../shared/utils/stringUtils';
import PerfectScrollbar from "react-perfect-scrollbar";
import "./OrderMenuItem.scss";

type OrderMenuItemProps = {
  itemBundle: ResponseItem | null;
  currentItemId?: number;
  className?: string;
  handleAddLineToCurrentOrder: Function;
  handleSelectItem: Function;
};

const CSSStyle: CSSProperties = {
  width: "100%",
  textOverflow: "ellipsis",
  overflow: "hidden",
  whiteSpace: "nowrap",
};

const colorDependOnQuantity = (quantity: number | undefined) => {
  if (quantity === undefined) return undefined;
  if (quantity === 0) return 'red';
  if (quantity < 5) return 'orange';
  if (quantity < 20) return 'gold';
  return 'green';
}

const OrderMenuItem: React.FC<OrderMenuItemProps> = React.memo((props) => {
  const {
    itemBundle,
    className,
    handleAddLineToCurrentOrder,
    handleSelectItem,
  } = props;

  const [loading, setLoading] = useState<boolean>(false);
  const [visible, togglePopover] = useState(false);
  const [options, setOptions] = useState<{
    optionid: number,
    optionquantity: number,
  }[]>([]);

  useEffect(() => {
    if (visible && itemBundle) {
      setOptions(itemBundle.options.map(option => {
        return {
          optionid: option.optionid,
          optionquantity: 0,
        }
      }))
    }
  }, [visible, itemBundle]);

  const handleCardSelect = (id: number | undefined) => {
    if (visible) {
      handleSelectItem(undefined);
    } else {
      handleSelectItem(id);
    }
  };

  const handleChangeOptionQuantity = (value: any, optionId: number) => {
    if (Number.isInteger(value)) {
      const tempOptions = [ ...options ];
      const index = tempOptions.findIndex(option => option.optionid === optionId);
      if (index > -1) {
        tempOptions[index].optionquantity = value;
        setOptions(tempOptions);
      }
    }
  }

  const handleFormSubmit = async (value: any) => {
    const newLine = {
      itemid: itemBundle?.itemid,
      itemquantity: value.quantity,
      note: value.note,
      options: options.filter(option => option.optionquantity !== 0),
      size: {
        optionid: value.size,
      }
    }
    setLoading(true);
    await handleAddLineToCurrentOrder(newLine);
    setLoading(false)
    handleSelectItem(undefined);
    togglePopover(false);
  };

  return (
    <Popover
      className="item-popover"
      visible={visible}
      trigger="click"
      onVisibleChange={(value) => {
        if (visible) handleSelectItem(undefined);
        togglePopover(value);
      }}
      content={
        <PerfectScrollbar>
          <Row justify="end">
            <CloseOutlined
              className="close-button"
              onClick={() => {
                handleSelectItem(undefined);
                togglePopover(false);
              }}
            />
          </Row>
          <Row
            className="item-popover"
            style={{
              borderRadius: "20px",
              padding: 16,
              minWidth: 320,
              height: 480,
            }}
            justify="center"
          >
            <Col style={{ ...CSSStyle, textAlign: "center" }}>
              <Avatar
                style={{ width: 64, height: 64 }}
                src={(itemBundle?.item.imgpath === null || itemBundle?.item.imgpath === "") ? "/item-sample.svg" : itemBundle?.item.imgpath}
              />
              <Typography.Title
                ellipsis={{ rows: 1, expandable: false }}
                className="mt-3"
                level={5}
              >
                {itemBundle?.item.name}
              </Typography.Title>

              <Form
                style={{ height: "100%" }}
                onFinish={handleFormSubmit}
              >
                <Tabs centered defaultActiveKey="1">
                  <Tabs.TabPane style={{ height: "100%" }} key="1" tab="Chung">
                    {itemBundle?.sizes && itemBundle?.sizes.length > 0 ?
                      <Form.Item
                        name="size"
                        rules={[
                          {
                            required: true,
                            message: "Vui lòng chọn kích cỡ!",
                          },
                        ]}
                        initialValue={itemBundle.sizes[0].optionid}
                      >
                        <Radio.Group>
                          {itemBundle.sizes.map((size, index) => 
                          <Row key={index}>
                            <Radio value={size.optionid}>{size.option.name} (+{currencyFormat(size.price)})</Radio>
                          </Row>
                          )}
                        </Radio.Group>
                      </Form.Item> 
                      :
                      <></>
                    }

                    <Row gutter={24}>
                      <Col span={14}>
                        <Form.Item name="promotion">
                          <Input maxLength={100} placeholder="Mã khuyến mãi" />
                        </Form.Item>
                      </Col>
                      <Col span={10}>
                        <Form.Item
                          name="quantity"
                          rules={[
                            {
                              required: true,
                              message: "Vui lòng nhập số lượng!",
                            },
                          ]}
                          initialValue={1}
                        >
                          <InputNumber min={1}/>
                        </Form.Item>
                      </Col>
                    </Row>

                    <Row gutter={24}>
                      <Col span={24}>
                        <Form.Item name="note">
                          <Input.TextArea
                            autoSize={{ minRows: 3, maxRows: 3 }}
                            maxLength={100}
                            placeholder="Ghi chú"
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                  </Tabs.TabPane>
                  <Tabs.TabPane
                    style={{ height: "100%" }}
                    key="2"
                    tab="Tuỳ chọn"
                  >
                    {itemBundle?.options && itemBundle?.options.length > 0 ?
                    <List className="mb-3">
                      {itemBundle?.options.map((option, index) =>
                      <List.Item key={index} >
                        <Row justify="space-between" align="middle" style={{ width: "100%" }}>
                          <Col span={18}>
                            <Typography.Text style={{ float: "left" }}>
                              {option.option.name} (+{currencyFormat(option.price)})
                            </Typography.Text>
                          </Col>
                          <Col span={6}>
                            <InputNumber
                              style={{ width: "100%", float: "right" }}
                              defaultValue={0}
                              min={0}
                              value={options.find(optionForm => optionForm.optionid === option.optionid)?.optionquantity}
                              onChange={(value) => handleChangeOptionQuantity(value, option.optionid)}
                            />
                          </Col>
                        </Row>
                      </List.Item>
                      )}
                    </List>
                    :
                    <Empty 
                      className="mt-3"
                      description="Không có tùy chọn nào"
                    />
                    }
                  </Tabs.TabPane>
                </Tabs>
                
                <Row justify="center" style={{ height: "100%" }} >
                  <Button 
                    type="primary" 
                    htmlType="submit" 
                    style={{ position: "absolute", bottom: "0" }}
                    loading={loading}
                  >
                    Xác nhận
                  </Button>
                </Row>
              </Form>
            </Col>
          </Row>
        </PerfectScrollbar>
      }
    >
      <Card
        onBlur={() => togglePopover(!visible)}
        className={className}
        style={{ borderRadius: "20px" }}
        hoverable
        onClick={(e) => {
          handleCardSelect(itemBundle?.item.id);
        }}
      >
        <Col style={{ ...CSSStyle, textAlign: "center" }}>
          <Avatar 
            style={{ width: 64, height: 64 }} 
            src={(itemBundle?.item.imgpath === null || itemBundle?.item.imgpath === "") ? "/item-sample.svg" : itemBundle?.item.imgpath}
          />
          <Typography.Title
            ellipsis={{ rows: 1, expandable: false }}
            className="mt-3"
            level={5}
          >
            {itemBundle?.item.name}
          </Typography.Title>
          <Typography.Text>
            {currencyFormat(itemBundle?.price)} / {itemBundle?.item.unit}
          </Typography.Text>
          <Row justify="center" className="mt-3">
            <Tag color={colorDependOnQuantity(itemBundle?.branchitem?.currentquantity)}>
              Trong kho: {itemBundle?.branchitem?.currentquantity} {itemBundle?.item.unit}
            </Tag>
          </Row>
        </Col>
      </Card>
    </Popover>
  );
});

export default OrderMenuItem;
