import React, { useState } from "react";
import {
    Button,
    Form,
    Row,
    Input,
    Select,
    Col,
    Checkbox,
} from "antd";
import { 
    CModal,
    CCard,
    CCardBody,
} from "@coreui/react";
import "./AddNewOrderModal.scss";

export const tempOrderTypeData = [
    {
        id: 1,
        code: "TAKEAWAY",
        text: "Mang đi",
    },
    {
        id: 2,
        code: "ATPLACE",
        text: "Thanh toán tại quầy",
    }
]
  
type AddNewOderProps = {
    openAddOrderModal: boolean;
};
  
type AddNewOderDispatch = {
    handleAddOrder: Function;
    handleCloseAddOrderModal: Function;
};
  
type AddNewOderDefaultProps = AddNewOderProps & AddNewOderDispatch;
  
const MainOrderPanel: React.FC<AddNewOderDefaultProps> = React.memo((props) => {
    const { openAddOrderModal, handleAddOrder, handleCloseAddOrderModal } = props;
  
    const [loading, setLoading] = useState(false);
    const [isNextCurrent, setIsNextCurrent] = useState(true);
    const [form] = Form.useForm();

    const handleAddNewOrder = async (value: any) => {
        setLoading(true);
        await handleAddOrder({ ...value, isNextCurrent });
        setLoading(false)
        setIsNextCurrent(true);
        form.resetFields();
        handleCloseAddOrderModal();
    }
      
    return (
        <CModal
            centered
            backdrop
            width={200}
            onClose={handleCloseAddOrderModal}
            closeOnBackdrop
            show={openAddOrderModal}
            className="AddOderModal-styles"
        >
            <CCard className="align-middle justify-content-center jus AddOderModal-card-styles">
                <CCardBody className="AddOderModal-cardBody-styles">
                    <h4 className="AddOderModal-h2-styles">Tạo đơn hàng mới</h4>
                    <Form form={form} onFinish={handleAddNewOrder} className="mt-5">
                        <Row>
                            <Col span={24}>
                                <Form.Item
                                    name="ordertypeid"
                                    initialValue={tempOrderTypeData[0].id}
                                >
                                    <Select>
                                        {tempOrderTypeData.map((orderType, index) => 
                                            <Select.Option key={index} value={orderType.id}>{orderType.text}</Select.Option>
                                        )}
                                    </Select>
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                <Form.Item
                                    name="note"
                                >
                                    <Input.TextArea
                                        autoSize={{ minRows: 3, maxRows: 3 }}
                                        maxLength={100}
                                        placeholder="Ghi chú"
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row className="mb-5">
                            <Col span={24}>
                                <Checkbox 
                                    checked={isNextCurrent} 
                                    onClick={() => setIsNextCurrent(!isNextCurrent)}
                                >
                                    Chuyển qua đơn hàng sau khi tạo
                                    </Checkbox>
                            </Col>
                        </Row>

                        <Button
                            loading={loading}
                            type="primary"
                            htmlType="submit"
                        >
                            Tạo đơn hàng
                        </Button>
                    </Form>
                </CCardBody>
            </CCard>
        </CModal>
    );
})
  
export default MainOrderPanel;
