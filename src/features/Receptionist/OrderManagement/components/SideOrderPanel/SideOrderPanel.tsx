import { Col, Empty, Input, Row, Select, Spin, Switch, Tabs, Typography } from "antd";
import React, { useEffect, useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import OrderMenuItem from "../OrderMenuItem/OrderMenuItem";
import { ResponseMenu } from '../../OrderManagement.slice';
import "./SideOrderPanel.scss";
import { Branch, ItemType } from "@custom-types";
import { countItemType } from "../../../../SAC/MenuManagement/components/MainMenuPanel/MainMenuPanel";

type SideBillPanelProps = {
  changeBranchLoading: boolean;
  currentItemId: number | undefined;
  currentBranchId: number;
  mainMenu: ResponseMenu | undefined;
  branchList: Branch[];
  itemTypeList: ItemType[];
};

type SideBillPanelDispatch = {
  handleAddLineToCurrentOrder: Function;
  handleChangeBranchId: Function;
  handleSelectItem: Function;
};

type SideBillPanelDefaultProps = SideBillPanelProps & SideBillPanelDispatch;

const SidePanel: React.FC<SideBillPanelDefaultProps> = React.memo((props) => {
  const { 
    changeBranchLoading,
    mainMenu, 
    currentItemId, 
    currentBranchId, 
    branchList, 
    itemTypeList,
  } = props;

  const {
    handleAddLineToCurrentOrder,
    handleChangeBranchId,
    handleSelectItem 
  } = props;

  const [loading, setLoading] = useState<boolean>(true);
  const [displayTabMode, setDisplayTabMode] = useState<boolean>(true);
  const [filtered, setFiltered] = useState(mainMenu?.items);

  useEffect(() => {
    if (mainMenu) {
      setLoading(false);
      setFiltered(mainMenu.items);
    } else {
      setLoading(true);
    }
  }, [mainMenu]);

  const handleSearch = (value: string) => {
    if (!value) {
      setFiltered(mainMenu?.items);
    } else {
      const temp = mainMenu?.items.filter((itemBundle) =>
        itemBundle?.item.name.toLocaleLowerCase().includes(value.toLowerCase())
      );
      setFiltered(temp);
    }
  };

  return (
    <>
      <Row justify="space-between" align="middle" gutter={24}>
        <Col>
          <Row>
            <Input
              onChange={(e) => {
                handleSearch(e.target.value);
              }}
              className="mb-3"
              placeholder="Tìm kiếm món ăn"
            />
          </Row>
        </Col>
        <Col>
          <Row justify="end" align="middle">
            <Select
              className="mb-3"
              loading={changeBranchLoading}
              value={currentBranchId}
              onChange={(e) => handleChangeBranchId(e)}
              disabled={currentBranchId === -1 ? true : false}
            >
              <Select.Option value={-1} hidden>Không có chi nhánh nào</Select.Option>
              {branchList?.map((branch, index) => (
                <Select.Option key={index} value={branch.id}>{branch.code} | {branch.name}</Select.Option>
              ))}
            </Select> 
          </Row>
        </Col>
        <Col>
          <Row justify="end" align="middle">
            <Switch
              unCheckedChildren="Tab"
              checkedChildren="Tab"
              checked={displayTabMode}
              onChange={() => setDisplayTabMode(!displayTabMode)}
            />
          </Row>
        </Col>
      </Row>

      <Row
        style={{ height: "100%", padding: "0px", margin: 0 }}
        justify="center"
      >
        <Col span={24}>
          {loading ? (
            <Row style={{ height: "100%" }} justify="center">
              <Spin
                size="large"
                style={{ textAlign: "center", margin: "auto" }}
                spinning={loading}
              />
            </Row>
          ) : (
            <>
              {filtered && filtered.length > 0 ? (
                <>
                  <Scrollbars>
                    {!displayTabMode ? 
                    <Row
                      className="mt-3 mb-5"
                      justify={filtered?.length > 0 ? "start" : "center"}
                      style={{ margin: 0 }}
                      gutter={[24, 24]}
                    >
                      {countItemType(itemTypeList, filtered?.map(itemBundle => {
                          return itemBundle?.item;
                        }))?.map((itemType, index) => (
                        <Col span={24} key={index}>
                          <Row justify="start" className="mb-3">
                            <Typography.Title level={3}>{itemType.name}</Typography.Title>
                          </Row>
                          <Row gutter={[24, 24]}>
                            {filtered?.length > 0 ? (
                              filtered?.filter((itemBundle) => itemBundle?.item.typeid === itemType.id).map((itemBundle, itemBundleIndex) => (
                                <Col key={itemBundleIndex} span={8}>
                                  <OrderMenuItem
                                      currentItemId={currentItemId}
                                      handleSelectItem={handleSelectItem}
                                      handleAddLineToCurrentOrder={handleAddLineToCurrentOrder}
                                      itemBundle={itemBundle}
                                      className={
                                        currentItemId === itemBundle?.item.id
                                          ? "active-item-from-list"
                                          : ""
                                      }
                                    />
                                </Col>
                              ))
                            ) : (
                              <Col>
                                <Empty description="Không có món nào!" />
                              </Col>
                            )}
                          </Row>
                        </Col>
                      ))}
                    </Row>
                    :
                    (<Row justify="center">
                      <Tabs centered defaultActiveKey="1" style={{ width: '100%' }}>
                        <Tabs.TabPane tab="Tất cả" key="1">
                          <Row
                            className="mt-3 mb-5"
                            justify={filtered?.length > 0 ? "start" : "center"}
                            style={{ margin: 0 }}
                            gutter={[24, 24]}
                          >
                            {filtered?.length > 0 ? (
                              filtered?.map((itemBundle, index) => (
                                <Col key={index} span={8}>
                                  <OrderMenuItem
                                    currentItemId={currentItemId}
                                    handleSelectItem={handleSelectItem}
                                    handleAddLineToCurrentOrder={handleAddLineToCurrentOrder}
                                    itemBundle={itemBundle}
                                    className={
                                      currentItemId === itemBundle?.item.id
                                        ? "active-item-from-list"
                                        : ""
                                    }
                                  />
                                </Col>
                              ))
                            ) : (
                              <Col>
                                <Empty description="Không có món nào!" />
                              </Col>
                            )}
                          </Row>
                        </Tabs.TabPane>
                        {countItemType(itemTypeList, filtered?.map(itemBundle => {
                          return itemBundle?.item;
                        }))?.map((itemType, index) => (
                          <Tabs.TabPane tab={itemType.name} key={(index + 2).toString()}>
                            <Row
                              className="mt-3 mb-5"
                              justify={filtered?.length > 0 ? "start" : "center"}
                              style={{ margin: 0 }}
                              gutter={[24, 24]}
                            >
                              {filtered?.length > 0 ? (
                                filtered?.filter((itemBundle) => itemBundle?.item.typeid === itemType.id).map((itemBundle, itemBundleIndex) => (
                                  <Col key={itemBundleIndex} span={8}>
                                    <OrderMenuItem
                                      currentItemId={currentItemId}
                                      handleSelectItem={handleSelectItem}
                                      handleAddLineToCurrentOrder={handleAddLineToCurrentOrder}
                                      itemBundle={itemBundle}
                                      className={
                                        currentItemId === itemBundle?.item.id
                                          ? "active-item-from-list"
                                          : ""
                                      }
                                    />
                                  </Col>
                                ))
                              ) : (
                                <Col>
                                  <Empty description="Không có món nào!" />
                                </Col>
                              )}
                            </Row>
                          </Tabs.TabPane>
                        ))}
                      </Tabs>
                    </Row>)}
                  </Scrollbars>
                </>
              ) : (
                <Col span={24}>
                  <Empty
                    style={{ marginTop: "30vh" }}
                    description="Hiện chưa có món nào!"
                  />
                </Col>
              )}
            </>
          )}
        </Col>
      </Row>
    </>
  );
});

export default SidePanel;
