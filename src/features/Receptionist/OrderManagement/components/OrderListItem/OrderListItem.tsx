import React, { useEffect, useState } from "react";
import { Row, Col, Typography, List, InputNumber, Tag, Collapse, Form, Radio, Input, Avatar, Empty } from "antd";
import { CaretRightOutlined, DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Line, ResponseItem } from "../../OrderManagement.slice";
import { currencyFormat } from "../../../../../shared/utils/stringUtils";
import "./OrderListItem.scss";
import ConfirmDialog from "../../../../../shared/components/dialog/modal/modal";
import UpdateLineModal from "../../../../../shared/components/dialog/modal/modal";

type OrderListItemProps = {
  activeKey: string | undefined;
  line: Line;
  itemBundle: ResponseItem | null | undefined;
  handleDeleteLineOfCurrentOrder: Function;
  handleUpdateLineOfCurrentOrder: Function;
};

const OrderListItem: React.FC<OrderListItemProps> = React.memo((props) => {
  const { line, activeKey, itemBundle } = props;
  const { 
    handleDeleteLineOfCurrentOrder,
    handleUpdateLineOfCurrentOrder,
  } = props;

  const [form] = Form.useForm();
  const [key, setKey] = useState<string | undefined>(undefined);
  const [openConfirmDeleteLine, setOpenConfirmDeleteLine] = useState<boolean>(false);
  const [openUpdateLine, setOpenUpdateLine] = useState<boolean>(false);
  const [options, setOptions] = useState<{
    optionid: number,
    optionquantity: number,
    isDefault: boolean,
  }[]>([]);

  useEffect(() => {
    setKey(activeKey);
  }, [activeKey]);

  useEffect(() => {
    if (openUpdateLine && itemBundle) {
      form.setFieldsValue({
        size: line.size.optionid,
        note: line.note,
        quantity: line.itemquantity,
      });
      setOptions(itemBundle?.options.map(option => {
        const quantity = line.options.find(optionInLine => optionInLine.optionid === option.optionid);
        return { 
          optionid: option.optionid, 
          optionquantity: quantity !== undefined ? quantity.optionquantity : 0,
          isDefault: quantity !== undefined ? true : false,
        }
      }))
    }
  }, [openUpdateLine, itemBundle, form, line]);

  const handleUpdateLine = async (value: any) => {
    const updateLine = {
      line: line.line,
      itemid: line.itemid,
      itemquantity: value.quantity,
      note: value.note,
      options: options.filter(option => option.optionquantity !== 0 || option.isDefault),
    } as any;
    if (value.size) updateLine.size = {
      optionid: value.size,
    }
    await handleUpdateLineOfCurrentOrder(updateLine);
  }

  const handleChangeOptionQuantity = (value: any, optionId: number) => {
    if (Number.isInteger(value)) {
      const tempOptions = [ ...options ];
      const index = tempOptions.findIndex(option => option.optionid === optionId);
      if (index > -1) {
        tempOptions[index].optionquantity = value;
        setOptions(tempOptions);
      }
    }
  }

  const optionCollapseHeader = (
    <Row>
      <Col span={23}>
        <Row justify="space-between" align="middle" style={{ padding: 0 }}>
          <Col span={14}>
            <Row gutter={8}>
              <Col>
                <Row>
                  <Typography.Title style={{ cursor: "pointer" }} level={5}>
                    {line.item.name}{"\xa0"}
                  </Typography.Title>
                  <Typography.Text>
                    ({currencyFormat(+line.itemprice + +line.size?.optiontotal)}/{line.item.unit})
                  </Typography.Text>
                </Row>
              </Col>
              <Col>
                <Tag color="success">{line.size?.option.name}</Tag>
              </Col>
            </Row>
            <Typography.Text type={line.note ? "secondary" : "warning"}>{line.note ? line.note : "Không có ghi chú"}</Typography.Text>
          </Col>
          <Col span={3}>
            <Row justify="center">
              <Typography.Title level={5}>Số lượng</Typography.Title>
            </Row>
            <Row justify="center">
              <Typography.Text>{line.itemquantity}</Typography.Text>
            </Row>
          </Col>
          <Col span={7}>
            <Row justify="center">
              <Typography.Title level={5}>Thành tiền</Typography.Title>
            </Row>
            <Row justify="center">
              <Typography.Text style={{ float: "right" }}>
                {currencyFormat(line.total)}
              </Typography.Text>
            </Row>
          </Col>
        </Row>
      </Col>
      
      <Col span={1}>
        <Row justify="space-between" style={{ height: "100%" }}>
          <EditOutlined
            style={{ color: "#f0e924" }}
            onClick={async event => {
              setOpenUpdateLine(true);
              event.stopPropagation();
            }}
          />
          <DeleteOutlined
            style={{ color: "#f00a19" }}
            onClick={async event => {
              setOpenConfirmDeleteLine(true);
              event.stopPropagation();
            }}
          />
        </Row>
      </Col>
    </Row>
  )

  const confirmDialogContent = (
    <Row justify="center">
      <Typography.Title level={5}>
        Bạn có muốn xóa món ăn này ra khỏi đơn hàng ?
      </Typography.Title>
    </Row>
  )

  const updateLineModalContent = (
    <Row justify="center">
      <Col span={24}>
        <Avatar
          style={{ width: 64, height: 64 }}
          src={(itemBundle?.item.imgpath === null || itemBundle?.item.imgpath === "") ? "/item-sample.svg" : itemBundle?.item.imgpath}
        />
        <Row 
          justify="center"
          className="mt-3"
        >
          <Typography.Title
            ellipsis={{ rows: 1, expandable: false }}
            level={5}
          >
            {itemBundle?.item.name}{"\xa0"}
          </Typography.Title>
        </Row>
        <Row justify="center" align="top">
          <Typography.Text>
            ({currencyFormat(line.itemprice)}/{line.item.unit})
          </Typography.Text>
        </Row>
        <Form 
          form={form}
          style={{ height: "100%", marginTop: "20px" }}
          onFinish={handleUpdateLine}
        >
          {itemBundle?.sizes && itemBundle?.sizes.length > 0 ?
            <Form.Item
              name="size"
              rules={[
                {
                  required: true,
                  message: "Vui lòng chọn kích cỡ!",
                },
              ]}
              initialValue={itemBundle.sizes[0].optionid}
            >
              <Radio.Group>
                {itemBundle.sizes.map((size, index) => 
                  <Radio 
                    key={index} 
                    value={size.optionid}
                    style={{ marginLeft: "4px", marginRight: "4px" }}
                  >
                    {size.option.name} (+{currencyFormat(size.price)})
                  </Radio>
                )}
              </Radio.Group>
            </Form.Item> 
            :
            <></>
          }

          <Row justify="space-between">
            <Col span={14}>
              <Form.Item name="promotion">
                <Input maxLength={100} placeholder="Mã khuyến mãi" />
              </Form.Item>
            </Col>
            <Col span={10}>
              <Row justify="end">
                <Form.Item
                  name="quantity"
                  rules={[
                    {
                      required: true,
                      message: "Vui lòng nhập số lượng!",
                    },
                  ]}
                  initialValue={1}
                >
                  <InputNumber min={1}/>
                </Form.Item>
              </Row>
            </Col>
          </Row>

          <Row gutter={24}>
            <Col span={24}>
              <Form.Item name="note">
                <Input.TextArea
                  autoSize={{ minRows: 3, maxRows: 3 }}
                  maxLength={100}
                  placeholder="Ghi chú"
                />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Col>
      <Col span={24}>
        <Typography.Title level={4} >Tùy chọn</Typography.Title>
      </Col>
      {itemBundle?.options && itemBundle?.options.length > 0 ?
        <List className="mb-3">
          {itemBundle?.options.map((option, index) =>
          <List.Item key={index} >
            <Row justify="space-between" align="middle" style={{ width: "100%" }}>
              <Col span={18}>
                <Typography.Text style={{ float: "left" }}>
                  {option.option.name} (+{currencyFormat(option.price)})
                </Typography.Text>
              </Col>
              <Col span={6}>
                <InputNumber
                  style={{ width: "100%", float: "right" }}
                  defaultValue={0}
                  min={0}
                  value={options.find(optionForm => optionForm.optionid === option.optionid)?.optionquantity}
                  onChange={(value) => handleChangeOptionQuantity(value, option.optionid)}
                />
              </Col>
            </Row>
          </List.Item>
          )}
        </List>
        :
        <Empty 
          className="mt-3"
          description="Không có tùy chọn nào"
        />
      }
    </Row>
  )
  
  return (
    <List.Item 
      className="order-list-item"
      style={{ 
        padding: "10px 0 10px 0"
      }}  
    >
      <ConfirmDialog
        content={confirmDialogContent}
        title="Xác nhận xóa"
        show={openConfirmDeleteLine}
        onClose={() => setOpenConfirmDeleteLine(false)}
        onChooseAsync={async (result) => {
          if (result) await handleDeleteLineOfCurrentOrder(line.line);
        }}
      />
      <UpdateLineModal
        content={updateLineModalContent}
        title="Cập nhập món ăn"
        width={300}
        show={openUpdateLine}
        confirmButtonName="Lưu"
        onClose={() => setOpenUpdateLine(false)}
        onChooseAsync={async (result) => {
          if (result) await handleUpdateLine(form.getFieldsValue());
        }}
      />
      <Row justify="space-between" style={{ width: "100%" }}>
        <Col span={24}>
          <Row justify="space-between" align="middle">
            <Col span={24}>
              <Collapse 
                activeKey={key}
                onChange={(key) => key instanceof Array ? setKey(key[0]) : setKey(key)}
                style={{ padding: 0 }}
                expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
                ghost
              >
                <Collapse.Panel
                  key={1} 
                  header={optionCollapseHeader}
                  showArrow={false}
                >
                  <Row>
                    <Col span={23}>
                      {line.options.length > 0 ? 
                      line.options.map((option, index) => 
                        <Row justify="space-between" key={index} className="mt-2">
                          <Col span={14}>
                            <Tag color="gold">
                              <Typography.Text>{option.option.name}</Typography.Text>
                            </Tag>
                          </Col>
                          <Col span={3}>
                            <InputNumber value={option.optionquantity} disabled/>
                          </Col>
                          <Col span={7}>
                            <Row justify="center">
                              <Typography>+ {currencyFormat(option.optiontotal)}/{line.item.unit}</Typography>
                            </Row>
                          </Col>
                        </Row>
                      )
                      : 
                      <Typography.Text type="secondary">Không có tùy chọn nào</Typography.Text>}
                    </Col>
                  </Row>
                </Collapse.Panel>
              </Collapse>
            </Col>
          </Row>
        </Col>
      </Row>
    </List.Item>
  );
});

export default OrderListItem;
