import { currencyFormat } from "@utils";
import { Col, Row, Statistic, Divider, Button } from "antd";
import React from "react";
import { ResponseOrder } from "../../OrderManagement.slice";
import "./BottomBar.scss";

type BottomBarProps = {
  currentOrder: ResponseOrder | undefined;
};

type BottomBarDispatch = {
  handleDeleteOrder: Function
};

type BottomBarDefaultProps = BottomBarProps & BottomBarDispatch;

const BottomBar: React.FC<BottomBarDefaultProps> = React.memo((props) => {
  const { 
    currentOrder, 
    handleDeleteOrder,
  } = props;

  return (
    <Row
      className="bottom-bar pt-3 "
      gutter={24}
      justify="space-between"
      align="middle"
    >
      <Col span={6}>
        <Statistic title="Tổng cộng" value={currencyFormat(currentOrder?.ordertotal)} />
      </Col>
      <Col span={1}>
        <Divider type="vertical" style={{ height: "90%" }} />
      </Col>
      <Col span={6}>
        <Statistic title="Khuyến mãi" value={0} />
      </Col>
      <Col span={1}>
        <Divider type="vertical" style={{ height: "90%" }} />
      </Col>
      <Col span={9}>
        <Row justify="end">
          <Col span={12}>
            <Button 
              style={{ width: "100%" }} 
              type="link" 
              danger
              onClick={() => handleDeleteOrder(undefined)}
            >
              Huỷ đơn
            </Button>
          </Col>
          <Col span={12}>
            <Button type="primary">
              Thanh toán
            </Button>
          </Col>
        </Row>
      </Col>
    </Row>
  );
});

export default BottomBar;
