import React from "react";
import { List, Row, Col, Typography, Tag } from "antd";
import { EditOutlined, CloseOutlined } from "@ant-design/icons";
import "./SavedOrder.scss";
import { Order } from "@custom-types";
import { currencyFormat, fullDateFormat } from "@utils/stringUtils";

type SavedOrderProps = {
  isSelected: boolean;
  order: Order;
  handleChangeCurrentOrder: Function;
  handleSetDeleteOrderId: Function;
  handleSetUpdateOrderId: Function;
};

const tempOrderTypeData = [
  {
    id: 1,
    text: "Mang đi",
    color: "purple",
  },
  {
    id: 2,
    text: "Tại quán",
    color: "blue",
  }
]

const orderStatusDisplay = [
  {
    statuscode: "EDITING",
    text: "Đang chỉnh sửa",
    color: "gold",
  },
  {
    statuscode: "PAID",
    text: "Đã thanh toán",
    color: "lime",
  },
]

const SavedOrder: React.FC<SavedOrderProps> = React.memo((props) => {
  const { order, handleChangeCurrentOrder, isSelected, handleSetDeleteOrderId, handleSetUpdateOrderId } = props;

  return (
    <List.Item
      className={isSelected ? "saved-order-list-item selected-item" : "saved-order-list-item"}
      style={{ cursor: "pointer" }}
      actions={[
        <EditOutlined 
          style={{ cursor: "pointer" }} 
          onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            handleSetUpdateOrderId(order)
          }}
        />,
        <CloseOutlined 
          style={{ cursor: "pointer" }} 
          onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            handleSetDeleteOrderId(order.id);
          }}
        />,
      ]}
      onClick={() => handleChangeCurrentOrder(order.id)}
    >
      <Row justify="space-between" style={{ width: "100%" }}>
        <Col span={7}>
          <Row justify="start" align="middle" gutter={8}>
            <Col>
              <Typography.Title level={5} style={{ marginBottom: 0 }}>{order.id} - {order.tableid === null ? "Chưa có bàn" : `Bàn số ${order.tableid}` }</Typography.Title>
            </Col>
            <Col>
              <Tag color={tempOrderTypeData.find(data => data.id === order.ordertypeid)?.color}>
                {tempOrderTypeData.find(data => data.id === order.ordertypeid)?.text}
              </Tag>
            </Col>
          </Row>
          <Row justify="start" align="middle">
            <Typography.Text type={order.note ? "secondary" : "warning"}>{order.note ? order.note : "Không có ghi chú"}</Typography.Text>
          </Row>
        </Col>
        <Col span={7}>
          <Row justify="center" align="middle" style={{ height: "100%" }}>
            <Typography.Text disabled>{fullDateFormat(order.createdat)}</Typography.Text>
          </Row>
        </Col>
        <Col span={6}>
          <Row justify="center" align="middle" style={{ height: "100%" }}>
            <Typography.Text>{currencyFormat(order.total)}</Typography.Text>
          </Row>
        </Col>
        <Col span={4}>
          <Row justify="center" align="middle" style={{ height: "100%" }}>
            <Tag 
              color={orderStatusDisplay.find(data => data.statuscode === order.statuscode)?.color}
              style={{ marginRight: 0 }}
            >
              {orderStatusDisplay.find(data => data.statuscode === order.statuscode)?.text}
            </Tag>
          </Row>
        </Col>
      </Row>
    </List.Item>
  );
});

export default SavedOrder;
