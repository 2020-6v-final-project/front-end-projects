import { CFade } from "@coreui/react";
import { Branch, ItemType, Order } from "@custom-types";
import { Col, Row } from "antd";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { RootState } from "src/store";
import MainOrderPanel from "./components/MainOrderPanel/MainOrderPanel";
import SideOrderPanel from "./components/SideOrderPanel/SideOrderPanel";
import { 
  fetchCurrentOrderAsync,
  fetchMainMenuAsync,
  fetchOrderListAsync,
  addLineToCurrentOrderAsync,
  addOrderAsync,
  deleteOrderAsync,
  updateOrderAsync,
  deleteLineOfCurrentOrderAsync,
  updateLineOfCurrentOrderAsync,
} from './OrderManagement.slice';
import { 
  ResponseMenu,
  ResponseOrder,
} from './OrderManagement.slice';
import {
  mainMenuSelector,
  currenOrderSelector,
  orderListSelector,
} from './OrderManagement.selector';
import { selectBranchOfReceptionistUserList } from '../../LoginPage/LoginPage.selector';
import { selectItemTypeList } from '../../SAC/ItemIngredientTypeManagement/ItemIngredientTypeManagement.selector';
import { getItemTypeAsync } from '../../SAC/ItemIngredientTypeManagement/ItemIngredientTypeManagement.slice';
import swal from "sweetalert";

type OrderManagementState = {
  mainMenu: ResponseMenu | undefined;
  currentOrder: ResponseOrder | undefined;
  orderList: Order[];
  branchList: Branch[];
  itemTypeList: ItemType[];
};

type OrderManagementDispatch = {
  fetchCurrentOrderAsync: Function;
  fetchMainMenuAsync: Function;
  fetchOrderListAsync: Function;
  addLineToCurrentOrderAsync: Function;
  addOrderAsync: Function;
  getItemTypeAsync: Function;
  deleteOrderAsync: Function;
  updateOrderAsync: Function;
  deleteLineOfCurrentOrderAsync: Function;
  updateLineOfCurrentOrderAsync: Function;
};

export type OrderManagementDefaultProps = OrderManagementState & OrderManagementDispatch;

const OrderManagementContainer: React.FC<OrderManagementDefaultProps> = React.memo((props) => {
    const {
      mainMenu,
      currentOrder,
      orderList,
      branchList,
      itemTypeList,
    } = props;

    const {
      fetchCurrentOrderAsync,
      fetchMainMenuAsync,
      fetchOrderListAsync,
      addLineToCurrentOrderAsync,
      addOrderAsync,
      deleteOrderAsync,
      getItemTypeAsync,
      updateOrderAsync,
      deleteLineOfCurrentOrderAsync,
      updateLineOfCurrentOrderAsync,
    } = props;

    const [loading, setLoading] = useState<{
      changeBranchAction: boolean,
      changeCurrentOrderAction: boolean,
    }>({
      changeBranchAction: false,
      changeCurrentOrderAction: false,
    });
    const [currentBranchId, setCurrentBranchId] = useState<number>(-1);
    const [currentItemId, setCurrenItemId] = useState<number | undefined>(undefined);

    useEffect(() => {
      const fetchData = async () => {
        if (branchList && branchList.length > 0) {
          await getItemTypeAsync();
          fetchMainMenuAsync(branchList[0]?.id);
          fetchOrderListAsync(branchList[0]?.id);
          setCurrentBranchId(branchList[0]?.id);
        }
      }
      fetchData();
    }, [fetchMainMenuAsync, fetchOrderListAsync, getItemTypeAsync, branchList]);

    // Orders
    const handleAddOrder = async (order: any) => {
      await addOrderAsync(currentBranchId, order);
    };

    const handleUpdateOrder = async (orderId: number, order: any) => {
      await updateOrderAsync(currentBranchId, orderId, order);
    }

    const handleDeleteOrder = async (orderId?: number) => {
      if (orderId === undefined) await deleteOrderAsync(currentBranchId, currentOrder?.id, true);
      else await deleteOrderAsync(currentBranchId, orderId, false);
    }

    // Current Order
    const handleChangeCurrentOrder = async (orderId: number) => {
      setLoading({ ...loading, changeCurrentOrderAction: true });
      await fetchCurrentOrderAsync(currentBranchId, orderId);
      setLoading({ ...loading, changeCurrentOrderAction: false });
    }

    const handleAddLineToCurrentOrder = async (line: any) => {
      if (currentOrder) {
        await addLineToCurrentOrderAsync(currentBranchId, currentOrder?.id, line);
      } else {
        swal("Thông báo", "Chưa có đơn hàng để thêm vào", "warning");
      }
    }

    const handleUpdateLineToOfCurrentOrder = async (line: any) => {
      await updateLineOfCurrentOrderAsync(currentBranchId, currentOrder?.id, line);
    }

    const handleDeleteLineToOfCurrentOrder = async (lineId: number) => {
      await deleteLineOfCurrentOrderAsync(currentBranchId, currentOrder?.id, lineId);
    }

    const handleChangeBranchId = async (value: number) => {
      setCurrentBranchId(value);
      setLoading({ ...loading, changeBranchAction: true });
      await fetchMainMenuAsync(value);
      await fetchOrderListAsync(value);
      setLoading({ ...loading, changeBranchAction: false });
    }

    return (
      <CFade>
        <Row justify="space-between" style={{ height: "100vh" }} gutter={24}>
          <Col span={12}>
            <SideOrderPanel
              changeBranchLoading={loading.changeBranchAction}
              currentItemId={currentItemId}
              currentBranchId={currentBranchId}
              branchList={branchList}
              itemTypeList={itemTypeList}
              mainMenu={mainMenu}
              handleAddLineToCurrentOrder={handleAddLineToCurrentOrder}
              handleChangeBranchId={handleChangeBranchId}
              handleSelectItem={(value: number) => setCurrenItemId(value)}
            />
          </Col>
          <Col span={12}>
            <MainOrderPanel
              changeCurrentOrderLoading={loading.changeCurrentOrderAction}
              orderList={orderList}
              mainMenu={mainMenu}
              currentOrder={currentOrder}
              handleChangeCurrentOrder={handleChangeCurrentOrder}
              handleAddOrder={handleAddOrder}
              handleDeleteLineOfCurrentOrder={handleDeleteLineToOfCurrentOrder}
              handleUpdateLineToOfCurrentOrder={handleUpdateLineToOfCurrentOrder}
              handleDeleteOrder={handleDeleteOrder}
              handleUpdateOrder={handleUpdateOrder}
            />
          </Col>
        </Row>
      </CFade>
    );
  }
);

const mapState = (state: RootState) => {
  return {
    mainMenu: mainMenuSelector(state),
    currentOrder: currenOrderSelector(state),
    orderList: orderListSelector(state),
    branchList: selectBranchOfReceptionistUserList(state),
    itemTypeList: selectItemTypeList(state),
  };
};

const mapDispatch = {
  fetchCurrentOrderAsync,
  fetchMainMenuAsync,
  fetchOrderListAsync,
  addLineToCurrentOrderAsync,
  addOrderAsync,
  getItemTypeAsync,
  deleteLineOfCurrentOrderAsync,
  updateLineOfCurrentOrderAsync,
  deleteOrderAsync,
  updateOrderAsync,
};

export default connect(mapState, mapDispatch)(OrderManagementContainer);
