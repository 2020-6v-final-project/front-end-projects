import { 
  Menu, 
  Order, 
  Table, 
  Item, 
  Option, 
  OrderItem, 
  OrderItemOption,
  MenuItem,
  MenuItemOption,
  BranchItem,
} from "@custom-types";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AppThunk } from "src/store";
import {
  getMainMenu,
  getCurrentOrder,
  addLineToCurrentOrder,
  getOrderList,
  addOrder,
  deleteOrder,
  updateOrder,
  updateLineOfCurrentOrder,
  deleteLineOfCurrentOrder,
} from '@apis';
import swal from "sweetalert";

// Types and state declaration
export type Line = OrderItem & {
  item: Item;
  size: (OrderItemOption & {
    option: Option
  });
  options: (OrderItemOption & {
    option: Option
  })[];
}

export type ResponseOrder = Order & {
  lines: Line[],
};

export type ResponseItem = MenuItem & {
  item: Item;
  branchitem: BranchItem;
  sizes: (MenuItemOption & {
    option: Option;
  })[];
  options: (MenuItemOption & {
    option: Option;
  })[];
};

export type ResponseMenu = Menu & {
  items: (ResponseItem | null)[],
};

type OrderManagementState = {
  currentOrder: ResponseOrder | undefined,
  mainMenu: ResponseMenu | undefined,
  orderList: Order[];
  tableList: Table[];
};

const initialState: OrderManagementState = {
  currentOrder: undefined,
  mainMenu: undefined,
  orderList: [],
  tableList: [],
};

// Slice
const OrderManagementSlice = createSlice({
  name: "orderManagement",
  initialState,
  reducers: {
    // Main Menu
    fetchMainMenuSuccess: (state, action: PayloadAction<ResponseMenu>) => {
      state.mainMenu = action.payload;
      return state;
    },
    fetchMainMenuFailed: (state) => {
      return state;
    },
    // Current Order
    fetchCurrentOrderSuccess: (state, action: PayloadAction<ResponseOrder | undefined>) => {
      let responseOrder = action.payload;
      responseOrder?.lines.sort((lineA, lineB) => lineA.line - lineB.line);
      state.currentOrder = responseOrder;
      return state;
    },
    fetchCurrentOrderFailed: (state) => {
      return state;
    },
    addLineToCurrentOrderSuccess: (state, action: PayloadAction<ResponseOrder>) => {
      let responseOrder = action.payload;
      responseOrder?.lines.sort((lineA, lineB) => lineA.line - lineB.line);
      state.currentOrder = responseOrder;
      return state;
    },
    addLineToCurrentOrderFailed: (state) => {
      return state;
    },
    updateLineOfCurrentOrderSuccess: (state, action: PayloadAction<ResponseOrder>) => {
      let responseOrder = action.payload;
      responseOrder?.lines.sort((lineA, lineB) => lineA.line - lineB.line);
      state.currentOrder = responseOrder;
      return state;
    },
    updateLineOfCurrentOrderFailed: (state) => {
      return state;
    },
    deleteLineOfCurrentOrderSuccess: (state, action: PayloadAction<ResponseOrder>) => {
      let responseOrder = action.payload;
      responseOrder?.lines.sort((lineA, lineB) => lineA.line - lineB.line);
      state.currentOrder = responseOrder;
      return state;
    },
    deleteLineOfCurrentOrderFailed: (state) => {
      return state;
    },
    // Orders
    fetchOrderListSuccess: (state, action: PayloadAction<Order[]>) => {
      state.orderList = action.payload.sort(((orderA, orderB) => orderA.id - orderB.id));
      return state;
    },
    fetchOrderListFailed: (state) => {
      return state;
    },
    addOrderSuccess: (state, action: PayloadAction<Order>) => {
      state.orderList.push(action.payload);
      state.orderList.sort(((orderA, orderB) => orderA.id - orderB.id));
      return state;
    },
    addOrderFailed: (state) => {
      return state;
    },
    deleteOrderSuccess: (state, action: PayloadAction<Order[]>) => {
      state.orderList = action.payload.sort(((orderA, orderB) => orderA.id - orderB.id));
      return state;
    },
    deleteOrderFailed: (state) => {
      return state;
    },
    updateOrderSuccess: (state, action: PayloadAction<Order>)=> {
      const index = state.orderList.findIndex(order => order.id === action.payload.id);
      if (index > -1) {
        state.orderList.splice(index, 1, action.payload);
      }
      return state;
    },
    updateOrderFailed: (state) => {
      return state;
    },
  }
});

// Main Menu
export const fetchMainMenuAsync = (branchId: number): AppThunk => async (dispatch) => {
  try {
    await getMainMenu(branchId, (data: ResponseMenu) => {
      dispatch(fetchMainMenuSuccess(data));
      dispatch(fetchCurrentOrderSuccess(undefined));
    });
  } catch (e) {
    swal("Lấy thực đơn chính thất bại!", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
    dispatch(fetchMainMenuFailed());
  }
};

// Current Order
export const fetchCurrentOrderAsync = (branchId: number, orderId: number): AppThunk => async (dispatch) => {
  try {
    await getCurrentOrder(branchId, orderId, (data: ResponseOrder) => {
      dispatch(fetchCurrentOrderSuccess(data));
      const order = { ...data } as any;
      delete order.lines;
      dispatch(updateOrderSuccess(order));
    });
  } catch (e) {
    swal("Lấy đơn hàng thất bại!", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
    dispatch(fetchCurrentOrderFailed());
  }
};

export const addLineToCurrentOrderAsync = (branchId: number, orderId: number, line: any): AppThunk => async (dispatch) => {
  try {
    await addLineToCurrentOrder(branchId, orderId, line, (data: ResponseOrder) => {
      dispatch(addLineToCurrentOrderSuccess(data));
      const order = { ...data } as any;
      delete order.lines;
      dispatch(updateOrderSuccess(order));
    })
  } catch (e) {
    swal("Thêm món ăn vào đơn hàng thất bại!", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
    dispatch(addLineToCurrentOrderFailed());
  }
};

export const updateLineOfCurrentOrderAsync = (branchId: number, orderId: number, line: any): AppThunk => async (dispatch) => {
  try {
    await updateLineOfCurrentOrder(branchId, orderId, line, (data: ResponseOrder) => {
      dispatch(updateLineOfCurrentOrderSuccess(data));
      const order = { ...data } as any;
      delete order.lines;
      dispatch(updateOrderSuccess(order));
    })
  } catch (e) {
    swal("Cập nhập món ăn của đơn hàng thất bại!", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
    dispatch(updateLineOfCurrentOrderFailed());
  }
}

export const deleteLineOfCurrentOrderAsync = (branchId: number, orderId: number, lineId: number): AppThunk => async (dispatch) => {
  try {
    await deleteLineOfCurrentOrder(branchId, orderId, lineId, (data: ResponseOrder) => {
      dispatch(deleteLineOfCurrentOrderSuccess(data));
      const order = { ...data } as any;
      delete order.lines;
      dispatch(updateOrderSuccess(order));
    })
  } catch (e) {
    swal("Xóa món ăn khỏi đơn hàng thất bại!", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
    dispatch(deleteLineOfCurrentOrderFailed());
  }
}

// Orders
export const fetchOrderListAsync = (branchId: number): AppThunk => async (dispatch) => {
  try {
    await getOrderList(branchId, (data: Order[]) => {
      dispatch(fetchOrderListSuccess(data));
    });
  } catch (e) {
    swal("Lấy đơn hàng thất bại!", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
    dispatch(fetchOrderListFailed());
  }
};

export const addOrderAsync = (branchId: number, order: any): AppThunk => async (dispatch) => {
  try {
    let isNextCurrentOrder = false;
    if (order.isNextCurrent !== undefined) {
      isNextCurrentOrder = order.isNextCurrent;
      delete order.isNextCurrent;
    }
    await addOrder(branchId, order, async (data: Order) => {
      if (isNextCurrentOrder) {
        await getCurrentOrder(data.branchid, data.id, (currentOrder: ResponseOrder) => {
          dispatch(fetchCurrentOrderSuccess(currentOrder));
        })
      }
      // swal("Thêm đơn hàng thành công", "Đơn hàng đã được thêm vào danh sách", "success");
      dispatch(addOrderSuccess(data));
    })
  } catch (e) {
    swal("Thêm đơn hàng thất bại!", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
    dispatch(addOrderFailed());
  }
};

export const updateOrderAsync = (branchId: number, orderId: number, order: any): AppThunk => async (dispatch) => {
  try {
    await updateOrder(branchId, orderId, order, (data: Order) => {
      dispatch(updateOrderSuccess(data));
    })
  } catch (e) {
    swal("Cập nhập đơn hàng thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
    dispatch(updateOrderFailed());
  }
}

export const deleteOrderAsync = (branchId: number, orderId: number, isCurrent: boolean): AppThunk => async (dispatch) => {
  try {
    await deleteOrder(branchId, orderId, (data: Order[]) => {
      dispatch(deleteOrderSuccess(data));
      if (isCurrent) dispatch(fetchCurrentOrderSuccess(undefined));
    })
  } catch (e) {
    swal("Xóa đơn hàng thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
    dispatch(deleteOrderFailed());
  }
}

// Export part
export const {
  fetchMainMenuSuccess,
  fetchMainMenuFailed,
  fetchCurrentOrderSuccess,
  fetchCurrentOrderFailed,
  addLineToCurrentOrderSuccess,
  addLineToCurrentOrderFailed,
  addOrderSuccess,
  addOrderFailed,
  fetchOrderListSuccess,
  fetchOrderListFailed,
  updateLineOfCurrentOrderSuccess,
  updateLineOfCurrentOrderFailed,
  deleteLineOfCurrentOrderSuccess,
  deleteLineOfCurrentOrderFailed,
  deleteOrderSuccess,
  deleteOrderFailed,
  updateOrderSuccess,
  updateOrderFailed,
} = OrderManagementSlice.actions;

export default OrderManagementSlice.reducer;
