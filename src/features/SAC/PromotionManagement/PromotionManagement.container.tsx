import { FAB } from "@components";
import { CFade } from "@coreui/react";
import { Row } from "antd";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { RootState } from "src/store";
import AddPromotionModal from "./components/AddPromotionModal/AddPromotionModal";
import PromotionTable from "./components/PromotionTable/PromotionTable";
import { 
  Promotion,
  Item,
  ItemType,
} from "@custom-types";
import { selectItemList } from "../../SAC/ItemManagement/ItemManagement.selector";
import { getItemAsync } from "../../SAC/ItemManagement/ItemManagement.slice";
import { selectItemTypeList } from "../../SAC/ItemIngredientTypeManagement/ItemIngredientTypeManagement.selector";
import { getItemTypeAsync } from "../../SAC/ItemIngredientTypeManagement/ItemIngredientTypeManagement.slice";
import { selectPromotionList } from "./PromotionManagement.selector";
import {
  fetchPromotionListAsync,
  addPromotionAsync,
  updatePromotionAsync,
  deactivatePromotionAsync,
  reactivatePromotionAsync,
} from "./PromotionManagement.slice";

type PromotionManagementState = {
  promotionList: Promotion[];
  itemList: Item[];
  itemTypeList: ItemType[];
};

type PromotionManagementDispatch = {
  fetchPromotionListAsync: Function;
  addPromotionAsync: Function;
  updatePromotionAsync: Function;
  deactivatePromotionAsync: Function;
  reactivatePromotionAsync: Function;
  getItemAsync: Function;
  getItemTypeAsync: Function;
};

type PromotionManagementDefaultProps = PromotionManagementState & PromotionManagementDispatch;

const PromotionManagementContainer: React.FC<PromotionManagementDefaultProps> = React.memo(
  (props) => {
    const { 
      promotionList,
      itemList,
      itemTypeList,
    } = props;

    const {
      fetchPromotionListAsync,
      addPromotionAsync,
      updatePromotionAsync,
      deactivatePromotionAsync,
      reactivatePromotionAsync,
      getItemAsync,
      getItemTypeAsync,
    } = props;

    const [loading, setLoading] = useState<boolean>(true);
    const [openAddPromotionModal, setOpenAddPromotionModal] = useState(false);

    useEffect(() => {
      const fetchData = async () => {
        setLoading(true);
        await Promise.all([
          getItemTypeAsync(),
          getItemAsync(),
          fetchPromotionListAsync(),
        ]);
        setLoading(false);
      }
      fetchData();
    }, [getItemTypeAsync, getItemAsync, fetchPromotionListAsync]);

    const handleReactivateOrDeactivate = async (statusCode: string, promotionId: number) => {
      if (statusCode === "AVAILABLE") await deactivatePromotionAsync(promotionId);
      else await reactivatePromotionAsync(promotionId);
    }

    return (
      <CFade>
        <div style={{ overflow: "hidden" }}>
          <Row gutter={48}>
            <PromotionTable 
              promotionList={promotionList}
              itemList={itemList}
              itemTypeList={itemTypeList}
              loading={loading}
              updatePromotionAsync={updatePromotionAsync}
              handleReactivateOrDeactivate={handleReactivateOrDeactivate}
            />
          </Row>
        </div>

        <FAB callback={() => setOpenAddPromotionModal(true)} />
        <AddPromotionModal
          show={openAddPromotionModal}
          itemList={itemList}
          itemTypeList={itemTypeList}
          onClose={() => setOpenAddPromotionModal(false)}
          addPromotionAsync={addPromotionAsync}
        />
      </CFade>
    );
  }
);

const mapState = (state: RootState) => {
  return {
    promotionList: selectPromotionList(state),
    itemList: selectItemList(state),
    itemTypeList: selectItemTypeList(state),
  };
};
const mapDispatch = {
  fetchPromotionListAsync,
  addPromotionAsync,
  updatePromotionAsync,
  deactivatePromotionAsync,
  reactivatePromotionAsync,
  getItemAsync,
  getItemTypeAsync,
};

export default connect(mapState, mapDispatch)(PromotionManagementContainer);
