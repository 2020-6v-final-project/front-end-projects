import { Promotion } from '@custom-types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
    getPromotionList,
    addPromotion,
    updatePromotion,
    deactivatePromotion,
    reactivatePromotion,
} from "@apis";
import { AppThunk } from "src/store";
import swal from "sweetalert";
import { errorMessage } from "@shared/constants/ErrorConstants"

type PromotionManagementInitialState = {
    promotionList: Promotion[],
}

const initialState: PromotionManagementInitialState = {
    promotionList: [],
}

const PromotionManagementSlice = createSlice({
    name: 'promotionManagement',
    initialState,
    reducers: {
        fetchPromotionListSuccess: (state, action: PayloadAction<Promotion[]>) => {
            action.payload.sort((promotionA, promotionB) => promotionA.id - promotionB.id);
            state.promotionList = action.payload;
            return state;
        },
        fetchPromotionListFailed: (state) => {
            return state;
        },
        addPromotionSuccess: (state, action: PayloadAction<Promotion>) => {
            state.promotionList.push(action.payload);
            state.promotionList.sort((promotionA, promotionB) => promotionA.id - promotionB.id);
            return state;
        },
        addPromotionFailed: (state) => {
            return state;
        },
        updatePromotionSuccess: (state, action: PayloadAction<Promotion>) => {
            const index = state.promotionList.findIndex(promotion => promotion.id === action.payload.id);
            if (index > -1) {
                state.promotionList.splice(index, 1 , action.payload);
            }
            return state;
        },
        updatePromotionFailed: (state) => {
            return state;
        },
        deactivatePromotionSuccess: (state, action: PayloadAction<Promotion>) => {
            const index = state.promotionList.findIndex(promotion => promotion.id === action.payload.id);
            if (index > -1) {
                state.promotionList.splice(index, 1 , action.payload);
            }
            return state;
        },
        deactivatePromotionFailed: (state) => {
            return state;
        },
        reactivatePromotionSuccess: (state, action: PayloadAction<Promotion>) => {
            const index = state.promotionList.findIndex(promotion => promotion.id === action.payload.id);
            if (index > -1) {
                state.promotionList.splice(index, 1 , action.payload);
            }
            return state;
        },
        reactivatePromotionFailed: (state) => {
            return state;
        },
    }
});

export const fetchPromotionListAsync = (): AppThunk => async (dispatch) => {
    try {
        await getPromotionList((data: Promotion[]) => {
            dispatch(fetchPromotionListSuccess(data));
        });
    } catch (e) {
        const errorBundle = errorMessage(e, "PROMOTION_MANAGEMENT");
        swal(errorBundle.title, errorBundle.description, "error");
        dispatch(fetchPromotionListFailed());
    }
}

export const addPromotionAsync = (promotion: any): AppThunk => async (dispatch) => {
    try {
        await addPromotion(promotion, (data: Promotion) => {
            dispatch(addPromotionSuccess(data));
        });
    } catch (e) {
        const errorBundle = errorMessage(e, "PROMOTION_MANAGEMENT");
        swal(errorBundle.title, errorBundle.description, "error");
        dispatch(addPromotionFailed());
    }
}

export const updatePromotionAsync = (promotionId: number, promotion: any): AppThunk => async (dispatch) => {
    try {
        await updatePromotion(promotionId, promotion, (data: Promotion) => {
            dispatch(updatePromotionSuccess(data));
        });
    } catch (e) {
        const errorBundle = errorMessage(e, "PROMOTION_MANAGEMENT");
        swal(errorBundle.title, errorBundle.description, "error");
        dispatch(updatePromotionFailed());
    }
}

export const deactivatePromotionAsync = (promotionId: number): AppThunk => async (dispatch) => {
    try {
        await deactivatePromotion(promotionId, (data: Promotion) => {
            dispatch(deactivatePromotionSuccess(data));
        });
    } catch (e) {
        const errorBundle = errorMessage(e, "PROMOTION_MANAGEMENT");
        swal(errorBundle.title, errorBundle.description, "error");
        dispatch(deactivatePromotionFailed());
    }
}

export const reactivatePromotionAsync = (promotionId: number): AppThunk => async (dispatch) => {
    try {
        await reactivatePromotion(promotionId, (data: Promotion) => {
            dispatch(reactivatePromotionSuccess(data));
        });
    } catch (e) {
        const errorBundle = errorMessage(e, "PROMOTION_MANAGEMENT");
        swal(errorBundle.title, errorBundle.description, "error");
        dispatch(reactivatePromotionFailed());
    }
}

const {
    fetchPromotionListSuccess,
    fetchPromotionListFailed,
    addPromotionSuccess,
    addPromotionFailed,
    updatePromotionSuccess,
    updatePromotionFailed,
    deactivatePromotionSuccess,
    deactivatePromotionFailed,
    reactivatePromotionSuccess,
    reactivatePromotionFailed,
} = PromotionManagementSlice.actions;

export default PromotionManagementSlice.reducer;
