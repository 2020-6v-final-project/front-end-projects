import { SubHeaderText } from "@components";
import { Item, ItemType, Promotion, PromotionBenefit, PromotionCondition } from "@custom-types";
import {
  Button,
  Col,
  Drawer,
  Form,
  Input,
  InputNumber,
  List,
  Radio,
  Row,
  Select,
  Tabs,
  Timeline,
  Typography,
} from "antd";
import React, { useEffect, useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import swal from "sweetalert";
import { promotionConditionLine, promotionBenefitLine } from "../AddPromotionModal/AddPromotionModal";
import "./PromotionDetailModal.scss";

type PromotionDetailsModalProps = {
  show: boolean;
  promotion: Promotion | undefined;
  itemList: Item[];
  itemTypeList: ItemType[];
  onClose: () => void;
  updatePromotionAsync: Function;
};

const PromotionDetailsModal: React.FC<PromotionDetailsModalProps> = (props) => {
  const { show, promotion, itemList, itemTypeList } = props;
  const {
    onClose,
    updatePromotionAsync,
  } = props;

  const [form] = Form.useForm();
  const [form1] = Form.useForm();
  const [form2] = Form.useForm();
  const [form3] = Form.useForm();
  const [form4] = Form.useForm();
  const [form5] = Form.useForm();
  const [form6] = Form.useForm();
  const [form7] = Form.useForm();

  const [loading, setLoading] = useState<boolean>(false);
  const [isPercent, setIsPercent] = useState<boolean>(false);
  const [conditions, setConditions] = useState<PromotionCondition[]>([]);
  const [benefits, setBenefits] = useState<PromotionBenefit[]>([]);

  useEffect(() => {
    form.setFieldsValue({
      name: promotion?.name,
      code: promotion?.code,
      allow_apply_with_other: promotion?.allow_apply_with_other,
      description: promotion?.description,
    });
    setConditions(promotion ? promotion.conditions : []);
    setBenefits(promotion ? promotion.benefits : []);
  }, [show, promotion, form]);

  const handleAddPromotion = async (value: any) => {
    if (benefits.length === 0) return swal("Thông báo", "Khuyến mãi phải có tối thiểu 1 ưu đãi", "warning");
    const sendData = {
      ...value,
      conditions,
      benefits,
    }
    setLoading(true);
    await updatePromotionAsync(promotion?.id, sendData); 
    setLoading(false);
    onClose();
    form1.resetFields();
    form2.resetFields();
    form3.resetFields();
    form4.resetFields();
    form5.resetFields();
    form6.resetFields();
    form7.resetFields();
  };

  const handleAddPromotionCondition = (type: number, itemId?: number, itemQuantity?: number, orderTotal?: number, itemTypeId?: number) => {
    const index = conditions.findIndex(condition => condition.type === type);
    const tempCondition = [...conditions];
    
    switch (type) {
      case 1: 
        if (itemId && itemQuantity) {
          const index = conditions.findIndex((condition: any) => condition.type === type && condition.itemid === itemId);
          if (index > -1) {
            tempCondition.splice(index, 1, {
              type: type,
              itemid: itemId,
              item_quantity: itemQuantity,
            });
            setConditions(tempCondition);
          } else setConditions([...conditions ,{
            type: type,
            itemid: itemId,
            item_quantity: itemQuantity,
          }]);
        }
        break;
      case 2:
        if (orderTotal) {
          if (index > -1) {
            tempCondition.splice(index, 1 , {
              type: type,
              order_total: orderTotal,
            });
            setConditions(tempCondition);
          } else setConditions([...conditions ,{
            type: type,
            order_total: orderTotal,
          }]);
        }
        break;
      case 3:
        if (itemQuantity) {
          if (index > -1) {
            tempCondition.splice(index, 1, {
              type: type,
              item_quantity: itemQuantity,
            });
            setConditions(tempCondition);
          } else setConditions([...conditions ,{
            type: type,
            item_quantity: itemQuantity,
          }]);
        }
        break;
      case 4:
        if (itemTypeId && itemQuantity) {
          const index = conditions.findIndex((condition: any) => condition.type === type && condition.item_type_id === itemTypeId);
          if (index > -1) {
            tempCondition.splice(index, 1, {
              type: type,
              item_type_id: itemTypeId,
              item_quantity: itemQuantity,
            });
            setConditions(tempCondition);
          } else setConditions([...conditions ,{
            type: type,
            item_type_id: itemTypeId,
            item_quantity: itemQuantity,
          }]);
        }
        break;
      default:
        break;
    }
  }

  const handleRemovePromotionCondition = (index: number) => {
    const tempCondition = [...conditions];
    tempCondition.splice(index, 1);
    setConditions(tempCondition);
  }

  const handleAddPromotionBenefit = (type: number, itemId?: number, itemQuantity?: number, value?: number, percent?: number, maxValue?: number) => {
    const index = benefits.findIndex(benefit => benefit.type === type);
    const tempBenefit = [...benefits];

    switch (type) {
      case 1: 
        if (value) {
          if (index > -1) {
            tempBenefit.splice(index, 1, {
              type: type,
              value: value,
            });
            setBenefits(tempBenefit);
          } else setBenefits([...benefits ,{
            type: type,
            value: value,
          }]);
        } else if (percent && maxValue) {
          if (index > -1) {
            tempBenefit.splice(index, 1, {
              type: type,
              percent: percent,
              max_value: maxValue,
            });
            setBenefits(tempBenefit);
          } else setBenefits([...benefits ,{
            type: type,
            percent: percent,
            max_value: maxValue,
          }]);
        }
        break;
      case 2:
        if (value && itemId) {
          const index = benefits.findIndex((benefit: any) => benefit.type === type && benefit.itemid === itemId);
          if (index > -1) {
            tempBenefit.splice(index, 1, {
              type: type,
              itemid: itemId,
              value: value,
            });
            setBenefits(tempBenefit);
          } else setBenefits([...benefits ,{
            type: type,
            itemid: itemId,
            value: value,
          }]);
        } else if (percent && maxValue && itemId) {
          const index = benefits.findIndex((benefit: any) => benefit.type === type && benefit.itemid === itemId);
          if (index > -1) {
            tempBenefit.splice(index, 1, {
              type: type,
              itemid: itemId,
              percent: percent,
              max_value: maxValue,
            });
            setBenefits(tempBenefit);
          } else setBenefits([...benefits ,{
            type: type,
            itemid: itemId,
            percent: percent,
            max_value: maxValue,
          }]);
        }
        break;
      case 3:
        if (itemId && itemQuantity) {
          if (index > -1) {
            tempBenefit.splice(index, 1, {
              type: type,
              itemid: itemId,
              item_quantity: itemQuantity,
            });
            setBenefits(tempBenefit);
          } else setBenefits([...benefits ,{
            type: type,
            itemid: itemId,
            item_quantity: itemQuantity,
          }]);
        }
        break;
      default:
        break;
    }
  }

  const handleRemovePromotionBenefit = (index: number) => {
    const tempBenefit = [...benefits];
    tempBenefit.splice(index, 1);
    setBenefits(tempBenefit);
  }

  return (
    <Drawer
      visible={show}
      width="500"
      onClose={onClose}
      zIndex={1}
      className="add-promotion-modal"
    >
      <Scrollbars autoHide>
        <Row style={{ margin: 20 }} justify="center" align="middle">
          <Col span={24}>
            <SubHeaderText style={{ textAlign: "center" }} className="mb-4">
              Chỉnh sửa ưu đãi
            </SubHeaderText>

            <Form
              form={form}
              style={{ minWidth: 350 }}
              name="addPromotionForm"
              onFinish={handleAddPromotion}
            >
              <Row justify="center">
                <Col span={24}>
                  <Form.Item
                    name="name"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền tên khuyến mãi",
                      },
                    ]}
                  >
                    <Input
                      style={{
                        border: "none",
                        textAlign: "center",
                        fontSize: 18,
                        fontWeight: 400,
                        width: "100%",
                      }}
                      placeholder="Tên khuyến mãi"
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row justify="center" align="middle" className="mb-4">
                <Col span={24}>
                  <Form.Item
                    name="allow_apply_with_other"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng chọn vào trường này",
                      },
                    ]}
                    initialValue={true}
                  >
                    <Radio.Group style={{ width: "100%" }}>
                      <Row justify="center">
                        <Col>
                          <Radio value={true}>Được cộng dồn</Radio>
                        </Col>
                        <Col>
                          <Radio value={false}>Không cộng dồn</Radio>
                        </Col>
                      </Row>
                    </Radio.Group>
                  </Form.Item>
                </Col>
              </Row>

              <Row justify="center">
                <Col span={24}>
                  <Form.Item
                    name="code"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền mã khuyến mãi",
                      },
                    ]}
                  >
                    <Input
                      style={{
                        textAlign: "center",
                        width: "100%",
                      }}
                      placeholder="Mã khuyễn mãi"
                    />
                  </Form.Item>
                </Col>
              </Row>

              <Row justify="center">
                <Col span={24}>
                  <Form.Item
                    name="description"
                  >
                    <Input.TextArea
                      showCount
                      maxLength={100}
                      autoSize={{ minRows: 3, maxRows: 3 }}
                      placeholder="Mô tả"
                    />
                  </Form.Item>
                </Col>
              </Row>
            </Form>

            <Tabs centered defaultActiveKey="1" style={{ width: "100%" }}>
              <Tabs.TabPane tab="Điều kiện" key="1">
                <Row justify="center" align="middle" className="mb-4">
                  <Tabs
                    tabPosition="left"
                    style={{ width: "100%" }}
                    defaultActiveKey="1"
                  >
                    <Tabs.TabPane tab="Món ăn" key="1">
                      <Row className="mt-3" justify="center">
                        <Form 
                          form={form1} 
                          onFinish={(value) => {
                            handleAddPromotionCondition(1, value.itemid, value.item_quantity);
                            form1.resetFields();
                          }}
                        >
                          <Col style={{ textAlign: "center" }} span={24}>
                            <Form.Item
                              name="itemid"
                              rules={[
                                {
                                  required: true,
                                  message: "Vui lòng chọn món ăn",
                                },
                              ]}
                            >
                              <Select
                                className="mb-3"
                                style={{ width: "100%", minWidth: 250 }}
                                placeholder="Tên món"
                              >
                                {itemList.map((item, index) => 
                                  <Select.Option key={index} value={item.id}>{item.name}</Select.Option>
                                )}
                              </Select>
                            </Form.Item>
                            <Form.Item
                              name="item_quantity"
                              rules={[
                                {
                                  required: true,
                                  message: "Vui lòng chọn số lượng món áp dụng",
                                },
                              ]}
                            >
                              <InputNumber
                                min={1}
                                className="mb-3"
                                style={{ width: "100%" }}
                                placeholder="Số lượng"
                              />
                            </Form.Item>
                            <Button htmlType="submit">
                              Thêm
                            </Button>
                          </Col>
                        </Form>
                      </Row>
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Loại món ăn" key="2">
                      <Row className="mt-3" justify="center">
                        <Form 
                          form={form2} 
                          onFinish={(value) => {
                            handleAddPromotionCondition(4, undefined, value.item_quantity, undefined, value.item_type_id);
                            form2.resetFields();
                          }}
                        >
                          <Col style={{ textAlign: "center" }} span={24}>
                            <Form.Item
                              name="item_type_id"
                              rules={[
                                {
                                  required: true,
                                  message: "Vui lòng chọn loại món ăn",
                                },
                              ]}
                            >
                              <Select
                                className="mb-3"
                                style={{ width: "100%", minWidth: 250 }}
                                placeholder="Tên loại món ăn"
                              >
                                {itemTypeList.map((itemType, index) => 
                                  <Select.Option key={index} value={itemType.id}>{itemType.name}</Select.Option>
                                )}
                              </Select>
                            </Form.Item>
                            <Form.Item
                              name="item_quantity"
                              rules={[
                                {
                                  required: true,
                                  message: "Vui lòng chọn số lượng món áp dụng",
                                },
                              ]}
                            >
                              <InputNumber
                                min={1}
                                className="mb-3"
                                style={{ width: "100%" }}
                                placeholder="Số lượng"
                              />
                            </Form.Item>
                            <Button htmlType="submit">
                              Thêm
                            </Button>
                          </Col>
                        </Form>
                      </Row>
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Hoá đơn" key="3">
                      <Row className="mt-5" justify="center">
                        <Form 
                          form={form3} 
                          onFinish={(value) => {
                            handleAddPromotionCondition(2, undefined, undefined, value.order_total);
                            form3.resetFields();
                          }}
                        >
                          <Col style={{ textAlign: "center" }} span={24}>
                            <Form.Item
                              name="order_total"
                              rules={[{
                                required: true,
                                message: "Vui lòng điền giá tiền tối thiểu của đơn hàng",
                              }]}
                            >
                              <InputNumber
                                min={1}
                                className="mb-3"
                                style={{ width: "100%", minWidth: 250 }}
                                placeholder="Giá trị đơn tối thiểu"
                              />  
                            </Form.Item>
                            <Button htmlType="submit">Thêm</Button>
                          </Col>
                        </Form>
                      </Row>
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Số lượng" key="4">
                      <Row className="mt-5" justify="center">
                        <Form 
                          form={form4} 
                          onFinish={(value) => {
                            handleAddPromotionCondition(3, undefined, value.item_quantity);
                            form4.resetFields();
                          }}
                        >
                          <Col style={{ textAlign: "center" }} span={24}>
                            <Form.Item
                              name="item_quantity"
                              rules={[{
                                required: true,
                                message: "Vui lòng điền số món tối thiểu",
                              }]}
                            >
                              <InputNumber
                                min={1}
                                className="mb-3"
                                style={{ width: "100%", minWidth: 250 }}
                                placeholder="Tổng số món tối thiểu"
                              />
                            </Form.Item>
                            <Button htmlType="submit">Thêm</Button>
                          </Col>
                        </Form>
                      </Row>
                    </Tabs.TabPane>
                  </Tabs>
                </Row>
                <Row className="mt-3">
                  <List
                    header={
                      <Typography.Title level={5}>
                        Danh sách điều kiện
                      </Typography.Title>
                    }
                  >
                    <Timeline className="mt-3">
                      {conditions.map((condition, index) => 
                        promotionConditionLine(condition, index, itemList, itemTypeList, handleRemovePromotionCondition)
                      )}
                    </Timeline>
                  </List>
                </Row>
              </Tabs.TabPane>
              <Tabs.TabPane tab="Ưu đãi" key="2">
                <Row justify="center" align="middle" className="mb-4">
                  <Tabs
                    tabPosition="left"
                    style={{ width: "100%" }}
                    defaultActiveKey="1"
                  >
                    <Tabs.TabPane tab="Giảm giá món" key="1">
                      <Row className="mt-3" justify="center">
                        <Form
                          form={form5} 
                          onFinish={(value) => {
                            handleAddPromotionBenefit(2, value.itemid, undefined, value.value, value.percent, value.max_value);
                            form5.resetFields();
                          }}
                        >
                          <Col style={{ textAlign: "center" }} span={24}>
                            <Form.Item
                              name="itemid"
                              rules={[
                                {
                                  required: true,
                                  message: "Vui lòng chọn món ăn",
                                },
                              ]}
                            >
                              <Select
                                style={{ width: "100%", minWidth: 250 }}
                                placeholder="Tên món"
                              >
                                {itemList.map((item, index) => 
                                  <Select.Option key={index} value={item.id}>{item.name}</Select.Option>
                                )}
                              </Select>
                            </Form.Item>
                            <Row gutter={16}>
                              <Col span={16}>
                                {isPercent ? 
                                  <Form.Item
                                    name="percent"
                                    rules={[{
                                      required: true,
                                      message: "Vui lòng điền % giảm giá"
                                    }]}
                                  >
                                    <InputNumber
                                      min={1}
                                      max={100}
                                      style={{ width: "100%" }}
                                      placeholder="Giảm"
                                    /> 
                                  </Form.Item>
                                  : 
                                  <Form.Item
                                    name="value"
                                    rules={[{
                                      required: true,
                                      message: "Vui lòng điền số tiền giảm giá"
                                    }]}
                                  >
                                    <InputNumber
                                      min={1}
                                      style={{ width: "100%" }}
                                      placeholder="Số tiền giảm"
                                    /> 
                                  </Form.Item>}
                              </Col>
                              <Col span={8}>
                                <Select
                                  style={{ width: "100%" }}
                                  defaultValue="vnd"
                                  value={isPercent ? "%" : "vnd"}
                                  onChange={(value) => {
                                    if (value === "%") {
                                      setIsPercent(true);
                                      form5.setFieldsValue({...form5.getFieldsValue(), value: undefined});
                                    } else {
                                      setIsPercent(false);
                                      form5.setFieldsValue({...form5.getFieldsValue(), max_value: undefined, percent: undefined});
                                    }
                                  }}
                                >
                                  <Select.Option value="%">%</Select.Option>
                                  <Select.Option value="vnd">đ</Select.Option>
                                </Select>
                              </Col>
                            </Row>
                            {isPercent ? 
                              <Form.Item
                                name="max_value"
                                rules={[{
                                  required: true,
                                  message: "Vui lòng điền số tiền giảm giá tối đa"
                                }]}
                              >
                                <InputNumber
                                  min={1}
                                  className="mb-3"
                                  style={{ width: "100%", minWidth: 250 }}
                                  placeholder="Giảm tối đa"
                                />
                              </Form.Item>
                              : <></>}
                            <Button htmlType="submit">Thêm</Button>
                          </Col>
                        </Form>
                      </Row>
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Giảm giá đơn" key="2">
                      <Row className="mt-3" justify="center">
                        <Form
                          form={form6} 
                          onFinish={(value) => {
                            handleAddPromotionBenefit(1, undefined, undefined, value.value, value.percent, value.max_value);
                            form6.resetFields();
                          }}
                        >
                          <Col style={{ textAlign: "center" }} span={24}>
                            <Row gutter={16}>
                              <Col span={16}>
                                {isPercent ? 
                                  <Form.Item
                                    name="percent"
                                    rules={[{
                                      required: true,
                                      message: "Vui lòng điền % giảm giá"
                                    }]}
                                  >
                                    <InputNumber
                                      min={1}
                                      max={100}
                                      style={{ width: "100%" }}
                                      placeholder="Giảm"
                                    /> 
                                  </Form.Item>
                                  : 
                                  <Form.Item
                                    name="value"
                                    rules={[{
                                      required: true,
                                      message: "Vui lòng điền số tiền giảm giá"
                                    }]}
                                  >
                                    <InputNumber
                                      min={1}
                                      style={{ width: "100%" }}
                                      placeholder="Số tiền giảm"
                                    /> 
                                  </Form.Item>}
                              </Col>
                              <Col span={8}>
                                <Select
                                  style={{ width: "100%" }}
                                  defaultValue="vnd"
                                  value={isPercent ? "%" : "vnd"}
                                  onChange={(value) => {
                                    if (value === "%") {
                                      setIsPercent(true);
                                      form6.setFieldsValue({...form6.getFieldsValue(), value: undefined});
                                    } else {
                                      setIsPercent(false);
                                      form6.setFieldsValue({...form6.getFieldsValue(), max_value: undefined, percent: undefined});
                                    }
                                  }}
                                >
                                  <Select.Option value="%">%</Select.Option>
                                  <Select.Option value="vnd">đ</Select.Option>
                                </Select>
                              </Col>
                            </Row>
                            {isPercent ? 
                              <Form.Item
                                name="max_value"
                                rules={[{
                                  required: true,
                                  message: "Vui lòng điền số tiền giảm giá tối đa"
                                }]}
                              >
                                <InputNumber
                                  min={1}
                                  className="mb-3"
                                  style={{ width: "100%", minWidth: 250 }}
                                  placeholder="Giảm tối đa"
                                />
                              </Form.Item>
                              : <></>}
                            <Button htmlType="submit">Thêm</Button>
                          </Col>
                        </Form>
                      </Row>
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Tặng món" key="3">
                      <Row className="mt-3" justify="center">
                        <Form
                          form={form7} 
                          onFinish={(value) => {
                            handleAddPromotionBenefit(3, value.itemid, value.item_quantity);
                            form7.resetFields();
                          }}
                        >
                          <Col style={{ textAlign: "center" }} span={24}>
                            <Form.Item
                              name="itemid"
                              rules={[
                                {
                                  required: true,
                                  message: "Vui lòng chọn món ăn",
                                },
                              ]}
                            >
                              <Select
                                style={{ width: "100%", minWidth: 250 }}
                                placeholder="Tên món"
                              >
                                {itemList.map((item, index) => 
                                  <Select.Option key={index} value={item.id}>{item.name}</Select.Option>
                                )}
                              </Select>
                            </Form.Item>
                            <Form.Item
                              name="item_quantity"
                              rules={[{
                                required: true,
                                message: "Vui lòng điền số lượng món tặng"
                              }]}
                            >
                              <InputNumber
                                min={1}
                                style={{ width: "100%" }}
                                placeholder="Số lượng"
                              /> 
                            </Form.Item>
                            <Button htmlType="submit">Thêm</Button>
                          </Col>
                        </Form>
                      </Row>
                    </Tabs.TabPane>
                  </Tabs>
                </Row>
                <Row className="mt-3">
                  <List
                    header={
                      <Typography.Title level={5}>
                        Danh sách ưu đãi
                      </Typography.Title>
                    }
                  >
                    <Timeline className="mt-3">
                      {benefits.map((benefit, index) => 
                        promotionBenefitLine(benefit, index, itemList, handleRemovePromotionBenefit)
                      )}
                    </Timeline>
                  </List>
                </Row>
              </Tabs.TabPane>
            </Tabs>

            <Row className="mt-3" justify="center">
              <Button 
                loading={loading} 
                type="primary" 
                onClick={() => form.submit()}>
                Lưu
              </Button>
            </Row>
          </Col>
        </Row>
      </Scrollbars>
    </Drawer>
  );
};

export default PromotionDetailsModal;
