import React, { useEffect, useState } from "react";
import { Table, Input, Button, Typography, Space, Tag, Row, Col } from "antd";
import "./PromotionTable.scss";
import { promotionDescriptionMapper } from "@features/SAC/CampaignManagement/components/MainCampaignPanel/MainCampaignPanel";
import { ColumnsType } from "antd/lib/table";
import { Item, ItemType, Promotion } from "@custom-types";
import { SearchOutlined } from "@ant-design/icons";
import PromotionDetailModal from "../PromotionDetailModal/PromotionDetailModal";
import { Scrollbars } from "react-custom-scrollbars";

type PromotionTableProps = {
  promotionList: Promotion[];
  itemList: Item[];
  itemTypeList: ItemType[];
  loading: boolean;
};

type PromotionTableDispatch = {
  updatePromotionAsync: Function;
  handleReactivateOrDeactivate: Function;
};

type PromotionTableDefaultProps = PromotionTableProps & PromotionTableDispatch;

const PromotionTable: React.FC<PromotionTableDefaultProps> = React.memo(
  (props) => {
    const { promotionList, itemList, itemTypeList, loading } = props;
    const { updatePromotionAsync, handleReactivateOrDeactivate } = props;

    const [actionLoading, setActionLoading] = useState<{
      id: number,
      loading: boolean,
    }[]>(promotionList?.map(promotion => {
      return {id: promotion.id, loading: false}
    }));
    const [openPromotionDetailModal, setOpenPromotionDetailModal] = useState<boolean>(false);
    const [currentPromotion, setCurrentPromotion] = useState<Promotion | undefined>(undefined);

    useEffect(() => {
      setActionLoading(promotionList?.map(promotion => {
        return {id: promotion.id, loading: false}
      }));
    }, [promotionList]);

    const columns: ColumnsType<Promotion> = [
      {
        align: "center",
        title: "Tên",
        dataIndex: "name",
        key: "name",
        sorter: (item1: any, item2: any) => item1.name.localeCompare(item2.name),
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
          <div style={{ padding: 8 }}>
            <Input
              placeholder="Tìm tên"
              value={selectedKeys[0]}
              onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
              onPressEnter={() => confirm()}
              style={{ width: 188, marginBottom: 8, display: 'block' }}
            />
            <Space>
              <Button
                type="primary"
                onClick={() => confirm()}
                size="small"
                style={{ width: 90 }}
              >
                Tìm
              </Button>
              <Button onClick={() => clearFilters ? clearFilters() : null} size="small" style={{ width: 90 }}>
                Xoá
              </Button>
              <Button
                type="link"
                size="small"
                onClick={() => {
                  confirm({ closeDropdown: false });
                }}
              >
                Lọc
              </Button>
            </Space>
          </div> 
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value: any, record: any) => record
            ? record.name.toString().toLowerCase().includes(value.toLowerCase())
            : '',
      },
      {
        align: "center",
        title: "Mã",
        key: "code",
        dataIndex: "code",
        sorter: (item1: any, item2: any) => item1.code.localeCompare(item2.code),
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
          <div style={{ padding: 8 }}>
            <Input
              placeholder="Tìm mã"
              value={selectedKeys[0]}
              onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
              onPressEnter={() => confirm()}
              style={{ width: 188, marginBottom: 8, display: 'block' }}
            />
            <Space>
              <Button
                type="primary"
                onClick={() => confirm()}
                size="small"
                style={{ width: 90 }}
              >
                Tìm
              </Button>
              <Button onClick={() => clearFilters ? clearFilters() : null} size="small" style={{ width: 90 }}>
                Xoá
              </Button>
              <Button
                type="link"
                size="small"
                onClick={() => {
                  confirm({ closeDropdown: false });
                }}
              >
                Lọc
              </Button>
            </Space>
          </div> 
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value: any, record: any) => record
            ? record.code.toString().toLowerCase().includes(value.toLowerCase())
            : '',
      },
      {
        align: "center",
        title: "Mô tả",
        key: "description",
        render: (record: Promotion) => {
          if (record.description !== null)
            return <Typography.Text>{record.description}</Typography.Text>
          else 
            return <Typography.Text type="secondary">Không có</Typography.Text>
        }
      },
      {
        align: "center",
        title: "Cộng dồn",
        key: "allow_apply_with_other",
        render: (record: Promotion) => (
          <span>{record.allow_apply_with_other ? "Có" : "Không"}</span>
        ),
      },
      {
        align: "center",
        title: "Trạng thái",
        key: "statuscode",
        render: (record: Promotion) => (
          record.statuscode === "AVAILABLE" ? <Tag color="green">Hoạt động</Tag> :
          <Tag color="red">Đã khóa</Tag>
        )
      },
      {
        title: <Row justify="center"><Typography.Text strong>Điều kiện</Typography.Text></Row>,
        key: "conditions",
        render: (record: Promotion) => (
          <>
          {record.conditions.length === 0 ?
            <Typography.Text type="secondary">Không điều kiện</Typography.Text>
            :
            <ul>
              {
                promotionDescriptionMapper(record.conditions, record.benefits, itemList, itemTypeList)
                  .conditions
              }
            </ul>
          }
          </>
        ),
      },
      {
        title: <Row justify="center"><Typography.Text strong>Ưu đãi</Typography.Text></Row>,
        key: "benefits",
        render: (record: Promotion) => (
          <ul>
            {
              promotionDescriptionMapper(record.conditions, record.benefits, itemList, itemTypeList)
                .benefits
            }
          </ul>
        ),
      },
      {
        align: "center",  
        title: "Hành động",
        key: "benefits",
        render: (record: Promotion) => { 
          return (
            <Button 
              danger 
              size="small"
              loading={actionLoading.find(loading => loading.id === record.id)?.loading}
              onClick={async (e) => {
                e.preventDefault();
                e.stopPropagation();
                const loadingIndex = actionLoading.findIndex(loading => loading.id === record.id);
                if (loadingIndex > -1) {
                  let tempLoading = [...actionLoading];
                  tempLoading.splice(loadingIndex, 1, { id: actionLoading[loadingIndex].id, loading: true });
                  
                  setActionLoading(tempLoading);
                  await handleReactivateOrDeactivate(record.statuscode, record.id);

                  tempLoading = [...actionLoading];
                  tempLoading.splice(loadingIndex, 1, { id: actionLoading[loadingIndex].id, loading: false });
                  setActionLoading(tempLoading);
                }
              }}  
            >
              {record.statuscode === "AVAILABLE" ? "Khóa" : "Kích hoạt"}
            </Button>
          )
        },
      },
    ];

    return (
      <Col span={24} >
        <Scrollbars
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              className="track-horizontal"
              style={{ display: "none" }}
            />
          )}
          renderThumbHorizontal={(props) => (
            <div
              {...props}
              className="thumb-horizontal"
              style={{ display: "none" }}
            />
          )}
          autoHide
          autoHeight
          autoHeightMin="80vh"
        >
          <PromotionDetailModal
            show={openPromotionDetailModal}
            promotion={currentPromotion}
            itemList={itemList}
            itemTypeList={itemTypeList}
            onClose={() => setOpenPromotionDetailModal(false)}
            updatePromotionAsync={updatePromotionAsync}
          />
          <Table 
            rowKey={(record) => record.id}
            dataSource={promotionList} 
            columns={columns} 
            loading={loading}
            pagination={{ pageSize: 5, position: ['bottomCenter'] }}
            onRow={(record: Promotion) => {
              return {
                onClick: () => {
                  setCurrentPromotion(record);
                  setOpenPromotionDetailModal(true);
                }
              }
            }}
          />
        </Scrollbars>
      </Col>
    );
  }
);

export default PromotionTable;
