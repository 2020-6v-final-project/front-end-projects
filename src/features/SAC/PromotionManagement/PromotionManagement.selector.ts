import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectPromotionList = createSelector(
    [(state: RootState) => state.promotion.promotionList],
    (promotionList) => promotionList
)
