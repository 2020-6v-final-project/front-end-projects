import { FAB } from "@components";
import { CFade } from "@coreui/react";
import { Branch, Repository } from "@custom-types";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { RootState } from "src/store";
import { branchListSelector } from "../BranchManagement/BranchManagement.selector";
import { fetchBranchListAsync } from "../BranchManagement/BranchManagement.slice";
import AddRepositoryItemModal from "./components/AddRepositoryItemModal/AddRepositoryItemModal";
import RepositoriesTable from "./components/RepositoriesTable/RepositoriesTable";
import { selectRepositoriesList } from "./RepositoryManagement.selector";
import {
  addRepositoryAsync,
  deactivateRepositoryAsync,
  getRepositoriesAsync,
  reactivateRepositoryAsync,
  removeRepositoryAsync,
  updateRepositoryAsync,
} from "./RepositoryManagement.slice";
type RepositoryManagementState = {
  branchList: Array<Branch>;
  repositoriesList: Array<any>;
};

type RepositoryManagementDispatch = {
  getRepositoriesAsync: Function;
  addRepositoryAsync: Function;
  updateRepositoryAsync: Function;
  reactivateRepositoryAsync: Function;
  deactivateRepositoryAsync: Function;
  fetchBranchListAsync: Function;
};

type RepositoryManagementDefaultProps = RepositoryManagementState &
  RepositoryManagementDispatch;

const RepositoryManagementContainer: React.FC<RepositoryManagementDefaultProps> = React.memo(
  (props) => {
    const {
      repositoriesList,
      getRepositoriesAsync,
      branchList,
      fetchBranchListAsync,
      addRepositoryAsync,
      deactivateRepositoryAsync,
      reactivateRepositoryAsync,
      //  updateRepositoryAsync
    } = props;
    const [addModalVisible, toggleAddModal] = useState(false);

    useEffect(() => {
      fetchBranchListAsync();
      getRepositoriesAsync();
    }, [fetchBranchListAsync, getRepositoriesAsync]);

    const handleToggleAddModal = () => {
      toggleAddModal(!addModalVisible);
    };

    const handleUpdate = async (value: any) => {
      // await updateRepositoryAsync(e);
    };

    const handleAddRepository = async (e: any) => {
      if (e !== undefined) await addRepositoryAsync(e);
    };

    const handleDeactivateOrReactivate = async (
      repo: Repository,
      statusCode: string
    ) => {
      if (statusCode === "AVAILABLE") await deactivateRepositoryAsync(repo);
      else await reactivateRepositoryAsync(repo);
    };

    return (
      <CFade>
        <RepositoriesTable
          branches={branchList}
          data={repositoriesList}
          handleDeactivateOrReactivate={handleDeactivateOrReactivate}
          handleUpdate={handleUpdate}
        />
        <FAB callback={handleToggleAddModal} />
        <AddRepositoryItemModal
          show={addModalVisible}
          toggleModal={handleToggleAddModal}
          onFinish={handleAddRepository}
        />
      </CFade>
    );
  }
);

const mapState = (state: RootState) => {
  return {
    repositoriesList: selectRepositoriesList(state),
    branchList: branchListSelector(state),
  };
};

const mapDispatch = {
  fetchBranchListAsync,
  removeRepositoryAsync,
  updateRepositoryAsync,
  addRepositoryAsync,
  reactivateRepositoryAsync,
  deactivateRepositoryAsync,
  getRepositoriesAsync,
};

export default connect(mapState, mapDispatch)(RepositoryManagementContainer);
