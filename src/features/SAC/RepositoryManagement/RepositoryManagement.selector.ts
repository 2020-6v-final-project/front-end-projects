import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectRepositoriesList = createSelector(
    [(state: RootState) => state.repository.repositoriesList],
    (repositoriesList) => repositoriesList
)