import {
  Button,
  Col,
  Input,
  Row,
  Space,
  Table,
  Tag
} from "antd";
import { ColumnsType } from "antd/lib/table";
import React, { useState, useEffect } from "react";
import "./RepositoriesTable.scss";


type RepositoriesTableProps = {
  data: Array<any>,
  branches: Array<any>,
  handleDeactivateOrReactivate: Function,
  handleUpdate: Function,
};

const RepositoriesTable: React.FC<RepositoriesTableProps> = React.memo((props) => {
  const {data, handleDeactivateOrReactivate } = props;
  const [filteredData, setFilteredData] = useState(data);
  useEffect(()=>{
      setFilteredData(data);
  },[data]);

  // const handleBranchFilter = (value?: any) => {
  //   if (value.name === "") {
  //       setFilteredData(data);
  //   } else {
  //       let temp = data.filter((item: any) => (item?.branchid === value.id));
  //       setFilteredData(temp);
  //   }
  // }
  const handleSearch = (value: string) => {
    if (!value) {
      setFilteredData(data);
    } else {
      let temp = data.filter((item: any) =>
        item.name
          .toLocaleLowerCase()
          .includes(value.toLocaleLowerCase())
      );
      setFilteredData(temp);
    }
  };
  const columns: ColumnsType = [
    {
      align: "center",
      title: "Id",
      dataIndex: "id",
      key: "id",
    },
    {
      align: "center",
      title: 'Tên Kho',
      dataIndex: 'name',
      key: 'name',
      defaultSortOrder: 'ascend',
    },
    {
      align: "center",
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",
    },
    {
      align: 'center',
      title: 'Trạng thái',
      key: 'statuscode',
      dataIndex: 'statuscode',
      render: (text: string) => <Tag color = {text === "AVAILABLE" ? "success" : "error"}>{text === "AVAILABLE" ? "Hoạt động" : "Vô hiệu"}</Tag>,
    },
    {
      align: "center",
      title: "Thao tác",
      key: "action",
      render: (text: string, record: any) => (
        <Space size="middle">
          <Button
            onClick={(e) => handleDeactivateOrReactivate(record,record.statuscode)}
            type="primary" danger={record?.statuscode === "AVAILABLE" ? true : false} 
            size="small"
          >
            {record?.statuscode === "AVAILABLE"
                            ? "Khóa"
                            : "Mở khóa"}
          </Button>
        </Space>
      ),
    },
  ];

  return (
    <>
      <Row>
        <Col span="6">
          <Input
            onChange={(e) => {
              handleSearch(e.target.value);
            }}
            className="my-4"
            placeholder="Tìm kiếm kho"
          />
        </Col>
      </Row>
      <Table
        bordered
        size="small"
        className="ingredients-table"
        columns={columns}
        pagination={{ position: ["bottomCenter"] }}
        dataSource={filteredData}
        scroll={{ y: "55vh" }}
      />
    </>
  );
}
);

export default RepositoriesTable;
