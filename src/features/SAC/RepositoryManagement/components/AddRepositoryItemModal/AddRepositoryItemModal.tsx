import { Modal, Form, Typography, Button, Row, Col, Input } from 'antd';
import React, { useState, useEffect } from 'react';
import './AddRepositoryItemModal.scss';

type AddRepositoryItemModalProps = {
    show: boolean,
    onFinish: Function,
    toggleModal: Function,
}

const AddRepositoryItemModal: React.FC<AddRepositoryItemModalProps> = (props) => {
    const { onFinish, show, toggleModal } = props;
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();
    const [formValue, setFormValue] = useState<any>({
        name:"",
        address:"",
      });
    useEffect(() => {
        if (!show) {
          form.resetFields();
        }
      }, [show, form]);
    

    const handleAddRepository = () => {
        const sendData = {
            ...formValue,
        };
        onFinish(sendData);
        form.resetFields();
        toggleModal();
    };

    // const handleIngredientSelect = () => { }

    return (
        <Modal visible={show} centered footer={false} onCancel={(e) => toggleModal()} zIndex={1} className="add-storage-item-modal">
            <Row justify="center">
                <Typography.Title level={4}>Thêm nguyên liệu</Typography.Title>
            </Row>
            <Form className="mt-3 px-5" form={form}
              name="addTableForm"
              onFinish={() => {
                setLoading(true);
                handleAddRepository();
                setLoading(false);
              }} >
                <Row gutter={24} justify="center">
                    <Col span={24}>
                        <Form.Item rules={[
                            {
                                required: true,
                                message: 'Vui lòng điền tên kho!'
                            }
                        ]
                        }>
                            <Input style={{ width: '100%' }} name="name" placeholder="Tên kho" value={formValue.name ||""}
                            onChange={(e) =>
                                setFormValue({ ...formValue, name: e.target.value })
                            }/>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={24} justify="center">
                    <Col span={24}>
                        <Form.Item rules={[
                            {
                                required: true,
                                message: 'Vui lòng điền địa chỉ!'
                            }
                        ]
                        }>
                            <Input style={{ width: '100%' }} name="address" placeholder="Địa chỉ" value={formValue.address ||""}
                            onChange={(e) =>
                                setFormValue({ ...formValue, address: e.target.value })
                            }/>
                        </Form.Item>
                    </Col>
                </Row>
                <Row justify="center">
                    <Button  loading={loading}  onClick={() => {
                      setLoading(true);
                      onFinish();
                      setLoading(false);}}
                      htmlType="submit" type="primary">Thêm</Button>
                </Row>
            </Form>
        </Modal>
    )
}

export default AddRepositoryItemModal;
