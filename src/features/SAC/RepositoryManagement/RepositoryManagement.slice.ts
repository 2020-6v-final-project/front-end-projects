import { getRepositoryById, removeRepository, reactivateRepository, deactivateRepository, updateRepository, addRepository, getRepositoriesList } from '@apis';
import { Repository } from '@custom-types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { orderBy } from 'lodash';
import { AppThunk } from 'src/store';
import swal from 'sweetalert';

type RepositoryManagementInitialState = {
    repositoriesList: Array<Repository>,
}

const initialState: RepositoryManagementInitialState = {
    repositoriesList: [],
}

const RepositoryManagementSlice = createSlice({
    name: 'RepositoryManagement',
    initialState,
    reducers: {
        getRepositoriesSuccess: (state, action: PayloadAction<Repository[]>) => {
            state.repositoriesList = action.payload;
            state.repositoriesList = orderBy(state.repositoriesList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        getRepositoriesFailed: (state) => {
            return state;
        },
        getRepositoryByIdSuccess: (state, action: PayloadAction<Repository>) => {
            state.repositoriesList = [];
            state.repositoriesList.push(action.payload);
            return state;
        },
        getRepositoryByIdFailed: (state) => {
            return state;
        },
        addRepositorySuccess: (state, action: PayloadAction<Repository[]>) => {
            state.repositoriesList = action.payload;
            state.repositoriesList = orderBy(state.repositoriesList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        addRepositoryFailed: (state) => {
            return state;
        },
        removeRepositorySuccess: (state, action: PayloadAction<Repository[]>) => {
            state.repositoriesList = action.payload;
            state.repositoriesList = orderBy(state.repositoriesList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        removeRepositoryFailed: (state) => {
            return state;
        },
        updateRepositorySuccess: (state, action: PayloadAction<Repository>) => {
            const index = state.repositoriesList?.findIndex(repo => repo.id === action.payload.id);
            if (index > -1) {
                state.repositoriesList?.splice(index, 1, action.payload);
            }
        },
        updateRepositoryFailed: (state) => {
            return state;
        },
        deactivateRepositorySuccess: (state, action: PayloadAction<Repository[]>) => {
            state.repositoriesList = action.payload;
            state.repositoriesList = orderBy(state.repositoriesList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        deactivateRepositoryFailed: (state) => {
            return state;
        },
        reactivateRepositorySuccess: (state, action: PayloadAction<Repository[]>) => {
            state.repositoriesList = action.payload;
            state.repositoriesList = orderBy(state.repositoriesList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        reactivateRepositoryFailed: (state) => {
            return state;
        },
    }

})

export const getRepositoryByIdAsync = (repoId:number): AppThunk => async (dispatch) => {
    try {
        await getRepositoryById(repoId,(data: Repository) => {
            dispatch(getRepositoryByIdSuccess(data));
        });
    } catch (e) {
        dispatch(getRepositoryByIdFailed());
    }
}

export const getRepositoriesAsync = (): AppThunk => async (dispatch) => {
    try {
        await getRepositoriesList((data: Repository[]) => {
            dispatch(getRepositoriesSuccess(data));
        });
    } catch (e) {
        dispatch(getRepositoriesFailed());
    }
}

export const updateRepositoryAsync = (table: any): AppThunk => async (dispatch) => {
    try {
        await updateRepository(table, (data: Repository) => {
            swal("Thành công", "Kho đã được cập nhật!", "success");
            dispatch(updateRepositorySuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(updateRepositoryFailed());
    }
}

export const addRepositoryAsync = (Repository: any): AppThunk => async (dispatch) => {
    try {
        await addRepository(Repository, (data: Repository[]) => {
            swal("Thành công", "Kho đã được thêm!", "success");
            dispatch(addRepositorySuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(addRepositoryFailed());
    }
}

export const deactivateRepositoryAsync = (Repository: any): AppThunk => async (dispatch) => {
    try {
        await deactivateRepository(Repository, (data: Repository[]) => {
            swal("Thành công", "Kho đã bị khóa", "success");
            dispatch(deactivateRepositorySuccess(data));
        })
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(deactivateRepositoryFailed());
    }
}

export const reactivateRepositoryAsync = (Repository: any): AppThunk => async (dispatch) => {
    try {
        await reactivateRepository(Repository, (data: Repository[]) => {
            swal("Thành công", "Kho đã được mở khóa", "success");
            dispatch(reactivateRepositorySuccess(data));
        })
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(reactivateRepositoryFailed());
    }
}


export const removeRepositoryAsync = (Repository: any): AppThunk => async (dispatch) => {
    try {
        await removeRepository(Repository, (data: Repository[]) => {
            swal("Thành công", "Kho đã được xóa", "success");
            dispatch(removeRepositorySuccess(data));
        })
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(removeRepositoryFailed());
    }
}

const {
    getRepositoriesSuccess,
    getRepositoriesFailed,
    addRepositorySuccess,
    addRepositoryFailed,
    removeRepositorySuccess,
    removeRepositoryFailed,
    updateRepositorySuccess,
    updateRepositoryFailed,
    deactivateRepositorySuccess,
    deactivateRepositoryFailed,
    reactivateRepositorySuccess,
    reactivateRepositoryFailed,
    getRepositoryByIdSuccess,
    getRepositoryByIdFailed,
} = RepositoryManagementSlice.actions;

export {
    getRepositoriesSuccess,
    getRepositoriesFailed,
    addRepositorySuccess,
    addRepositoryFailed,
    removeRepositorySuccess,
    removeRepositoryFailed,
    updateRepositorySuccess,
    updateRepositoryFailed,
    deactivateRepositorySuccess,
    deactivateRepositoryFailed,
    reactivateRepositorySuccess,
    reactivateRepositoryFailed,
};

export default RepositoryManagementSlice.reducer;
