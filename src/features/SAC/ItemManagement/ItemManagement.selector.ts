import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectItemList = createSelector(
    [(state: RootState) => state.item.itemList],
    (itemList) => itemList
)

export const selectOptionList = createSelector(
    [(state: RootState) => state.item.optionList],
    (optionList) => optionList
)