import React, { CSSProperties } from "react";
import { Avatar, Col, Row, Typography } from "antd";
import "./ItemFromList.scss";
import { currencyFormat } from "../../../../../shared/utils/stringUtils";
import { Item } from "@custom-types";

type ItemFromListProps = {
  item: Item;
};

const CSSStyle: CSSProperties = {
  textOverflow: "ellipsis",
  overflow: "hidden",
  whiteSpace: "nowrap",
};

const ItemFromList: React.FC<ItemFromListProps> = React.memo((props) => {
  const { item } = props;
  return (
    <Row
      className="item-from-list"
      justify="start"
      gutter={32}
      align="middle"
      style={{
        width: "100%",
        ...CSSStyle,
      }}
      wrap={false}
    >
      <Col>
        <Avatar style={{ width: 64, height: 64 }} src={(item?.imgpath === null || item?.imgpath ==="") ? "/item-sample.svg" : item.imgpath} />
      </Col>
      <Col style={{ ...CSSStyle }}>
        <Typography.Title ellipsis={{ rows: 1, expandable: false }} level={5}>
          {item.name}
        </Typography.Title>
        <Typography.Text ellipsis>
          {currencyFormat(item.price)} / {item.unit}
        </Typography.Text>
      </Col>
    </Row>
  );
});

export default ItemFromList;
