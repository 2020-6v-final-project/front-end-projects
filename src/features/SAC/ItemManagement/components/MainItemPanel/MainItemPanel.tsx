import {
  Ingredient,
  Item,
  ItemIngredient,
  Option,
  ItemOption,
} from "@custom-types";
import {
  currencyFormat,
  numberToStringWithCommas,
  shortDateFormat,
} from "@shared/utils/";
import {
  Avatar,
  Button,
  Col,
  Descriptions,
  Divider,
  Empty,
  Row,
  Statistic,
  Typography,
  Tag,
  Table,
  Space,
} from "antd";
import React, { useState } from "react";
import { ColumnsType } from "antd/lib/table";
import { Scrollbars } from "react-custom-scrollbars";
import "./MainItemPanel.scss";
import { Link } from "react-router-dom";
import AddOptionDrawer from "./AddOptionDrawer/AddOptionDrawer";
import OptionDetailModal from "./OptionDetailModal/OptionDetailModal";

type MainItemPanelProps = {
  item: Item | undefined;
  ingredientList: Array<Ingredient>;
  optionList: Array<Option>;
  addOptionToItemAsync: Function;
  updateOptionOfItemAsync: Function;
  deleteOptionFromItemAsync: Function;
  handleEditButtonClick: Function;
  handleDeactivateOrReactivate: Function;
};

const MainItemPanel: React.FC<MainItemPanelProps> = React.memo((props) => {
  const {
    item,
    ingredientList,
    optionList,
    addOptionToItemAsync,
    updateOptionOfItemAsync,
    deleteOptionFromItemAsync,
    handleEditButtonClick,
    handleDeactivateOrReactivate,
  } = props;

  const [loading, setLoading] = useState<boolean>(false);
  const [deleteOptionLoading, setDeleteOptionLoading] = useState<boolean>(
    false
  );
  const [selectedOptionDetail, setSelectedOptionDetail] = useState<
    (Option & ItemOption) | undefined
  >(undefined);
  const [openAddOptionDrawer, setOpenAddOptionDrawer] = useState<boolean>(
    false
  );
  const toggleAddOptionModal = () =>
    setOpenAddOptionDrawer(!openAddOptionDrawer);
  const [openOptionDetailDrawer, setOpenOptionDetailDrawer] = useState<boolean>(
    false
  );
  const toggleOptionDetailModal = () =>
    setOpenOptionDetailDrawer(!openOptionDetailDrawer);

  const columns: ColumnsType = [
    {
      align: "center",
      title: "Id",
      dataIndex: "id",
      key: "id",
      sorter: (item1: any, item2: any) => item1.id - item2.id,
      defaultSortOrder: "ascend",
    },
    {
      align: "center",
      title: "Tên tùy chọn",
      dataIndex: "name",
      key: "name",
      sorter: (item1: any, item2: any) => item1.name.localeCompare(item2.name),
    },
    {
      align: "center",
      title: "Giá",
      dataIndex: "price",
      key: "price",
      sorter: (item1: any, item2: any) => item1.price - item2.price,
      render: (value: number) => currencyFormat(value),
    },
    {
      align: "center",
      title: "Thao tác",
      key: "action",
      render: (text: string, record: any) => (
        <Space size="middle">
          <Button
            onClick={() => {
              setSelectedOptionDetail(record);
              toggleOptionDetailModal();
            }}
            size="small"
          >
            Chỉnh sửa
          </Button>
          <Button
            onClick={async () => {
              setDeleteOptionLoading(true);
              await deleteOptionFromItemAsync(item?.id, record?.id);
              setDeleteOptionLoading(false);
            }}
            danger
            size="small"
            loading={deleteOptionLoading}
          >
            Xóa
          </Button>
        </Space>
      ),
    },
  ];
  return (
    <Col span={17} className="main-item-panel">
      <AddOptionDrawer
        itemId={item?.id}
        ingredientList={ingredientList}
        optionList={optionList?.filter(
          (defaultOption) =>
            !item?.options
              ?.map((option) => option.id)
              .includes(defaultOption.id)
        )}
        show={openAddOptionDrawer}
        onFinish={addOptionToItemAsync}
        toggleModal={toggleAddOptionModal}
      />
      <OptionDetailModal
        itemId={item?.id}
        option={selectedOptionDetail}
        ingredientList={ingredientList}
        show={openOptionDetailDrawer}
        onFinish={updateOptionOfItemAsync}
        toggleModal={toggleOptionDetailModal}
      />
      {!item ? (
        <Row justify="center">
          <Empty
            style={{ marginTop: "36vh" }}
            description="Tạo hoặc chọn món để xem thông tin!"
          />
        </Row>
      ) : (
        <Scrollbars
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              className="track-horizontal"
              style={{ display: "none" }}
            />
          )}
          renderThumbHorizontal={(props) => (
            <div
              {...props}
              className="thumb-horizontal"
              style={{ display: "none" }}
            />
          )}
          autoHide
        >
          <Row justify="end" gutter={16}>
            <Col>
              <Button
                loading={loading}
                danger={item?.statuscode === "AVAILABLE" ? true : false}
                type={item?.statuscode === "AVAILABLE" ? undefined : "primary"}
                onClick={async () => {
                  setLoading(true);
                  await handleDeactivateOrReactivate(
                    item?.id,
                    item?.statuscode
                  );
                  setLoading(false);
                }}
                shape="round"
              >
                {item?.statuscode === "AVAILABLE"
                  ? "Khóa món ăn"
                  : "Mở khóa món ăn"}
              </Button>
            </Col>
            <Col>
              <Button onClick={() => handleEditButtonClick()} shape="round">
                Sửa
              </Button>
            </Col>
          </Row>
          <Row className="mb-5" justify="center" align="middle">
            <Col span={12}>
              <Row justify="center">
                <Avatar
                  src={
                    item?.imgpath === null || item?.imgpath === ""
                      ? "/item-sample-large.jpg"
                      : item.imgpath
                  }
                  className="item-cover-image"
                />
              </Row>
            </Col>
            <Col span={12}>
              <Typography.Title level={1}>{item?.name}</Typography.Title>
              <Typography.Paragraph>{item?.description}</Typography.Paragraph>
            </Col>
          </Row>
          <Row justify="space-between" className="mb-5" gutter={24}>
            <Col span={4}>
              <Statistic title="Giá" value={currencyFormat(item?.price)} />
            </Col>
            <Col span={1}>
              <Divider style={{ height: "100%" }} type="vertical" />
            </Col>
            <Col span={4}>
              <Statistic title="Đơn vị" value={item?.unit} />
            </Col>
            <Col span={1}>
              <Divider style={{ height: "100%" }} type="vertical" />
            </Col>
            <Col span={4}>
              <Statistic
                title="Trạng thái"
                value={
                  item?.statuscode === "AVAILABLE" ? "Hoạt động" : "Vô hiệu"
                }
                valueStyle={
                  item?.statuscode === "AVAILABLE"
                    ? { color: "#3f8600" }
                    : { color: "#cf1322" }
                }
              />
            </Col>
            <Col span={1}>
              <Divider style={{ height: "100%" }} type="vertical" />
            </Col>
            <Col span={5}>
              <Statistic
                title="Cập nhật lần cuối"
                valueStyle={
                  item?.lastmodified ? { fontSize: 18, marginTop: 8 } : {}
                }
                value={
                  !item?.lastmodified
                    ? "Không có"
                    : shortDateFormat(item.lastmodified)
                }
              />
            </Col>
          </Row>

          <Row
            justify="space-between"
            gutter={24}
            style={{ paddingBottom: 24 }}
          >
            <Col span={24}>
              <Row className="mb-3" justify="center">
                <Typography.Title level={4}>Nguyên liệu</Typography.Title>
              </Row>
              <Row justify="space-between">
                <Col span={24}>
                  {item?.ingredients.length > 0 ? (
                    <Descriptions
                      className="mb-5"
                      layout="horizontal"
                      bordered
                      column={3}
                    >
                      {item?.ingredients?.map(
                        (itemIngredient: ItemIngredient, index: number) => {
                          const ingredient:
                            | Ingredient
                            | undefined = ingredientList.find(
                            (value) => value.id === itemIngredient.ingredientid
                          );
                          return (
                            <>
                              <Descriptions.Item
                                label="Nguyên liệu"
                                key={index * 3}
                              >
                                <Link
                                  to={
                                    "/ingredient-management/" + ingredient?.id
                                  }
                                >
                                  <Tag>{ingredient?.name}</Tag>
                                </Link>
                              </Descriptions.Item>
                              <Descriptions.Item
                                label="Số lượng"
                                key={index * 3 + 1}
                              >
                                {numberToStringWithCommas(
                                  itemIngredient.quantity
                                )}
                              </Descriptions.Item>
                              <Descriptions.Item
                                label="Đơn vị"
                                key={index * 3 + 2}
                              >
                                {ingredient?.unit}
                              </Descriptions.Item>
                            </>
                          );
                        }
                      )}
                    </Descriptions>
                  ) : (
                    <Empty className="mb-5" description="Chưa có dữ liệu" />
                  )}
                </Col>
              </Row>
              <Row
                justify={item?.recipe?.trim() ? "start" : "center"}
                className="mb-5 recipe-row"
                gutter={24}
              >
                <Col className="recipe-col" span={24}>
                  <Row className="mb-3" justify="center">
                    <Typography.Title level={4}>Công thức</Typography.Title>
                  </Row>
                  <Row
                    className="mb-3"
                    justify={item?.recipe?.trim() ? "start" : "center"}
                  >
                    {item?.recipe?.trim() ? (
                      <Typography.Paragraph>
                        {item?.recipe}
                      </Typography.Paragraph>
                    ) : (
                      <Empty description="Chưa có dữ liệu" />
                    )}
                  </Row>
                </Col>
              </Row>
              <Row
                justify={item?.options?.length > 0 ? "start" : "center"}
                className="mb-5 recipe-row"
                gutter={24}
              >
                <Col className="recipe-col" span={24}>
                  <Row className="mb-3" justify="space-between" gutter={20}>
                    <Col>
                      <Typography.Title level={4}>
                        Tùy chọn khác{" "}
                      </Typography.Title>
                    </Col>
                    <Col>
                      <Button
                        type="primary"
                        shape="round"
                        onClick={toggleAddOptionModal}
                      >
                        Thêm
                      </Button>
                    </Col>
                  </Row>
                  <Row
                    className="mb-3"
                    justify={item?.options.length > 0 ? "start" : "center"}
                  >
                    <Col span={24}>
                      {item?.options.length > 0 ? (
                        <Table
                          className="ingredients-table"
                          columns={columns}
                          pagination={{ position: ["bottomCenter"] }}
                          dataSource={item?.options}
                        />
                      ) : (
                        <Empty description="Chưa có dữ liệu" />
                      )}
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
        </Scrollbars>
      )}
    </Col>
  );
});

export default MainItemPanel;
