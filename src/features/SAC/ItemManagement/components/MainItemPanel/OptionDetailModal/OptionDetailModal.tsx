import { SubHeaderText } from "@components";
import CIcon from "@coreui/icons-react";
import { Ingredient, ItemOption, Option } from "@custom-types";
import {
  AutoComplete,
  Button,
  Col,
  Drawer,
  Form,
  Input,
  Row,
  Timeline,
  Typography,
} from "antd";
import React, { ChangeEvent, useState, useEffect } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import "./OptionDetailModal.scss";

type AddOptionDrawerProps = {
  option: Option & ItemOption | undefined;
  itemId: number | undefined;
  ingredientList: Array<Ingredient>;
  show: boolean;
  onFinish: Function;
  toggleModal: Function;
};

const AddOptionDrawer: React.FC<AddOptionDrawerProps> = (props) => {
  const { show, toggleModal, onFinish, ingredientList, itemId, option} = props;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [ingredients, setIngredients] = useState<any>([]);
  const [autoCompleteText, setAutoCompleteText] = useState("");
  const [formValue, setFormValue] = useState<any>({
    id: null,
    name: "",
    price: "",
    note: "",
  });

  useEffect(() => {
    setFormValue({
        id: option?.id,
        name: option?.name,
        price: option?.price,
        note: option?.note,
    });
        form?.setFieldsValue({
        name: option?.name,
        price: option?.price,
        note: option?.note,
    });
    setIngredients(option?.ingredients?.map(ingredient => { 
        return { 
            ingredientid: ingredient.ingredientid, 
            name: ingredient.name,
            unit: ingredient.unit,
            quantity: ingredient.ingredientquantity,
        }
    }));
  }, [show, form, option]);

  const handleSelectIngredient = (item: any) => {
    let newIngredient = {};
    const index = ingredients.findIndex(
      (ingredient: any) => item.id === ingredient.ingredientid
    );
    if (index > -1) {
      newIngredient = {
        ingredientid: item.id,
        name: item.name,
        unit: item.unit,
        quantity: ingredients[index].quantity + 1,
      };
      let temp = [] as any;
      Object.assign(temp, ingredients);
      temp.splice(index, 1, newIngredient);
      setIngredients(temp);
    } else {
      newIngredient = {
        ingredientid: item.id,
        name: item.name,
        unit: item.unit,
        quantity: 1,
      };
      setIngredients(ingredients.concat(newIngredient));
    }
  };

  const handleRemoveIngredient = (item: any) => {
    setIngredients(
      ingredients.filter(
        (ingredient: any) => ingredient.ingredientid !== item.ingredientid
      )
    );
  };

  const handleQuantityChange = (
    item: any,
    e: ChangeEvent<HTMLInputElement>
  ) => {
    let newIngredient = {};
    const index = ingredients.findIndex(
      (ingredient: any) => ingredient.ingredientid === item.ingredientid
    );
    if (index > -1) {
      newIngredient = {
        ingredientid: item.ingredientid,
        name: item.name,
        unit: item.unit,
        quantity: e.target.value,
      };
      let temp = [] as any;
      Object.assign(temp, ingredients);
      temp.splice(index, 1, newIngredient);
      setIngredients(temp);
    }
  };

  const handleUpdateOptionToItem = async () => {
    ingredients.forEach((ingredient: any) => {
      delete ingredient.name;
      delete ingredient.unit;
    });
    const sendData = {
      ...formValue,
      price: parseInt(formValue.price),
      ingredients: ingredients,
    };
    setLoading(true);
    await onFinish(itemId, sendData);
    setLoading(false);
    setFormValue({
        id: option?.id,
        name: option?.name,
        price: option?.price,
        note: option?.note,
    });
        form?.setFieldsValue({
        name: option?.name,
        price: option?.price,
        note: option?.note,
    });
    setIngredients(option?.ingredients?.map(ingredient => { 
        return { 
            ingredientid: ingredient.ingredientid, 
            quantity: ingredient.ingredientquantity,
        }
    }));
    toggleModal();
  };

  return (
    <Drawer
      visible={show}
      width="500"
      onClose={() => {
        form.resetFields();
        toggleModal();
        setIngredients([]);
      }}
      zIndex={1}
    >
      <Scrollbars autoHide>
        <Row style={{ margin: 20 }} justify="center" align="middle">
          <Col span={24}>
            <SubHeaderText style={{ textAlign: "center" }} className="mb-4">
              Chỉnh sửa tùy chọn
            </SubHeaderText>
            <Form
              form={form}
              name="addOptionToItemForm"
              onFinish={handleUpdateOptionToItem}
            >
              <Row gutter={24} justify="center">
                <Col span={24}>
                  <Form.Item
                    name="name"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền tên tùy chọn",
                      },
                    ]}
                  >
                    <Input
                      placeholder="Tên tùy chọn"
                      value={formValue.name || ""}
                      disabled={true}
                      onChange={(e) =>
                        setFormValue({ ...formValue, name: e.target.value })
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row justify="space-between">
                <Col span={24}>
                  <Form.Item
                    name="price"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền giá tiền",
                      },
                    ]}
                  >
                    <Input
                      type="number"
                      placeholder="Giá tiền"
                      value={formValue.price || ""}
                      onChange={(e) =>
                        setFormValue({
                          ...formValue,
                          price: e.target.value,
                        })
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row justify="center">
                <Col span={24}>
                  <Form.Item
                    name="note"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền ghi chú",
                      },
                    ]}
                  >
                    <Input.TextArea
                      showCount
                      maxLength={100}
                      autoSize={{ minRows: 3, maxRows: 3 }}
                      placeholder="Ghi chú"
                      value={formValue.note || ""}
                      onChange={(e) =>
                        setFormValue({
                          ...formValue,
                          note: e.target.value,
                        })
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Typography.Title level={5}>Nguyên liệu</Typography.Title>
              </Row>
              <Row className="mt-2 mb-5">
                <Col span={24}>
                  <AutoComplete
                    allowClear
                    onSelect={() => setAutoCompleteText("")}
                    onChange={(value) => {
                      setAutoCompleteText(value);
                    }}
                    value={autoCompleteText}
                    style={{ width: "100%" }}
                    placeholder="Nguyên liệu"
                    filterOption={(inputValue, option) =>
                      option!.value
                        .toUpperCase()
                        .indexOf(inputValue.toUpperCase()) !== -1
                    }
                  >
                    {ingredientList
                      .filter(
                        (ingredient) => ingredient.statuscode === "AVAILABLE"
                      )
                      .map((ingredient) => (
                        <AutoComplete.Option
                          key={ingredient.id}
                          value={ingredient.name}
                        >
                          <Row
                            key={ingredient.id}
                            onClick={(e) => handleSelectIngredient(ingredient)}
                          >
                            {ingredient.name}
                          </Row>
                        </AutoComplete.Option>
                      ))}
                  </AutoComplete>
                </Col>
              </Row>
              <Row justify="center">
                <Col>
                  <Timeline>
                    {ingredients?.map((item: any, index: number) => (
                      <Timeline.Item key={index}>
                        <Row
                          justify="space-between"
                          className="mb-3"
                          gutter={24}
                        >
                          <Col span={8}>
                            <Input value={item?.name} disabled />
                          </Col>
                          <Col span={6}>
                            <Input
                              type="number"
                              defaultValue={1}
                              min={1}
                              value={item.quantity}
                              onChange={(e) => handleQuantityChange(item, e)}
                            />
                          </Col>
                          <Col span={6}>
                            <Input value={item?.unit} disabled />
                          </Col>
                          <Col span={4}>
                            <Button
                              onClick={() => {
                                handleRemoveIngredient(item);
                              }}
                              type="text"
                            >
                              <CIcon name="cil-delete"></CIcon>
                            </Button>
                          </Col>
                        </Row>
                      </Timeline.Item>
                    ))}
                  </Timeline>
                </Col>
              </Row>
              <Row justify="center">
                <Form.Item>
                  <Button loading={loading} type="primary" htmlType="submit">
                    Lưu
                  </Button>
                </Form.Item>
              </Row>
            </Form>
          </Col>
        </Row>
      </Scrollbars>
    </Drawer>
  );
};

export default AddOptionDrawer;
