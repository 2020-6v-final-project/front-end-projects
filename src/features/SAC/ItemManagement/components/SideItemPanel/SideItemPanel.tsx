import { Item, ItemType } from "@shared";
import {
  Col,
  Divider,
  Empty,
  Input,
  List,
  Popover,
  Row,
  Spin,
  Tabs,
  Select,
} from "antd";
import React, { useEffect, useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import ItemFromList from "../ItemFromList/ItemFromList";
import "./SideItemPanel.scss";

type ItemSidePanelProps = {
  items: Array<Item>;
  currentItem?: Item;
  itemTypeList: Array<ItemType>;
};

type ItemSidePanelDispatch = {
  handleItemSelect: Function;
};

type ItemSidePanelDefaultProps = ItemSidePanelProps & ItemSidePanelDispatch;

const SidePanel: React.FC<ItemSidePanelDefaultProps> = React.memo((props) => {
  const { items, currentItem, itemTypeList } = props;
  const { handleItemSelect } = props;
  const [loading, setLoading] = useState(true);
  const [filtered, setFiltered] = useState(items);
  const [shouldRender, setShouldRender] = useState(false);

  useEffect(() => {
    if (items.length < 1) {
      setLoading(false);
      setShouldRender(false);
    } else {
      setLoading(false);
      setFiltered(items);
      setShouldRender(true);
    }
  }, [items]);

  const handleSearch = (value: string) => {
    if (!value) {
      setFiltered(items);
    } else {
      const temp = items.filter(
        (item) =>
          item.name.toLocaleLowerCase().includes(value.toLowerCase()) ||
          item.price === Number(value)
      );
      setFiltered(temp);
    }
  };

  return (
    <Col span={7} style={{ height: "100%" }} className="side-menu">
      <Row justify="space-between" gutter={16}>
        <Col span={14}>
          <Input
            style={{ width: "100%" }}
            onChange={(e) => {
              handleSearch(e.target.value);
            }}
            className="mb-2"
            placeholder="Tìm kiếm món ăn"
          />
        </Col>
        <Col span={10}>
          <Select
            style={{ width: "100%" }}
            placeholder="Sắp xếp"
            defaultValue={"name_asc"}
            defaultActiveFirstOption
          >
            <Select.Option value="name_asc" key={1}>
              Tên tăng dần
            </Select.Option>
            <Select.Option value="name_desc" key={2}>
              Tên giảm dần
            </Select.Option>
            <Select.Option value="baseprice_asc" key={3}>
              Giá tăng dần
            </Select.Option>
            <Select.Option value="baseprice_desc" key={4}>
              Giá giảm dần
            </Select.Option>
          </Select>
        </Col>
      </Row>

      <Row justify="center">
        <Tabs
          centered
          defaultActiveKey="1"
          style={{ width: "100%", padding: "0px" }}
        >
          <Tabs.TabPane tab="Tất cả" key="1">
            <Row style={{ height: "80vh", padding: "0px" }} justify="center">
              <Col span={23}>
                {loading ? (
                  <Spin
                    size="large"
                    style={{ textAlign: "center", margin: "auto" }}
                    spinning={loading}
                  />
                ) : (
                  <>
                    {shouldRender ? (
                      <>
                        <Scrollbars>
                          <List className="list-item">
                            {filtered.map((item, index) => (
                              <Popover key={index} content={item.description}>
                                <List.Item
                                  className={
                                    currentItem?.id === item.id
                                      ? "active-item-from-list"
                                      : ""
                                  }
                                  onClick={() => handleItemSelect(item)}
                                >
                                  <ItemFromList item={item} />
                                </List.Item>
                              </Popover>
                            ))}
                          </List>
                        </Scrollbars>
                      </>
                    ) : (
                      <Col span={24}>
                        <Empty
                          style={{ marginTop: "30vh" }}
                          description="Hiện chưa có món nào!"
                        />
                      </Col>
                    )}
                  </>
                )}
              </Col>
              <Col span={1}>
                <Divider type="vertical" style={{ height: "100%" }} />
              </Col>
            </Row>
          </Tabs.TabPane>
          {itemTypeList?.map((itemType, itemTypeIndex) => (
            <Tabs.TabPane
              tab={itemType.name}
              key={(itemTypeIndex + 2).toString()}
            >
              <Row style={{ height: "65vh", padding: "0px" }} justify="center">
                <Col span={23}>
                  {loading ? (
                    <Spin
                      size="large"
                      style={{ textAlign: "center", margin: "auto" }}
                      spinning={loading}
                    />
                  ) : (
                    <>
                      {shouldRender ? (
                        <>
                          <Scrollbars>
                            <List className="list-item">
                              {filtered
                                .filter((item) => item.typeid === itemType.id)
                                .map((item, index) => (
                                  <Popover
                                    key={index}
                                    content={item.description}
                                  >
                                    <List.Item
                                      className={
                                        currentItem?.id === item.id
                                          ? "active-item-from-list"
                                          : ""
                                      }
                                      onClick={() => handleItemSelect(item)}
                                    >
                                      <ItemFromList key={index} item={item} />
                                    </List.Item>
                                  </Popover>
                                ))}
                            </List>
                          </Scrollbars>
                        </>
                      ) : (
                        <Col span={24}>
                          <Empty
                            style={{ marginTop: "30vh" }}
                            description="Hiện chưa có món nào!"
                          />
                        </Col>
                      )}
                    </>
                  )}
                </Col>
                <Col span={1}>
                  <Divider type="vertical" style={{ height: "100%" }} />
                </Col>
              </Row>
            </Tabs.TabPane>
          ))}
        </Tabs>
      </Row>
    </Col>
  );
});

export default SidePanel;
