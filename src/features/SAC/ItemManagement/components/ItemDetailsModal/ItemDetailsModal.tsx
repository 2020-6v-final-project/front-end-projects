import { SubHeaderText } from "@components";
import CIcon from "@coreui/icons-react";
import { Ingredient, Item, ItemType } from "@custom-types";
import {
  AutoComplete,
  Button,
  Col,
  Drawer,
  Form,
  Input,
  Row,
  Timeline,
  Select,
  Typography,
  Upload,
} from "antd";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import { uploadFileToBlob } from "../../../../Azure/azure-storage-blob";
import React, { ChangeEvent, useEffect, useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import "./ItemDetailsModal.scss";
import swal from "sweetalert";

type ItemDetailsModalProps = {
  item: Item | undefined;
  itemTypeList: Array<ItemType>;
  ingredientList: Array<Ingredient>;
  show: boolean;
  onFinish: Function;
  toggleModal: Function;
  azureKey: string;
};

const ItemDetailsModal: React.FC<ItemDetailsModalProps> = (props) => {
  const {
    show,
    toggleModal,
    onFinish,
    ingredientList,
    item,
    itemTypeList,
    azureKey,
  } = props;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [ingredients, setIngredients] = useState<any>(item?.ingredients);
  const [loadImg, setLoadImg] = useState<boolean>(false);
  const [imageUrl, setImageUrl] = useState<any>("");
  const [autoCompleteText, setAutoCompleteText] = useState("");
  const [formValue, setFormValue] = useState<any>({
    name: "",
    price: 0,
    unit: "",
    recipe: "",
    description: "",
    typeid: "",
  });

  useEffect(() => {
    if (item) {
      setFormValue({
        name: item.name,
        price: item.price,
        unit: item.unit,
        recipe: item.recipe,
        description: item.description,
        typeid: item.typeid,
      });
      form?.setFieldsValue({
        name: item?.name,
        price: item?.price,
        unit: item?.unit,
        description: item?.description,
        recipe: item?.recipe,
        typeid: item?.typeid,
      });
      setIngredients(item.ingredients);
    }
  }, [item, form, show]);

  const handleSelectIngredient = (item: any) => {
    let newIngredient = {};
    const index = ingredients.findIndex(
      (ingredient: any) => item.id === ingredient.ingredientid
    );
    if (index > -1) {
      newIngredient = {
        ingredientid: item.id,
        name: item.name,
        unit: item.unit,
        quantity: parseInt(ingredients[index].quantity) + 1,
      };
      let temp = [] as any;
      Object.assign(temp, ingredients);
      temp.splice(index, 1, newIngredient);
      setIngredients(temp);
    } else {
      newIngredient = {
        ingredientid: item.id,
        name: item.name,
        unit: item.unit,
        quantity: 1,
      };
      setIngredients(ingredients.concat(newIngredient));
    }
  };

  const handleRemoveIngredient = (item: any) => {
    setIngredients(
      ingredients.filter(
        (ingredient: any) => ingredient.ingredientid !== item.ingredientid
      )
    );
  };

  const handleQuantityChange = (
    item: any,
    e: ChangeEvent<HTMLInputElement>
  ) => {
    let newIngredient = {};
    const index = ingredients.findIndex(
      (ingredient: any) => ingredient.ingredientid === item.ingredientid
    );
    if (index > -1) {
      newIngredient = {
        ingredientid: item.ingredientid,
        name: item.name,
        unit: item.unit,
        quantity: e.target.value,
      };
      let temp = [] as any;
      Object.assign(temp, ingredients);
      temp.splice(index, 1, newIngredient);
      setIngredients(temp);
    }
  };

  const handleUpdateItem = async () => {
    let tempIngredients = [...ingredients] as any;
    tempIngredients = tempIngredients.map((ingredient: any) => {
      let tempIngredient = {
        ...ingredient,
        itemid: item?.id,
        quantity: parseInt(ingredient.quantity),
      };
      delete tempIngredient.name;
      delete tempIngredient.unit;
      return tempIngredient;
    });
    const sendData = {
      ...formValue,
      id: item?.id,
      price: parseInt(formValue.price),
      ingredients: tempIngredients,
      imgpath: imageUrl,
    };
    setLoading(true);
    await onFinish(sendData);
    setLoading(false);
    setImageUrl("");
    setLoadImg(false);
    toggleModal();
  };

  const setInitialFormData = () => {
    form.setFieldsValue({
      name: item?.name,
      price: item?.price,
      unit: item?.unit,
      description: item?.description,
      recipe: item?.recipe,
    });
    setIngredients(item?.ingredients);
  };

  const handleImgChange = async (info: any) => {
    const isJpgOrPng =
      info.file.type === "image/jpeg" || info.file.type === "image/png";
    const isLt2M = info.file.size / 1024 / 1024 < 2;
    setLoadImg(true);
    if (!isJpgOrPng) {
      swal("Thất bại", "Bạn chỉ có thể tải file JPG/PNG!", "error");
    } else if (!isLt2M) {
      swal("Thất bại", "Kích cỡ file vượt quá 2MB!", "error");
    } else if (imageUrl !== "") {
      setImageUrl("");
    } else if (info.file) {
      const responseIMG: any = await uploadFileToBlob(info.file, azureKey);
      setImageUrl(responseIMG.url);
      setLoadImg(false);
    }
  };

  return (
    <Drawer
      visible={show}
      width="500"
      onClose={() => {
        toggleModal();
        setInitialFormData();
      }}
      zIndex={1}
      className="add-item-modal"
    >
      <Scrollbars autoHide>
        <Row style={{ margin: 20 }} justify="center" align="middle">
          <Col>
            <SubHeaderText style={{ textAlign: "center" }} className="mb-4">
              Chỉnh sửa món ăn
            </SubHeaderText>
            <Row justify="center" className="mb-3">
              <Upload
                name="avatar"
                listType="picture-card"
                className="avatar-uploader upload-img"
                showUploadList={false}
                beforeUpload={() => false}
                onChange={(e) => handleImgChange(e)}
              >
                {imageUrl ? (
                  <img src={imageUrl} alt="avatar" style={{ width: "100%" }} />
                ) : (
                  <div>
                    {loadImg ? <LoadingOutlined /> : <PlusOutlined />}
                    <div style={{ marginTop: 8 }}>Tải ảnh</div>
                  </div>
                )}
              </Upload>
            </Row>
            <Form form={form} name="addItemForm" onFinish={handleUpdateItem}>
              <Row justify="center">
                <Col>
                  <Form.Item
                    name="name"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền tên món",
                      },
                    ]}
                    initialValue={item?.name}
                  >
                    <Input
                      style={{
                        border: "none",
                        textAlign: "center",
                        fontSize: 18,
                        fontWeight: 400,
                      }}
                      placeholder="Tên món"
                      onChange={(e) =>
                        setFormValue({ ...formValue, name: e.target.value })
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={24} justify="space-between">
                <Col span={8}>
                  <Form.Item
                    name="price"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền giá tiền",
                      },
                    ]}
                    initialValue={item?.price}
                  >
                    <Input
                      type="number"
                      placeholder="Giá tiền"
                      value={formValue.price || ""}
                      onChange={(e) =>
                        setFormValue({
                          ...formValue,
                          price: e.target.value,
                        })
                      }
                    />
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item
                    name="unit"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền đơn vị",
                      },
                    ]}
                    initialValue={item?.unit}
                  >
                    <Input
                      placeholder="Đơn vị"
                      value={formValue.unit || ""}
                      onChange={(e) =>
                        setFormValue({ ...formValue, unit: e.target.value })
                      }
                    />
                  </Form.Item>
                </Col>

                <Col span={8}>
                  <Form.Item
                    name="typeid"
                    rules={[
                      {
                        required: false,
                        message: "Vui lòng chọn loại món",
                      },
                    ]}
                    initialValue={item?.typeid}
                  >
                    <Select
                      placeholder="Loại món"
                      value={formValue.typeid || ""}
                      onChange={(value) =>
                        setFormValue({ ...formValue, typeid: value })
                      }
                    >
                      {itemTypeList?.map((itemType) => (
                        <Select.Option key={itemType.id} value={itemType.id}>
                          {itemType.name}
                        </Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
              <Row justify="center">
                <Col span={24}>
                  <Form.Item
                    name="description"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền mô tả",
                      },
                    ]}
                    initialValue={item?.description}
                  >
                    <Input.TextArea
                      showCount
                      maxLength={100}
                      autoSize={{ minRows: 3, maxRows: 3 }}
                      placeholder="Mô tả"
                      value={formValue.description || ""}
                      onChange={(e) =>
                        setFormValue({
                          ...formValue,
                          description: e.target.value,
                        })
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row justify="center">
                <Col span={24}>
                  <Form.Item
                    name="recipe"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền mô tả",
                      },
                    ]}
                    initialValue={item?.recipe}
                  >
                    <Input.TextArea
                      autoSize={{ minRows: 3, maxRows: 3 }}
                      placeholder="Công thức"
                      value={formValue.description || ""}
                      onChange={(e) =>
                        setFormValue({ ...formValue, recipe: e.target.value })
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Typography.Title level={5}>Nguyên liệu</Typography.Title>
              </Row>
              <Row gutter={24} className="mt-2 mb-5">
                <Col span={24}>
                  <AutoComplete
                    allowClear
                    value={autoCompleteText}
                    onChange={(value) => setAutoCompleteText(value)}
                    onSelect={() => setAutoCompleteText("")}
                    style={{ width: "100%" }}
                    placeholder="Nguyên liệu"
                    filterOption={(inputValue, option) =>
                      option!.value
                        .toUpperCase()
                        .indexOf(inputValue.toUpperCase()) !== -1
                    }
                  >
                    {ingredientList.map((item: any) => (
                      <AutoComplete.Option key={item.id} value={item.name}>
                        <Row
                          key={item.id}
                          onClick={() => handleSelectIngredient(item)}
                        >
                          {item.name}
                        </Row>
                      </AutoComplete.Option>
                    ))}
                  </AutoComplete>
                </Col>
              </Row>
              <Row justify="center">
                <Col>
                  <Timeline>
                    {ingredients?.map((item: any, index: number) => (
                      <Timeline.Item key={index}>
                        <Row
                          justify="space-between"
                          className="mb-3"
                          gutter={24}
                        >
                          <Col span={8}>
                            <Input value={item?.name} disabled />
                          </Col>
                          <Col span={6}>
                            <Input
                              type="number"
                              defaultValue={1}
                              min={1}
                              value={item.quantity}
                              onChange={(e) => handleQuantityChange(item, e)}
                            />
                          </Col>
                          <Col span={6}>
                            <Input value={item?.unit} disabled />
                          </Col>
                          <Col span={4}>
                            <Button
                              onClick={() => {
                                handleRemoveIngredient(item);
                              }}
                              type="text"
                            >
                              <CIcon name="cil-delete"></CIcon>
                            </Button>
                          </Col>
                        </Row>
                      </Timeline.Item>
                    ))}
                  </Timeline>
                </Col>
              </Row>
              <Row justify="center">
                <Form.Item>
                  <Button loading={loading} type="primary" htmlType="submit">
                    Lưu
                  </Button>
                </Form.Item>
              </Row>
            </Form>
          </Col>
        </Row>
      </Scrollbars>
    </Drawer>
  );
};

export default ItemDetailsModal;
