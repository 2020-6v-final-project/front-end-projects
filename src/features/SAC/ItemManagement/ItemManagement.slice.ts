import { 
    getItemList, 
    updateItem, 
    addItem, 
    reactivateItem, 
    deactivateItem,
    getOptionList,
    addOptionToItem,
    updateOptionOfItem,
    deleteOptionFromItem,
} from '@apis';
import { Item, Option } from '@custom-types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { orderBy } from 'lodash';
import { AppThunk } from 'src/store';
import swal from 'sweetalert';

type ItemManagementInitialState = {
    itemList: Array<Item>,
    optionList: Array<Option>,
}

const initialState: ItemManagementInitialState = {
    itemList: [],
    optionList: [],
}

const ItemManagementSlice = createSlice({
    name: 'itemManagement',
    initialState,
    reducers: {
        getItemsSuccess: (state, action: PayloadAction<Item[]>) => {
            state.itemList = action.payload;
            state.itemList = orderBy(state.itemList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        getItemsFailed: (state) => {
            return state;
        },
        addItemSuccess: (state, action: PayloadAction<Item>) => {
            state.itemList?.push(action.payload);
            state.itemList = orderBy(state.itemList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        addItemFailed: (state) => {
            return state;
        },
        updateItemSuccess: (state, action: PayloadAction<Item>) => {
            const index = state.itemList?.findIndex(item => item.id === action.payload.id);
            if (index > -1) {
                state.itemList?.splice(index, 1, action.payload);
            }
        },
        updateItemFailed: (state) => {
            return state;
        },
        removeItemSuccess: (state, action: PayloadAction<number>) => {
            state.itemList = state.itemList.filter((item: Item) => item.id !== action.payload);
            return state;
        },
        removeItemFailed: (state) => {
            return state;
        },
        deactivateItemSuccess: (state, action: PayloadAction<Item>) => {
            const index = state.itemList?.findIndex(item => item.id === action.payload.id);
            if (index > -1) {
                state.itemList?.splice(index, 1, action.payload);
            }
            return state;
        },
        deactivateItemFailed: (state) => {
            return state;
        },
        reactivateItemSuccess: (state, action: PayloadAction<Item>) => {
            const index = state.itemList?.findIndex(item => item.id === action.payload.id);
            if (index > -1) {
                state.itemList?.splice(index, 1, action.payload);
            }
            return state;
        },
        reactivateItemFailed: (state) => {
            return state;
        },
        // Item Option
        getOptionsSuccess: (state, action: PayloadAction<Option[]>) => {
            state.optionList = action.payload;
            state.optionList = orderBy(state.optionList, ["id"], ["asc"]);
            return state;
        },
        getOptionsFailed: (state) => {
            return state;
        },
        addOptionToItemSuccess: (state, action: PayloadAction<Item>) => {
            const index = state.itemList?.findIndex(item => item.id === action.payload.id);
            if (index > -1) {
                state.itemList?.splice(index, 1, action.payload);
            }
        },
        addOptionToItemFailed: (state) => {
            return state;
        },
        updateOptionOfItemSuccess: (state, action: PayloadAction<Item>) => {
            const index = state.itemList?.findIndex(item => item.id === action.payload.id);
            if (index > -1) {
                state.itemList?.splice(index, 1, action.payload);
            }
        },
        updateOptionOfItemFailed: (state) => {
            return state;
        },
        deleteOptionFromItemSuccess: (state, action: PayloadAction<Item>) => {
            const index = state.itemList?.findIndex(item => item.id === action.payload.id);
            if (index > -1) {
                state.itemList?.splice(index, 1, action.payload);
            }
        },
        deleteOptionFromItemFailed: (state) => {
            return state;
        }
    }

})

export const getItemAsync = (): AppThunk => async (dispatch) => {
    try {
        await getItemList((data: Item[]) => {
            dispatch(getItemsSuccess(data));
        });
    } catch (e) {
        swal("Lấy danh sách món ăn thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(getItemsFailed());
    }
}

export const updateItemAsync = (item: any): AppThunk => async (dispatch) => {
    try {
        await updateItem(item, (data: Item) => {
            swal("Thành công", "Món ăn đã được cập nhật!", "success");
            dispatch(updateItemSuccess(data));
        });
    } catch (e) {
        swal("Cập nhập món ăn thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(updateItemFailed());
    }
}

export const addItemAsync = (item: any): AppThunk => async (dispatch) => {
    try {
        await addItem(item, (data: Item) => {
            swal("Thành công", "Món ăn đã được thêm!", "success");
            dispatch(addItemSuccess(data));
        });
    } catch (e) {
        swal("Thêm món ăn thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(addItemFailed());
    }
}

export const deactivateItemAsync = (itemId: number): AppThunk => async (dispatch) => {
    try {
        await deactivateItem(itemId, (data: Item) => {
            swal("Thành công", "Món ăn đã bị khóa", "success");
            dispatch(deactivateItemSuccess(data));
        })
    } catch (e) {
        swal("Khóa món ăn thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(deactivateItemFailed());
    }
}

export const reactivateItemAsync = (itemId: number): AppThunk => async (dispatch) => {
    try {
        await reactivateItem(itemId, (data: Item) => {
            swal("Thành công", "Món ăn đã được mở khóa", "success");
            dispatch(reactivateItemSuccess(data));
        })
    } catch (e) {
        swal("Mở khóa món ăn thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(reactivateItemFailed());
    }
}

export const getOptionsAsync = (): AppThunk => async (dispatch) => {
    try {
        await getOptionList((data: Option[]) => {
            dispatch(getOptionsSuccess(data));
        })
    } catch (e) {
        swal("Lấy danh sách option thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(getOptionsFailed());
    }
}

export const addOptionToItemAsync = (itemId: number, option: any): AppThunk => async (dispatch) => {
    try {
        await addOptionToItem(itemId, option, (data: Item) => {
            swal("Thành công", "Tùy chọn đã được thêm vào món ăn", "success");
            dispatch(addOptionToItemSuccess(data));
        })
    } catch (e) {
        swal("Thêm tùy chọn vào món ăn thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(addOptionToItemFailed());
    }
}

export const updateOptionOfItemAsync = (itemId: number, option: any): AppThunk => async (dispatch) => {
    try {
        await updateOptionOfItem(itemId, option, (data: Item) => {
            swal("Thành công", "Tùy chọn đã được cập nhập", "success");
            dispatch(updateOptionOfItemSuccess(data));
        })
    } catch (e) {
        swal("Cập nhập tùy chọn thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(updateOptionOfItemFailed());
    }
}

export const deleteOptionFromItemAsync = (itemId: number, optionId: number): AppThunk => async (dispatch) => {
    try {
        await deleteOptionFromItem(itemId, optionId, (data: Item) => {
            swal("Thành công", "Đã xóa tùy chọn khỏi món ăn", "success");
            dispatch(deleteOptionFromItemSuccess(data));
        })
    } catch (e) {
        swal("Xóa tùy chọn thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(deleteOptionFromItemFailed());
    }
}

const {
    getItemsSuccess,
    getItemsFailed,
    addItemSuccess,
    addItemFailed,
    updateItemSuccess,
    updateItemFailed,
    // removeItemSuccess,
    // removeItemFailed,
    deactivateItemSuccess,
    deactivateItemFailed,
    reactivateItemSuccess,
    reactivateItemFailed,
    getOptionsSuccess,
    getOptionsFailed,
    addOptionToItemSuccess,
    addOptionToItemFailed,
    updateOptionOfItemSuccess,
    updateOptionOfItemFailed,
    deleteOptionFromItemSuccess,
    deleteOptionFromItemFailed
} = ItemManagementSlice.actions;

export default ItemManagementSlice.reducer;
