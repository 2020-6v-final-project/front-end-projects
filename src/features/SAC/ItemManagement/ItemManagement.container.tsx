import { FAB } from "@components";
import { CFade } from "@coreui/react";
import { Ingredient, Item, ItemType, Option } from "@shared";
import { history } from "@shared/components";
import { message, Row } from "antd";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { RootState } from "src/store";
import { selectAzureConfig } from "../../Azure/AzureManagement.selector";
import { getKeyAsync } from "../../Azure/AzureManagement.slice";
import { selectIngredientList } from "../IngredientManagement/IngredientManagement.selector";
import { getIngredientAsync } from "../IngredientManagement/IngredientManagement.slice";
import { selectItemTypeList } from "../ItemIngredientTypeManagement/ItemIngredientTypeManagement.selector";
import { getItemTypeAsync } from "../ItemIngredientTypeManagement/ItemIngredientTypeManagement.slice";
import AddItemModal from "./components/AddItemModal/AddItemModal";
import ItemDetailsModal from "./components/ItemDetailsModal/ItemDetailsModal";
import MainItemPanel from "./components/MainItemPanel/MainItemPanel";
import SideItemPanel from "./components/SideItemPanel/SideItemPanel";
import "./ItemManagement.selector";
import { selectItemList, selectOptionList } from "./ItemManagement.selector";
import {
  addItemAsync,
  addOptionToItemAsync,
  deactivateItemAsync,
  deleteOptionFromItemAsync,
  getItemAsync,
  getOptionsAsync,
  reactivateItemAsync,
  updateItemAsync,
  updateOptionOfItemAsync,
} from "./ItemManagement.slice";

type ItemManagementState = {
  itemList: Array<Item>;
  itemTypeList: Array<ItemType>;
  ingredientList: Array<Ingredient>;
  optionList: Array<Option>;
  match: any;
  azureKey: string;
};

type ItemManagementDispatch = {
  getItemAsync: Function;
  updateItemAsync: Function;
  addItemAsync: Function;
  reactivateItemAsync: Function;
  deactivateItemAsync: Function;
  getIngredientAsync: Function;
  getItemTypeAsync: Function;
  getKeyAsync: Function;
  getOptionsAsync: Function;
  addOptionToItemAsync: Function;
  updateOptionOfItemAsync: Function;
  deleteOptionFromItemAsync: Function;
};

type ItemManagementDefaultProps = ItemManagementState & ItemManagementDispatch;

const ItemManagementContainer: React.FC<ItemManagementDefaultProps> = React.memo(
  (props) => {
    const {
      itemList,
      ingredientList,
      match,
      itemTypeList,
      azureKey,
      optionList,
    } = props;
    const {
      getItemAsync,
      addItemAsync,
      deactivateItemAsync,
      reactivateItemAsync,
      getIngredientAsync,
      updateItemAsync,
      getItemTypeAsync,
      getKeyAsync,
      getOptionsAsync,
      addOptionToItemAsync,
      updateOptionOfItemAsync,
      deleteOptionFromItemAsync,
    } = props;

    const [currentItem, setcurrentItem] = useState<Item | undefined>(
      itemList[0] || undefined
    );
    const [addItemModalIsOpen, toggleAddItemModal] = useState(false);
    const [itemDetailsModalIsOpen, toggleItemDetailsModal] = useState(false);

    useEffect(() => {
      const fetchData = async () => {
        await getIngredientAsync();
        await getItemAsync();
        await getItemTypeAsync();
        await getKeyAsync();
        await getOptionsAsync();
      };
      fetchData();
    }, [
      getItemAsync,
      getIngredientAsync,
      getItemTypeAsync,
      getKeyAsync,
      getOptionsAsync,
    ]);

    useEffect(() => {
      if (itemList.length > 0 && currentItem) {
        setcurrentItem(itemList.find((item) => item.id === currentItem.id));
      }
    }, [itemList, currentItem]);

    useEffect(() => {
      if (itemList.length > 0) {
        if (!match.params.id) {
          setcurrentItem(itemList[0]);
          history.push("/item-management");
          return;
        }
        const target = itemList.find(
          (item) => item.id === Number(match.params.id)
        );
        if (target) {
          setcurrentItem(target);
        } else {
          history.push("/item-management");
          message.error("Không tìm thấy món!");
        }
      }
    }, [match.params.id, itemList]);

    const handleItemSelect = (item: Item) => {
      setcurrentItem(item);
      history.push("/item-management/" + item.id);
    };

    const handleAddItem = async (value: any) => {
      await addItemAsync(value);
    };

    const handleUpdateItem = async (value: any) => {
      await updateItemAsync(value);
    };

    const handleDeactivateOrReactivateItem = async (
      itemId: number,
      statusCode: string
    ) => {
      if (statusCode === "AVAILABLE") await deactivateItemAsync(itemId);
      else await reactivateItemAsync(itemId);
    };

    return (
      <CFade>
        <div style={{ overflow: "hidden" }}>
          <Row gutter={48}>
            <SideItemPanel
              items={itemList}
              currentItem={currentItem}
              itemTypeList={itemTypeList}
              handleItemSelect={handleItemSelect}
            />
            <MainItemPanel
              item={currentItem}
              ingredientList={ingredientList}
              optionList={optionList}
              addOptionToItemAsync={addOptionToItemAsync}
              updateOptionOfItemAsync={updateOptionOfItemAsync}
              deleteOptionFromItemAsync={deleteOptionFromItemAsync}
              handleEditButtonClick={() =>
                toggleItemDetailsModal(!itemDetailsModalIsOpen)
              }
              handleDeactivateOrReactivate={handleDeactivateOrReactivateItem}
            />
          </Row>
        </div>

        <FAB callback={() => toggleAddItemModal(!addItemModalIsOpen)} />
        <AddItemModal
          azureKey={azureKey}
          show={addItemModalIsOpen}
          onFinish={handleAddItem}
          toggleModal={toggleAddItemModal}
          ingredientList={ingredientList}
          itemTypeList={itemTypeList}
        ></AddItemModal>
        <ItemDetailsModal
          azureKey={azureKey}
          show={itemDetailsModalIsOpen}
          onFinish={handleUpdateItem}
          toggleModal={toggleItemDetailsModal}
          item={currentItem}
          ingredientList={ingredientList}
          itemTypeList={itemTypeList}
        />
      </CFade>
    );
  }
);

const mapState = (state: RootState) => {
  return {
    itemList: selectItemList(state),
    ingredientList: selectIngredientList(state),
    itemTypeList: selectItemTypeList(state),
    azureKey: selectAzureConfig(state),
    optionList: selectOptionList(state),
  };
};
const mapDispatch = {
  getItemAsync,
  updateItemAsync,
  addItemAsync,
  reactivateItemAsync,
  deactivateItemAsync,
  getIngredientAsync,
  getItemTypeAsync,
  getKeyAsync,
  getOptionsAsync,
  addOptionToItemAsync,
  updateOptionOfItemAsync,
  deleteOptionFromItemAsync,
};

export default connect(mapState, mapDispatch)(ItemManagementContainer);
