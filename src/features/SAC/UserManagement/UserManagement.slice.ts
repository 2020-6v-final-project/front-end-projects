import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AppThunk } from "src/store";
import { User } from "@custom-types";
import {
  getUserList,
  addUser,
  updateUser,
  deactivateUser,
  reactivateUser,
} from "@apis";
import swal from "sweetalert";

// Types and state declaration
type UserManagementState = {
  UserList: User[];
};

const initialState: UserManagementState = {
  UserList: []
};

// Slice
const UserManagementSlice = createSlice({
  name: "userManagement",
  initialState,
  reducers: {
    addUserSuccess: (state, action: PayloadAction<User>) => {
      state.UserList.push(action.payload);
      return state;
    },
    addUserFailed: (state) => {
      return state;
    },
    updateUserSuccess: (state, action: PayloadAction<User>) => {
      const index = state.UserList.findIndex(
        (item: User) => item.id === action.payload.id
      );

      if (index > -1) state.UserList.splice(index, 1, action.payload);
      return state;
    },
    updateUserFailed: (state) => {
      return state;
    },
    fetchUserListSuccess: (state, action: PayloadAction<User[]>) => {
      state.UserList = action.payload;
      return state;
    },
    fetchUserListFailed: (state) => {
      return state;
    },
    deactivateUserSuccess: (state, action: PayloadAction<User>) => {
      const index = state.UserList.findIndex(
        (item: User) => item.id === action.payload.id
      );

      if (index > -1) state.UserList.splice(index, 1, action.payload);
      return state;
    },
    deactivateUserFailed: (state) => {
      return state;
    },
    reactivateUserSuccess: (state, action: PayloadAction<User>) => {
      const index = state.UserList.findIndex(
        (item: User) => item.id === action.payload.id
      );

      if (index > -1) state.UserList.splice(index, 1, action.payload);
      return state;
    },
    reactivateUserFailed: (state) => {
      return state;
    }
  }
});

// Thunk for async actions
export const fetchUserListAsync = (): AppThunk => async (dispatch) => {
  try {
    let userList: User[];
    await getUserList((data: User[]) => {
      userList = data;
      userList.sort((item1: User, item2: User) => {
        return item1.id - item2.id;
      });
      dispatch(fetchUserListSuccess(userList));
    });
  } catch (e) {
    dispatch(fetchUserListFailed());
  }
};

export const addUserAsync = (user: any): AppThunk => async (
  dispatch
) => {
  try {
    await addUser(user, (data: User) => {
      swal("Thêm thành công!", "Người dùng đã được thêm vào hệ thống!", "success");
      dispatch(addUserSuccess(data));
    });
  } catch (e) {
    swal("Thêm không thành công!", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
    dispatch(addUserFailed());
  }
};

export const updateUserAsync = (user: any): AppThunk => async (
  dispatch
) => {
  try {
    await updateUser(user, (data: User) => {
      swal("Cập nhật thành công!", "Người dùng đã được cập nhật!", "success");
      dispatch(updateUserSuccess(data));
    });
  } catch (e) {
    swal("Cập nhật không thành công!", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
    dispatch(updateUserFailed());
  }
};

export const deactivateUserAsync = (userId: number): AppThunk => async (dispatch) => {
  try {
    await deactivateUser(userId, (data: User) => {
      swal("Cập nhật thành công!", "Người dùng đã bị khóa!", "success");
      dispatch(deactivateUserSuccess(data));
    });
  } catch (e) {
    swal("Cập nhật không thành công!", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
    dispatch(deactivateUserFailed());
  }
};

export const reactivateUserAsync = (userId: number): AppThunk => async (dispatch) => {
  try {
    await reactivateUser(userId, (data: User) => {
      swal("Cập nhật thành công!", "Người dùng đã được kích hoạt!", "success");
      dispatch(reactivateUserSuccess(data));
    });
  } catch (e) {
    swal("Cập nhật không thành công!", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
    dispatch(reactivateUserFailed());
  }
};

// Export part
export const {
  addUserSuccess,
  addUserFailed,
  fetchUserListSuccess,
  fetchUserListFailed,
  updateUserSuccess,
  updateUserFailed,
  deactivateUserSuccess,
  deactivateUserFailed,
  reactivateUserSuccess,
  reactivateUserFailed,
} = UserManagementSlice.actions;

export default UserManagementSlice.reducer;
