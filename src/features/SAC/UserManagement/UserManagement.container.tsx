import { WidgetsDropDown } from '@components';
import { CFade } from '@coreui/react';
import { Branch, User, Role } from '@custom-types';
import React from 'react';
import { connect } from 'react-redux';
import { RootState } from 'src/store';
import { branchListSelector } from '../BranchManagement/BranchManagement.selector';
import { fetchBranchListAsync } from '../BranchManagement/BranchManagement.slice';
import ListUsers from './components/ListUsers/ListUsers';
import { userListSelector } from './UserManagement.selector';
import { 
    addUserAsync, 
    fetchUserListAsync,
    updateUserAsync,
    deactivateUserAsync,
    reactivateUserAsync,
} from './UserManagement.slice';
import { roleListSelector } from '../RoleManagement/RoleManagement.selector';
import { fetchRoleListAsync } from '../RoleManagement/RoleManagement.slice';
import { getKeyAsync } from "../../Azure/AzureManagement.slice";
import { selectAzureConfig } from "../../Azure/AzureManagement.selector";
import { Scrollbars } from 'react-custom-scrollbars';

type UserManagementState = {
    UserList: User[],
    BranchList: Branch[],
    RoleList: Role[],
    azureKey: string,
}

type UserManagementDispatch = {
    fetchUserListAsync: Function,
    addUserAsync: Function,
    updateUserAsync: Function,
    fetchBranchListAsync: Function,
    fetchRoleListAsync: Function,
    deactivateUserAsync: Function,
    reactivateUserAsync: Function,
    getKeyAsync: Function;
}

export type UserManagementDefaultProps = UserManagementState & UserManagementDispatch;

const UserManagementContainer: React.FC<UserManagementDefaultProps> = React.memo((props) => {
    return (
        <CFade>
            <WidgetsDropDown />
            <Scrollbars
                autoHeightMin={0}
                autoHeightMax={600}
                autoHeight
            >
                <ListUsers {...props} />
            </Scrollbars>
        </CFade>
    )
});

const mapState = (state: RootState) => {
    return {
        UserList: userListSelector(state),
        BranchList: branchListSelector(state),
        RoleList: roleListSelector(state),
        azureKey: selectAzureConfig(state),
    }
}
const mapDispatch = { 
    fetchRoleListAsync,
    fetchBranchListAsync, 
    fetchUserListAsync, 
    addUserAsync, 
    updateUserAsync,
    deactivateUserAsync,
    reactivateUserAsync,
    getKeyAsync,
};

export default connect(mapState, mapDispatch)(UserManagementContainer);
