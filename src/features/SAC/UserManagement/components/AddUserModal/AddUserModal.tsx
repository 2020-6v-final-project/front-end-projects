import {
  CButton,
  CCard,
  CCardBody,
  CForm,
  CInput,
  CInputGroup,
  CModal,
  CSelect,
  CSpinner,
} from "@coreui/react";
import React, { ChangeEvent, FormEvent, useEffect, useState } from "react";
import { Row, Col, Timeline, Button,Input, Select, Typography, Divider, Empty, Upload } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { Branch, Role } from '@custom-types';
import "./AddUserModal.scss";
import CIcon from '@coreui/icons-react';
import { freeSet } from "@coreui/icons";
import swal from 'sweetalert';
import { uploadFileToBlob } from '../../../../Azure/azure-storage-blob';

type AddUserModalState = {
  isShow: boolean,
  branchList: Branch[],
  roleList: Role[],
  azureKey: string,
}

type AddUserModalDispatch = {
  addUserAsync: Function,
  onFinish: Function,
}

type RoleTuple = {
  id: number,
  branchids: number[],
}

type AddUserModalDefaultProps = AddUserModalState & AddUserModalDispatch;

const getAvailableBranch = (roleId: number, roleList: Role[], branchList: Branch[], roleTuples: RoleTuple[]) => {
  const targetRole = roleList.find(role => role.id === roleId);
  if (targetRole && targetRole.isall) {
    const fakeBranch = {
      id: -1,
    } as Branch;
    return [fakeBranch];
  }
  const targetRoleTuple = roleTuples.find(roleRuple => roleRuple.id === roleId);
  if (targetRoleTuple) return branchList.filter(branch => !targetRoleTuple.branchids.includes(branch.id));
  return branchList;
}

const AddUserModal: React.FC<AddUserModalDefaultProps> = (props) => {
  const { addUserAsync, onFinish, isShow, roleList, branchList, azureKey } = props;

  const [loading, setLoading] = useState<boolean>(false);
  const [userRoleTuples, setUserRoleTuples] = useState<RoleTuple[]>([]);
  const [selectedRole, setSelectedRole] = useState<number>(-1);
  const [selectedBranch, setSelectedBranch] = useState<number>(-1);
  
  const [loadImg, setLoadImg] = useState<boolean>(false);
  const [imageUrl, setImageUrl] = useState<any>(null);

  const [formData, setFormData] = useState({
    firstname: "",
    middlename: "",
    lastname: "",
    phone: "",
    dateofbirth: "",
    address: "",
    gender: "MALE",
    email: "",
    imgpath: null,
  });

  useEffect(() => {
    setFormData({
      firstname: "",
      middlename: "",
      lastname: "",
      phone: "",
      dateofbirth: "",
      address: "",
      gender: "MALE",
      email: "",
      imgpath: null,
    });
    if (roleList?.length > 0) {
      setSelectedRole(roleList[0]?.id);
      setSelectedBranch(getAvailableBranch(roleList[0]?.id, roleList, branchList, [])[0]?.id);
    }
    setUserRoleTuples([]);
    setLoadImg(false);
    setImageUrl(null);
  }, [isShow, branchList, roleList]);

  const handleAddRoleTuple = () => {
    const index = userRoleTuples?.findIndex((roleTuple) => roleTuple.id === selectedRole);
    if (index > -1) {
      const newRole = { ...userRoleTuples[index] } as RoleTuple;
      if (!newRole.branchids.includes(selectedBranch) && !roleList?.find(role => role.id === newRole.id)?.isall) {
        newRole.branchids.push(selectedBranch);
        let temp = [] as RoleTuple[];
        Object.assign(temp, userRoleTuples);
        temp.splice(index, 1, newRole);
        setUserRoleTuples(temp);
      }
    } else {
      const roleTuple = { 
        id: selectedRole, 
        branchids: [selectedBranch],
      } as RoleTuple;
      if (roleList?.find(role => role.id === roleTuple.id)?.isall) roleTuple.branchids = [];
      setUserRoleTuples([ ...userRoleTuples, roleTuple ]);
    }
  }

  const handleRemoveBranchRole = (roleTupleId: number, branchId: number) => {
    const roleTupleIndex = userRoleTuples.findIndex(roleTuple => roleTuple.id === roleTupleId);
    if (roleTupleIndex > -1) {
      const tempRoleTuple = userRoleTuples[roleTupleIndex];
      const branchIndex = tempRoleTuple.branchids?.findIndex(Id => Id === branchId);
      if (branchIndex > -1) {
        tempRoleTuple.branchids?.splice(branchIndex, 1);
        const userTempRoleTuple = [ ...userRoleTuples ];
        if (tempRoleTuple && tempRoleTuple.branchids && tempRoleTuple.branchids.length > 0) userTempRoleTuple.splice(userRoleTuples.findIndex(roleTuple => roleTuple.id === roleTupleId), 1, tempRoleTuple);
        else userTempRoleTuple.splice(userRoleTuples.findIndex(roleTuple => roleTuple.id === roleTupleId), 1);
        setUserRoleTuples(userTempRoleTuple);
      }
    }
  }

  const handleRemoveRole = (roleTupleId: number) => setUserRoleTuples(userRoleTuples.filter(roleTuple => roleTuple.id !== roleTupleId));
  
  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const handleImgChange = async (info: any) => {
    const isJpgOrPng = info.file.type === 'image/jpeg' || info.file.type === 'image/png';
    const isLt2M = info.file.size / 1024 / 1024 < 2;
    setLoadImg(true);
    if (!isJpgOrPng) {
      swal("Thất bại", "Bạn chỉ có thể tải file JPG/PNG!", "error");

    }
    else if (!isLt2M) {
      swal("Thất bại", "Kích cỡ file vượt quá 2MB!", "error");
    }
    else if(imageUrl !== "")
    {
      setImageUrl("");
    }
    else if (info.file) {
      const responseIMG: any = await uploadFileToBlob(info.file, azureKey);
      setImageUrl(responseIMG.url);
      setLoadImg(false);
    }
  };

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    const sendData = formData as any;
    sendData.hasroles = userRoleTuples;
    sendData.imgpath = imageUrl;
    setLoading(true);
    await addUserAsync(sendData);
    setLoading(false);
    onFinish();
  };
  
  return (
    <CModal
      centered
      backdrop
      closeOnBackdrop
      show={isShow}
      onClose={() => onFinish()}
      size="lg"
      className="AddUserModal-styles"
    >
      <CCard className="align-middle justify-content-center jus AddUserModal-card-styles">
        <CCardBody className="AddUserModal-cardBody-styles">
          <h4 className="AddUserModal-h2-styles">Tạo tài khoản</h4>
          <Upload
            name="avatar"
            listType="picture-card"
            className="avatar-uploader upload-img"
            showUploadList={false}
            beforeUpload={() => false}
            onChange={(e) => handleImgChange(e)}
          >
            {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : 
            <div>
              {loadImg ? <LoadingOutlined /> : <PlusOutlined />}
              <div style={{ marginTop: 8 }}>Tải ảnh</div>
            </div>
            }
          </Upload>
          <CForm onSubmit={handleSubmit}>
            <CInputGroup>
              <CInput
                onChange={onChange}
                required
                placeholder="Email"
                className="AddUserModal-input-styles"
                type="text"
                name="email"
                value={formData.email || ""}
              />
            </CInputGroup>
            <CInputGroup>
              <CInput
                onChange={onChange}
                required
                placeholder="Họ"
                className="AddUserModal-input-styles"
                type="text"
                name="lastname"
                value={formData.lastname || ""}
              />
              <CInput
                onChange={onChange}
                placeholder="Tên đệm"
                className="AddUserModal-input-styles"
                type="text"
                name="middlename"
                value={formData.middlename || ""}
              />
              <CInput
                onChange={onChange}
                required
                placeholder="Tên"
                className="AddUserModal-input-styles"
                type="text"
                name="firstname"
                value={formData.firstname || ""}
              />
            </CInputGroup>
            <CInputGroup>
              <CInput
                onChange={onChange}
                required
                placeholder="Ngày sinh"
                className="AddUserModal-input-styles"
                type="date"
                name="dateofbirth"
                value={formData.dateofbirth || ""}
              />
              <CSelect
                onChange={onChange}
                required
                value={formData.gender || ""}
                placeholder="Giới tính"
                className="AddUserModal-input-styles"
                type="select"
                name="gender"
              >
                <option value="MALE">Nam</option>
                <option value="FEMALE">Nữ</option>
              </CSelect>
            </CInputGroup>
            <CInputGroup>
              <CInput
                onChange={onChange}
                required
                placeholder="Số điện thoại"
                className="AddUserModal-input-styles"
                type="number"
                name="phone"
                value={formData.phone || ""}
              />
            </CInputGroup>
            <CInputGroup>
              <CInput
                onChange={onChange}
                required
                placeholder="Địa chỉ"
                className="AddUserModal-input-styles"
                type="text"
                name="address"
                value={formData.address || ""}
              />
            </CInputGroup>  
            <Divider>
              <Typography.Title level={4}>Phân quyền</Typography.Title>
            </Divider>
            <Row gutter={12} className="mt-2 mb-5 ml-3">
              <Col span={8}>
                <Row justify="start">
                  <Typography.Title level={5}>Quyền</Typography.Title>
                </Row>
                <Row justify="start">
                  <Select 
                    value={selectedRole}
                    onChange={(value) => {
                      setSelectedRole(value);
                      setSelectedBranch(getAvailableBranch(value, roleList, branchList, userRoleTuples)[0]?.id);
                    }}
                  >
                    {roleList.map((role, index) => 
                      <Select.Option key={index} value={role.id}>
                        {role.code} - {role.name}
                      </Select.Option>  
                    )}
                  </Select>
                </Row>
              </Col>
              <Col span={12}>
                <Row justify="start">
                  <Typography.Title level={5}>Chi nhánh áp dụng</Typography.Title>
                </Row>
                <Row justify="start">
                  <Select 
                    value={selectedBranch}
                    onChange={(value) => setSelectedBranch(value)}
                  >
                    {getAvailableBranch(selectedRole, roleList, branchList, userRoleTuples)?.map((branch, index) => 
                      <Select.Option key={index} value={branch.id}>
                        {branch.id === -1 ? "Tất cả chi nhánh" : `${branch.code} - ${branch.name}`}
                      </Select.Option>  
                    )}
                  </Select>
                </Row>
              </Col>
              <Col span={4}>
                <Row justify="center" align="bottom" style={{ height: '100%' }}>
                  <Button onClick={handleAddRoleTuple} type="primary">Thêm</Button>
                </Row>
              </Col>
            </Row>
            <Divider>Danh sách quyền</Divider>
            <Row justify="center">
              <Timeline style={{ width: '100%' }}>
              {userRoleTuples?.length > 0 ? userRoleTuples.map((roleTuple, roleTupleIndex) => (
                <Timeline.Item key={roleTupleIndex} >
                  <Row gutter={24}>
                    <Col span={20}>
                      <Row justify="start" align="bottom" style={{ height: '100%' }}>
                        <Typography.Title level={5} style={{ margin: '0', padding: '0' }}>{roleList?.find(role => role.id === roleTuple.id)?.name}</Typography.Title>
                      </Row>
                    </Col>
                    <Col span={4}>
                      <Row justify="center" align="bottom" style={{ height: '100%' }}>
                        <Button onClick={() => handleRemoveRole(roleTuple.id)} type="text" ><CIcon content={freeSet.cilTrash}></CIcon></Button>
                      </Row>
                    </Col>
                  </Row>
                  <Row className="mt-4" justify="center">
                    <Timeline style={{ width: '100%' }}>
                      {roleTuple.branchids.map((branchId, index) => (
                          <Timeline.Item key={index}>
                              <Row justify="space-between" className="mb-1" gutter={24}>
                                  <Col span={20}>
                                      <Input value={branchList?.find(branch => branch.id === branchId)?.name} disabled />
                                  </Col>
                                  <Col span={4}>
                                      <Button onClick={() => handleRemoveBranchRole(roleTuple.id, branchId)} type="text"><CIcon name="cil-delete"></CIcon></Button>
                                  </Col>
                              </Row>
                          </Timeline.Item>
                      ))}
                    </Timeline>
                  </Row>
                </Timeline.Item>
              )) :
                <Empty
                  description="Chưa có phân quyền"
                />
              }
              </Timeline>
            </Row>
            
            <CButton
              required
              type="submit"
              color="primary"
              disabled={loading}
              className="AddUserModal-submitButton-styles btn my-3"
            >
              {loading ? <CSpinner size="sm" color="success"/> : ""} Tạo nhân viên
            </CButton>
          </CForm>
        </CCardBody>
      </CCard>
    </CModal>
  );
};
export default AddUserModal;
