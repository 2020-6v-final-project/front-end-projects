import { freeSet } from "@coreui/icons";
import CIcon from "@coreui/icons-react";
import {
  CBadge,
  CButton,
  CCard,
  CCardBody,
  CCol,
  CInputGroup,
  CRow,
} from "@coreui/react";
import React, { useEffect, useState } from "react";
import { DataTable } from "@components";
import { UserManagementDefaultProps } from "../../UserManagement.container";
import { User } from "@custom-types";
import AddUserModal from "../AddUserModal/AddUserModal";
import UserDetailModal from "../UserDetailModal/UserDetailModal";
import { shortDateFormat } from "@utils/stringUtils";
import "./ListUsers.scss";

const ListUsers: React.FC<UserManagementDefaultProps> = React.memo((props) => {
  const {
    UserList,
    BranchList,
    RoleList,
    fetchBranchListAsync,
    fetchRoleListAsync,
    fetchUserListAsync,
    addUserAsync,
    updateUserAsync,
    reactivateUserAsync,
    deactivateUserAsync,
    azureKey,
  } = props;

  const [loading, setLoading] = useState<boolean>(true);
  const [openAddUserModal, setOpenAddUserModal] = useState<boolean>(false);
  const [openUserDetailModal, setOpenUserDetailModal] = useState<boolean>(
    false
  );

  const [userDetailIndex, setUserDetailIndex] = useState<number>(-1);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      await fetchUserListAsync();
      await fetchRoleListAsync();
      await fetchBranchListAsync();
      setLoading(false);
    };
    fetchData();
  }, [fetchUserListAsync, fetchRoleListAsync, fetchBranchListAsync]);

  const onAddUserToggle: any = () => setOpenAddUserModal(!openAddUserModal);

  const onUserDetailToggle: any = (index: number) => {
    setUserDetailIndex(index);
    setOpenUserDetailModal(!openUserDetailModal);
  };

  const userTableMapper = (item: User) => {
    return {
      id: item?.id,
      ID: item?.id,
      username: "" || item?.username,
      firstname: "" || item?.firstname,
      middlename: "" || item?.middlename,
      lastname: "" || item?.lastname,
      dateofbirth: "" || item?.dateofbirth,
      address: "" || item?.address,
      gender: "" || item?.gender,
      joinedat: "" || item?.joinedat,
      phone: "" || item?.phone,
      email: "" || item?.email,
      statusid: "" || item?.statusid,
      statuscode: "" || item?.statuscode,
      imgpath: "" || item?.imgpath,
      hasroles: item?.hasroles?.map(role => { return { id: role.id, branchids: role.branchids }}),
      "Họ và tên": "" || item?.lastname + " " + item?.middlename + " " + item?.firstname,
      "Tên tài khoản": "" || item?.username,
      "Ngày sinh": "" || shortDateFormat(item?.dateofbirth),
      "Ngày vào công ty": "" || shortDateFormat(item?.joinedat),
      "Email": "" || item?.email,
      "Số điện thoại": "" || item?.phone,
      "Giới tính": "" || item?.gender === "MALE" ? "Nam" : "Nữ",
    };
  };

  const userTableField = [
    { key: "ID", _classes: "font-weight-bold" },
    "Tên tài khoản",
    "Họ và tên",
    "Email",
    "Ngày sinh",
    "Ngày vào công ty",
    "Số điện thoại",
    "Giới tính",
    "Trạng thái",
  ];

  const userTableScopedSlots = {
    "Trạng thái": (item: any) => (
      <td>
        <CBadge color={item.statuscode === "AVAILABLE" ? "success" : "danger"}>{item.statuscode}</CBadge>
      </td>
    ),
  };

  const userTableOnRowClick = (item: User, index: number) => {
    onUserDetailToggle(index);
  };
  
  return (
    <CRow>
      <CCol>
        <AddUserModal
          isShow={openAddUserModal}
          onFinish={onAddUserToggle}
          addUserAsync={addUserAsync}
          branchList={BranchList}
          roleList={RoleList}
          azureKey={azureKey}
        />
        <UserDetailModal
          branchList={BranchList}
          roleList={RoleList}
          isShow={openUserDetailModal}
          onClose={onUserDetailToggle}
          data={userDetailIndex > -1 && UserList ? userTableMapper(UserList[userDetailIndex]) : undefined}
          updateUserAsync={updateUserAsync}
          reactivateUserAsync={reactivateUserAsync}
          deactivateUserAsync={deactivateUserAsync}
        />
        <CCard>
          <CCardBody>
            <CInputGroup className="justify-content-between px-0 mx-0">
              <CRow className="col-md-12 px-0 my-2">
                <CCol sm="10">
                  <h4>Quản lý tài khoản</h4>
                </CCol>
                <CCol className="px-0" sm="2">
                  <CButton
                    color="warning"
                    className="float-right"
                    style={{ color: "white" }}
                    onClick={onAddUserToggle}
                  >
                    {" "}
                    <CIcon className="mr-2" content={freeSet.cilUserPlus} />
                    Tạo tài khoản
                  </CButton>
                </CCol>
              </CRow>
            </CInputGroup>

            <DataTable
              fields={userTableField}
              items={loading ? [] : UserList}
              itemsMapper={userTableMapper}
              scopedSlots={userTableScopedSlots}
              onRowClick={userTableOnRowClick}
              loading={loading}
            />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
});

export default ListUsers;
