import {
  CBadge, 
  CButton,
  CCard,
  CCardBody,
  CForm,
  CInput,
  CInputGroup,
  CModal,
  CSelect,
  CSpinner,
} from '@coreui/react';
import React, { ChangeEvent, FormEvent, useEffect, useState } from 'react';
import userPlusImg from '@assets/user-plus.png';
import { Role, Branch } from '@custom-types';
import './UserDetailModal.scss';
import { Row, Col, Timeline, Button, Input, Divider, Select, Typography, Empty, Avatar } from 'antd';
import CIcon from '@coreui/icons-react';
import { freeSet } from "@coreui/icons";

type UserDetailModalState = {
  isShow: boolean,
  data: any,
  branchList: Branch[],
  roleList: Role[],
} 

type UserDetailModalDispatch = {
  onClose: Function,
  updateUserAsync: Function,
  reactivateUserAsync: Function,
  deactivateUserAsync: Function,
}

type RoleTuple = {
  id: number,
  branchids: number[],
}

type UserDetailModalDefaultProps = UserDetailModalState & UserDetailModalDispatch;

const getAvailableBranch = (roleId: number, roleList: Role[], branchList: Branch[], roleTuples: RoleTuple[]) => {
  const targetRole = roleList.find(role => role.id === roleId);
  if (targetRole && targetRole.isall) {
    const fakeBranch = {
      id: -1,
    } as Branch;
    return [fakeBranch];
  }
  const targetRoleTuple = roleTuples.find(roleRuple => roleRuple.id === roleId);
  if (targetRoleTuple) return branchList.filter(branch => !targetRoleTuple.branchids.includes(branch.id));
  return branchList;
}

const UserDetailModal: React.FC<UserDetailModalDefaultProps> = (props) => {
  const { 
    isShow, 
    onClose, 
    data, 
    updateUserAsync, 
    reactivateUserAsync,
    deactivateUserAsync,
    branchList, 
    roleList,
  } = props;

  const [loading, setLoading] = useState<{
    activationAction: boolean,
    updateAction: boolean,
  }>({
    activationAction: false,
    updateAction: false,
  });
  const [userRoleTuples, setUserRoleTuples] = useState<RoleTuple[]>([]);
  const [selectedRole, setSelectedRole] = useState<number>(-1);
  const [selectedBranch, setSelectedBranch] = useState<number>(-1);

  const [isEditMode, setEditMode] = useState(false);
  const toggleEditMode = () => {
    setEditMode(!isEditMode);
  }

  const [formData, setFormData] = useState({
    id: "",
    firstname: "",
    middlename: "",
    lastname: "",
    email: "",
    dateofbirth: "",
    address: "",
    phone: "",
    gender: "",
    imgpath: "",
  });
  
  useEffect(() => {
    if (data) {
      setFormData({
        id: data.id,
        firstname: data.firstname,
        middlename: data.middlename,
        lastname: data.lastname,
        email: data.email,
        dateofbirth: data.dateofbirth,
        address: data.address,
        phone: data.phone,
        gender: data.gender,
        imgpath: data.imgpath,
      });
      if (roleList?.length > 0) {
        setSelectedRole(roleList[0]?.id);
        setSelectedBranch(getAvailableBranch(roleList[0]?.id, roleList, branchList, [])[0]?.id);
      }
      setUserRoleTuples(data.hasroles);
    }
  }, [isShow, data, branchList, roleList]);

  const handleAddRoleTuple = () => {
    const index = userRoleTuples?.findIndex((roleTuple) => roleTuple.id === selectedRole);
    if (index > -1) {
      const newRole = { ...userRoleTuples[index] } as RoleTuple;
      if (!newRole.branchids.includes(selectedBranch) && !roleList?.find(role => role.id === newRole.id)?.isall) {
        newRole.branchids = newRole.branchids.concat(selectedBranch);
        let temp = [] as RoleTuple[];
        Object.assign(temp, userRoleTuples);
        temp.splice(index, 1, newRole);
        setUserRoleTuples(temp);
      }
    } else {
      const roleTuple = { 
        id: selectedRole, 
        branchids: [selectedBranch],
      } as RoleTuple;
      if (roleList?.find(role => role.id === roleTuple.id)?.isall) roleTuple.branchids = [];
      setUserRoleTuples([ ...userRoleTuples, roleTuple ]);
    }
  }

  const handleRemoveBranchRole = (roleTupleId: number, branchId: number) => {
    const roleTupleIndex = userRoleTuples.findIndex(roleTuple => roleTuple.id === roleTupleId);
    if (roleTupleIndex > -1) {
      const tempRoleTuple = userRoleTuples[roleTupleIndex];
      const branchIndex = tempRoleTuple.branchids?.findIndex(Id => Id === branchId);
      if (branchIndex > -1) {
        tempRoleTuple.branchids?.splice(branchIndex, 1);
        const userTempRoleTuple = [ ...userRoleTuples ];
        if (tempRoleTuple && tempRoleTuple.branchids && tempRoleTuple.branchids.length > 0) userTempRoleTuple.splice(userRoleTuples.findIndex(roleTuple => roleTuple.id === roleTupleId), 1, tempRoleTuple);
        else userTempRoleTuple.splice(userRoleTuples.findIndex(roleTuple => roleTuple.id === roleTupleId), 1);
        setUserRoleTuples(userTempRoleTuple);
      }
    }
  }

  const handleRemoveRole = (roleTupleId: number) => setUserRoleTuples(userRoleTuples.filter(roleTuple => roleTuple.id !== roleTupleId));

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    const sendData = formData as any;
    sendData.hasroles = userRoleTuples;
    setLoading({ ...loading, updateAction: true });
    await updateUserAsync(sendData);
    setLoading({ ...loading, updateAction: false });
    setEditMode(false);
    onClose(-1);
  };

  const handleDeactivateOrReactivate = async () => {
    setLoading({ ...loading, activationAction: true });
    if (data?.statuscode === "AVAILABLE") await deactivateUserAsync(data?.id);
    else await reactivateUserAsync(data?.id);
    setLoading({ ...loading, activationAction: false });
  }

  return (
    <CModal
      centered
      backdrop
      show={isShow}
      size="lg"
      onClose={() => { setEditMode(false); onClose(-1); }}
      className="UserDetailModal-styles">
      <CCard className="align-middle UserDetailModal-card-styles" >
        <CCardBody className="UserDetailModal-cardBody-styles">
          <h4 className="UserDetailModal-h2-styles" >Thông tin tài khoản</h4>
          <Row justify="center">
            <Avatar src={data?.imgpath ? data?.imgpath : userPlusImg} className="UserDetailModal-img-styles my-3" />
          </Row>
          <Row justify="center" className="mt-1">
            <CBadge color={data?.statuscode === "AVAILABLE" ? "success" : "danger"}>{data?.statuscode}</CBadge>
          </Row>
          <Row justify="center" className="mt-3">
            <CButton 
              size="small" 
              className="UserDetailModal-optionButton-styles btn-sm btn-outline-warning" 
              onClick={toggleEditMode}
            >
              {isEditMode ? "Huỷ" : "Sửa"}
            </CButton>
            <CButton 
              size="small" 
              className="UserDetailModal-optionButton-styles btn-sm btn-outline-danger" 
              onClick={handleDeactivateOrReactivate}
              disabled={loading.updateAction}
            >
              {loading.activationAction ? <CSpinner size="sm" color="success"/> : ""} {data?.statuscode === "AVAILABLE" ? "Chặn" : "Bỏ chặn"}
            </CButton>
          </Row>
          <CForm onSubmit={handleSubmit}>
            <CInputGroup >
              <CInput 
              onChange={onChange}
                readOnly={!isEditMode}
                placeholder="Email"
                className="UserDetailModal-input-styles"
                type="text" 
                name="email"
                value={formData.email || ""} />
            </CInputGroup>
            <CInputGroup >
              <CInput 
                readOnly={!isEditMode}
                onChange={onChange}
                required
                placeholder="Họ"
                className="UserDetailModal-input-styles"
                type="text"
                name="lastname"
                value={formData.lastname || ""}
              />
              <CInput 
                readOnly={!isEditMode}
                onChange={onChange}
                placeholder="Tên đệm"
                className="UserDetailModal-input-styles"
                type="text"
                name="middlename"
                value={formData.middlename || ""}
              />
              <CInput 
                readOnly={!isEditMode}
                onChange={onChange}
                required
                placeholder="Tên"
                className="UserDetailModal-input-styles"
                type="text"
                name="firstname"
                value={formData.firstname || ""}
              />
            </CInputGroup>
            <CInputGroup>
              <CInput 
                onChange={onChange}
                readOnly={!isEditMode}
                placeholder="Ngày sinh"
                className="UserDetailModal-input-styles"
                type="date"
                name="dateofbirth"
                value={formData.dateofbirth.substring(0, 10) || ""}
              />
              <CSelect
                onChange={onChange}
                readOnly={!isEditMode}
                required
                value={formData.gender || ""}
                placeholder="Giới tính"
                className="AddUserModal-input-styles"
                type="select"
                name="gender"
              >
                <option value="MALE">Nam</option>
                <option value="FEMALE">Nữ</option>
              </CSelect>
            </CInputGroup>
            <CInputGroup>
              <CInput 
                onChange={onChange}
                readOnly={!isEditMode}
                placeholder="Số điện thoại"
                className="UserDetailModal-input-styles"
                type="number"
                name="phone"
                value={formData.phone || ""}
              />
            </CInputGroup>
            <CInputGroup >
              <CInput 
                onChange={onChange}
                readOnly={!isEditMode}
                placeholder="Địa chỉ"
                className="UserDetailModal-input-styles"
                type="text" 
                name="address"
                value={formData.address || ""}
              />
            </CInputGroup>
            <Divider>
              <Typography.Title level={4}>Phân quyền</Typography.Title>
            </Divider>
            <Row gutter={12} className="mt-2 mb-5 ml-3">
              <Col span={8}>
                <Row justify="start">
                  <Typography.Title level={5}>Quyền</Typography.Title>
                </Row>
                <Row justify="start">
                  <Select 
                    disabled={!isEditMode}
                    value={selectedRole}
                    onChange={(value) => {
                      setSelectedRole(value);
                      setSelectedBranch(getAvailableBranch(value, roleList, branchList, userRoleTuples)[0]?.id);
                    }}
                  >
                    {roleList.map((role, index) => 
                      <Select.Option key={index} value={role.id}>
                        {role.code} - {role.name}
                      </Select.Option>  
                    )}
                  </Select>
                </Row>
              </Col>
              <Col span={12}>
                <Row justify="start">
                  <Typography.Title level={5}>Chi nhánh áp dụng</Typography.Title>
                </Row>
                <Row justify="start">
                  <Select 
                    disabled={!isEditMode}
                    value={selectedBranch}
                    onChange={(value) => setSelectedBranch(value)}
                  >
                    {getAvailableBranch(selectedRole, roleList, branchList, userRoleTuples)?.map((branch, index) => 
                      <Select.Option key={index} value={branch.id}>
                        {branch.id === -1 ? "Tất cả chi nhánh" : `${branch.code} - ${branch.name}`}
                      </Select.Option>  
                    )}
                  </Select>
                </Row>
              </Col>
              <Col span={4}>
                <Row justify="center" align="bottom" style={{ height: '100%' }}>
                  <Button disabled={!isEditMode} onClick={handleAddRoleTuple} type="primary">Thêm</Button>
                </Row>
              </Col>
            </Row>
            <Divider>Danh sách quyền</Divider>
            <Row justify="center">
              <Timeline style={{ width: '100%' }}>
              {userRoleTuples?.length > 0 ? userRoleTuples.map((roleTuple, roleTupleIndex) => (
                <Timeline.Item key={roleTupleIndex} >
                  <Row gutter={24}>
                    <Col span={20}>
                      <Row justify="start" align="bottom" style={{ height: '100%' }}>
                        <Typography.Title level={5} style={{ margin: '0', padding: '0' }}>{roleList?.find(role => role.id === roleTuple.id)?.name}</Typography.Title>
                      </Row>
                    </Col>
                    <Col span={4}>
                      <Row justify="center" align="bottom" style={{ height: '100%' }}>
                        <Button disabled={!isEditMode} onClick={() => handleRemoveRole(roleTuple.id)} type="text" ><CIcon content={freeSet.cilTrash}></CIcon></Button>
                      </Row>
                    </Col>
                  </Row>
                  <Row className="mt-4" justify="center">
                    <Timeline style={{ width: '100%' }}>
                      {roleTuple.branchids.map((branchId, index) => (
                          <Timeline.Item key={index}>
                              <Row justify="space-between" className="mb-1" gutter={24}>
                                  <Col span={20}>
                                      <Input value={branchList?.find(branch => branch.id === branchId)?.name} disabled />
                                  </Col>
                                  <Col span={4}>
                                      <Button disabled={!isEditMode} onClick={() => handleRemoveBranchRole(roleTuple.id, branchId)} type="text"><CIcon name="cil-delete"></CIcon></Button>
                                  </Col>
                              </Row>
                          </Timeline.Item>
                      ))}
                    </Timeline>
                  </Row>
                </Timeline.Item>
              )) :
                <Empty
                  description="Chưa có phân quyền"
                />
              }
              </Timeline>
            </Row>

            <CButton
              required
              type="submit"
              color="primary"
              disabled={loading.updateAction && isEditMode}
              className="AddUserModal-submitButton-styles btn my-3"
            >
              {loading.updateAction ? <CSpinner size="sm" color="success"/> : ""} Lưu
            </CButton>
          </CForm>
        </CCardBody>
      </CCard>
    </CModal>
  );
}
export default UserDetailModal;
