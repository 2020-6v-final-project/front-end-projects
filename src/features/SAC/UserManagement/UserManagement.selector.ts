import { RootState } from "src/store";
import { createSelector } from "reselect";

export const userListSelector = createSelector(
  [(state: RootState) => state.user.UserList],
  (UserList) => {
    return UserList;
  }
);
