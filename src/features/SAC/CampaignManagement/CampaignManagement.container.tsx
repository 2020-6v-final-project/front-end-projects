import { FAB } from "@components";
import { CFade } from "@coreui/react";
import { history } from "@shared/components";
import { Row, message } from "antd";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { RootState } from "src/store";
import MainCampaignPanel from "./components/MainCampaignPanel/MainCampaignPanel";
import CampaignModal from "./components/CampaignModal/CampaignModal";
import AddCampaignModal from "./components/AddCampaignModal/AddCampaignModal";
import SideCampaignPanel from "./components/SideCampaignPanel/SideCampaignPanel";
import { Branch, Campaign, Item, ItemType, Promotion } from "@custom-types";
import { selectAzureConfig } from "../../Azure/AzureManagement.selector";
import { getKeyAsync } from "../../Azure/AzureManagement.slice";
import { selectCampaignList } from "./CampaignManagement.selector";
import {
  fetchCampaignListAsync,
  addCampaignAsync,
  updateCampaignAsync,
  deactivateCampaignAsync,
  reactivateCampaignAsync,
} from "./CampaignManagement.slice";
import { selectPromotionList } from "../PromotionManagement/PromotionManagement.selector";
import { fetchPromotionListAsync } from "../PromotionManagement/PromotionManagement.slice";
import { branchListSelector } from "../BranchManagement/BranchManagement.selector";
import { fetchBranchListAsync } from "../BranchManagement/BranchManagement.slice";
import { selectItemList } from "../ItemManagement/ItemManagement.selector";
import { getItemAsync } from "../ItemManagement/ItemManagement.slice";
import { selectItemTypeList } from "../ItemIngredientTypeManagement/ItemIngredientTypeManagement.selector";
import { getItemTypeAsync } from "../ItemIngredientTypeManagement/ItemIngredientTypeManagement.slice";

type CampaignManagementState = {
  campaignList: Campaign[];
  branchList: Branch[];
  promotionList: Promotion[];
  itemList: Item[];
  itemTypeList: ItemType[];
  match: any;
  azureKey: string;
};

type CampaignManagementDispatch = {
  getKeyAsync: Function;
  fetchCampaignListAsync: Function;
  addCampaignAsync: Function;
  updateCampaignAsync: Function;
  deactivateCampaignAsync: Function;
  reactivateCampaignAsync: Function;
  fetchPromotionListAsync: Function;
  fetchBranchListAsync: Function;
  getItemAsync: Function;
  getItemTypeAsync: Function;
};

type CampaignManagementDefaultProps = CampaignManagementState & CampaignManagementDispatch;

const CampaignManagementContainer: React.FC<CampaignManagementDefaultProps> = React.memo(
  (props) => {
    const { azureKey, itemList, itemTypeList, branchList, promotionList, campaignList, match } = props;
    const {
      getKeyAsync,
      fetchCampaignListAsync,
      addCampaignAsync,
      updateCampaignAsync,
      deactivateCampaignAsync,
      reactivateCampaignAsync,
      fetchPromotionListAsync,
      fetchBranchListAsync,
      getItemAsync,
      getItemTypeAsync,
    } = props;

    const [currentCampaign, setCurrentCampaign] = useState<Campaign | undefined>(undefined);
    const [openAddCampaignModal, setOpenAddCampaignModal] = useState<boolean>(false);
    const [openCampaignDetailModal, setOpenCampaignDetailModal] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);

    useEffect(() => {
      const fetchData = async () => {
        if (currentCampaign === undefined) {
          if (match.params.id === undefined) setLoading(true);
          await Promise.all([
            getKeyAsync(),
            getItemAsync(),
            getItemTypeAsync(),
            fetchBranchListAsync(),
            fetchPromotionListAsync(),
            fetchCampaignListAsync(),
          ]);
          if (match.params.id === undefined) setLoading(false);
        }
      }
      fetchData();
    }, [getKeyAsync, getItemTypeAsync, getItemAsync, fetchBranchListAsync, fetchPromotionListAsync, fetchCampaignListAsync, currentCampaign, match.params.id]);

    useEffect(() => {
      if (campaignList.length > 0 && currentCampaign) {
        setCurrentCampaign(campaignList.find((campaign) => campaign.id === currentCampaign.id));
      }
    }, [campaignList, currentCampaign]);

    useEffect(() => {
      if (campaignList.length > 0) {
        if (!match.params.id) {
          setCurrentCampaign(campaignList[0]);
          history.push("/campaign-management");
          return;
        }
        const target = campaignList.find(
          (campaign) => campaign.id === Number(match.params.id)
        );
        if (target) {
          setCurrentCampaign(target);
        } else {
          history.push("/campaign-management");
          message.error("Không tìm thấy chương trình!");
        }
      }
    }, [match.params.id, campaignList]);

    const handleCampaignSelect = (campaign: Campaign) => {
      setCurrentCampaign(campaign);
      history.push("/campaign-management/" + campaign.id);
    };

    const handleDeactivateOrReactivate = async (statusCode: string, campaignId: number) => {
      if (statusCode === "AVAILABLE") await deactivateCampaignAsync(campaignId);
      else await reactivateCampaignAsync(campaignId);
    }

    return (
      <CFade>
        <div style={{ overflow: "hidden" }}>
          <Row gutter={48}>
            <SideCampaignPanel
              globalLoading={loading}
              campaigns={campaignList}
              currentCampaign={currentCampaign}
              handleCampaignSelect={handleCampaignSelect}
            />
            <MainCampaignPanel 
              globalLoading={loading}
              campaign={currentCampaign}
              branchList={branchList}
              itemList={itemList}
              itemTypeList={itemTypeList}
              handleDeactivateOrReactivate={handleDeactivateOrReactivate}
              toggleCampaignDetailModal={() => setOpenCampaignDetailModal(true)}
            />
          </Row>
        </div>

        <FAB callback={() => setOpenAddCampaignModal(true)} />
        <CampaignModal
          azureKey={azureKey}
          show={openCampaignDetailModal}
          onClose={() => setOpenCampaignDetailModal(false)}
          campaign={currentCampaign}
          branchList={branchList}
          itemList={itemList}
          itemTypeList={itemTypeList}
          promotionList={promotionList}
          updateCampaignAsync={updateCampaignAsync}
        />
        <AddCampaignModal
          azureKey={azureKey}
          show={openAddCampaignModal}
          onClose={() => setOpenAddCampaignModal(false)}
          branchList={branchList}
          itemList={itemList}
          itemTypeList={itemTypeList}
          promotionList={promotionList}
          addCampaignAsync={addCampaignAsync}
        />
      </CFade>
    );
  }
);

const mapState = (state: RootState) => {
  return {
    azureKey: selectAzureConfig(state),
    campaignList: selectCampaignList(state),
    branchList: branchListSelector(state),
    promotionList: selectPromotionList(state),
    itemList: selectItemList(state),
    itemTypeList: selectItemTypeList(state),
  };
};
const mapDispatch = {
  getKeyAsync,
  fetchCampaignListAsync,
  addCampaignAsync,
  updateCampaignAsync,
  deactivateCampaignAsync,
  reactivateCampaignAsync,
  fetchPromotionListAsync,
  fetchBranchListAsync,
  getItemAsync,
  getItemTypeAsync,
};

export default connect(mapState, mapDispatch)(CampaignManagementContainer);
