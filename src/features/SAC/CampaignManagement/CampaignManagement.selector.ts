import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectCampaignList = createSelector(
    [(state: RootState) => state.campaign.campaignList],
    (campaignList) => campaignList
)
