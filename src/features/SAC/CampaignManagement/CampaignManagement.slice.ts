import { Campaign } from '@custom-types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
    getCampaignList,
    addCampaign,
    updateCampaign,
    deactivateCampaign,
    reactivateCampaign,
} from "@apis";
import { AppThunk } from "src/store";
import swal from "sweetalert";
import { errorMessage } from "@shared/constants/ErrorConstants"

type CampaignManagementInitialState = {
    campaignList: Campaign[],
}

const initialState: CampaignManagementInitialState = {
    campaignList: [],
}

const CampaignManagementSlice = createSlice({
    name: 'campaignManagement',
    initialState,
    reducers: {
        fetchCampaignListSuccess: (state, action: PayloadAction<Campaign[]>) => {
            action.payload.sort((campaignA, campaignB) => campaignA.id - campaignB.id);
            state.campaignList = action.payload;
            return state;
        },
        fetchCampaignListFailed: (state) => {
            return state;
        },
        addCampaignSuccess: (state, action: PayloadAction<Campaign>) => {
            state.campaignList.push(action.payload);
            state.campaignList.sort((campaignA, campaignB) => campaignA.id - campaignB.id);
            return state;
        },
        addCampaignFailed: (state) => {
            return state;
        },
        updateCampaignSuccess: (state, action: PayloadAction<Campaign>) => {
            const index = state.campaignList.findIndex(campaign => campaign.id === action.payload.id);
            if (index > -1) {
                state.campaignList.splice(index, 1 , action.payload);
            }
            return state;
        },
        updateCampaignFailed: (state) => {
            return state;
        },
        deactivateCampaignSuccess: (state, action: PayloadAction<Campaign>) => {
            const index = state.campaignList.findIndex(campaign => campaign.id === action.payload.id);
            if (index > -1) {
                state.campaignList.splice(index, 1 , action.payload);
            }
            return state;
        },
        deactivateCampaignFailed: (state) => {
            return state;
        },
        reactivateCampaignSuccess: (state, action: PayloadAction<Campaign>) => {
            const index = state.campaignList.findIndex(campaign => campaign.id === action.payload.id);
            if (index > -1) {
                state.campaignList.splice(index, 1 , action.payload);
            }
            return state;
        },
        reactivateCampaignFailed: (state) => {
            return state;
        },
    }
});

export const fetchCampaignListAsync = (): AppThunk => async (dispatch) => {
    try {
        await getCampaignList((data: Campaign[]) => {
            dispatch(fetchCampaignListSuccess(data));
        });
    } catch (e) {
        const errorBundle = errorMessage(e, "CAMPAIGN_MANAGEMENT");
        swal(errorBundle.title, errorBundle.description, "error");
        dispatch(fetchCampaignListFailed());
    }
}

export const addCampaignAsync = (campaign: any): AppThunk => async (dispatch) => {
    try {
        await addCampaign(campaign, (data: Campaign) => {
            swal("Thêm chương trình thành công", "Chương trình đã được thêm vào hệ thống", "success");
            dispatch(addCampaignSuccess(data));
        });
    } catch (e) {
        const errorBundle = errorMessage(e, "CAMPAIGN_MANAGEMENT");
        swal(errorBundle.title, errorBundle.description, "error");
        dispatch(addCampaignFailed());
    }
}

export const updateCampaignAsync = (campaignId: number, campaign: any): AppThunk => async (dispatch) => {
    try {
        await updateCampaign(campaignId, campaign, (data: Campaign) => {
            swal("Cập nhập chương trình thành công", "Chương trình đã được cập nhập", "success");
            dispatch(updateCampaignSuccess(data));
        });
    } catch (e) {
        const errorBundle = errorMessage(e, "CAMPAIGN_MANAGEMENT");
        swal(errorBundle.title, errorBundle.description, "error");
        dispatch(updateCampaignFailed());
    }
}

export const deactivateCampaignAsync = (campaignId: number): AppThunk => async (dispatch) => {
    try {
        await deactivateCampaign(campaignId, (data: Campaign) => {
            swal("Khóa chương trình thành công", "Chương trình đã bị vô hiệu", "success");
            dispatch(deactivateCampaignSuccess(data));
        });
    } catch (e) {
        const errorBundle = errorMessage(e, "CAMPAIGN_MANAGEMENT");
        swal(errorBundle.title, errorBundle.description, "error");
        dispatch(deactivateCampaignFailed());
    }
}

export const reactivateCampaignAsync = (campaignId: number): AppThunk => async (dispatch) => {
    try {
        await reactivateCampaign(campaignId, (data: Campaign) => {
            swal("Mở khóa chương trình thành công", "Chương trình đã hoạt động", "success");
            dispatch(reactivateCampaignSuccess(data));
        });
    } catch (e) {
        const errorBundle = errorMessage(e, "CAMPAIGN_MANAGEMENT");
        swal(errorBundle.title, errorBundle.description, "error");
        dispatch(reactivateCampaignFailed());
    }
}

const {
    fetchCampaignListSuccess,
    fetchCampaignListFailed,
    addCampaignSuccess,
    addCampaignFailed,
    updateCampaignSuccess,
    updateCampaignFailed,
    deactivateCampaignSuccess,
    deactivateCampaignFailed,
    reactivateCampaignSuccess,
    reactivateCampaignFailed,
} = CampaignManagementSlice.actions;

export default CampaignManagementSlice.reducer;
