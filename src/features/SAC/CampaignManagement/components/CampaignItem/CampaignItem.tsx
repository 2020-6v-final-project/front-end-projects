import React, { useEffect, useState } from "react";
import { List, Col, Row, Typography, Input, Select } from "antd";
import { CloseOutlined } from "@ant-design/icons";
import "./CampaignItem.scss";
import { Item, ItemType, Promotion } from "@custom-types";
import { promotionDescriptionMapper } from "../MainCampaignPanel/MainCampaignPanel";

type CampaignItemProps = {
  minOfMaxQuantity?: number;
  promotion?: Promotion; 
  promotionCampaign: {
    promotion_id: number;
    max_quantity: number | null;
  }
  itemList: Item[];
  itemTypeList: ItemType[];
  handleRemovePromotion: Function;
  handleUpdateMaxQuantity: Function;
};

const CampaignItem: React.FC<CampaignItemProps> = React.memo((props) => {
  const { minOfMaxQuantity, promotion, promotionCampaign, handleRemovePromotion, handleUpdateMaxQuantity, itemList, itemTypeList } = props;

  const [maxQuantityOption, setMaxQuantityOption] = useState<string>("no_limit");

  useEffect(() => {
    if (promotionCampaign.max_quantity !== null) setMaxQuantityOption("limit");
    else setMaxQuantityOption("no_limit");
  }, [promotionCampaign]);

  return (
    <List.Item actions={[<CloseOutlined style={{ cursor: "pointer" }} onClick={() => handleRemovePromotion(promotion?.id)}/>]}>
      <Row style={{ width: "100%" }}>
        <Col span={16}>
          <Typography.Title level={5}>{promotion?.name}</Typography.Title>
          <Typography.Text>
            <ul>
              {promotion ? 
                promotionDescriptionMapper(
                  promotion.conditions,
                  promotion.benefits,
                  itemList,
                  itemTypeList,
                ).benefits
              : "Không có thông tin"}
            </ul>
          </Typography.Text>
        </Col>
        <Col span={8}>
          <Row justify="center" align="middle">
            <Typography.Title level={5}>Tối đa áp dụng</Typography.Title>
          </Row>
          <Row justify="center" align="middle">
            <Select 
              value={maxQuantityOption} 
              style={{ width: "100%", textAlign: "center" }} 
              className="mb-3"
              onChange={(value) => {
                if (value === "no_limit") handleUpdateMaxQuantity(null);
                else handleUpdateMaxQuantity(1);
                setMaxQuantityOption(value);
              }}
            >
              <Select.Option value={"no_limit"}>Không giới hạn</Select.Option>
              <Select.Option value={"limit"}>Giới hạn</Select.Option>
            </Select>
          </Row>
          {maxQuantityOption === "limit" && promotionCampaign.max_quantity !== null ?
            <Row justify="center" align="middle">
              <Input 
                min={minOfMaxQuantity}
                type="number"
                placeholder="Tối đa"
                style={{ textAlign: "center" }}
                value={promotionCampaign.max_quantity}
                onChange={(e) => handleUpdateMaxQuantity(e.target.value ? parseInt(e.target.value) : null)}
              />
            </Row>
          : <></>}
        </Col>
      </Row>
    </List.Item>
  );
});

export default CampaignItem;
