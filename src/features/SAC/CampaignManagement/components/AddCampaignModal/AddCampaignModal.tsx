import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import { SubHeaderText } from "@components";
import CIcon from "@coreui/icons-react";
import { Branch, Item, ItemType, Promotion } from "@custom-types";
import { uploadFileToBlob } from "@features/Azure/azure-storage-blob";
import {
  AutoComplete,
  Button,
  Checkbox,
  Col,
  DatePicker,
  Drawer,
  Form,
  Input,
  List,
  Row,
  Tabs,
  Timeline,
  TimePicker,
  Typography,
  Upload,
} from "antd";
import moment from "moment";
import React, { useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import swal from "sweetalert";
import PromoItem from "../CampaignItem/CampaignItem";
import "./AddCampaignModal.scss";

type AddCampaignModalProps = {
  show: boolean;
  onClose: Function;
  azureKey: string;
  itemList: Item[];
  itemTypeList: ItemType[];
  branchList: Branch[];
  promotionList: Promotion[];
  addCampaignAsync: Function;
};

const AddCampaignModal: React.FC<AddCampaignModalProps> = (props) => {
  const { show, onClose, azureKey, itemList, itemTypeList, branchList, promotionList, addCampaignAsync } = props;

  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);
  const [loadImg, setLoadImg] = useState<boolean>(false);
  const [branches, setBranches] = useState<number[]>([]);
  const [promotions, setPromotions] = useState<{
    promotion_id: number,
    max_quantity: number | null,
  }[]>([]);
  const [imageUrl, setImageUrl] = useState<any>("");

  const [checkAll, setCheckAll] = useState(false);

  const handleAddBranch = (addBranchId: number) => {
    if (!branches.includes(addBranchId)) setBranches([ ...branches, addBranchId ]);
  }

  const handleRemoveBranch = (removedBranchId: number) => {
    setBranches(branches.filter(branchId => branchId !== removedBranchId));
  } 

  const handleAddAndUpdatePromotion = (addPromotionId: number, maxQuantity: number | null) => {
    const index = promotions.findIndex(promotion => promotion.promotion_id === addPromotionId);
    if (index > -1) {
      const tempPromotions = [...promotions];
      tempPromotions.splice(index, 1, {
        promotion_id: addPromotionId,
        max_quantity: maxQuantity,
      })
      setPromotions(tempPromotions);
    } else {
      setPromotions([ ...promotions, {
        promotion_id: addPromotionId,
        max_quantity: maxQuantity,
      }])
    }
  }

  const handleRemovePromotion = (removedPromotionId: number) => {
    setPromotions(promotions.filter(promotion => promotion.promotion_id !== removedPromotionId));
  }

  const handleImgChange = async (info: any) => {
    const isJpgOrPng =
      info.file.type === "image/jpeg" || info.file.type === "image/png";
    const isLt2M = info.file.size / 1024 / 1024 < 2;
    setLoadImg(true);
    if (!isJpgOrPng) {
      swal("Thất bại", "Bạn chỉ có thể tải file JPG/PNG!", "error");
    } else if (!isLt2M) {
      swal("Thất bại", "Kích cỡ file vượt quá 2MB!", "error");
    } else if (imageUrl !== "") {
      setImageUrl("");
    } else if (info.file) {
      const responseIMG: any = await uploadFileToBlob(info.file, azureKey);
      setImageUrl(responseIMG.url);
      setLoadImg(false);
    }
  }

  const handleAddCampaign = async (value: any) => {
    const sendData = {
      name: value.name,
      description: value.description,
      start_after: (value.date[0] as moment.Moment).startOf("day").toDate(),
      end_before: (value.date[1] as moment.Moment).endOf("day").toDate(),
      start_hour: (value.time[0] as moment.Moment).toDate().getHours(),
      end_hour: (value.time[1] as moment.Moment).toDate().getHours(),
      weekday: value.weekday.includes(-1) ? "0,1,2,3,4,5,6" : value.weekday.toString(),
      imgpath: imageUrl ? imageUrl : null,
      branches,
      promotions,
    }
    setLoading(true);
    await addCampaignAsync(sendData);
    console.log(sendData);
    setLoading(false);
    onClose();
  }

  return (
    <Drawer
      visible={show}
      width="800"
      onClose={() => onClose()}
      zIndex={1}
    >
      <Scrollbars autoHide>
        <Row style={{ margin: 20 }} justify="center" align="middle">
          <Col span={24}>
            <SubHeaderText style={{ textAlign: "center" }} className="mb-4">
              Thêm khuyến mãi
            </SubHeaderText>

            <Tabs centered defaultActiveKey="1">
              <Tabs.TabPane tab="Thông tin chung" key="1">
                <Row justify="center" className="my-3">
                  <Upload
                    name="avatar"
                    listType="picture-card"
                    className="avatar-uploader upload-img"
                    showUploadList={false}
                    beforeUpload={() => false}
                    onChange={(e) => handleImgChange(e)}
                  >
                    {imageUrl ? (
                      <img
                        src={imageUrl}
                        alt="avatar"
                        style={{ width: "100%" }}
                      />
                    ) : (
                      <div>
                        {loadImg ? <LoadingOutlined /> : <PlusOutlined />}
                        <div style={{ marginTop: 8 }}>Tải ảnh</div>
                      </div>
                    )}
                  </Upload>
                </Row>

                <Form
                  form={form}
                  name="addCampaignForm"
                  onFinish={handleAddCampaign}
                >
                  <Row justify="center">
                    <Col span={24}>
                      <Form.Item
                        name="name"
                        rules={[
                          {
                            required: true,
                            message: "Vui lòng điền tên khuyến mãi",
                          },
                        ]}
                      >
                        <Input
                          style={{
                            border: "none",
                            textAlign: "center",
                            fontSize: 18,
                            fontWeight: 400,
                            width: "100%",
                          }}
                          placeholder="Tên khuyến mãi"
                        />
                      </Form.Item>
                    </Col>
                  </Row>

                  <Row className="mb-3">
                    <Typography.Title level={5}>
                      Thời gian áp dụng
                    </Typography.Title>
                  </Row>

                  <Row gutter={24} justify="space-between">
                    <Col span={24}>
                      <Form.Item
                        name="date"
                        rules={[
                          {
                            required: true,
                            message: "Vui lòng điền ngày khuyến mãi",
                          },
                        ]}
                      >
                        <DatePicker.RangePicker
                          placeholder={["Ngày bắt đầu", "Ngày kết thúc"]}
                          ranges={{
                            "Hôm nay": [
                              moment().startOf("day"),
                              moment().endOf("day"),
                            ],
                            "Tuần này": [
                              moment().startOf("week"),
                              moment().endOf("week"),
                            ],
                            "Tháng này": [
                              moment().startOf("month"),
                              moment().endOf("month"),
                            ],
                            "Quý này": [
                              moment().startOf("quarter"),
                              moment().endOf("quarter"),
                            ],
                          }}
                          format="DD/MM/YYYY"
                          bordered={false}
                          style={{ width: "100%" }}
                        />
                      </Form.Item>
                    </Col>
                  </Row>

                  <Row gutter={24} justify="space-between">
                    <Col span={24}>
                      <Form.Item
                        name="time"
                        rules={[
                          {
                            required: true,
                            message: "Vui lòng điền khung giờ khuyến mãi",
                          },
                        ]}
                      >
                        <TimePicker.RangePicker
                          placeholder={["Giờ bắt đầu", "Giờ kết thúc"]}
                          style={{ width: "100%" }}
                          format="HH"
                          bordered={false}
                        />
                      </Form.Item>
                    </Col>
                  </Row>

                  <Row gutter={24} justify="space-between">
                    <Col span={24}>
                      <Form.Item
                        name="weekday"
                        rules={[
                          {
                            required: true,
                            message: "Vui lòng chọn ngày trong tuần",
                          },
                        ]}
                      >
                        <Checkbox.Group style={{ width: "100%" }}>
                          <Row gutter={[16, 24]}>
                            <Col span={6}>
                              <Checkbox
                                value={-1}
                                onChange={() => setCheckAll(!checkAll)}
                              >
                                Cả tuần
                              </Checkbox>
                            </Col>
                            <Col span={6}>
                              <Checkbox value={0} disabled={checkAll}>
                                Thứ 2
                              </Checkbox>
                            </Col>
                            <Col span={6}>
                              <Checkbox value={1} disabled={checkAll}>
                                Thứ 3
                              </Checkbox>
                            </Col>
                            <Col span={6}>
                              <Checkbox value={2} disabled={checkAll}>
                                Thứ 4
                              </Checkbox>
                            </Col>
                            <Col span={6}>
                              <Checkbox value={3} disabled={checkAll}>
                                Thứ 5
                              </Checkbox>
                            </Col>
                            <Col span={6}>
                              <Checkbox value={4} disabled={checkAll}>
                                Thứ 6
                              </Checkbox>
                            </Col>
                            <Col span={6}>
                              <Checkbox value={5} disabled={checkAll}>
                                Thứ 7
                              </Checkbox>
                            </Col>
                            <Col span={6}>
                              <Checkbox value={6} disabled={checkAll}>
                                Chủ nhật
                              </Checkbox>
                            </Col>
                          </Row>
                        </Checkbox.Group>
                      </Form.Item>
                    </Col>
                  </Row>

                  <Row justify="center">
                    <Col span={24}>
                      <Form.Item
                        name="description"
                      >
                        <Input.TextArea
                          showCount
                          maxLength={100}
                          autoSize={{ minRows: 3, maxRows: 3 }}
                          placeholder="Mô tả"
                        />
                      </Form.Item>
                    </Col>
                  </Row>

                  <Row className="mb-3">
                    <Typography.Title level={5}>
                      Chi nhánh áp dụng
                    </Typography.Title>
                  </Row>

                  <Row className="mb-3">
                    <AutoComplete
                      style={{ width: "100%" }}
                      placeholder="Nhập tên chi nhánh"
                      filterOption={(inputValue, option) => option?.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                      allowClear
                    >
                      {branchList.filter(branch => !branches.includes(branch.id)).map((branch, index) => (
                        <AutoComplete.Option key={index} value={`${branch.name} (${branch.code})`}>
                          <Row onClick={() => handleAddBranch(branch.id)}>
                            {branch.name} ({branch.code})
                          </Row>
                        </AutoComplete.Option>
                      ))}
                    </AutoComplete>
                  </Row>

                  <Row justify="center" className="mt-4">
                    <Col span={24}>
                      <Timeline>
                        {branches.map((branchId, index) => (
                          <Timeline.Item key={index}>
                            <Row
                              justify="space-between"
                              className="mb-3"
                              gutter={24}
                            >
                              <Col>
                                <Row align="middle" style={{ height: "100%" }}>
                                  <Typography.Text strong >{branchList?.find(branch => branch.id === branchId)?.name} ({branchList?.find(branch => branch.id === branchId)?.code})</Typography.Text>
                                </Row>
                              </Col>
                              <Col>
                              <Button
                                onClick={() => handleRemoveBranch(branchId)}
                                type="text"
                              >
                                <CIcon name="cil-delete"></CIcon>
                              </Button>
                              </Col>
                            </Row>
                          </Timeline.Item>
                        ))}
                      </Timeline>
                    </Col>
                  </Row>
                </Form>
              </Tabs.TabPane>
              <Tabs.TabPane tab="Chi tiết ưu đãi" key="2" style={{ width: "100%" }}>
                <Row
                  justify="space-between"
                  className="my-3"
                  style={{ minWidth: 350 }}
                >
                  <AutoComplete
                    style={{ width: "100%" }}
                    placeholder="Nhập tên ưu đãi"
                    filterOption={(inputValue, option) => option?.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                    allowClear
                  >
                    {promotionList.filter(promotion => !promotions.find(promotion_campaign => promotion_campaign.promotion_id === promotion.id)).map((promotion, index) => (
                      <AutoComplete.Option key={index} value={`${promotion.name} - ${promotion.code}`}>
                        <Row onClick={() => handleAddAndUpdatePromotion(promotion.id, 0)}>
                          {promotion.name} - {promotion.code}
                        </Row>
                      </AutoComplete.Option>
                    ))}
                  </AutoComplete>
                </Row>

                <Row className="mt-3">
                  <List
                    header={
                      <Typography.Title level={5}>
                        Danh sách ưu đãi
                      </Typography.Title>
                    }
                  >
                    {promotions.map((promotion_campaign, index) => (
                      <PromoItem 
                        key={index}
                        minOfMaxQuantity={0}
                        promotion={promotionList.find(promotion => promotion.id === promotion_campaign.promotion_id)}
                        promotionCampaign={promotion_campaign}
                        handleRemovePromotion={handleRemovePromotion}
                        itemList={itemList}
                        itemTypeList={itemTypeList}
                        handleUpdateMaxQuantity={(maxQuantity: number | null) => handleAddAndUpdatePromotion(promotion_campaign.promotion_id, maxQuantity)}
                      />
                    ))}
                  </List>
                </Row>
              </Tabs.TabPane>
            </Tabs>

            <Row className="mt-5" justify="center">
              <Button 
                loading={loading} 
                type="primary"
                onClick={() => form.submit()}>
                Lưu
              </Button>
            </Row>
          </Col>
        </Row>
      </Scrollbars>
    </Drawer>
  );
};

export default AddCampaignModal;
