import { currencyFormat, shortDateFormat } from "@shared/utils/";
import {
  Avatar,
  Button,
  Col,
  Descriptions,
  Divider,
  Empty,
  Input,
  Row,
  Statistic,
  Tag,
  Collapse,
  Typography,
  Spin,
} from "antd";
import React, { useEffect, useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import "./MainCampaignPanel.scss";
import { Branch, Campaign, CampaignBranch, Item, ItemType, Promotion, PromotionBenefit, PromotionCondition } from "@custom-types";
import { colors } from "../../../PromotionManagement/components/AddPromotionModal/AddPromotionModal";

type MainCampaignPanelProps = {
  globalLoading: boolean;
  campaign?: Campaign;
  branchList: Branch[];
  itemList: Item[];
  itemTypeList: ItemType[];
  handleDeactivateOrReactivate: Function;
  toggleCampaignDetailModal: Function;
};

export const promotionDescriptionMapper = (
  conditions: PromotionCondition[],
  benefits: PromotionBenefit[],
  itemList: Item[],
  itemTypeList: ItemType[],
) => {
  let resultConditions = [] as any;
  let resultBenefits = [] as any;

  conditions.forEach((condition: any, index: number) => {
    switch (condition.type) {
      case 1:
        resultConditions.push(
          <li className="my-3" key={index}>
            Mua tối thiểu <Tag color={colors.find(color => color.name === "quantity")?.color} style={{margin: 0}}>{condition.item_quantity}</Tag> món{" "}
            {<Tag color={colors.find(color => color.name === "item")?.color} style={{margin: 0}}>{itemList.find(item => item.id === condition.itemid)?.name}</Tag>}
          </li>
        );
        break;
      case 2: 
        resultConditions.push(
          <li className="my-3" key={index}>
            Đơn hàng tối thiểu <Tag color={colors.find(color => color.name === "money")?.color} style={{margin: 0}}>{currencyFormat(condition.order_total)}</Tag>{" "}
          </li>
        );
        break;
      case 3: 
        resultConditions.push(
          <li className="my-3" key={index}>
            Mua tối thiểu {<Tag color={colors.find(color => color.name === "quantity")?.color} style={{margin: 0}}>{condition.item_quantity}</Tag>} món
          </li>
        );
        break;
      case 4: 
        resultConditions.push(
          <li className="my-3" key={index}>
            Mua tối thiểu {<Tag color={colors.find(color => color.name === "quantity")?.color} style={{margin: 0}}>{condition.item_quantity}</Tag>} món thuộc loại {<Tag color={colors.find(color => color.name === "type")?.color} style={{margin: 0}}>{itemTypeList.find(itemType => itemType.id === condition.item_type_id)?.name}</Tag>}
          </li>
        );
      break;
      default: 
        break;
    }
  });

  benefits.forEach((benefit: any, index: number) => {
    switch (benefit.type) {
      case 1: 
        resultBenefits.push(
          <li className="my-3" key={index}>
            Giảm {" "}
            {benefit.percent ?
            <Tag color={colors.find(color => color.name === "percent")?.color} style={{margin: 0}}>
                {benefit.percent + " %"}
            </Tag>
            :
            <Tag color={colors.find(color => color.name === "money")?.color} style={{margin: 0}}>
              {currencyFormat(benefit.value)}
            </Tag>
            }{" "}
            cho tổng đơn hàng
            {benefit.max_value ? (
              <span>
                , tối đa{" "}
                <Tag color={colors.find(color => color.name === "money")?.color} style={{margin: 0}}>
                  {currencyFormat(benefit.max_value)}
                </Tag>
              </span>
            ) : (
              ""
            )}
          </li>
        );
        break;
      case 2: 
        resultBenefits.push(
          <li className="my-3" key={index}>
            Giảm{" "}
            {benefit.percent ?
            <Tag color={colors.find(color => color.name === "percent")?.color} style={{margin: 0}}>
              {benefit.percent + " %"}
            </Tag>
            :
            <Tag color={colors.find(color => color.name === "money")?.color} style={{margin: 0}}>
              {currencyFormat(benefit.value)}
            </Tag>
            } món <Tag color={colors.find(color => color.name === "item")?.color} style={{margin: 0}}>{itemList.find(item => item.id === benefit.itemid)?.name}</Tag>
            {benefit.max_value ? (
              <span>
                , tối đa <Tag color={colors.find(color => color.name === "money")?.color} style={{margin: 0}}>{currencyFormat(benefit.max_value)}</Tag>
              </span>
            ) : (
              ""
            )}
          </li>
        );
        break;
      case 3: 
        resultBenefits.push(
          <li className="my-3" key={index}>
            Tặng <Tag color={colors.find(color => color.name === "quantity")?.color} style={{margin: 0}}>{benefit.item_quantity}</Tag> món <Tag color={colors.find(color => color.name === "item")?.color} style={{margin: 0}}>{itemList.find(item => item.id === benefit.itemid)?.name}</Tag>
          </li>
        );
        break;
      default:
        break;
    }
  });

  return {
    conditions: [...resultConditions],
    benefits: [...resultBenefits],
  };
};

export const getDayInWeek = (days_string: string) => {
  let dayInWeek = "";
  const days = days_string.split(",").map(days => parseInt(days));
  for(const day of days) {
    switch (day) {
      case 0: 
        dayInWeek += "Thứ 2, ";
        break;
      case 1:
        dayInWeek += "Thứ 3, ";
        break;
      case 2:
        dayInWeek += "Thứ 4, ";
        break;
      case 3:
        dayInWeek += "Thứ 5, ";
        break;
      case 4:
        dayInWeek += "Thứ 6, ";
        break;
      case 5:
        dayInWeek += "Thứ 7, ";
        break;
      case 6:
        dayInWeek += "Chủ nhật, ";
        break;
      default:
        break;
    }
  }
  return dayInWeek.substr(0, dayInWeek.lastIndexOf(","));
}

const getBranchList = (campaignBranchList: CampaignBranch[], branchList: Branch[]) => {
  let branchDetailList = campaignBranchList.map(campaignBranch => branchList.find(branch => branch.id === campaignBranch.branch_id)).filter(branchDetail => branchDetail !== undefined);
  return (
    <ul className="ml-3">
      {branchDetailList.map((branchDetail, index) => (
        <li key={index} className="mt-2">{branchDetail?.name} <Tag color="cyan">{branchDetail?.code}</Tag></li>
      ))}
    </ul>
  )
}

const MainCampaignPanel: React.FC<MainCampaignPanelProps> = React.memo(
  (props) => {
    const { globalLoading, campaign, branchList, itemList, itemTypeList } = props;
    const { 
      handleDeactivateOrReactivate,
      toggleCampaignDetailModal,
    } = props;

    const [tagColor, setTagColor] = useState("#9e9e9e");
    const [tagContent, setTagContent] = useState("");
    const [activationLoading, setActivationLoading] = useState<boolean>(false);
    const [filtered, setFiltered] = useState<(Promotion & {
      current_quantity: number;
      max_quantity: number | null;
    })[]>([]);
    const [searchText, setSearchText] = useState<string>("");

    useEffect(() => {
      switch (campaign?.statuscode) {
        case "AVAILABLE": {
          setTagColor("#198754");
          setTagContent("Hoạt động");
          break;
        }
        case "DISABLED": {
          setTagColor("#dc3545");
          setTagContent("Vô hiệu");
          break;
        }
        default: {
          setTagColor("#9e9e9e");
          setTagContent("Hết hạn");
          break;
        }
      }
      setSearchText("");
      setFiltered(campaign ? campaign.promotions : []);
    }, [campaign]);

    const handleSearch = (value: string) => {
      setSearchText(value);
      if (!value) {
        setFiltered(campaign ? campaign.promotions : []);
      } else {
        const temp = campaign?.promotions.filter(
          (promotion) =>
            promotion.name.toLocaleLowerCase().includes(value.toLowerCase())
            || promotion.code.toLocaleLowerCase().includes(value.toLowerCase())
        );
        setFiltered(temp ? temp : []);
      }
    };

    return (
      <Col span={17} className="main-campaign-panel">
        {globalLoading ? 
          <Spin
            size="large"
            style={{ textAlign: "center", margin: "auto" }}
            spinning={globalLoading}
          />
        :
        !campaign ? (
          <Row justify="center">
            <Empty
              style={{ marginTop: "36vh" }}
              description="Tạo hoặc chọn chương trình để xem thông tin!"
            />
          </Row>
        ) : (
          <Scrollbars
            renderTrackHorizontal={(props) => (
              <div
                {...props}
                className="track-horizontal"
                style={{ display: "none" }}
              />
            )}
            renderThumbHorizontal={(props) => (
              <div
                {...props}
                className="thumb-horizontal"
                style={{ display: "none" }}
              />
            )}
            autoHide
          >
            <Row justify="end" gutter={16}>
              <Col>
                <Button
                  loading={activationLoading}
                  danger={campaign?.statuscode === "AVAILABLE" ? true : false}
                  type={
                    campaign?.statuscode === "AVAILABLE" ? undefined : "primary"
                  }
                  shape="round"
                  onClick={async () => {
                    setActivationLoading(true);
                    await handleDeactivateOrReactivate(campaign.statuscode, campaign.id);
                    setActivationLoading(false);
                  }}
                >
                  {campaign?.statuscode === "AVAILABLE"
                    ? "Khóa chương trình"
                    : "Mở khóa chương trình"}
                </Button>
              </Col>
              <Col>
                <Button shape="round">Kết thúc</Button>
              </Col>
              <Col>
                <Button 
                  shape="round"
                  onClick={() => toggleCampaignDetailModal()}
                >
                  Sửa
                </Button>
              </Col>
            </Row>
            <Row className="mb-5" justify="center" align="middle">
              <Col span={12}>
                <Row justify="center">
                  <Avatar
                    size={224}
                    src={
                      !campaign?.imgpath
                        ? "/item-sample-large.jpg"
                        : campaign.imgpath
                    }
                    className="item-cover-image"
                  />
                </Row>
              </Col>
              <Col span={12}>
                <Typography.Title level={1}>{campaign?.name}</Typography.Title>
                <Typography.Paragraph>
                  {campaign?.description}
                </Typography.Paragraph>
              </Col>
            </Row>
            <Row justify="space-between" className="mb-5" gutter={24}>
              <Col span={4}>
                <Statistic
                  title="Trạng thái"
                  value={tagContent}
                  valueStyle={{ color: tagColor }}
                />
              </Col>
              <Col span={1}>
                <Divider style={{ height: "100%" }} type="vertical" />
              </Col>
              <Col span={4}>
                <Statistic
                  title="Ngày tạo"
                  valueStyle={
                    campaign?.created_at
                      ? { fontSize: 18, marginTop: 8 } 
                      : {}
                  }
                  value={
                    !campaign?.created_at
                      ? "Không có"
                      : shortDateFormat(campaign.created_at)
                  }
                />
              </Col>
              <Col span={1}>
                <Divider style={{ height: "100%" }} type="vertical" />
              </Col>
              <Col span={5}>
                <Statistic
                  title="Lần cập nhập cuối"
                  valueStyle={
                    campaign?.updated_at 
                    ? { fontSize: 18, marginTop: 8 }
                    : {}
                  }
                  value={
                    !campaign?.updated_at
                      ? "Không có"
                      : shortDateFormat(campaign.updated_at)
                  }
                />
              </Col>
            </Row>

            <Row
              justify="space-between"
              gutter={24}
              style={{ paddingBottom: 24 }}
            >
              <Col span={24}>
                <Row className="mb-3">
                  <Typography.Title level={4}>
                    Thông tin áp dụng
                  </Typography.Title>
                </Row>
                <Row justify="space-between">
                  <Col span={24}>
                    <Descriptions column={2} bordered>
                      <Descriptions.Item label="Bắt đầu từ">
                        {shortDateFormat(campaign.start_after)}
                      </Descriptions.Item>
                      <Descriptions.Item label="Kết thúc trước">
                        {shortDateFormat(campaign.end_before)}
                      </Descriptions.Item>
                      <Descriptions.Item label="Ngày trong tuần">
                        {getDayInWeek(campaign.weekday)}
                      </Descriptions.Item>
                      <Descriptions.Item label="Khung giờ">
                        {`${campaign?.start_hour}:00 - ${campaign.end_hour}:00`}
                      </Descriptions.Item>
                      <Descriptions.Item span={2} label="Chi nhánh áp dụng">
                        {getBranchList(campaign.branches, branchList)}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row
              className="mt-3"
              justify="space-between"
              gutter={24}
              style={{ paddingBottom: 24 }}
            >
              <Col span={24}>
                <Row className="mb-3" gutter={24}>
                  <Col>
                    <Typography.Title level={4}>Ưu đãi</Typography.Title>
                  </Col>
                  <Col>
                    <Input 
                      value={searchText}
                      onChange={(e) => handleSearch(e.target.value)}
                      placeholder="Tìm kiếm ưu đãi" 
                    />
                  </Col>
                </Row>
                <Row justify="space-between">
                  <Col span={24}>
                    <Collapse ghost defaultActiveKey={campaign?.promotions.map((promotion, index) => index)}>
                      {filtered.map((promotion, index) => (
                        <Collapse.Panel
                          key={index}
                          header={
                            <Row justify="space-between" align="middle">
                              <Col>
                                <Typography.Title level={5}>
                                  {promotion.name} <Tag color="gold">{promotion.code}</Tag>
                                </Typography.Title>
                              </Col>
                            </Row>
                          }
                        >
                          <Descriptions bordered column={3}>
                            <Descriptions.Item span={1} label="Tối đa">
                              {promotion.max_quantity ? promotion.max_quantity : "Không giới hạn"}
                            </Descriptions.Item>
                            {promotion.max_quantity && promotion.max_quantity - promotion.current_quantity >= 0 ? 
                            <>
                              <Descriptions.Item span={1} label="Đã áp dụng">
                                {promotion.current_quantity}
                              </Descriptions.Item>
                              <Descriptions.Item span={1} label="Còn lại">
                                {promotion.max_quantity - promotion.current_quantity}
                              </Descriptions.Item>
                            </>
                            : 
                            <Descriptions.Item span={2} label="Đã áp dụng">
                                {promotion.current_quantity}
                              </Descriptions.Item>}
                            <Descriptions.Item span={3} label="Điều kiện">
                              <ul className="px-5">
                                {
                                  promotionDescriptionMapper(
                                    promotion.conditions,
                                    promotion.benefits,
                                    itemList,
                                    itemTypeList,
                                  ).conditions
                                }
                              </ul>
                            </Descriptions.Item>
                            <Descriptions.Item span={3} label="Ưu đãi">
                              <ul className="px-5">
                                {
                                  promotionDescriptionMapper(
                                    promotion.conditions,
                                    promotion.benefits,
                                    itemList,
                                    itemTypeList,
                                  ).benefits
                                }
                              </ul>
                            </Descriptions.Item>
                          </Descriptions>
                        </Collapse.Panel>
                      ))}
                    </Collapse>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Scrollbars>
        )}
      </Col>
    );
  }
);

export default MainCampaignPanel;
