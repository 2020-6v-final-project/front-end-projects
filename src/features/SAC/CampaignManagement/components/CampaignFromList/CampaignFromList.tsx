import React, { CSSProperties, useEffect, useState } from "react";
import { Avatar, Col, Row, Tag, Typography } from "antd";
import "./CampaignFromList.scss";

type CampaignFromListProps = {
  campaign: any;
};

const CSSStyle: CSSProperties = {
  textOverflow: "ellipsis",
  overflow: "hidden",
  whiteSpace: "nowrap",
};

const CampaignFromList: React.FC<CampaignFromListProps> = React.memo(
  (props) => {
    const { campaign } = props;

    const [tagColor, setTagColor] = useState("");
    const [tagContent, setTagContent] = useState("");

    useEffect(() => {
      switch (campaign.statuscode) {
        case "AVAILABLE": {
          setTagColor("success");
          setTagContent("Hoạt động");
          break;
        }
        case "DISABLED": {
          setTagColor("error");
          setTagContent("Vô hiệu");
          break;
        }
        default: {
          setTagColor("");
          setTagContent("Hết hạn");
          break;
        }
      }
    }, [campaign.statuscode]);

    return (
      <Row
        className="campaign-from-list"
        justify="start"
        gutter={32}
        align="middle"
        style={{
          width: "100%",
          ...CSSStyle,
        }}
        wrap={false}
      >
        <Col>
          <Avatar
            style={{ width: 64, height: 64 }}
            src={!campaign?.imgpath ? "/item-sample.svg" : campaign.imgpath}
          />
        </Col>
        <Col style={{ ...CSSStyle }}>
          <Typography.Title ellipsis={{ rows: 1, expandable: false }} level={5}>
            {campaign.name}
          </Typography.Title>
          <Tag color={tagColor}>{tagContent}</Tag>
        </Col>
      </Row>
    );
  }
);

export default CampaignFromList;
