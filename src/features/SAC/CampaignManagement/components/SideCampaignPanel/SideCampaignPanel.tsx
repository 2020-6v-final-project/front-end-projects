import { Campaign } from "@custom-types";
import {
  Col,
  Divider,
  Empty,
  Input,
  List,
  Popover,
  Row,
  Spin,
  Tabs,
  Select,
} from "antd";
import React, { useEffect, useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import CampaignFromList from "../CampaignFromList/CampaignFromList";
import "./SideCampaignPanel.scss";

type CampaignSidePanelProps = {
  globalLoading: boolean;
  campaigns: Campaign[];
  currentCampaign?: Campaign;
};

type CampaignSidePanelDispatch = {
  handleCampaignSelect: Function;
};

type CampaignSidePanelDefaultProps = CampaignSidePanelProps &
  CampaignSidePanelDispatch;

const SidePanel: React.FC<CampaignSidePanelDefaultProps> = React.memo(
  (props) => {
    const { campaigns, currentCampaign, globalLoading } = props;
    const { handleCampaignSelect } = props;
    const [filtered, setFiltered] = useState(campaigns);
    const [shouldRender, setShouldRender] = useState(false);

    useEffect(() => {
      if (campaigns?.length < 1) {
        setShouldRender(false);
      } else {
        setFiltered(campaigns);
        setShouldRender(true);
      }
    }, [campaigns]);

    const handleSearch = (value: string) => {
      if (!value) {
        setFiltered(campaigns);
      } else {
        const temp = campaigns.filter(
          (campaign) =>
            campaign.name.toLocaleLowerCase().includes(value.toLowerCase())
        );
        setFiltered(temp);
      }
    };

    return (
      <Col span={7} style={{ height: "90vh" }} className="side-menu">
        <Row justify="space-between" gutter={16}>
          <Col span={14}>
            <Input
              style={{ width: "100%" }}
              onChange={(e) => {
                handleSearch(e.target.value);
              }}
              className="mb-2"
              placeholder="Tìm kiếm chương trình"
            />
          </Col>
          <Col span={10}>
            <Select
              style={{ width: "100%" }}
              placeholder="Sắp xếp"
              defaultValue={"name_asc"}
              defaultActiveFirstOption
            >
              <Select.Option value="name_asc" key={1}>
                Tên tăng dần
              </Select.Option>
              <Select.Option value="name_desc" key={2}>
                Tên giảm dần
              </Select.Option>
              <Select.Option value="baseprice_asc" key={3}>
                Giá tăng dần
              </Select.Option>
              <Select.Option value="baseprice_desc" key={4}>
                Giá giảm dần
              </Select.Option>
            </Select>
          </Col>
        </Row>

        <Row justify="center" align="middle" style={{ height: "100%" }}>
          {globalLoading ? 
          <Spin
            size="large"
            style={{ textAlign: "center", margin: "auto" }}
            spinning={globalLoading}
          />
          :
          <Tabs
            centered
            defaultActiveKey="1"
            style={{ width: "100%", padding: "0px" }}
          >
            <Tabs.TabPane tab="Tất cả" key="1">
              <Row style={{ height: "80vh", padding: "0px" }} justify="center">
                <Col span={23}>
                  {shouldRender ? 
                  <Scrollbars>
                    <List
                      className="list-campaign"
                      style={{ marginBottom: "20px" }}
                    >
                      {filtered?.map((campaign, index) => (
                        <Popover key={index} content={campaign.name}>
                          <List.Item
                            className={
                              currentCampaign?.id === campaign.id
                                ? "active-campaign-from-list"
                                : ""
                            }
                            onClick={() =>
                              handleCampaignSelect(campaign)
                            }
                          >
                            <CampaignFromList campaign={campaign} />
                          </List.Item>
                        </Popover>
                      ))}
                    </List>
                  </Scrollbars>
                  : 
                  <Col span={24}>
                    <Empty
                      style={{ marginTop: "30vh" }}
                      description="Hiện chưa có món nào!"
                    />
                  </Col>
                  }
                </Col>
                <Col span={1}>
                  <Divider type="vertical" style={{ height: "100%" }} />
                </Col>
              </Row>
            </Tabs.TabPane>
            <Tabs.TabPane tab="Hoạt động" key="2">
              <Row style={{ height: "80vh", padding: "0px" }} justify="center">
                <Col span={23}>
                  {shouldRender ? 
                  <Scrollbars>
                    <List
                      className="list-campaign"
                      style={{ marginBottom: "20px" }}
                    >
                      {filtered?.filter(campain => campain.statuscode === "AVAILABLE").map((campaign, index) => (
                        <Popover key={index} content={campaign.name}>
                          <List.Item
                            className={
                              currentCampaign?.id === campaign.id
                                ? "active-campaign-from-list"
                                : ""
                            }
                            onClick={() =>
                              handleCampaignSelect(campaign)
                            }
                          >
                            <CampaignFromList campaign={campaign} />
                          </List.Item>
                        </Popover>
                      ))}
                    </List>
                  </Scrollbars>
                  : 
                  <Col span={24}>
                    <Empty
                      style={{ marginTop: "30vh" }}
                      description="Hiện chưa có món nào!"
                    />
                  </Col>
                  }
                </Col>
                <Col span={1}>
                  <Divider type="vertical" style={{ height: "100%" }} />
                </Col>
              </Row>
            </Tabs.TabPane>
            <Tabs.TabPane tab="Hết hạn" key="3">
            <Row style={{ height: "80vh", padding: "0px" }} justify="center">
                <Col span={23}>
                  {shouldRender ? 
                  <Scrollbars>
                    <List
                      className="list-campaign"
                      style={{ marginBottom: "20px" }}
                    >
                      {filtered?.filter(campain => campain.statuscode === "UNAVAILABLE").map((campaign, index) => (
                        <Popover key={index} content={campaign.name}>
                          <List.Item
                            className={
                              currentCampaign?.id === campaign.id
                                ? "active-campaign-from-list"
                                : ""
                            }
                            onClick={() =>
                              handleCampaignSelect(campaign)
                            }
                          >
                            <CampaignFromList campaign={campaign} />
                          </List.Item>
                        </Popover>
                      ))}
                    </List>
                  </Scrollbars>
                  : 
                  <Col span={24}>
                    <Empty
                      style={{ marginTop: "30vh" }}
                      description="Hiện chưa có món nào!"
                    />
                  </Col>
                  }
                </Col>
                <Col span={1}>
                  <Divider type="vertical" style={{ height: "100%" }} />
                </Col>
              </Row>
            </Tabs.TabPane>
            <Tabs.TabPane tab="Vô hiệu" key="4">
              <Row style={{ height: "80vh", padding: "0px" }} justify="center">
                <Col span={23}>
                  {shouldRender ? 
                  <Scrollbars>
                    <List
                      className="list-campaign"
                      style={{ marginBottom: "20px" }}
                    >
                      {filtered?.filter(campain => campain.statuscode === "DISABLED").map((campaign, index) => (
                        <Popover key={index} content={campaign.name}>
                          <List.Item
                            className={
                              currentCampaign?.id === campaign.id
                                ? "active-campaign-from-list"
                                : ""
                            }
                            onClick={() =>
                              handleCampaignSelect(campaign)
                            }
                          >
                            <CampaignFromList campaign={campaign} />
                          </List.Item>
                        </Popover>
                      ))}
                    </List>
                  </Scrollbars>
                  : 
                  <Col span={24}>
                    <Empty
                      style={{ marginTop: "30vh" }}
                      description="Hiện chưa có món nào!"
                    />
                  </Col>
                  }
                </Col>
                <Col span={1}>
                  <Divider type="vertical" style={{ height: "100%" }} />
                </Col>
              </Row>
            </Tabs.TabPane>
          </Tabs>}
        </Row>
      </Col>
    );
  }
);

export default SidePanel;
