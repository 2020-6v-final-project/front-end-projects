import { getIngredientList, updateIngredient, addIngredient, reactivateIngredient, deactivateIngredient } from '@apis';
import { Ingredient } from '@custom-types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { orderBy } from 'lodash';
import { AppThunk } from 'src/store';
import swal from 'sweetalert';

type IngredientManagementInitialState = {
    ingredientList: Array<Ingredient>,
}

const initialState: IngredientManagementInitialState = {
    ingredientList: [],
}

const IngredientManagementSlice = createSlice({
    name: 'ingredientManagement',
    initialState,
    reducers: {
        getIngredientsSuccess: (state, action: PayloadAction<Ingredient[]>) => {
            state.ingredientList = action.payload;
            state.ingredientList = orderBy(state.ingredientList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        getIngredientsFailed: (state) => {
            return state;
        },
        addIngredientSuccess: (state, action: PayloadAction<Ingredient>) => {
            state.ingredientList?.push(action.payload);
            state.ingredientList = orderBy(state.ingredientList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        addIngredientFailed: (state) => {
            return state;
        },
        updateIngredientSuccess: (state, action: PayloadAction<Ingredient>) => {
            const index = state.ingredientList?.findIndex(ingredient => ingredient.id === action.payload.id);
            if (index > -1) {
                state.ingredientList?.splice(index, 1, action.payload);
            }
        },
        updateIngredientFailed: (state) => {
            return state;
        },
        deactivateIngredientSuccess: (state, action: PayloadAction<Ingredient>) => {
            const index = state.ingredientList?.findIndex(ingredient => ingredient.id === action.payload.id);
            if (index > -1) {
                state.ingredientList?.splice(index, 1, action.payload);
            }
        },
        deactivateIngredientFailed: (state) => {
            return state;
        },
        reactivateIngredientSuccess: (state, action: PayloadAction<Ingredient>) => {
            const index = state.ingredientList?.findIndex(ingredient => ingredient.id === action.payload.id);
            if (index > -1) {
                state.ingredientList?.splice(index, 1, action.payload);
            }
        },
        reactivateIngredientFailed: (state) => {
            return state;
        },
    }

})

export const getIngredientAsync = (): AppThunk => async (dispatch) => {
    try {
        await getIngredientList((data: Ingredient[]) => {
            dispatch(getIngredientsSuccess(data));
        });
    } catch (e) {
        dispatch(getIngredientsFailed());
    }
}

export const addIngredientAsync = (ingredient: any): AppThunk => async (dispatch) => {
    try {
        await addIngredient(ingredient, (data: Ingredient) => {
            swal("Thành công", "Nguyên liệu đã được thêm!", "success");
            dispatch(addIngredientSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(addIngredientFailed());
    }
}

export const updateIngredientAsync = (ingredient: any): AppThunk => async (dispatch) => {
    try {
        await updateIngredient(ingredient, (data: Ingredient) => {
            swal("Thành công", "Nguyên liệu đã được cập nhật!", "success");
            dispatch(updateIngredientSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(updateIngredientFailed());
    }
}

export const deactivateIngredientAsync = (ingredientId: number): AppThunk => async (dispatch) => {
    try {
        await deactivateIngredient(ingredientId, (data: Ingredient) => {
            swal("Thành công", "Nguyên liệu đã bị khóa!", "success");
            dispatch(deactivateIngredientSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(deactivateIngredientFailed());
    }
}

export const reactivateIngredientAsync = (ingredientId: number): AppThunk => async (dispatch) => {
    try {
        await reactivateIngredient(ingredientId, (data: Ingredient) => {
            swal("Thành công", "Nguyên liệu đã được mở khóa!", "success");
            dispatch(reactivateIngredientSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(reactivateIngredientFailed());
    }
}

const {
    getIngredientsSuccess,
    getIngredientsFailed,
    addIngredientSuccess,
    addIngredientFailed,
    updateIngredientSuccess,
    updateIngredientFailed,
    deactivateIngredientSuccess,
    deactivateIngredientFailed,
    reactivateIngredientSuccess,
    reactivateIngredientFailed
} = IngredientManagementSlice.actions;

export default IngredientManagementSlice.reducer;
