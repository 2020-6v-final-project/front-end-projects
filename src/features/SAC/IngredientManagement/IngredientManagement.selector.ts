import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectIngredientList = createSelector(
    [(state: RootState) => state.ingredient.ingredientList],
    (ingredientList) => ingredientList
)