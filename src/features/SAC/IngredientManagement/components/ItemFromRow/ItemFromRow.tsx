import { Avatar, Card, Col, Row, Tag, Typography } from "antd";
import React, { CSSProperties } from "react";
import { currencyFormat } from "@utils";
import "./ItemFromRow.scss";

type ItemFromRowProps = {
  item: any | undefined;
  quantity: number | undefined;
  unit: string | undefined;
};

const styleProperties: CSSProperties = {
  overflow: "hidden",
  textOverflow: "ellipsis",
  whiteSpace: "nowrap",
};

const ItemFromRow: React.FC<ItemFromRowProps> = (props) => {
  const { item, quantity, unit } = props;
  return (
    <Card
      hoverable
      size="default"
      style={{ ...styleProperties, padding: 16, borderRadius: 20, width: 224 }}
    >
      <Row justify="center">
        <Avatar
          size={64}
          src={
            item?.imgpath === null || item?.imgpath === ""
              ? "/item-sample.svg"
              : item.imgpath
          }
        ></Avatar>
      </Row>
      <Row className="mt-3" justify="center">
        <Col style={styleProperties}>
          <Typography.Title style={{ textAlign: "center" }} level={4}>
            {item?.name}
          </Typography.Title>
        </Col>
      </Row>
      <Row justify="center" className="mb-3">
        <Col>
          <Tag color="orange">
            {quantity} {unit}
          </Tag>
        </Col>
      </Row>
      <Row justify="center">
        <Col>
          <Typography.Text>
            {currencyFormat(item?.price)} / {item?.unit}
          </Typography.Text>
        </Col>
      </Row>
    </Card>
  );
};

export default ItemFromRow;
