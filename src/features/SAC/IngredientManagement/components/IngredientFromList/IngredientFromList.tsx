import React, { CSSProperties } from "react";
import { Avatar, Col, Row, Typography } from "antd";
import "./IngredientFromList.scss";
import { Ingredient } from "@custom-types";

type IngredientFromListProps = {
  ingredient: Ingredient;
};

const styleProperties: CSSProperties = {
  textOverflow: "ellipsis",
  overflow: "hidden",
  whiteSpace: "nowrap",
};

const IngredientFromList: React.FC<IngredientFromListProps> = React.memo(
  (props) => {
    const { ingredient } = props;
    return (
      <Row
        className="ingredient-from-list"
        justify="start"
        gutter={32}
        align="middle"
        wrap={false}
        style={{ width: "100%" }}
      >
        <Col>
          <Avatar style={{ width: 64, height: 64 }} src={(ingredient?.imgpath === null || ingredient?.imgpath === "") ? "/item-sample.svg" : ingredient.imgpath}/>
        </Col>
        <Col style={{ ...styleProperties }}>
          <Typography.Title style={{ ...styleProperties }} level={5}>
            {ingredient?.name}
          </Typography.Title>
          {/* <Typography.Text style={{ ...styleProperties }}>
            {currencyFormat(ingredient?.baseprice)} / {ingredient.unit}
          </Typography.Text> */}
        </Col>
      </Row>
    );
  }
);

export default IngredientFromList;
