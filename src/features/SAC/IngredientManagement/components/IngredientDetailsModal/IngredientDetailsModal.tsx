import { SubHeaderText } from "@components";
import { Ingredient, IngredientType } from "@custom-types";
import { Button, Col, Drawer, Form, Input, Row, Upload, Select } from "antd";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import React, { useState, useEffect } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import { azureStorageBlobURL, uploadFileToBlob } from '../../../../Azure/azure-storage-blob';
import "./IngredientDetailsModal.scss";
import swal from "sweetalert";

type IngredientDetailsModalProps = {
  ingredient: Ingredient | undefined;
  show: boolean;
  onFinish: Function;
  toggleModal: Function;
  azureKey: string;
  ingredientTypeList: Array<IngredientType>;
};

const IngredientDetailsModal: React.FC<IngredientDetailsModalProps> = (
  props
) => {
  const {
    azureKey,
    ingredient,
    show,
    onFinish,
    toggleModal,
    ingredientTypeList,
  } = props;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [loadImg, setLoadImg] = useState<boolean>(false);
  const [imageUrl, setImageUrl] = useState<any>("");
  const [formValue, setFormValue] = useState<any>({
    name: "",
    unit: "",
    typeid: "",
  });

  useEffect(() => {
    setImageUrl(ingredient?.imgpath);
    setFormValue({
      name: ingredient?.name,
      unit: ingredient?.unit,
      typeid: ingredient?.typeid,
    });
    form?.setFieldsValue({
      name: ingredient?.name,
      unit: ingredient?.unit,
      typeid: ingredient?.typeid,
    });
  }, [ingredient, form, show]);

  const handleUpdateIngredient = async () => {
    const sendData = {
      ...formValue,
      id: ingredient?.id,
      imgpath: imageUrl,
    };
    setLoading(true);
    await onFinish(sendData);
    setLoading(false);
    form.resetFields();
    setImageUrl("");
    setLoadImg(false);
    toggleModal();
  };

  const setInitialFormData = () => {
    setFormValue({
      name: ingredient?.name,
      unit: ingredient?.unit,
    });
    form?.setFieldsValue({
      name: ingredient?.name,
      unit: ingredient?.unit,
    });
  };

  const handleImgChange = async (info: any) => {
    const isJpgOrPng =
      info.file.type === "image/jpeg" || info.file.type === "image/png";
    const isLt2M = info.file.size / 1024 / 1024 < 2;
    setLoadImg(true);
    if (!isJpgOrPng) {
      swal("Thất bại", "Bạn chỉ có thể tải file JPG/PNG!", "error");
    } else if (!isLt2M) {
      swal("Thất bại", "Kích cỡ file vượt quá 2MB!", "error");
    } else if (imageUrl !== "") {
      setImageUrl("");
    } else if (info.file) {
      const responseIMG: any = await uploadFileToBlob(info.file, azureKey);
      setImageUrl(responseIMG.url);
      setLoadImg(false);
    }
  };

  return (
    <Drawer
      visible={show}
      width="500"
      onClose={(e) => {
        toggleModal();
        setInitialFormData();
      }}
      zIndex={1}
      className="add-ingredient-modal"
    >
      <Scrollbars autoHide>
        <Row style={{ margin: 20 }} justify="center" align="middle">
          <Col>
            <SubHeaderText style={{ textAlign: "center" }} className="mb-4">
              Chỉnh sửa nguyên liệu
            </SubHeaderText>
            <Row justify="center" className="mb-3">
              <Upload
                name="avatar"
                listType="picture-card"
                className="avatar-uploader upload-img"
                action={azureStorageBlobURL + azureKey}
                showUploadList={false}
                beforeUpload={() => false}
                onChange={(e) => handleImgChange(e)}
              >
                {imageUrl ? (
                  <img src={imageUrl} alt="avatar" style={{ width: "100%" }} />
                ) : (
                  <div>
                    {loadImg ? <LoadingOutlined /> : <PlusOutlined />}
                    <div style={{ marginTop: 8 }}>Tải ảnh</div>
                  </div>
                )}
              </Upload>
            </Row>
            <Form
              form={form}
              name="addIngredientForm"
              onFinish={handleUpdateIngredient}
            >
              <Row justify="center">
                <Col span={24}>
                  <Form.Item
                    name="name"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền tên nguyên liệu",
                      },
                    ]}
                    initialValue={ingredient?.name}
                  >
                    <Input
                      style={{
                        border: "none",
                        textAlign: "center",
                        fontSize: 18,
                        fontWeight: 400,
                      }}
                      placeholder="Tên nguyên liệu"
                      onChange={(e) =>
                        setFormValue({ ...formValue, name: e.target.value })
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={24} justify="space-between">
                <Col span={12}>
                  <Form.Item
                    name="typeid"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng chọn loại nguyên liệu",
                      },
                    ]}
                    initialValue={ingredient?.typeid}
                  >
                    <Select
                      placeholder="Loại nguyên liệu"
                      value={formValue.typeid || ""}
                      onChange={(value) =>
                        setFormValue({ ...formValue, typeid: value })
                      }
                    >
                      {ingredientTypeList?.map((ingredientType) => (
                        <Select.Option
                          key={ingredientType.id}
                          value={ingredientType.id}
                        >
                          {ingredientType.name}
                        </Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    name="unit"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền đơn vị",
                      },
                    ]}
                    initialValue={ingredient?.unit}
                  >
                    <Input
                      placeholder="Đơn vị"
                      onChange={(e) =>
                        setFormValue({ ...formValue, unit: e.target.value })
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row justify="center">
                <Form.Item>
                  <Button loading={loading} type="primary" htmlType="submit">
                    Lưu
                  </Button>
                </Form.Item>
              </Row>
            </Form>
          </Col>
        </Row>
      </Scrollbars>
    </Drawer>
  );
};

export default IngredientDetailsModal;
