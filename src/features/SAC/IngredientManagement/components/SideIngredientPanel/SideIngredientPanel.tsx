import { Ingredient, IngredientType } from "@shared";
import {
  Col,
  Divider,
  Empty,
  Input,
  List,
  Popover,
  Row,
  Spin,
  Typography,
  Select,
  Tabs,
} from "antd";
import Text from "antd/lib/typography/Text";
import React, { useEffect, useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import IngredientFromList from "../IngredientFromList/IngredientFromList";
import "./SideIngredientPanel.scss";

type IngredientSidePanelProps = {
  ingredients: Array<Ingredient>;
  ingredientTypeList: Array<IngredientType>;
  handleIngredientSelect: Function;
  currentIngredient?: Ingredient;
};

const SidePanel: React.FC<IngredientSidePanelProps> = React.memo((props) => {
  const {
    ingredients,
    handleIngredientSelect,
    currentIngredient,
    ingredientTypeList,
  } = props;
  const [filteredIngredients, setFilteredIngredients] = useState<
    Ingredient[] | null
  >(null);
  const [shouldRender, setShouldRender] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (ingredients.length < 1) {
      setLoading(false);
      setShouldRender(false);
    } else {
      setLoading(false);
      setShouldRender(true);
      setFilteredIngredients(ingredients);
    }
  }, [ingredients]);

  const handleSearch = (value: string) => {
    if (value === "") setFilteredIngredients(ingredients);
    else
      setFilteredIngredients(
        ingredients.filter((ingredient) =>
          ingredient.name.toLocaleLowerCase().includes(value.toLowerCase())
        )
      );
  };

  const content = (ingredient: Ingredient) => (
    <Typography.Title level={5} style={{ fontWeight: 300 }}>
      Trạng thái:{" "}
      <Text
        type={ingredient?.statuscode === "AVAILABLE" ? "success" : "danger"}
      >
        {ingredient?.statuscode}
      </Text>
    </Typography.Title>
  );

  return (
    <Col span={7} style={{ height: "90vh" }}>
      <Row justify="space-between" gutter={16}>
        <Col span={14}>
          <Input
            style={{ width: "100%" }}
            onChange={(e) => {
              handleSearch(e.target.value);
            }}
            className="mb-2"
            placeholder="Tìm kiếm nguyên liệu"
          />
        </Col>
        <Col span={10}>
          <Select
            style={{ width: "100%" }}
            placeholder="Sắp xếp"
            defaultValue={"name_asc"}
            defaultActiveFirstOption
          >
            <Select.Option value="name_asc" key={1}>
              Tên tăng dần
            </Select.Option>
            <Select.Option value="name_desc" key={2}>
              Tên giảm dần
            </Select.Option>
            <Select.Option value="baseprice_asc" key={3}>
              Giá tăng dần
            </Select.Option>
            <Select.Option value="baseprice_desc" key={4}>
              Giá giảm dần
            </Select.Option>
          </Select>
        </Col>
      </Row>

      <Row justify="center">
        <Tabs
          centered
          defaultActiveKey="1"
          style={{ width: "100%", padding: "0px" }}
        >
          <Tabs.TabPane tab="Tất cả" key="1">
            <Row style={{ height: "80vh" }} justify="center">
              <Col span={23}>
                {loading ? (
                  <Spin
                    size="large"
                    style={{ textAlign: "center", margin: "auto" }}
                    spinning={loading}
                  />
                ) : (
                  <>
                    {shouldRender ? (
                      <>
                        <Scrollbars>
                          <List
                            className="list-ingredient"
                            style={{ marginBottom: "176px" }}
                          >
                            {filteredIngredients?.map((ingredient, index) => (
                              <Popover key={index} content={ingredient.name}>
                                <List.Item
                                  onClick={() =>
                                    handleIngredientSelect(ingredient)
                                  }
                                  className={
                                    currentIngredient?.id === ingredient.id
                                      ? "active-item-from-list"
                                      : ""
                                  }
                                >
                                  <IngredientFromList ingredient={ingredient} />
                                </List.Item>
                              </Popover>
                            ))}
                          </List>
                        </Scrollbars>
                      </>
                    ) : (
                      <Empty
                        style={{ marginTop: "30vh" }}
                        description="Chưa có nguyên liệu!"
                      />
                    )}
                  </>
                )}
              </Col>

              <Col span={1}>
                <Divider type="vertical" style={{ height: "100%" }} />
              </Col>
            </Row>
          </Tabs.TabPane>
          {ingredientTypeList.map((ingredientType, ingredientTypeIndex) => (
            <Tabs.TabPane
              tab={ingredientType.name}
              key={(ingredientTypeIndex + 2).toString()}
            >
              <Row style={{ height: "65vh" }} justify="center">
                <Col span={23}>
                  {loading ? (
                    <Spin
                      size="large"
                      style={{ textAlign: "center", margin: "auto" }}
                      spinning={loading}
                    />
                  ) : (
                    <>
                      {shouldRender ? (
                        <>
                          <Scrollbars>
                            <List
                              className="list-ingredient"
                              style={{ marginBottom: "176px" }}
                            >
                              {filteredIngredients
                                ?.filter(
                                  (ingredient) =>
                                    ingredient.typeid === ingredientType.id
                                )
                                .map((ingredient, index) => (
                                  <Popover
                                    key={index}
                                    title={ingredient.name}
                                    content={() => content(ingredient)}
                                  >
                                    <List.Item
                                      onClick={() =>
                                        handleIngredientSelect(ingredient)
                                      }
                                      className={
                                        currentIngredient?.id === ingredient.id
                                          ? "active-item-from-list"
                                          : ""
                                      }
                                    >
                                      <IngredientFromList
                                        ingredient={ingredient}
                                      />
                                    </List.Item>
                                  </Popover>
                                ))}
                            </List>
                          </Scrollbars>
                        </>
                      ) : (
                        <Empty
                          style={{ marginTop: "30vh" }}
                          description="Chưa có nguyên liệu!"
                        />
                      )}
                    </>
                  )}
                </Col>

                <Col span={1}>
                  <Divider type="vertical" style={{ height: "100%" }} />
                </Col>
              </Row>
            </Tabs.TabPane>
          ))}
        </Tabs>
      </Row>
    </Col>
  );
});

export default SidePanel;
