import { Ingredient, ItemType } from "@custom-types";
import {
  Avatar,
  Button,
  Col,
  Row,
  Typography,
  Divider,
  Statistic,
  Empty,
  Input,
  Select,
  Tabs,
} from "antd";
import React, { useEffect, useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import ItemFromRow from "../ItemFromRow/ItemFromRow";
import { shortDateFormat } from "@shared/utils/stringUtils";
import "./MainIngredientPanel.scss";
import { Link } from "react-router-dom";

type MainIngredientPanelProps = {
  ingredient: Ingredient | undefined;
  itemTypeList: Array<ItemType>;
  handleDeactivateOrReactivate: Function;
  handleEditButtonClick: Function;
};

const MainIngredientPanel: React.FC<MainIngredientPanelProps> = React.memo(
  (props) => {
    const {
      ingredient,
      itemTypeList,
      handleDeactivateOrReactivate,
      handleEditButtonClick,
    } = props;

    const [filtered, setFiltered] = useState(ingredient?.items || []);
    const [loading, setLoading] = useState<boolean>(false);

    useEffect(() => {
      setFiltered(ingredient?.items || []);
    }, [ingredient, itemTypeList]);

    const handleSearch = (value: string) => {
      if (!value) {
        setFiltered(ingredient?.items || []);
      } else {
        let temp = ingredient?.items.filter((item: any) =>
          item.name.toLocaleLowerCase().includes(value.toLocaleLowerCase())
        );
        setFiltered(temp || []);
      }
    };

    return (
      <Col span={17} className="main-ingredient-panel">
        {!ingredient ? (
          <Row justify="center" style={{ margin: "auto" }}>
            <Empty description="Tạo hoặc chọn nguyên liệu để xem thông tin!" />
          </Row>
        ) : (
          <Scrollbars
            renderTrackHorizontal={(props) => (
              <div
                {...props}
                className="track-horizontal"
                style={{ display: "none" }}
              />
            )}
            renderThumbHorizontal={(props) => (
              <div
                {...props}
                className="thumb-horizontal"
                style={{ display: "none" }}
              />
            )}
            autoHide
          >
            <Row justify="end" gutter={16} style={{ padding: 0 }}>
              <Col>
                <Button
                  danger={ingredient?.statuscode === "AVAILABLE" ? true : false}
                  type={
                    ingredient?.statuscode === "AVAILABLE"
                      ? undefined
                      : "primary"
                  }
                  onClick={async () => {
                    setLoading(true);
                    await handleDeactivateOrReactivate(
                      ingredient?.id,
                      ingredient?.statuscode
                    );
                    setLoading(false);
                  }}
                  shape="round"
                  loading={loading}
                >
                  {ingredient?.statuscode === "AVAILABLE"
                    ? "Khóa nguyên liệu"
                    : "Mở khóa nguyên liệu"}
                </Button>
              </Col>
              <Col>
                <Button onClick={() => handleEditButtonClick()} shape="round">
                  Sửa
                </Button>
              </Col>
            </Row>
            <Row className="mb-4" justify="center" align="middle">
              <Col span={24}>
                <Row justify="center">
                  <Avatar
                    src={
                      ingredient?.imgpath === null || ingredient?.imgpath === ""
                        ? "/item-sample-large.jpg"
                        : ingredient.imgpath
                    }
                    className="ingredient-cover-image"
                  />
                </Row>
              </Col>
            </Row>
            <Row justify="center" className="mb-4">
              <Col span={24}>
                <Typography.Title style={{ textAlign: "center" }} level={2}>
                  {ingredient?.name}
                </Typography.Title>
              </Col>
            </Row>
            <Row justify="space-between" className="mb-5">
              <Col span={4}>
                <Statistic title="Đơn vị" value={ingredient?.unit} />
              </Col>
              <Col span={1}>
                <Divider type="vertical" style={{ height: "100%" }} />
              </Col>
              <Col span={4}>
                <Statistic
                  title="Trạng thái"
                  valueStyle={
                    ingredient?.statuscode === "AVAILABLE"
                      ? { color: "#3f8600" }
                      : { color: "#cf1322" }
                  }
                  value={
                    ingredient.statuscode === "AVAILABLE"
                      ? "Hoạt động"
                      : "Vô hiệu"
                  }
                />
              </Col>
              <Col span={1}>
                <Divider type="vertical" style={{ height: "100%" }} />
              </Col>
              <Col span={5}>
                <Statistic
                  title="Ngày tạo"
                  valueStyle={
                    ingredient?.createdat ? { fontSize: 18, marginTop: 8 } : {}
                  }
                  value={
                    ingredient?.createdat
                      ? shortDateFormat(ingredient?.createdat)
                      : "Không có"
                  }
                />
              </Col>
              <Col span={1}>
                <Divider type="vertical" style={{ height: "100%" }} />
              </Col>
              <Col span={5}>
                <Statistic
                  title="Cập nhật"
                  valueStyle={
                    ingredient?.lastmodified
                      ? { fontSize: 18, marginTop: 8 }
                      : {}
                  }
                  value={
                    ingredient?.lastmodified
                      ? shortDateFormat(ingredient?.lastmodified)
                      : "Không có"
                  }
                />
              </Col>
            </Row>
            <Row gutter={24}>
              <Col>
                <Typography.Title level={4}>Các món ăn</Typography.Title>
              </Col>
              <Col>
                <Input
                  placeholder="Tìm kiếm món ăn"
                  onChange={(e) => handleSearch(e.target.value)}
                />
              </Col>
              <Col>
                <Select
                  placeholder="Sắp xếp"
                  defaultValue={"name_asc"}
                  defaultActiveFirstOption
                >
                  <Select.Option value="name_asc" key={1}>
                    Tên tăng dần
                  </Select.Option>
                  <Select.Option value="name_desc" key={2}>
                    Tên giảm dần
                  </Select.Option>
                  {/* <Select.Option value="baseprice_asc" key={3}>
                    Giá tăng dần
                  </Select.Option>
                  <Select.Option value="baseprice_desc" key={4}>
                    Giá giảm dần
                  </Select.Option> */}
                </Select>
              </Col>
            </Row>

            <Row justify="center" className="my-3">
              <Tabs centered defaultActiveKey="1">
                <Tabs.TabPane tab="Tất cả" key="1">
                  <Row
                    className="mt-3 mb-5"
                    style={{ padding: 0, margin: 0 }}
                    gutter={[24, 24]}
                  >
                    {filtered?.length > 0 ? (
                      <>
                        {filtered?.map((item, index) => {
                          return (
                            <Link
                              to={"/item-management/" + item.id}
                              key={index}
                            >
                              <Col>
                                <ItemFromRow
                                  item={item}
                                  quantity={item.quantity}
                                  unit={ingredient?.unit}
                                />
                              </Col>
                            </Link>
                          );
                        })}
                      </>
                    ) : (
                      <Col span={24}>
                        <Empty
                          style={{ margin: "auto" }}
                          className="mb-5"
                          description="Chưa có món nào!"
                        />
                      </Col>
                    )}
                  </Row>
                </Tabs.TabPane>
                {itemTypeList.map((itemType, itemTypeIndex) => (
                  <Tabs.TabPane
                    tab={itemType.name}
                    key={(itemTypeIndex + 2).toString()}
                  >
                    <Row
                      className="mt-3 mb-5"
                      style={{ padding: 0, margin: 0 }}
                      gutter={[24, 24]}
                    >
                      {filtered?.filter((item) => item.typeid === itemType.id)
                        .length > 0 ? (
                        <>
                          {filtered
                            ?.filter((item) => item.typeid === itemType.id)
                            .map((item, index) => {
                              return (
                                <Link
                                  to={"/item-management/" + item.id}
                                  key={index}
                                >
                                  <Col>
                                    <ItemFromRow
                                      item={item}
                                      quantity={item.quantity}
                                      unit={ingredient?.unit}
                                    />
                                  </Col>
                                </Link>
                              );
                            })}
                        </>
                      ) : (
                        <Col span={24}>
                          <Empty
                            style={{ margin: "auto" }}
                            className="mb-5"
                            description="Chưa có món nào!"
                          />
                        </Col>
                      )}
                    </Row>
                  </Tabs.TabPane>
                ))}
              </Tabs>
            </Row>
          </Scrollbars>
        )}
      </Col>
    );
  }
);

export default MainIngredientPanel;
