import { FAB } from "@components";
import { CFade } from "@coreui/react";
import { Ingredient, IngredientType, ItemType } from "@shared";
import { history } from "@shared/components";
import { message, Row } from "antd";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { RootState } from "src/store";
import { selectAzureConfig } from "../../Azure/AzureManagement.selector";
import { getKeyAsync } from "../../Azure/AzureManagement.slice";
import {
  selectIngredientTypeList,
  selectItemTypeList,
} from "../ItemIngredientTypeManagement/ItemIngredientTypeManagement.selector";
import {
  getIngredientTypeAsync,
  getItemTypeAsync,
} from "../ItemIngredientTypeManagement/ItemIngredientTypeManagement.slice";
import AddIngredientModal from "./components/AddIngredientModal/AddIngredientModal";
import IngredientDetailsModal from "./components/IngredientDetailsModal/IngredientDetailsModal";
import MainIngredientPanel from "./components/MainIngredientPanel/MainIngredientPanel";
import SideIngredientPanel from "./components/SideIngredientPanel/SideIngredientPanel";
import "./IngredientManagement.selector";
import { selectIngredientList } from "./IngredientManagement.selector";
import {
  addIngredientAsync,
  deactivateIngredientAsync,
  getIngredientAsync,
  reactivateIngredientAsync,
  updateIngredientAsync,
} from "./IngredientManagement.slice";

type IngredientManagementState = {
  ingredientList: Array<Ingredient>;
  itemTypeList: Array<ItemType>;
  ingredientTypeList: Array<IngredientType>;
  match: any;
  azureKey: string;
};

type IngredientManagementDispatch = {
  getIngredientAsync: Function;
  updateIngredientAsync: Function;
  addIngredientAsync: Function;
  reactivateIngredientAsync: Function;
  deactivateIngredientAsync: Function;
  getKeyAsync: Function;
  getIngredientTypeAsync: Function;
  getItemTypeAsync: Function;
};

type IngredientManagementDefaultProps = IngredientManagementState &
  IngredientManagementDispatch;

const IngredientManagementContainer: React.FC<IngredientManagementDefaultProps> = React.memo(
  (props) => {
    const {
      azureKey,
      ingredientList,
      match,
      ingredientTypeList,
      itemTypeList,
    } = props;
    const {
      getKeyAsync,
      getIngredientAsync,
      addIngredientAsync,
      reactivateIngredientAsync,
      deactivateIngredientAsync,
      updateIngredientAsync,
      getIngredientTypeAsync,
      getItemTypeAsync,
    } = props;

    const [currentIngredient, setCurrentIngredient] = useState<
      Ingredient | undefined
    >(ingredientList[0] || undefined);

    const [addIngredientModalIsOpen, toggleAddIngredientModal] = useState(
      false
    );
    const [
      ingredientDetailsModalIsOpen,
      toggleIngredientDetailsModal,
    ] = useState(false);

    useEffect(() => {
      const fetchData = async () => {
        await getItemTypeAsync();
        await getIngredientAsync();
        await getIngredientTypeAsync();
        await getKeyAsync();
      };
      fetchData();
    }, [
      getIngredientAsync,
      getKeyAsync,
      getIngredientTypeAsync,
      getItemTypeAsync,
    ]);

    useEffect(() => {
      if (ingredientList.length > 0 && currentIngredient) {
        setCurrentIngredient(
          ingredientList.find((item) => item.id === currentIngredient.id)
        );
      }
    }, [currentIngredient, ingredientList]);

    useEffect(() => {
      if (ingredientList.length > 0) {
        if (!match.params.id) {
          setCurrentIngredient(ingredientList[0]);
          history.push("/ingredient-management");
          return;
        }
        const target = ingredientList.find(
          (ingredient) => ingredient.id === Number(match.params.id)
        );
        if (target) {
          setCurrentIngredient(target);
        } else {
          history.push("/ingredient-management");
          message.error("Không tìm thấy nguyên liệu!");
        }
      } else {
        history.push("/ingredient-management");
      }
    }, [match.params.id, ingredientList]);

    const handleIngredientSelect = (ingredient: Ingredient | undefined) => {
      setCurrentIngredient(ingredient);
      history.push("/ingredient-management/" + ingredient?.id);
    };

    const handleAddIngredient = async (value: any) => {
      await addIngredientAsync(value);
    };

    const handleUpdateIngredient = async (value: any) => {
      await updateIngredientAsync(value);
    };

    const handleDeactivateOrReactivate = async (
      ingredientId: number,
      statusCode: string
    ) => {
      if (statusCode === "AVAILABLE")
        await deactivateIngredientAsync(ingredientId);
      else await reactivateIngredientAsync(ingredientId);
    };

    return (
      <CFade>
        <div style={{ overflow: "hidden" }}>
          <Row gutter={48}>
            <SideIngredientPanel
              currentIngredient={currentIngredient}
              ingredients={ingredientList}
              ingredientTypeList={ingredientTypeList}
              handleIngredientSelect={handleIngredientSelect}
            />
            <MainIngredientPanel
              ingredient={currentIngredient}
              itemTypeList={itemTypeList}
              handleEditButtonClick={() =>
                toggleIngredientDetailsModal(!ingredientDetailsModalIsOpen)
              }
              handleDeactivateOrReactivate={handleDeactivateOrReactivate}
            />
          </Row>
        </div>

        <FAB
          callback={() => toggleAddIngredientModal(!addIngredientModalIsOpen)}
        />
        <AddIngredientModal
          azureKey={azureKey}
          show={addIngredientModalIsOpen}
          onFinish={handleAddIngredient}
          toggleModal={toggleAddIngredientModal}
          ingredientTypeList={ingredientTypeList}
        ></AddIngredientModal>
        <IngredientDetailsModal
          azureKey={azureKey}
          show={ingredientDetailsModalIsOpen}
          onFinish={handleUpdateIngredient}
          toggleModal={toggleIngredientDetailsModal}
          ingredient={currentIngredient}
          ingredientTypeList={ingredientTypeList}
        />
      </CFade>
    );
  }
);

const mapState = (state: RootState) => {
  return {
    ingredientList: selectIngredientList(state),
    ingredientTypeList: selectIngredientTypeList(state),
    itemTypeList: selectItemTypeList(state),
    azureKey: selectAzureConfig(state),
  };
};
const mapDispatch = {
  getIngredientAsync,
  updateIngredientAsync,
  addIngredientAsync,
  reactivateIngredientAsync,
  deactivateIngredientAsync,
  getKeyAsync,
  getIngredientTypeAsync,
  getItemTypeAsync,
};

export default connect(mapState, mapDispatch)(IngredientManagementContainer);
