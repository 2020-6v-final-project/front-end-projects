import { CFade } from "@coreui/react";
import React from "react";
import BranchStatistics from './components/BranchStatistics/BranchStatistics';
import ListCompanies from "./components/ListBranches/ListBranches";
import { connect } from 'react-redux';
import { Branch, Currency, Menu, User } from '@custom-types';
import { branchListSelector } from './BranchManagement.selector';
import { 
    fetchBranchListAsync, 
    addBranchAsync,
    deactivateBranchAsync, 
    updateBranchAsync,
    reactivateBranchAsync, 
} from './BranchManagement.slice';
import { userListSelector } from '../UserManagement/UserManagement.selector';
import { 
    fetchUserListAsync,
} from '../UserManagement/UserManagement.slice';
import { selectMenuList } from '../MenuManagement/MenuManagement.selector';
import { 
    getMenuAsync,
} from '../MenuManagement/MenuManagement.slice'
import { selectCurrencyList } from '../CurrencyManagement/CurrencyManagement.selector';
import { 
    fetchCurrencyListAsync,
} from '../CurrencyManagement/CurrencyManagement.slice'
import { RootState } from 'src/store';

type BranchManagementState = {
    branchList: Branch[],
    userList: User[],
    menuList: Menu[],
    currencyList: Currency[],
}

type BranchManagementDispatch = {
    fetchBranchListAsync: Function,
    addBranchAsync: Function,
    deactivateBranchAsync: Function,
    updateBranchAsync: Function,
    reactivateBranchAsync: Function,
    fetchUserListAsync: Function,
    getMenuAsync: Function,
    fetchCurrencyListAsync: Function,
}

export type BranchManagementDefaultProps = BranchManagementState & BranchManagementDispatch;

const BranchManagementContainer: React.FC<BranchManagementDefaultProps> = React.memo((props) => {
    return (
        <CFade>
            <BranchStatistics />
            <ListCompanies {...props} />
        </CFade>
    );
});

const mapState = (state: RootState) => {
    return {
        branchList: branchListSelector(state),
        userList: userListSelector(state),
        menuList: selectMenuList(state),
        currencyList: selectCurrencyList(state),
    }
}
const mapDispatch = { 
    fetchBranchListAsync, 
    addBranchAsync, 
    deactivateBranchAsync, 
    reactivateBranchAsync,
    updateBranchAsync,
    fetchUserListAsync,
    getMenuAsync,
    fetchCurrencyListAsync,
};

export default connect(mapState, mapDispatch)(BranchManagementContainer);
