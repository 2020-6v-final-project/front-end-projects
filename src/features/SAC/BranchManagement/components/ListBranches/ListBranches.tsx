import { freeSet } from "@coreui/icons";
import CIcon from "@coreui/icons-react";
import {
  CBadge,
  CButton,
  CCard,
  CCardBody,
  CCol,
  CInputGroup,
  CRow,
} from "@coreui/react";
import React, { useEffect, useState } from "react";
import { DataTable } from "@components";
import { BranchManagementDefaultProps } from "../../BranchManagement.container";
import { Branch, User } from "@custom-types";
import AddBranchModal from "../AddBranchModal/AddBranchModal";
import BranchDetailModal from "../BranchDetailModal/BranchDetailModal";
import "./ListBranches.scss";

const getFullName = (
  userList?: User[],
  managerId?: number
): string | undefined => {
  const manager = userList?.find((user) => user.id === managerId);
  let fullName = "";
  if (manager?.lastname) fullName += manager.lastname + " ";
  if (manager?.middlename) fullName += manager.middlename + " ";
  return fullName + manager?.firstname;
};

const ListBranches: React.FC<BranchManagementDefaultProps> = React.memo(
  (props) => {
    const { userList, menuList, branchList, currencyList } = props;
    const {
      fetchBranchListAsync,
      addBranchAsync,
      deactivateBranchAsync,
      reactivateBranchAsync,
      updateBranchAsync,
      getMenuAsync,
      fetchUserListAsync,
      fetchCurrencyListAsync,
    } = props;
    //State for opening and closing modal
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [openAddBranchModal, setOpenAddBranchModal] = useState<boolean>(
      false
    );
    const [openBranchDetailModal, setOpenBranchDetailModal] = useState<boolean>(
      false
    );

    const [branchDetailIndex, setBranchDetailIndex] = useState<number>(-1);

    useEffect(() => {
      const fetchData = async () => {
        await getMenuAsync();
        await fetchCurrencyListAsync();
        await fetchUserListAsync();
        await fetchBranchListAsync();
        setIsLoading(false);
      };
      fetchData();
    }, [
      fetchBranchListAsync,
      getMenuAsync,
      fetchUserListAsync,
      fetchCurrencyListAsync,
    ]);

    const branchTableMapper = (item: Branch) => {
      return {
        ID: item?.id,
        "Mã Chi Nhánh": item?.code,
        "Tên Chi Nhánh": item?.name,
        "Địa Chỉ Chi Nhánh": item?.address,
        "Số Lượng Nhân Viên": item?.numberofusers,
        "Tiền Tệ":
          currencyList && currencyList.length > 0
            ? currencyList.find((currency) => currency.id === item?.currencyid)
                ?.code
            : "",
        "ID Chủ Chi Nhánh": item?.manager,
        "Tên Chủ Chi Nhánh": getFullName(userList, item.manager),
        currencyid: item?.currencyid,
        statuscode: item?.statuscode,
        statusid: item?.statusid,
        numberofusers: 0 || item.numberofusers,
        mainmenu: item?.mainmenu,
      };
    };

    const branchTableField = [
      { key: "ID", _classes: "font-weight-bold" },
      "Tên Chi Nhánh",
      "Mã Chi Nhánh",
      "ID Chủ Chi Nhánh",
      "Tên Chủ Chi Nhánh",
      "Số Lượng Nhân Viên",
      "Tiền Tệ",
      "Địa Chỉ Chi Nhánh",
      "Trạng Thái",
    ];

    const branchTableScopedSlots = {
      "Trạng Thái": (item: any) => (
        <td>
          <CBadge
            color={item.statuscode === "AVAILABLE" ? "success" : "danger"}
          >
            {item.statuscode}
          </CBadge>
        </td>
      ),
    };

    const branchTableOnRowClick = (value: any, index: number) =>
      onBranchDetailToggle(index);

    const onAddBranchToggle: any = () =>
      setOpenAddBranchModal(!openAddBranchModal);

    const onBranchDetailToggle: any = (index: number) => {
      setBranchDetailIndex(index);
      setOpenBranchDetailModal(!openBranchDetailModal);
    };
    return (
      <CRow>
        <CCol>
          <AddBranchModal
            isShow={openAddBranchModal}
            onFinish={onAddBranchToggle}
            addBranchAsync={addBranchAsync}
            userList={userList}
            menuList={menuList}
            currencyList={currencyList}
          />
          <BranchDetailModal
            deactivateBranchAsync={deactivateBranchAsync}
            reactivateBranchAsync={reactivateBranchAsync}
            updateBranchAsync={updateBranchAsync}
            isShow={openBranchDetailModal}
            onClose={onBranchDetailToggle}
            userList={userList}
            menuList={menuList}
            currencyList={currencyList}
            data={
              branchDetailIndex > -1 && branchList && branchList.length > 0
                ? branchTableMapper(branchList[branchDetailIndex])
                : undefined
            }
          />
          <CCard>
            <CCardBody>
              <CInputGroup className="justify-content-between px-0 mx-0">
                <CRow className="col-md-12 px-0 my-2">
                  <CCol sm="10">
                    <h4>Quản lý chi nhánh</h4>
                  </CCol>
                  <CCol className="px-0" sm="2">
                    <CButton
                      color="warning"
                      className="float-right"
                      style={{ color: "white" }}
                      onClick={onAddBranchToggle}
                    >
                      {" "}
                      <CIcon
                        className="mr-2"
                        content={freeSet.cilLibraryBuilding}
                      />
                      Thêm chi nhánh
                    </CButton>
                  </CCol>
                </CRow>
              </CInputGroup>
              <DataTable
                fields={branchTableField}
                items={!isLoading ? branchList : []}
                itemsMapper={branchTableMapper}
                scopedSlots={branchTableScopedSlots}
                onRowClick={branchTableOnRowClick}
                loading={isLoading}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    );
  }
);

export default ListBranches;
