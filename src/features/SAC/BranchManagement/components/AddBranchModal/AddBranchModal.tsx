import {
  CButton,
  CCard,
  CCardBody,
  CForm,
  CInput,
  CInputGroup,
  CModal,
  CSelect,
  CSpinner,
} from "@coreui/react";
import React, { ChangeEvent, FormEvent, useEffect, useState } from "react";
import swal from 'sweetalert';
import { BranchError, Currency, Menu, User } from '@custom-types';
import { BranchSchema } from "@utils";
import "./AddBranchModal.scss";

type AddBranchModalState = {
  isShow: boolean,
  userList: User[],
  menuList: Menu[],
  currencyList: Currency[],
}

type AddBranchModalDispatch = {
  addBranchAsync: Function,
  onFinish: Function,
}

type AddBranchModalDefaultProps = AddBranchModalState & AddBranchModalDispatch;

const getFullName = (manager: User): string | undefined => {
  let fullName = "";
  if (manager?.lastname) fullName += manager.lastname + " ";
  if (manager?.middlename) fullName += manager.middlename + " "; 
  return fullName + manager?.firstname;
}

const AddBranchModal: React.FC<AddBranchModalDefaultProps> = (props) => {
  const { userList, menuList, currencyList } = props;
  const { isShow, addBranchAsync, onFinish } = props;

  const [loading, setLoading] = useState<boolean>(false);
  const [formData, setFormData] = useState<any>({
    code: null,
    name: null,
    address: null,
    manager: null,
    mainmenu: null,
    currencyid: null,
  });

  const [formDataError, setFormDataError] = useState<BranchError>({
    code: true,
    name: true,
    address: true,
    manager: true,
    currencyid: true,
  });

  useEffect(() => {
    setFormData({
      code: null,
      name: null,
      address: null,
      manager: null,
      mainmenu: null,
      currencyid: null,
    });
    setFormDataError({
      code: true,
      name: true,
      address: true,
      manager: true,
      currencyid: true,
    });
  }, [isShow]);

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
    setFormDataError({
      ...formDataError,
      [e.target.name]: false,
    });
  };

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    const branch = formData;

    //Validate form data
    BranchSchema.validate(formData, { abortEarly: false }).then(
      async (valid: Object) => {
        //Call server
        setLoading(true);
        await addBranchAsync(branch);
        setLoading(false);
        onFinish(-1);
      },
      (errors: any) => {
        let newFormDataError: BranchError = { ...formDataError };

        for (let error of errors.errors)
        {
          let index: string = error.substring(0, error.indexOf(" "));
          newFormDataError[index] = true;
        }
        setFormDataError(newFormDataError);
        let keyDataError = "";
        for (const [key, value] of Object.entries(newFormDataError))
        {
          if (value === true) 
          {
            if (keyDataError !== "") keyDataError += ", ";
            keyDataError += key;
          }
        }
        if (keyDataError !== "")
          swal("Lỗi", "Vui lòng điền thông tin " + keyDataError, "error");
      }
    );
  };

  const onChangeCurrency = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({...formData, currencyid: e.target.value});
    setFormDataError({...formDataError, currencyid: false});
  }

  const onChangeManager = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({...formData, manager: e.target.value});
    setFormDataError({...formDataError, manager: false});
  }

  const onChangeMenu = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({...formData, mainmenu: e.target.value});
    setFormDataError({...formDataError, mainmenu: false});
  }

  return (
    <CModal
      centered
      backdrop
      width={200}
      onClose={() => onFinish(-1) }
      closeOnBackdrop
      show={isShow}
      className="AddBranchModal-styles"
    >
      <CCard className="align-middle justify-content-center jus AddBranchModal-card-styles">
        <CCardBody className="AddBranchModal-cardBody-styles">
          <h4 className="AddBranchModal-h2-styles">Thêm chi nhánh</h4>
          <CForm onSubmit={handleSubmit}>
            <CInputGroup>
              <CInput
                value={formData?.name || ''}
                onChange={onChange}
                placeholder="Tên chi nhánh"
                className="AddBranchModal-input-styles"
                type="text"
                name="name"
              />
            </CInputGroup>
            <CInputGroup>
              <CInput
                value={formData?.code || ''}
                onChange={onChange}
                placeholder="Mã chi nhánh"
                className="AddBranchModal-input-styles"
                type="text"
                name="code"
              />
            </CInputGroup>
            <CInputGroup>
              <CInput
                value={formData?.address || ''}
                onChange={onChange}
                placeholder="Địa chỉ chi nhánh"
                className="AddBranchModal-input-styles"
                type="text"
                name="address"
              />
            </CInputGroup>
            <CInputGroup>
              <CSelect
                type="select" 
                className="AddBranchModal-input-styles form-control"
                onChange={onChangeCurrency}
                value={formData?.currencyid !== null ? formData.currencyid : -1}
                >
                <option hidden value={-1}>Chọn loại tiền tệ</option>
                {currencyList ? currencyList.map((item: Currency) =>
                  <option key={item.id} value={item.id}>{item.code} - {item.name}</option>
                ) : null}
              </CSelect>
            </CInputGroup>
            <CInputGroup>
              <CSelect
                type="select" 
                className="AddBranchModal-input-styles form-control"
                onChange={onChangeManager}
                value={formData?.manager !== null ? formData.manager : -1}
                >
                <option hidden value={-1}>Chọn chủ chi nhánh</option>
                {userList ? userList.map((item: User) =>
                  <option key={item.id} value={item.id}>{item.id} - {getFullName(item)}</option>
                ) : null}
              </CSelect>
            </CInputGroup>
            <CInputGroup>
              <CSelect
                type="select" 
                className="AddBranchModal-input-styles form-control"
                onChange={onChangeMenu}
                disabled={menuList && menuList.length > 0 ? false : true}
                value={formData?.mainmenu !== null ? formData.mainmenu : -1}
                >
                <option hidden value={-1}>{menuList && menuList.length > 0 ? "Chọn menu" : "Hiện chưa có menu nào để chọn"}</option>
                {menuList ? menuList.map((item: Menu) =>
                  <option key={item.id} value={item.id}>{item.id} - {item.name}</option>
                ) : null}
              </CSelect>
            </CInputGroup>

            <CButton
              type="submit"
              color="primary"
              className="AddBranchModal-submitButton-styles btn my-3"
              disabled={loading}
            >
              {loading ? <CSpinner size="sm" color="success"/> : ""} Thêm
            </CButton>
          </CForm>
        </CCardBody>
      </CCard>
    </CModal>
  );
};

export default AddBranchModal;
