import {
  CBadge, CButton,
  CCard,
  CCardBody,
  CCol, CForm,
  CInput,
  CInputGroup,
  CModal,
  CRow,
  CSelect,
  CSpinner,
} from "@coreui/react";
import { BranchError, Currency, Menu, User } from '@custom-types';
import { BranchSchema } from "@utils";
import React, { ChangeEvent, FormEvent, useState, useEffect } from "react";
import swal from 'sweetalert';
import "./BranchDetailModal.scss";

type BranchDetailModalState = {
  isShow: boolean,
  data: any | undefined,
  userList: User[],
  menuList: Menu[],
  currencyList: Currency[],
}

type BranchDetailModalDispatch = {
  deactivateBranchAsync: Function,
  reactivateBranchAsync: Function,
  onClose: Function,
  updateBranchAsync: Function,
}

type BranchDetailModalDefaultProps = BranchDetailModalState & BranchDetailModalDispatch;

const getFullName = (manager: User): string | undefined => {
  let fullName = "";
  if (manager?.lastname) fullName += manager.lastname + " ";
  if (manager?.middlename) fullName += manager.middlename + " "; 
  return fullName + manager?.firstname;
}

const BranchDetailModal: React.FC<BranchDetailModalDefaultProps> = (props) => {
  const { data, userList, menuList, currencyList } = props;
  const { isShow, deactivateBranchAsync, onClose, updateBranchAsync, reactivateBranchAsync } = props;

  const [loading, setLoading] = useState<{
    updateAction: boolean,
    activationAction: boolean,
  }>({
    updateAction: false,
    activationAction: false,
  });
  const [isEditMode, setEditMode] = useState(false);
  const toggleEditMode = () => {
    setEditMode(!isEditMode);
  };

  const [formData, setFormData] = useState<any>({
    code: null,
    name: null,
    address: null,
    manager: null,
    mainmenu: null,
    currencyid: null,
  });

  const [formDataError, setFormDataError] = useState<BranchError>({
    code: true,
    name: true,
    address: true,
    manager: true,
    currencyid: true,
  });

  useEffect(() => {
    if(data) {
      setFormData({
        code: data['Mã Chi Nhánh'],
        name: data['Tên Chi Nhánh'],
        address: data['Địa Chỉ Chi Nhánh'],
        manager: data['ID Chủ Chi Nhánh'],
        mainmenu: data.mainmenu,
        currencyid: data.currencyid,
      });
      setFormDataError({
        code: true,
        name: true,
        address: true,
        manager: true,
        currencyid: true,
      });
    }
  }, [data, isShow]);

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
    setFormDataError({
      ...formDataError,
      [e.target.name]: false,
    });
  };

  const handleSubmit =
    async (e: FormEvent) => {
      e.preventDefault();
      const branch = formData;
      branch.id = data.ID;
      //Validaten form data
      BranchSchema.validate(formData, { abortEarly: false }).then(
        async (valid: Object) => {
          //Call server
          setLoading({ ...loading, updateAction: true });
          await updateBranchAsync(branch);
          setLoading({ ...loading, updateAction: false });
          setEditMode(false);
          onClose(-1);
        },
        (errors: any) => {
          let newFormDataError: BranchError = { ...formDataError };

          for (let error of errors.errors)
          {
            let index: string = error.substring(0, error.indexOf(" "));
            newFormDataError[index] = true;
          }
          setFormDataError(newFormDataError);
          let keyDataError = "";
          for (const [key, value] of Object.entries(newFormDataError))
          {
            if (value === true) 
            {
              if (keyDataError !== "") keyDataError += ", ";
              keyDataError += key;
            }
          }
          if (keyDataError !== "")
            swal("Lỗi", "Vui lòng điền thông tin " + keyDataError, "error");
        }
      );
    };

  const handleDeactivateOrReactivate = async () => {
    if (data?.statuscode === "AVAILABLE") await deactivateBranchAsync(data.ID);
    else await reactivateBranchAsync(data.ID);
  }

  const onChangeCurrency = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({...formData, currencyid: e.target.value});
    setFormDataError({...formDataError, currencyid: false});
  }

  const onChangeManager = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({...formData, manager: e.target.value});
    setFormDataError({...formDataError, manager: false});
  }

  const onChangeMenu = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({...formData, mainmenu: e.target.value});
    setFormDataError({...formDataError, mainmenu: false});
  }

  return (
    <CModal
      centered
      backdrop
      size='xl'
      show={isShow}
      onClose={() => { setEditMode(false); onClose(-1); }}
      className="BranchDetailModal-styles"
    >
      <CCard className="align-middle BranchDetailModal-card-styles">
        <CCardBody className="BranchDetailModal-cardBody-styles">
          <h4 className="BranchDetailModal-h2-styles">{data ? data["Tên Chi Nhánh"] : "Thông tin công ty"}</h4>
          <CRow className="my-3">
            <CCol className="BranchDetailModal-statistics-container">
              <h6 className="BranchDetailModal-light-text">Mã Chi Nhánh</h6>
              <h5>{data ? data["Mã Chi Nhánh"] : ""}</h5>
            </CCol>
            <CCol className="BranchDetailModal-statistics-container">
              <h6 className="BranchDetailModal-light-text">Chủ Chi Nhánh</h6>
              <h5>{data ? data["Tên Chủ Chi Nhánh"] : ""}</h5>
            </CCol>
            <CCol className="BranchDetailModal-statistics-container">
              <h6 className="BranchDetailModal-light-text">Số Lượng Nhân Viên</h6>
              <h5>{data ? data.numberofusers : ""}</h5>
            </CCol>
            <CCol className="BranchDetailModal-statistics-container">
              <h6 className="BranchDetailModal-light-text">Trạng thái</h6>
              <CBadge color={data?.statuscode === 'AVAILABLE' ? "success" : "danger"}>{data?.statuscode}</CBadge>
            </CCol>
          </CRow>

          <CRow className="my-3 justify-content-center">
            <CButton
              className="BranchDetailModal-optionButton-styles btn-outline-warning"
              onClick={toggleEditMode}
            >
              {isEditMode ? "Huỷ" : "Sửa thông tin"}
            </CButton>
            <CButton
              className={data?.statuscode === 'AVAILABLE' ? "BranchDetailModal-optionButton-styles btn-danger" : "BranchDetailModal-optionButton-styles btn-outline-secondary"}
              onClick={handleDeactivateOrReactivate}
              disabled={loading.activationAction}
            >
              {loading.activationAction ? <CSpinner size="sm" color="success"/> : ""} {data?.statuscode === "AVAILABLE" ? "Chặn chi nhánh" : "Kích hoạt chi nhánh"}
          </CButton>
          </CRow>

          <CForm className={isEditMode ? "BranchDetailModal-form" : "BranchDetailModal-form-hidden"} onSubmit={handleSubmit}>
          <CInputGroup>
              <CInput
                value={formData?.name || ''}
                onChange={onChange}
                placeholder="Tên chi nhánh"
                className="AddBranchModal-input-styles"
                type="text"
                name="name"
              />
            </CInputGroup>
            <CInputGroup>
              <CInput
                value={formData?.code || ''}
                onChange={onChange}
                placeholder="Mã chi nhánh"
                className="AddBranchModal-input-styles"
                type="text"
                name="code"
              />
            </CInputGroup>
            <CInputGroup>
              <CInput
                value={formData?.address || ''}
                onChange={onChange}
                placeholder="Địa chỉ chi nhánh"
                className="AddBranchModal-input-styles"
                type="text"
                name="address"
              />
            </CInputGroup>
            <CInputGroup>
              <CSelect
                type="select" 
                className="AddBranchModal-input-styles form-control"
                onChange={onChangeCurrency}
                value={formData?.currencyid !== null ? formData.currencyid : -1}
                >
                <option hidden value={-1}>Chọn loại tiền tệ</option>
                {currencyList ? currencyList.map((item: Currency) =>
                  <option key={item.id} value={item.id}>{item.code} - {item.name}</option>
                ) : null}
              </CSelect>
            </CInputGroup>
            <CInputGroup>
              <CSelect
                type="select" 
                className="AddBranchModal-input-styles form-control"
                onChange={onChangeManager}
                value={formData?.manager !== null ? formData.manager : -1}
                >
                <option hidden value={-1}>Chọn chủ chi nhánh</option>
                {userList ? userList.map((item: User) =>
                  <option key={item.id} value={item.id}>{item.id} - {getFullName(item)}</option>
                ) : null}
              </CSelect>
            </CInputGroup>
            <CInputGroup>
              <CSelect
                type="select" 
                className="AddBranchModal-input-styles form-control"
                onChange={onChangeMenu}
                disabled={menuList && menuList.length > 0 ? false : true}
                value={formData?.mainmenu !== null ? formData.mainmenu : -1}
                >
                <option hidden value={-1}>{menuList && menuList.length > 0 ? "Chọn menu" : "Hiện chưa có menu nào để chọn"}</option>
                {menuList ? menuList.map((item: Menu) =>
                  <option key={item.id} value={item.id}>{item.id} - {item.name}</option>
                ) : null}
              </CSelect>
            </CInputGroup>

            <CButton
              color="primary"
              type="submit"
              className="BranchDetailModal-submitButton-styles my-3"
              disabled={loading.updateAction}
            >
              {loading.updateAction ? <CSpinner size="sm" color="success"/> : ""} Lưu
            </CButton>
          </CForm>
        </CCardBody>
      </CCard>
    </CModal>
  );
};

export default BranchDetailModal;
