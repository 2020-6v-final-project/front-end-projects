import { ChartBarSimple, ChartLineSimple } from '@components';
import {
    CCol,
    CFade,
    CRow,
    CWidgetSimple
} from "@coreui/react";
import React from "react";
import "./BranchStatistics.scss";

const BranchStatistics: React.FC = (props) => {
    return (
        <CFade>
            <CRow>
                <CCol sm="4" lg="2">
                    <CWidgetSimple header="title" text="1,123">
                        <ChartLineSimple
                            className="branch-management-small-card"
                            borderColor="danger"
                        />
                    </CWidgetSimple>
                </CCol>
                <CCol sm="4" lg="2">
                    <CWidgetSimple header="title" text="1,123">
                        <ChartLineSimple
                            className="branch-management-small-card"
                            borderColor="primary"
                        />
                    </CWidgetSimple>
                </CCol>
                <CCol sm="4" lg="2">
                    <CWidgetSimple header="title" text="1,123">
                        <ChartLineSimple
                            className="branch-management-small-card"
                            borderColor="success"
                        />
                    </CWidgetSimple>
                </CCol>
                <CCol sm="4" lg="2">
                    <CWidgetSimple header="title" text="1,123">
                        <ChartBarSimple
                            className="branch-management-small-card"
                            backgroundColor="danger"
                        />
                    </CWidgetSimple>
                </CCol>
                <CCol sm="4" lg="2">
                    <CWidgetSimple header="title" text="1,123">
                        <ChartBarSimple
                            className="branch-management-small-card"
                            backgroundColor="primary"
                        />
                    </CWidgetSimple>
                </CCol>
                <CCol sm="4" lg="2">
                    <CWidgetSimple header="title" text="1,123">
                        <ChartBarSimple
                            className="branch-management-small-card"
                            backgroundColor="success"
                        />
                    </CWidgetSimple>
                </CCol>
            </CRow>
        </CFade>);
}

export default BranchStatistics;

