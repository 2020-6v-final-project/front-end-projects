import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AppThunk } from "src/store";
import { Branch } from "@custom-types";
import {
  getBranchList,
  addBranch,
  updateBranch,
  deactivateBranch,
  reactivateBranch
} from "../../../shared/api/branchAPI";
import { 
  updateBranchOfUserSuccess, 
  updateBranchOfUserFailed,
  getBranchOfUserSuccess,
  getBranchOfUserFailed, 
} from "../../LoginPage/LoginPage.slice";
import swal from "sweetalert";

// Types and state declaration
type BranchManagementState = {
  branchList: Branch[];
};

const initialState: BranchManagementState = {
  branchList: []
};

// Slice
const BranchManagementSlice = createSlice({
  name: "branchManagement",
  initialState,
  reducers: {
    addBranchSuccess: (state, action: PayloadAction<Branch>) => {
      state.branchList.push(action.payload);
      return state;
    },
    addBranchFailed: (state) => {
      return state;
    },
    updateBranchSuccess: (state, action: PayloadAction<Branch>) => {
      const index = state.branchList.findIndex(
        (item: Branch) => item.id === action.payload.id
      );

      state.branchList.splice(index, 1, action.payload);
      return state;
    },
    updateBranchFailed: (state) => {
      return state;
    },
    deactivateBranchSuccess: (state, action: PayloadAction<Branch>) => {
      const index = state.branchList.findIndex(
        (item: Branch) => item.id === action.payload.id
      );

      state.branchList.splice(index, 1, action.payload);
      return state;
    },
    deactivateBranchFailed: (state) => {
      return state;
    },
    reactivateBranchSuccess: (state, action: PayloadAction<Branch>) => {
      const index = state.branchList.findIndex(
        (item: Branch) => item.id === action.payload.id
      );

      state.branchList.splice(index, 1, action.payload);
      return state;
    },
    reactivateBranchFailed: (state) => {
      return state;
    },
    fetchBranchListSuccess: (state, action: PayloadAction<Branch[]>) => {
      state.branchList = action.payload;
      return state;
    },
    fetchBranchListFailed: (state) => {
      return state;
    }
  }
});

// Thunk for async actions
export const fetchBranchListAsync = (): AppThunk => async (dispatch) => {
  try {
    let branchList: Branch[];
    await getBranchList((data: Branch[]) => {
      branchList = data;
      branchList.sort((item1: Branch, item2: Branch) => {
        return item1.id - item2.id;
      });
      dispatch(fetchBranchListSuccess(branchList));
      dispatch(getBranchOfUserSuccess(branchList));
    });
  } catch (e) {
    dispatch(fetchBranchListFailed());
    dispatch(getBranchOfUserFailed());
  }
};

export const addBranchAsync = (branch: any): AppThunk => async (
  dispatch
) => {
  try {
    await addBranch(branch, (data: Branch) => {
      swal("Thêm thành công!", "Chi nhánh đã được thêm vào công ty!", "success");
      dispatch(addBranchSuccess(data));
    });
  } catch (e) {
    swal("Thêm không thành công!", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
    dispatch(addBranchFailed());
  }
};

export const updateBranchAsync = (branch: any): AppThunk => async (
  dispatch
) => {
  try {
    await updateBranch(branch, (data: Branch) => {
      swal("Cập nhật thành công!", "Chi nhánh đã được cập nhật!", "success");
      dispatch(updateBranchSuccess(data));
      dispatch(updateBranchOfUserSuccess(data));
    });
  } catch (e) {
    swal("Cập nhật không thành công!", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
    dispatch(updateBranchFailed());
    dispatch(updateBranchOfUserFailed());
  }
};

export const deactivateBranchAsync = (branchId: number): AppThunk => async (
  dispatch
) => {
  try {
    await deactivateBranch(branchId, (data: Branch) => {
      swal("Cập nhật thành công!", "Chi nhánh đã bị vô hiệu hóa!", "success");
      dispatch(deactivateBranchSuccess(data));
    });
  } catch (e) {
    swal("Cập nhật không thành công!", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
    dispatch(deactivateBranchFailed());
  }
};

export const reactivateBranchAsync = (branchId: number): AppThunk => async (
  dispatch
) => {
  try {
    await reactivateBranch(branchId, (data: Branch) => {
      swal("Cập nhật thành công!", "Chi nhánh đã được kích hoạt!", "success");
      dispatch(reactivateBranchSuccess(data));
    });
  } catch (e) {
    swal("Cập nhật không thành công!", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
    dispatch(reactivateBranchFailed());
  }
};

// Export part
export const {
  addBranchSuccess,
  addBranchFailed,
  deactivateBranchSuccess,
  deactivateBranchFailed,
  reactivateBranchSuccess,
  reactivateBranchFailed,
  fetchBranchListSuccess,
  fetchBranchListFailed,
  updateBranchSuccess,
  updateBranchFailed
} = BranchManagementSlice.actions;

export default BranchManagementSlice.reducer;
