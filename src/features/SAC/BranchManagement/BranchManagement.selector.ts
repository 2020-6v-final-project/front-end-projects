import { RootState } from "src/store";
import { createSelector } from "reselect";

export const branchListSelector = createSelector(
  [(state: RootState) => state.branch.branchList],
  (branchList) => {
    return branchList;
  }
);
