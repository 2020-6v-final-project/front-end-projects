import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectCurrencyList = createSelector(
    [(state: RootState) => state.currency.currencyList],
    (currencyList) => currencyList
)