import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AppThunk } from "src/store";
import { Currency } from "@custom-types";
import {
    getCurrencyList,
} from "../../../shared/api/currencyAPI";

// Types and state declaration
type CurrencyManagementState = {
  currencyList: Currency[];
};

const initialState: CurrencyManagementState = {
  currencyList: []
};

// Slice
const CurrencyManagementSlice = createSlice({
  name: "currencyManagement",
  initialState,
  reducers: {
    fetchCurrencyListSuccess: (state, action: PayloadAction<Currency[]>) => {
      state.currencyList = action.payload;
      return state;
    },
    fetchCurrencyListFailed: (state) => {
      return state;
    }
  }
});

// Thunk for async actions
export const fetchCurrencyListAsync = (): AppThunk => async (dispatch) => {
  try {
    let currencyList: Currency[];
    await getCurrencyList((data: Currency[]) => {
      currencyList = data;
      currencyList.sort((item1: Currency, item2: Currency) => {
        return item1.id - item2.id;
      });
      dispatch(fetchCurrencyListSuccess(currencyList));
    });
  } catch (e) {
    dispatch(fetchCurrencyListFailed());
  }
};

// Export part
export const {
  fetchCurrencyListSuccess,
  fetchCurrencyListFailed,
} = CurrencyManagementSlice.actions;

export default CurrencyManagementSlice.reducer;