import { 
    getIngredientTypeList,
    addIngredientType,
    updateIngredientType,
    deleteIngredientType,
    getItemTypeList,
    addItemType,
    updateItemType,
    deleteItemType,
} from '@apis';
import { IngredientType, ItemType } from '@custom-types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { orderBy } from 'lodash';
import { AppThunk } from 'src/store';
import swal from 'sweetalert';

type ItemTypeIngredientTypeManagementInitialState = {
    ingredientTypeList: Array<IngredientType>,
    itemTypeList: Array<ItemType>,
}

const initialState: ItemTypeIngredientTypeManagementInitialState = {
    ingredientTypeList: [],
    itemTypeList: [],
}

const ItemTypeIngredientTypeManagementSlice = createSlice({
    name: 'itemTypeIngredientTypeManagement',
    initialState,
    reducers: {
        // Ingredient Type
        getIngredientTypesSuccess: (state, action: PayloadAction<IngredientType[]>) => {
            state.ingredientTypeList = action.payload;
            state.ingredientTypeList = orderBy(state.ingredientTypeList, ["name"], ["asc"]);
            return state;
        },
        getIngredientTypesFailed: (state) => {
            return state;
        },
        addIngredientTypeSuccess: (state, action: PayloadAction<IngredientType>) => {
            state.ingredientTypeList?.push(action.payload);
            state.ingredientTypeList = orderBy(state.ingredientTypeList, ["name"], ["asc"]);
            return state;
        },
        addIngredientTypeFailed: (state) => {
            return state;
        },
        updateIngredientTypeSuccess: (state, action: PayloadAction<IngredientType>) => {
            const index = state.ingredientTypeList?.findIndex(ingredientType => ingredientType.id === action.payload.id);
            if (index > -1) {
                state.ingredientTypeList?.splice(index, 1, action.payload);
            }
        },
        updateIngredientTypeFailed: (state) => {
            return state;
        },
        deleteIngredientTypeSuccess: (state, action: PayloadAction<number>) => {
            const index = state.ingredientTypeList?.findIndex(ingredientType => ingredientType.id === action.payload);
            if (index > -1) {
                state.ingredientTypeList?.splice(index, 1);
            }
        },
        deleteIngredientTypeFailed: (state) => {
            return state;
        },
        // Item Type
        getItemTypesSuccess: (state, action: PayloadAction<ItemType[]>) => {
            state.itemTypeList = action.payload;
            state.itemTypeList = orderBy(state.itemTypeList, ["name"], ["asc"]);
            return state;
        },
        getItemTypesFailed: (state) => {
            return state;
        },
        addItemTypeSuccess: (state, action: PayloadAction<ItemType>) => {
            state.itemTypeList?.push(action.payload);
            state.itemTypeList = orderBy(state.itemTypeList, ["name"], ["asc"]);
            return state;
        },
        addItemTypeFailed: (state) => {
            return state;
        },
        updateItemTypeSuccess: (state, action: PayloadAction<ItemType>) => {
            const index = state.itemTypeList?.findIndex(itemType => itemType.id === action.payload.id);
            if (index > -1) {
                state.itemTypeList?.splice(index, 1, action.payload);
            }
        },
        updateItemTypeFailed: (state) => {
            return state;
        },
        deleteItemTypeSuccess: (state, action: PayloadAction<number>) => {
            const index = state.itemTypeList?.findIndex(itemType => itemType.id === action.payload);
            if (index > -1) {
                state.itemTypeList?.splice(index, 1);
            }
        },
        deleteItemTypeFailed: (state) => {
            return state;
        },
    }
})

export const getIngredientTypeAsync = (): AppThunk => async (dispatch) => {
    try {
        await getIngredientTypeList((data: IngredientType[]) => {
            dispatch(getIngredientTypesSuccess(data));
        });
    } catch (e) {
        dispatch(getIngredientTypesFailed());
    }
}

export const addIngredientTypeAsync = (ingredientType: any): AppThunk => async (dispatch) => {
    try {
        await addIngredientType(ingredientType, (data: IngredientType) => {
            swal("Thành công", "Loại nguyên liệu đã được thêm!", "success");
            dispatch(addIngredientTypeSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(addIngredientTypeFailed());
    }
}

export const updateIngredientTypeAsync = (ingredientType: any): AppThunk => async (dispatch) => {
    try {
        await updateIngredientType(ingredientType, (data: IngredientType) => {
            swal("Thành công", "Loại nguyên liệu đã được cập nhật!", "success");
            dispatch(updateIngredientTypeSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(updateIngredientTypeFailed());
    }
}

export const deleteIngredientTypeAsync = (ingredientTypeId: number): AppThunk => async (dispatch) => {
    try {
        await deleteIngredientType(ingredientTypeId, (data: any) => {
            if(data?.message === "Accepted") {
                swal("Thành công", "Loại nguyên liệu đã được xóa!", "success");
                dispatch(deleteIngredientTypeSuccess(ingredientTypeId));
            } else {
                swal("Thất bại", "Vui lòng thử lại!", "error");
                dispatch(deleteIngredientTypeFailed());
            }
        });
    } catch (e) {
        swal("Thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(deleteIngredientTypeFailed());
    }
}

export const getItemTypeAsync = (): AppThunk => async (dispatch) => {
    try {
        await getItemTypeList((data: ItemType[]) => {
            dispatch(getItemTypesSuccess(data));
        });
    } catch (e) {
        dispatch(getItemTypesFailed());
    }
}

export const addItemTypeAsync = (itemType: any): AppThunk => async (dispatch) => {
    try {
        await addItemType(itemType, (data: ItemType) => {
            swal("Thành công", "Loại món ăn đã được thêm!", "success");
            dispatch(addItemTypeSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(addItemTypeFailed());
    }
}

export const updateItemTypeAsync = (itemType: any): AppThunk => async (dispatch) => {
    try {
        await updateItemType(itemType, (data: ItemType) => {
            swal("Thành công", "Loại món ăn đã được cập nhật!", "success");
            dispatch(updateItemTypeSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(updateItemTypeFailed());
    }
}

export const deleteItemTypeAsync = (itemTypeId: number): AppThunk => async (dispatch) => {
    try {
        await deleteItemType(itemTypeId, (data: any) => {
            if(data?.message === "Accepted") {
                swal("Thành công", "Loại món ăn đã được xóa!", "success");
                dispatch(deleteItemTypeSuccess(itemTypeId));
            } else {
                swal("Thất bại", "Vui lòng thử lại!", "error");
                dispatch(deleteItemTypeFailed());
            }
        });
    } catch (e) {
        swal("Thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(deleteItemTypeFailed());
    }
}

const {
    getIngredientTypesSuccess,
    getIngredientTypesFailed,
    addIngredientTypeSuccess,
    addIngredientTypeFailed,
    updateIngredientTypeSuccess,
    updateIngredientTypeFailed,
    deleteIngredientTypeSuccess,
    deleteIngredientTypeFailed,
    getItemTypesSuccess,
    getItemTypesFailed,
    addItemTypeSuccess,
    addItemTypeFailed,
    updateItemTypeSuccess,
    updateItemTypeFailed,
    deleteItemTypeSuccess,
    deleteItemTypeFailed,
} = ItemTypeIngredientTypeManagementSlice.actions;

export default ItemTypeIngredientTypeManagementSlice.reducer;
