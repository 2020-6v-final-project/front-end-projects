import { freeSet } from "@coreui/icons";
import CIcon from "@coreui/icons-react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CInputGroup,
  CRow
} from "@coreui/react";
import React, { useEffect, useState } from "react";
import { DataTable } from "@components";
import { ItemIngredientTypeManagementDefaultProps } from "../../ItemIngredientTypeManagement.container";
import { IngredientType, ItemType } from '@custom-types';
import "./ListItemIngredientType.scss";
import AddIngredientTypeModal from '../AddIngedientTypeModal/AddIngredientTypeModal';
import IngredientTypeDetailModal from '../IngredientTypeDetailModal/IngredientTypeDetailModal';
import AddItemTypeModal from '../AddItemTypeModal/AddItemTypeModal';
import ItemTypeDetailModal from '../ItemTypeDetailModal/ItemTypeDetailModal';

const ListItemIngredientType: React.FC<ItemIngredientTypeManagementDefaultProps> = React.memo((props) => {
  const { ingredientTypeList, itemTypeList } = props;
  const { 
    getIngredientTypeAsync,
    addIngredientTypeAsync,
    updateIngredientTypeAsync,
    deleteIngredientTypeAsync,
    getItemTypeAsync,
    addItemTypeAsync,
    updateItemTypeAsync,
    deleteItemTypeAsync,
  } = props;
  //State for opening and closing modal
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [openAddIngredientTypeModal, setOpenAddIngredientTypeModal] = useState<boolean>(false);
  const [openIngredientTypeDetailModal, setOpenIngredientTypeDetailModal] = useState<boolean>(false);
  const [openAddItemTypeModal, setOpenAddItemTypeModal] = useState<boolean>(false);
  const [openItemTypeDetailModal, setOpenItemTypeDetailModal] = useState<boolean>(false);

  const [ingredientTypeDetailIndex, setIngredientTypeDetailIndex] = useState<number>(-1);
  const [itemTypeDetailIndex, setItemTypeDetailIndex] = useState<number>(-1);

  useEffect(() => {
    const fetchData = async () => {
        await getIngredientTypeAsync();
        await getItemTypeAsync();
        setIsLoading(false);
    }
    fetchData();
  }, [getIngredientTypeAsync, getItemTypeAsync]);

  const ingredientTypeTableMapper = (ingredientType: IngredientType) => {
    return {
      ID: ingredientType?.id,
      "Tên": ingredientType?.name,
      "Số Lượng Nguyên Liệu Thuộc Loại": ingredientType?.ingredients?.length,
      ingredients: ingredientType?.ingredients,
    };
  }

  const itemTypeTableMapper = (itemType: ItemType) => {
    return {
      ID: itemType?.id,
      "Tên": itemType?.name,
      "Số Lượng Món Ăn Thuộc Loại": itemType?.items?.length,
      items: itemType?.items,
    };
  }

  const ingredientTypeTableField = [
    { key: "ID", _classes: "font-weight-bold" },
    "Tên",
    "Số Lượng Nguyên Liệu Thuộc Loại",
  ];

  const itemTypeTableField = [
    { key: "ID", _classes: "font-weight-bold" },
    "Tên",
    "Số Lượng Món Ăn Thuộc Loại",
  ];

  const ingredientTypeTableOnRowClick = (value: any, index: number) => onIngredientTypeDetailToggle(index);
  const itemTypeTableOnRowClick = (value: any, index: number) => onItemTypeDetailToggle(index);

  const onAddIngredientTypeToggle: any = () => setOpenAddIngredientTypeModal(!openAddIngredientTypeModal);
  const onAddItemTypeToggle: any = () => setOpenAddItemTypeModal(!openAddItemTypeModal);

  const onIngredientTypeDetailToggle: any = (index: number) => {
    setIngredientTypeDetailIndex(index);
    setOpenIngredientTypeDetailModal(!openIngredientTypeDetailModal);
  };

  const onItemTypeDetailToggle: any = (index: number) => {
    setItemTypeDetailIndex(index);
    setOpenItemTypeDetailModal(!openItemTypeDetailModal);
  };

  return (
    <CRow>
      <CCol span={6}>
        <AddIngredientTypeModal
          isShow={openAddIngredientTypeModal}
          onFinish={onAddIngredientTypeToggle}
          addIngredientTypeAsync={addIngredientTypeAsync}
        />
        <IngredientTypeDetailModal
          isShow={openIngredientTypeDetailModal}
          onClose={onIngredientTypeDetailToggle}
          deleteIngredientTypeAsync={deleteIngredientTypeAsync}
          updateIngredientTypeAsync={updateIngredientTypeAsync}
          data={ingredientTypeDetailIndex > -1 && ingredientTypeList && ingredientTypeList.length > 0 
            ? ingredientTypeTableMapper(ingredientTypeList[ingredientTypeDetailIndex])
            : undefined
          }
        />
        <CCard>
          <CCardBody>
            <CInputGroup className="justify-content-between px-0 mx-0">
              <CRow className="col-md-12 px-0 my-2">
                <CCol sm="10">
                  <h4>Quản lý loại nguyên liệu</h4>
                </CCol>
                <CCol className="px-0" sm="2">
                  <CButton
                    color="warning"
                    className="float-right"
                    style={{ color: "white" }}
                    onClick={onAddIngredientTypeToggle}
                  >
                    {" "}
                    <CIcon
                      className="mr-2"
                      content={freeSet.cilLibraryBuilding}
                    />
                    Thêm
                  </CButton>
                </CCol>
              </CRow>
            </CInputGroup>
            <DataTable
              fields={ingredientTypeTableField}
              items={!isLoading ? ingredientTypeList : []}
              itemsMapper={ingredientTypeTableMapper}
              onRowClick={ingredientTypeTableOnRowClick}
              loading={isLoading} />
          </CCardBody>
        </CCard>
      </CCol>
      <CCol span={6}>
      <AddItemTypeModal
          isShow={openAddItemTypeModal}
          onFinish={onAddItemTypeToggle}
          addItemTypeAsync={addItemTypeAsync}
        />
        <ItemTypeDetailModal
          isShow={openItemTypeDetailModal}
          onClose={onItemTypeDetailToggle}
          deleteItemTypeAsync={deleteItemTypeAsync}
          updateItemTypeAsync={updateItemTypeAsync}
          data={itemTypeDetailIndex > -1 && itemTypeList && itemTypeList.length > 0 
            ? itemTypeTableMapper(itemTypeList[itemTypeDetailIndex])
            : undefined
          }
        />
        <CCard>
          <CCardBody>
            <CInputGroup className="justify-content-between px-0 mx-0">
              <CRow className="col-md-12 px-0 my-2">
                <CCol sm="10">
                  <h4>Quản lý loại món ăn</h4>
                </CCol>
                <CCol className="px-0" sm="2">
                  <CButton
                    color="warning"
                    className="float-right"
                    style={{ color: "white" }}
                    onClick={onAddItemTypeToggle}
                  >
                    {" "}
                    <CIcon
                      className="mr-2"
                      content={freeSet.cilLibraryBuilding}
                    />
                    Thêm
                  </CButton>
                </CCol>
              </CRow>
            </CInputGroup>
            <DataTable
              fields={itemTypeTableField}
              items={!isLoading ? itemTypeList : []}
              itemsMapper={itemTypeTableMapper}
              onRowClick={itemTypeTableOnRowClick}
              loading={isLoading} />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
});

export default ListItemIngredientType;
