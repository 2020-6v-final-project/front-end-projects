import {
    CButton,
    CCard,
    CCardBody,
    CForm,
    CInput,
    CInputGroup,
    CModal,
    CSpinner,
  } from "@coreui/react";
  import React, { ChangeEvent, FormEvent, useEffect, useState } from "react";
  import "./AddIngredientTypeModal.scss";
  
  type AddIngredienttypeModalState = {
    isShow: boolean,
  }
  
  type AddIngredienttypeModalDispatch = {
    addIngredientTypeAsync: Function,
    onFinish: Function,
  }
  
  type AddIngredienttypeModalDefaultProps = AddIngredienttypeModalState & AddIngredienttypeModalDispatch;
  
  const AddIngredienttypeModal: React.FC<AddIngredienttypeModalDefaultProps> = (props) => {
    const { isShow, addIngredientTypeAsync, onFinish } = props;
  
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [formData, setFormData] = useState<any>({
      name: null,
    });
  
    useEffect(() => {
      setFormData({
        name: null,
      });
    }, [isShow]);
  
    const onChange = (e: ChangeEvent<HTMLInputElement>) => {
      setFormData({
        ...formData,
        [e.target.name]: e.target.value,
      });
    };
  
    const handleSubmit = async (e: FormEvent) => {
      e.preventDefault();
      setIsLoading(true);
      await addIngredientTypeAsync(formData);
      setIsLoading(false);
      onFinish(-1);
    };
  
    return (
      <CModal
        centered
        backdrop
        width={200}
        onClose={() => onFinish(-1) }
        closeOnBackdrop
        show={isShow}
        className="AddIngredientTypeModal-styles"
      >
        <CCard className="align-middle justify-content-center jus AddIngredientTypeModal-card-styles">
          <CCardBody className="AddIngredientTypeModal-cardBody-styles">
            <h4 className="AddIngredientTypeModal-h2-styles">Thêm loại nguyên liệu</h4>
            <CForm onSubmit={handleSubmit}>
              <CInputGroup>
                <CInput
                  required
                  value={formData?.name || ''}
                  onChange={onChange}
                  placeholder="Tên loại nguyên liệu"
                  className="AddIngredientTypeModal-input-styles"
                  type="text"
                  name="name"
                />
              </CInputGroup>
  
              <CButton
                type="submit"
                color="primary"
                className="AddIngredientTypeModal-submitButton-styles btn my-3"
                disabled={isLoading}
              >
                {isLoading ? <CSpinner size="sm" color="success"/> : ""} Thêm
              </CButton>
            </CForm>
          </CCardBody>
        </CCard>
      </CModal>
    );
  };
  
  export default AddIngredienttypeModal;
  