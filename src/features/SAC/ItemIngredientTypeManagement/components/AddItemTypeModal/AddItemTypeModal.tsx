import {
    CButton,
    CCard,
    CCardBody,
    CForm,
    CInput,
    CInputGroup,
    CModal,
    CSpinner,
  } from "@coreui/react";
  import React, { ChangeEvent, FormEvent, useEffect, useState } from "react";
  import "./AddItemTypeModal.scss";
  
  type AddItemtypeModalState = {
    isShow: boolean,
  }
  
  type AddItemtypeModalDispatch = {
    addItemTypeAsync: Function,
    onFinish: Function,
  }
  
  type AddItemtypeModalDefaultProps = AddItemtypeModalState & AddItemtypeModalDispatch;
  
  const AddItemtypeModal: React.FC<AddItemtypeModalDefaultProps> = (props) => {
    const { isShow, addItemTypeAsync, onFinish } = props;
  
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [formData, setFormData] = useState<any>({
      name: null,
    });
  
    useEffect(() => {
      setFormData({
        name: null,
      });
    }, [isShow]);
  
    const onChange = (e: ChangeEvent<HTMLInputElement>) => {
      setFormData({
        ...formData,
        [e.target.name]: e.target.value,
      });
    };
  
    const handleSubmit = async (e: FormEvent) => {
      e.preventDefault();
      setIsLoading(true);
      await addItemTypeAsync(formData);
      setIsLoading(false);
      onFinish(-1);
    };
  
    return (
      <CModal
        centered
        backdrop
        width={200}
        onClose={() => onFinish(-1) }
        closeOnBackdrop
        show={isShow}
        className="AddItemTypeModal-styles"
      >
        <CCard className="align-middle justify-content-center jus AddItemTypeModal-card-styles">
          <CCardBody className="AddItemTypeModal-cardBody-styles">
            <h4 className="AddItemTypeModal-h2-styles">Thêm loại nguyên liệu</h4>
            <CForm onSubmit={handleSubmit}>
              <CInputGroup>
                <CInput
                  required
                  value={formData?.name || ''}
                  onChange={onChange}
                  placeholder="Tên loại món ăn"
                  className="AddItemTypeModal-input-styles"
                  type="text"
                  name="name"
                />
              </CInputGroup>
  
              <CButton
                type="submit"
                color="primary"
                className="AddItemTypeModal-submitButton-styles btn my-3"
                disabled={isLoading}
              >
                {isLoading ? <CSpinner size="sm" color="success"/> : ""} Thêm
              </CButton>
            </CForm>
          </CCardBody>
        </CCard>
      </CModal>
    );
  };
  
  export default AddItemtypeModal;
  