import {
    CButton,
    CCard,
    CCardBody,
    CForm,
    CInput,
    CInputGroup,
    CModal,
    CRow,
    CSpinner,
  } from "@coreui/react";
  import React, { ChangeEvent, FormEvent, useState, useEffect } from "react";
  import "./ItemTypeDetailModal.scss";
  
  type ItemTypeDetailModalState = {
    isShow: boolean,
    data: any | undefined,
  }
  
  type ItemTypeDetailModalDispatch = {
    onClose: Function,
    updateItemTypeAsync: Function,
    deleteItemTypeAsync: Function,
  }
  
  type ItemTypeDetailModalDefaultProps = ItemTypeDetailModalState & ItemTypeDetailModalDispatch;
  
  const ItemTypeDetailModal: React.FC<ItemTypeDetailModalDefaultProps> = (props) => {
    const { data } = props;
    const { isShow, onClose, updateItemTypeAsync, deleteItemTypeAsync } = props;
  
    const [isLoading, setIsLoading] = useState<{updateAction: boolean, deleteAction: boolean}>({
        updateAction: false, 
        deleteAction: false,
    });
  
    const [formData, setFormData] = useState<any>({
      id: null,
      name: null,
    });
  
    useEffect(() => {
      if(data) {
        setFormData({
          id: data.ID,
          name: data['Tên'],
        });
      }
    }, [data, isShow]);
  
    const onChange = (e: ChangeEvent<HTMLInputElement>) => {
      setFormData({
        ...formData,
        [e.target.name]: e.target.value,
      });
    };
  
    const handleSubmit = async (e: FormEvent) => {
        e.preventDefault();
        setIsLoading({...isLoading, updateAction: true});
        await updateItemTypeAsync(formData);
        setIsLoading({...isLoading, updateAction: false});
        onClose(-1);
    }

    const handleDelete = async (e: FormEvent) => {
        e.preventDefault();
        setIsLoading({...isLoading, deleteAction: true});
        await deleteItemTypeAsync(formData?.id);
        setIsLoading({...isLoading, deleteAction: false});
        onClose(-1);
    }
  
    return (
      <CModal
        centered
        backdrop
        show={isShow}
        onClose={() => { onClose(-1); }}
        className="ItemTypeDetailModal-styles"
      >
        <CCard className="align-middle ItemTypeDetailModal-card-styles">
          <CCardBody className="ItemTypeDetailModal-cardBody-styles">
            <h4 className="ItemTypeDetailModal-h2-styles">Thông tin loại món ăn</h4>
  
            <CRow className="my-3 justify-content-center">
              <CButton
                className="ItemTypeDetailModal-optionButton-styles btn-danger"
                onClick={handleDelete}
                disabled={isLoading.deleteAction}
              >
                {isLoading.deleteAction ? <CSpinner size="sm" color="success"/> : ""} Xóa
            </CButton>
            </CRow>
  
            <CForm className="ItemTypeDetailModal-form" onSubmit={handleSubmit}>
            <CInputGroup>
                <CInput
                  required
                  value={formData?.name || ''}
                  onChange={onChange}
                  placeholder="Tên loại món ăn"
                  className="AddItemTypeModal-input-styles"
                  type="text"
                  name="name"
                />
              </CInputGroup>
  
              <CButton
                type="submit"
                color="primary"
                className="AddItemTypeModal-submitButton-styles btn my-3"
                disabled={isLoading.updateAction}
              >
                {isLoading.updateAction ? <CSpinner size="sm" color="success"/> : ""} Lưu
              </CButton>
            </CForm>
          </CCardBody>
        </CCard>
      </CModal>
    );
  };
  
  export default ItemTypeDetailModal;
  