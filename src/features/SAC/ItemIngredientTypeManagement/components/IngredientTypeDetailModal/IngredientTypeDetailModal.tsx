import {
    CButton,
    CCard,
    CCardBody,
    CForm,
    CInput,
    CInputGroup,
    CModal,
    CRow,
    CSpinner,
  } from "@coreui/react";
  import React, { ChangeEvent, FormEvent, useState, useEffect } from "react";
  import "./IngredientTypeDetailModal.scss";
  
  type IngredientTypeDetailModalState = {
    isShow: boolean,
    data: any | undefined,
  }
  
  type IngredientTypeDetailModalDispatch = {
    onClose: Function,
    updateIngredientTypeAsync: Function,
    deleteIngredientTypeAsync: Function,
  }
  
  type IngredientTypeDetailModalDefaultProps = IngredientTypeDetailModalState & IngredientTypeDetailModalDispatch;
  
  const IngredientTypeDetailModal: React.FC<IngredientTypeDetailModalDefaultProps> = (props) => {
    const { data } = props;
    const { isShow, onClose, updateIngredientTypeAsync, deleteIngredientTypeAsync } = props;
  
    const [isLoading, setIsLoading] = useState<{updateAction: boolean, deleteAction: boolean}>({
        updateAction: false, 
        deleteAction: false,
    });
  
    const [formData, setFormData] = useState<any>({
      id: null,
      name: null,
    });
  
    useEffect(() => {
      if(data) {
        setFormData({
          id: data.ID,
          name: data['Tên'],
        });
      }
    }, [data, isShow]);
  
    const onChange = (e: ChangeEvent<HTMLInputElement>) => {
      setFormData({
        ...formData,
        [e.target.name]: e.target.value,
      });
    };
  
    const handleSubmit = async (e: FormEvent) => {
        e.preventDefault();
        setIsLoading({...isLoading, updateAction: true});
        await updateIngredientTypeAsync(formData);
        setIsLoading({...isLoading, updateAction: false});
        onClose(-1);
    }

    const handleDelete = async (e: FormEvent) => {
        e.preventDefault();
        setIsLoading({...isLoading, deleteAction: true});
        await deleteIngredientTypeAsync(formData?.id);
        setIsLoading({...isLoading, deleteAction: false});
        onClose(-1);
    }
  
    return (
      <CModal
        centered
        backdrop
        show={isShow}
        onClose={() => { onClose(-1); }}
        className="IngredientTypeDetailModal-styles"
      >
        <CCard className="align-middle IngredientTypeDetailModal-card-styles">
          <CCardBody className="IngredientTypeDetailModal-cardBody-styles">
            <h4 className="IngredientTypeDetailModal-h2-styles">Thông tin loại món ăn</h4>
  
            <CRow className="my-3 justify-content-center">
              <CButton
                className="IngredientTypeDetailModal-optionButton-styles btn-danger"
                onClick={handleDelete}
                disabled={isLoading.deleteAction}
              >
                {isLoading.deleteAction ? <CSpinner size="sm" color="success"/> : ""} Xóa
            </CButton>
            </CRow>
  
            <CForm className="IngredientTypeDetailModal-form" onSubmit={handleSubmit}>
            <CInputGroup>
                <CInput
                  required
                  value={formData?.name || ''}
                  onChange={onChange}
                  placeholder="Tên loại nguyên liệu"
                  className="AddIngredientTypeModal-input-styles"
                  type="text"
                  name="name"
                />
              </CInputGroup>
  
              <CButton
                type="submit"
                color="primary"
                className="AddIngredientTypeModal-submitButton-styles btn my-3"
                disabled={isLoading.updateAction}
              >
                {isLoading.updateAction ? <CSpinner size="sm" color="success"/> : ""} Lưu
              </CButton>
            </CForm>
          </CCardBody>
        </CCard>
      </CModal>
    );
  };
  
  export default IngredientTypeDetailModal;
  