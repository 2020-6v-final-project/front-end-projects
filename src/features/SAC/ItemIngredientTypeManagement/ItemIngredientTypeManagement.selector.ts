import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectIngredientTypeList = createSelector(
    [(state: RootState) => state.itemTypeIngredientType.ingredientTypeList],
    (ingredientTypeList) => ingredientTypeList
)

export const selectItemTypeList = createSelector(
    [(state: RootState) => state.itemTypeIngredientType.itemTypeList],
    (itemTypeList) => itemTypeList
)