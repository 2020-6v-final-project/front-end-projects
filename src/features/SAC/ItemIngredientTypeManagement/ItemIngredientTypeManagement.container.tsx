import { CFade } from "@coreui/react";
import React from "react";
import { connect } from 'react-redux';
import { IngredientType, ItemType } from '@custom-types';
import { selectIngredientTypeList, selectItemTypeList } from './ItemIngredientTypeManagement.selector';
import { 
    getIngredientTypeAsync,
    addIngredientTypeAsync,
    updateIngredientTypeAsync,
    deleteIngredientTypeAsync,
    getItemTypeAsync,
    addItemTypeAsync,
    updateItemTypeAsync,
    deleteItemTypeAsync,
} from './ItemIngredientTypeManagement.slice';
import { RootState } from 'src/store';
import ListItemIngredientType from './components/ListItemIngredientType/ListItemIngredientType';

type ItemIngredientTypeManagementState = {
    ingredientTypeList: IngredientType[],
    itemTypeList: ItemType[],
}

type ItemIngredientTypeManagementDispatch = {
    getIngredientTypeAsync: Function,
    addIngredientTypeAsync: Function,
    updateIngredientTypeAsync: Function,
    deleteIngredientTypeAsync: Function,
    getItemTypeAsync: Function,
    addItemTypeAsync: Function,
    updateItemTypeAsync: Function,
    deleteItemTypeAsync: Function,
}

export type ItemIngredientTypeManagementDefaultProps = ItemIngredientTypeManagementState & ItemIngredientTypeManagementDispatch;

const ItemIngredientTypeManagementContainer: React.FC<ItemIngredientTypeManagementDefaultProps> = React.memo((props) => {
    return (
        <CFade>
            <ListItemIngredientType {...props} />
        </CFade>
    );
});

const mapState = (state: RootState) => {
    return {
        ingredientTypeList: selectIngredientTypeList(state),
        itemTypeList: selectItemTypeList(state),
    }
}
const mapDispatch = { 
    getIngredientTypeAsync,
    addIngredientTypeAsync,
    updateIngredientTypeAsync,
    deleteIngredientTypeAsync,
    getItemTypeAsync,
    addItemTypeAsync,
    updateItemTypeAsync,
    deleteItemTypeAsync
};

export default connect(mapState, mapDispatch)(ItemIngredientTypeManagementContainer);
