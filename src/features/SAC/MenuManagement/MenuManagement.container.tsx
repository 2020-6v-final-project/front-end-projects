import { FAB, history } from "@components";
import { CFade } from "@coreui/react";
import { Item, ItemType, Menu } from "@shared";
import { message, Row } from "antd";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { RootState } from "src/store";
import { selectAzureConfig } from "../../Azure/AzureManagement.selector";
import { getKeyAsync } from "../../Azure/AzureManagement.slice";
import { selectItemTypeList } from "../ItemIngredientTypeManagement/ItemIngredientTypeManagement.selector";
import { getItemTypeAsync } from "../ItemIngredientTypeManagement/ItemIngredientTypeManagement.slice";
import { selectItemList } from "../ItemManagement/ItemManagement.selector";
import { getItemAsync } from "../ItemManagement/ItemManagement.slice";
import AddMenuModal from "./components/AddMenuModal/AddMenuModal";
import DetailMenuModal from "./components/DetailMenuModal/DetailMenuModal";
import MainMenuPanel from "./components/MainMenuPanel/MainMenuPanel";
import SideMenuPanel from "./components/SideMenuPanel/SideMenuPanel";
import { selectMenuList } from "./MenuManagement.selector";
import {
  addMenuAsync,
  deactivateMenuAsync,
  getMenuAsync,
  reactivateMenuAsync,
  removeMenuAsync,
  updateMenuAsync,
} from "./MenuManagement.slice";

type MenuManagementState = {
  menuList: Array<Menu>;
  itemList: Array<Item>;
  itemTypeList: Array<ItemType>;
  match: any;
  azureKey: string;
};

type MenuManagementDispatch = {
  getMenuAsync: Function;
  addMenuAsync: Function;
  updateMenuAsync: Function;
  reactivateMenuAsync: Function;
  deactivateMenuAsync: Function;
  getItemAsync: Function;
  removeMenuAsync: Function;
  getKeyAsync: Function;
  getItemTypeAsync: Function;
};

type MenuManagementDefaultProps = MenuManagementState & MenuManagementDispatch;

const MenuManagementContainer: React.FC<MenuManagementDefaultProps> = React.memo(
  (props) => {
    const {
      azureKey,
      itemTypeList,
      menuList,
      itemList,
      getItemAsync,
      addMenuAsync,
      deactivateMenuAsync,
      reactivateMenuAsync,
      getMenuAsync,
      updateMenuAsync,
      removeMenuAsync,
      getKeyAsync,
      match,
      getItemTypeAsync,
    } = props;

    const [currentMenu, setCurrentMenu] = useState<any | undefined>(
      menuList[0] || undefined
    );

    const [AddMenuModalIsOpen, toggleAddMenuModal] = useState(false);
    const [MenuDetailsModalIsOpen, toggleMenuDetailsModal] = useState(false);

    useEffect(() => {
      const fetchData = async () => {
        await getKeyAsync();
        await getItemTypeAsync();
        await getItemAsync();
        await getMenuAsync();
      };
      fetchData();
    }, [getMenuAsync, getItemAsync, getKeyAsync, getItemTypeAsync]);

    useEffect(() => {
      if (menuList.length > 0 && currentMenu) {
        setCurrentMenu(menuList.find((item) => item.id === currentMenu.id));
      }
    }, [menuList, currentMenu]);

    useEffect(() => {
      if (menuList.length > 0) {
        if (!match.params.id) {
          setCurrentMenu(menuList[0]);
          history.push("/menu-management");
          return;
        }
        const target = menuList.find(
          (menu) => menu.id === Number(match.params.id)
        );
        if (target) {
          setCurrentMenu(target);
        } else {
          history.push("/menu-management");
          message.error("Không tìm thấy thực đơn!");
        }
      }
    }, [match.params.id, menuList]);

    const handleMenuSelect = (menu: any) => {
      setCurrentMenu(menu);
      history.push("/menu-management/" + menu.id);
    };

    const handleAddMenu = async (value: any) => {
      await addMenuAsync(value);
    };

    const handleUpdateMenu = async (value: any) => {
      await updateMenuAsync(value);
    };

    const handleRemoveMenu = async (value: any) => {
      await removeMenuAsync(value);
    };

    const handleDeactivateOrReactivateMenu = async (
      menuId: number,
      statusCode: string
    ) => {
      if (statusCode === "AVAILABLE") await deactivateMenuAsync(menuId);
      else await reactivateMenuAsync(menuId);
    };

    return (
      <CFade>
        <div style={{ overflow: "hidden" }}>
          <Row gutter={48}>
            <SideMenuPanel
              currentMenu={currentMenu}
              menus={menuList}
              handleMenuSelect={handleMenuSelect}
            />
            <MainMenuPanel
              menu={currentMenu}
              itemList={itemList}
              itemTypeList={itemTypeList}
              handleEditButtonClick={() =>
                toggleMenuDetailsModal(!MenuDetailsModalIsOpen)
              }
              handleDeactivateOrReactivate={handleDeactivateOrReactivateMenu}
              handleRemoveMenu={handleRemoveMenu}
            />
          </Row>
        </div>

        <FAB callback={() => toggleAddMenuModal(!AddMenuModalIsOpen)} />
        <AddMenuModal
          azureKey={azureKey}
          show={AddMenuModalIsOpen}
          onFinish={handleAddMenu}
          toggleModal={toggleAddMenuModal}
          itemList={itemList}
        ></AddMenuModal>
        <DetailMenuModal
          azureKey={azureKey}
          show={MenuDetailsModalIsOpen}
          onFinish={handleUpdateMenu}
          toggleModal={toggleMenuDetailsModal}
          menu={currentMenu}
          itemList={itemList}
        />
      </CFade>
    );
  }
);

const mapState = (state: RootState) => {
  return {
    menuList: selectMenuList(state),
    itemList: selectItemList(state),
    itemTypeList: selectItemTypeList(state),
    azureKey: selectAzureConfig(state),
  };
};
const mapDispatch = {
  removeMenuAsync,
  getItemAsync,
  updateMenuAsync,
  addMenuAsync,
  reactivateMenuAsync,
  deactivateMenuAsync,
  getMenuAsync,
  getKeyAsync,
  getItemTypeAsync,
};

export default connect(mapState, mapDispatch)(MenuManagementContainer);
