import { removeMenu, getMenuList, updateMenu, addMenu, reactivateMenu, deactivateMenu, requestNewMenu } from '@apis';
import { Menu } from '@custom-types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { orderBy } from 'lodash';
import { AppThunk } from 'src/store';
import swal from 'sweetalert';

type MenuManagementInitialState = {
    menuList: Array<Menu>,
}

const initialState: MenuManagementInitialState = {
    menuList: [],
}

const MenuManagementSlice = createSlice({
    name: 'menuManagement',
    initialState,
    reducers: {
        getMenusSuccess: (state, action: PayloadAction<Menu[]>) => {
            state.menuList = action.payload;
            state.menuList = orderBy(state.menuList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        getMenusFailed: (state) => {
            return state;
        },
        addMenuSuccess: (state, action: PayloadAction<Menu>) => {
            state.menuList?.push(action.payload);
            state.menuList = orderBy(state.menuList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        addMenuFailed: (state) => {
            return state;
        },
        removeMenuSuccess: (state, action: PayloadAction<Menu[]>) => {
            state.menuList = action.payload;
            state.menuList = orderBy(state.menuList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        removeMenuFailed: (state) => {
            return state;
        },
        updateMenuSuccess: (state, action: PayloadAction<Menu>) => {
            const index = state.menuList?.findIndex(menu => menu.id === action.payload.id);
            if (index > -1) {
                state.menuList?.splice(index, 1, action.payload);
            }
        },
        updateMenuFailed: (state) => {
            return state;
        },
        deactivateMenuSuccess: (state, action: PayloadAction<Menu>) => {
            const index = state.menuList?.findIndex(menu => menu.id === action.payload.id);
            if (index > -1) {
                state.menuList?.splice(index, 1, action.payload);
            }
            return state;
        },
        deactivateMenuFailed: (state) => {
            return state;
        },
        reactivateMenuSuccess: (state, action: PayloadAction<Menu>) => {
            const index = state.menuList?.findIndex(menu => menu.id === action.payload.id);
            if (index > -1) {
                state.menuList?.splice(index, 1, action.payload);
            }
            return state;
        },
        reactivateMenuFailed: (state) => {
            return state;
        },
        requestNewMenuSuccess: (state, action: PayloadAction<Menu[]>) => {
            state.menuList = action.payload;
            state.menuList = orderBy(state.menuList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        requestNewMenuFailed: (state) => {
            return state;
        }
    }

})

export const getMenuAsync = (): AppThunk => async (dispatch) => {
    try {
        await getMenuList((data: Menu[]) => {
            dispatch(getMenusSuccess(data));
        });
    } catch (e) {
        dispatch(getMenusFailed());
    }
}

export const updateMenuAsync = (menu: any): AppThunk => async (dispatch) => {
    try {
        await updateMenu(menu, (data: Menu) => {
            swal("Thành công", "Thực đơn đã được cập nhật!", "success");
            dispatch(updateMenuSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(updateMenuFailed());
    }
}

export const addMenuAsync = (menu: any): AppThunk => async (dispatch) => {
    try {
        await addMenu(menu, (data: Menu) => {
            swal("Thành công", "Thực đơn đã được thêm!", "success");
            dispatch(addMenuSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(addMenuFailed());
    }
}

export const deactivateMenuAsync = (menuId: number): AppThunk => async (dispatch) => {
    try {
        await deactivateMenu(menuId, (data: Menu) => {
            swal("Thành công", "Thực đơn đã bị khóa", "success");
            dispatch(deactivateMenuSuccess(data));
        })
    } catch (e) {
        swal("Thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(deactivateMenuFailed());
    }
}

export const reactivateMenuAsync = (menuId: number): AppThunk => async (dispatch) => {
    try {
        await reactivateMenu(menuId, (data: Menu) => {
            swal("Thành công", "Thực đơn đã được mở khóa", "success");
            dispatch(reactivateMenuSuccess(data));
        })
    } catch (e) {
        swal("Thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(reactivateMenuFailed());
    }
}

export const removeMenuAsync = (menuid: number): AppThunk => async (dispatch) => {
    try {
        await removeMenu(menuid, (data: Menu[]) => {
            swal("Thành công", "Thực đơn đã được xóa", "success");
            dispatch(removeMenuSuccess(data));
        })
    } catch (e) {
        swal("Thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(removeMenuFailed());
    }
}

export const requestNewMenuAsyns = (menuId: number, data: any): AppThunk => async (dispatch) => {
    try {
        await requestNewMenu(menuId, data, (data: Menu[]) => {
            swal("Thành công", "Gửi yêu cầu thành công", "success");
            dispatch(requestNewMenuSuccess(data));
        })
    } catch (e) {
        swal("Thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(requestNewMenuFailed());
    }
}

const {
    getMenusSuccess,
    getMenusFailed,
    addMenuSuccess,
    addMenuFailed,
    removeMenuSuccess,
    removeMenuFailed,
    updateMenuSuccess,
    updateMenuFailed,
    deactivateMenuSuccess,
    deactivateMenuFailed,
    reactivateMenuSuccess,
    reactivateMenuFailed,
    requestNewMenuSuccess,
    requestNewMenuFailed,
} = MenuManagementSlice.actions;

export {
    getMenusSuccess,
    getMenusFailed,
    addMenuSuccess,
    addMenuFailed,
    removeMenuSuccess,
    removeMenuFailed,
    updateMenuSuccess,
    updateMenuFailed,
    deactivateMenuSuccess,
    deactivateMenuFailed,
    reactivateMenuSuccess,
    reactivateMenuFailed,
    requestNewMenuSuccess,
    requestNewMenuFailed,
};

export default MenuManagementSlice.reducer;
