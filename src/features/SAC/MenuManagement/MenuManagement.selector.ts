import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectMenuList = createSelector(
    [(state: RootState) => state.menu.menuList],
    (menuList) => menuList
)