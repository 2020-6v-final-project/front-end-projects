import React, { CSSProperties } from "react";
import { Row, Col, Card, Avatar, Typography } from "antd";
import "./ItemFromRow.scss";
import { Item } from "@custom-types";
import { currencyFormat } from "@utils";

type ItemFromRowProps = {
  item: Item | undefined;
};

const styleProperties: CSSProperties = {
  overflow: "hidden",
  textOverflow: "ellipsis",
  whiteSpace: "nowrap",
};

const ItemFromRow: React.FC<ItemFromRowProps> = React.memo((props) => {
  const { item } = props;
  return (
    <Card
      hoverable
      size="default"
      style={{ ...styleProperties, padding: 16, width: 224, borderRadius: 20 }}
    >
      <Row justify="center">
        <Avatar
          size={64}
          src={
            item?.imgpath === null || item?.imgpath === ""
              ? "/item-sample.svg"
              : item?.imgpath
          }
        ></Avatar>
      </Row>
      <Row className="mt-3" justify="center">
        <Col style={styleProperties}>
          <Typography.Title style={{ textAlign: "center" }} level={5}>
            {item?.name}
          </Typography.Title>
        </Col>
      </Row>
      <Row justify="center">
        <Col>
          <Typography.Text>
            {currencyFormat(item?.price)} / {item?.unit}
          </Typography.Text>
        </Col>
      </Row>
    </Card>
  );
});

export default ItemFromRow;
