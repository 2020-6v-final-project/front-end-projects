import { Item, Menu, ItemType } from "@custom-types";
import {
  Avatar,
  Button,
  Col,
  Input,
  Row,
  Typography,
  Empty,
  Statistic,
  Divider,
  Select,
  Tabs,
} from "antd";
import { shortDateFormat } from "@shared/utils";
import React, { useEffect, useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import swal from "sweetalert";
import ItemFromRow from "../ItemFromRow/ItemFromRow";
import "./MainMenuPanel.scss";
import { Link } from "react-router-dom";

type MainMenuPanelProps = {
  menu: Menu | undefined;
  itemList: Array<Item>;
  itemTypeList: Array<ItemType>;
  handleEditButtonClick: Function;
  handleDeactivateOrReactivate: Function;
  handleRemoveMenu: Function;
};

export const countItemType = (itemTypeList: ItemType[], itemList: (Item | undefined)[]) => {
  const typeIdsOfItemList = itemList.map((item) => item?.typeid);
  return itemTypeList.filter((itemType) =>
    typeIdsOfItemList.includes(itemType.id)
  );
};

const statusTextAndColor = (
  statusCode: string
): { text: string; color: string } => {
  switch (statusCode) {
    case "AVAILABLE":
      return {
        text: "Hoạt động",
        color: "#a5ca34",
      };
    case "DISABLED":
      return {
        text: "Vô Hiệu",
        color: "#f581c4",
      };
    case "PENDINGAPPROVED":
      return {
        text: "Chờ duyệt",
        color: "#fa9665",
      };
    default:
      return {
        text: "NaN",
        color: "#a5ca34",
      };
  }
};

const MainMenuPanel: React.FC<MainMenuPanelProps> = (props) => {
  const {
    menu,
    handleEditButtonClick,
    handleDeactivateOrReactivate,
    itemList,
    handleRemoveMenu,
    itemTypeList,
  } = props;

  const [loading, setLoading] = useState<{
    deleteAction: boolean;
    activationAction: boolean;
  }>({
    deleteAction: false,
    activationAction: false,
  });
  const [menuItems, setMenuItems] = useState<Item[]>([]);
  const [filtered, setFiltered] = useState(menuItems);

  const handleDeleteMenuClick = () => {
    swal({
      title: "Bạn có chắc chắn muốn xóa thực đơn?",
      text: "Bạn không thể khôi phục thực đơn sau khi xóa!",
      icon: "warning",
      buttons: ["Hủy", "Xác Nhận"],
      dangerMode: true,
    }).then(async (willDelete) => {
      if (willDelete) {
        setLoading({ ...loading, deleteAction: true });
        await handleRemoveMenu(menu?.id);
        setLoading({ ...loading, deleteAction: true });
      }
    });
  };

  const handleSearch = (value: string) => {
    if (!value) {
      setFiltered(menuItems);
    } else {
      let temp = menuItems?.filter((item) =>
        item.name.toLocaleLowerCase().includes(value.toLocaleLowerCase())
      );
      setFiltered(temp);
    }
  };

  useEffect(() => {
    const loadItemsOfMenu = () => {
      let items = [] as Item[];

      menu?.itemList.forEach((menuItem) => {
        const rowItem: Item | undefined = itemList.find(
          (value) => value.id === menuItem.itemid
        );
        if (rowItem !== undefined) {
          items.push(rowItem);
          items[items.length - 1] = {
            ...items[items.length - 1],
            price: menuItem.price,
          };
        }
      });

      setMenuItems(items);
      setFiltered(items);
    };
    loadItemsOfMenu();
  }, [menu, itemList]);

  return (
    <Col span={17} className="main-menu-panel">
      {!menu ? (
        <Row justify="center" style={{ marginTop: "36vh" }}>
          <Empty description="Tạo hoặc chọn thực đơn để xem thông tin!" />
        </Row>
      ) : (
        <Scrollbars
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              className="track-horizontal"
              style={{ display: "none" }}
            />
          )}
          renderThumbHorizontal={(props) => (
            <div
              {...props}
              className="thumb-horizontal"
              style={{ display: "none" }}
            />
          )}
          autoHide
        >
          <Row justify="end" gutter={16}>
            <Col>
              <Button
                hidden
                type="primary"
                danger
                onClick={handleDeleteMenuClick}
                shape="round"
                loading={loading.deleteAction}
              >
                Xóa
              </Button>
            </Col>
            <Col>
              <Button
                danger={menu?.statuscode === "AVAILABLE" ? true : false}
                onClick={() =>
                  handleDeactivateOrReactivate(menu?.id, menu?.statuscode)
                }
                shape="round"
                loading={loading.activationAction}
              >
                {menu?.statuscode === "AVAILABLE"
                  ? "Khóa Thực đơn"
                  : "Mở khóa thực đơn"}
              </Button>
            </Col>
            <Col>
              <Button onClick={() => handleEditButtonClick()} shape="round">
                Sửa
              </Button>
            </Col>
          </Row>
          <Row className="mb-5" justify="center" align="middle">
            <Col span={12}>
              <Row justify="center">
                <Avatar
                  src={
                    menu?.imgpath === null || menu?.imgpath === ""
                      ? "/item-sample-large.jpg"
                      : menu?.imgpath
                  }
                  className="menu-cover-image"
                />
              </Row>
            </Col>
            <Col span={12}>
              <Typography.Title level={1}>{menu?.name}</Typography.Title>
              <Typography.Paragraph>{menu?.description}</Typography.Paragraph>
            </Col>
          </Row>

          <Row justify="space-between" className="mb-5">
            <Col span={4}>
              <Statistic title="Số món" value={menu.itemList?.length || 0} />
            </Col>
            <Col span={1}>
              <Divider type="vertical" style={{ height: "100%" }} />
            </Col>
            <Col span={4}>
              <Statistic
                title="Số loại món"
                value={countItemType(itemTypeList, menuItems).length || 0}
              />
            </Col>
            <Col span={1}>
              <Divider type="vertical" style={{ height: "100%" }} />
            </Col>
            <Col span={4}>
              <Statistic
                title="Trạng thái"
                valueStyle={{
                  color: statusTextAndColor(menu?.statuscode).color,
                }}
                value={statusTextAndColor(menu?.statuscode).text}
              />
            </Col>
            <Col span={1}>
              <Divider type="vertical" style={{ height: "100%" }} />
            </Col>
            <Col span={5}>
              <Statistic
                title="Ngày tạo"
                valueStyle={
                  menu?.createdat ? { fontSize: 18, marginTop: 8 } : {}
                }
                value={
                  menu?.createdat
                    ? shortDateFormat(menu?.createdat)
                    : "Không có"
                }
              />
            </Col>
          </Row>

          <Row gutter={24}>
            <Col>
              <Typography.Title level={4}>Các món ăn</Typography.Title>
            </Col>
            <Col>
              <Input
                onChange={(e) => {
                  handleSearch(e.target.value);
                }}
                className="mb-3"
                placeholder="Tìm kiếm món ăn"
              ></Input>
            </Col>
            <Col>
              <Select
                placeholder="Sắp xếp"
                defaultValue={"name_asc"}
                defaultActiveFirstOption
              >
                <Select.Option value="name_asc" key={1}>
                  Tên tăng dần
                </Select.Option>
                <Select.Option value="name_desc" key={2}>
                  Tên giảm dần
                </Select.Option>
                <Select.Option value="baseprice_asc" key={3}>
                  Giá tăng dần
                </Select.Option>
                <Select.Option value="baseprice_desc" key={4}>
                  Giá giảm dần
                </Select.Option>
              </Select>
            </Col>
          </Row>

          <Row justify="center" className="my-3">
            <Tabs centered defaultActiveKey="1">
              <Tabs.TabPane tab="Tất cả" key="1">
                <Row
                  className="mt-3 mb-5"
                  justify={filtered?.length > 0 ? "start" : "center"}
                  style={{ margin: 0 }}
                  gutter={[24, 24]}
                >
                  {filtered?.length > 0 ? (
                    filtered?.map((menuItem: any, index: number) => (
                      <Link key={index} to={"/item-management/" + menuItem.id}>
                        <Col>
                          <ItemFromRow item={menuItem} />
                        </Col>
                      </Link>
                    ))
                  ) : (
                    <Col>
                      <Empty description="Không có món nào!" />
                    </Col>
                  )}
                </Row>
              </Tabs.TabPane>
              {countItemType(itemTypeList, filtered).map((itemType, index) => (
                <Tabs.TabPane tab={itemType.name} key={(index + 2).toString()}>
                  <Row
                    className="mt-3 mb-5"
                    justify={filtered?.length > 0 ? "start" : "center"}
                    style={{ margin: 0 }}
                    gutter={[24, 24]}
                  >
                    {filtered?.length > 0 ? (
                      filtered
                        ?.filter((item) => item.typeid === itemType.id)
                        .map((menuItem, menuItemIndex) => (
                          <Link key={menuItemIndex} to={"/item-management/" + menuItem.id}>
                            <Col>
                              <ItemFromRow item={menuItem} />
                            </Col>
                          </Link>
                        ))
                    ) : (
                      <Col>
                        <Empty description="Không có món nào!" />
                      </Col>
                    )}
                  </Row>
                </Tabs.TabPane>
              ))}
            </Tabs>
          </Row>
        </Scrollbars>
      )}
    </Col>
  );
};

export default MainMenuPanel;
