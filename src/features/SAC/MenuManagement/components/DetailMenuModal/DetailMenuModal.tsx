import { SubHeaderText } from "@components";
import { CIcon } from "@coreui/icons-react";
import {
  AutoComplete,
  Button,
  Col,
  Drawer,
  Form,
  Input,
  Row,
  Timeline,
  Typography,
  Upload,
} from "antd";
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { uploadFileToBlob} from '../../../../Azure/azure-storage-blob';
import React, { useState, useEffect } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import { Item } from "@custom-types";
import "./DetailMenuModal.scss";
import swal from 'sweetalert';

type DetailMenuModalProps = {
  show: boolean;
  onFinish: Function;
  toggleModal: Function;
  itemList: Array<Item>;
  menu: any | null;
  azureKey:string;
};

const DetailMenuModal: React.FC<DetailMenuModalProps> = (props) => {
  const { azureKey, itemList, show, toggleModal, onFinish, menu } = props;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [loadImg,setLoadImg] = useState<boolean>(false);
  const [imageUrl, setImageUrl] = useState<any>("");
  const [items, setItems] = useState([] as any);
  const [formValue, setFormValue] = useState<any>({
    id: "",
    name: "",
    description: "",
  });
  const [autoComplete, setAutoComplete] = useState<string>("");

  const handleSelectItem = (item: any) => {
    let newItem = {};
    const index = items.findIndex(
      (selectItem: any) => item.id === selectItem.id
    );
    if (index > -1) {
      newItem = {
        ...items[index],
      };
      let temp = [] as any;
      Object.assign(temp, items);
      temp.splice(index, 1, newItem);
      setItems(temp);
    } else {
      newItem = item;
      setItems(items.concat(newItem));
    }
  };

  const handleRemoveItem = (removeItem: any) => {
    setItems(items.filter((item: any) => item.id !== removeItem.id));
  };

  const handleUpdateMenu = async () => {
    const sendData = {
      ...formValue,
      itemList: items,
      imgpath:imageUrl,
    };
    setLoading(true);
    await onFinish(sendData);
    setLoading(false);
    form.resetFields();
    setItems([]);
    setImageUrl("");
    setLoadImg(false);
    toggleModal();
  };

  useEffect(() => {
    setImageUrl(menu?.imgpath);
    const loadItemsOfMenu = () => {
      let items = [] as Item[];
      if (menu?.itemList?.length > 0) {
        menu.itemList.forEach((menuItem: any) => {
          const rowItem: Item | undefined = itemList.find(
            (value) => value.id === menuItem.itemid
          );
          if (rowItem !== undefined) {
            items.push(rowItem);
            items[items.length - 1] = { ...items[items.length - 1], price: menuItem.price };
          }
        });
      }
      
      setItems(items);
    };

    if (menu) {
      setFormValue({
        id: menu?.id,
        name: menu?.name,
        description: menu?.description,
      });
      loadItemsOfMenu();
      form?.setFieldsValue({
        name: menu?.name,
        description: menu?.description,
      });
    }
  }, [menu, form, itemList, show]);

  const handleImgChange = async (info:any) => {
    const isJpgOrPng = info.file.type === 'image/jpeg' || info.file.type === 'image/png';
    const isLt2M = info.file.size / 1024 / 1024 < 2;
    setLoadImg(true);
    if (!isJpgOrPng) {
      swal("Thất bại", "Bạn chỉ có thể tải file JPG/PNG!", "error");

    }
    else if (!isLt2M) {
      swal("Thất bại", "Kích cỡ file vượt quá 2MB!", "error");
    }
    else if(imageUrl !== "")
    {
      setImageUrl("");
    }
    else if (info.file) {
      const responseIMG:any = await uploadFileToBlob(info.file, azureKey);
      setImageUrl(responseIMG.url);
      setLoadImg(false);
    }
 
  };
  return (
    <Drawer
      visible={show}
      width="500"
      onClose={(e) => toggleModal()}
      zIndex={1}
      className="add-item-modal"
    >
      <Scrollbars autoHide>
        <Row style={{ margin: 20 }} justify="center" align="middle">
          <Col span={24}>
            <SubHeaderText style={{ textAlign: "center" }} className="mb-4">
              Chỉnh sửa thực đơn
            </SubHeaderText>
            <Row justify="center" className="mb-3">
              <Upload
                  name="avatar"
                  listType="picture-card"
                  className="avatar-uploader upload-img"
                  showUploadList={false}
                  beforeUpload={() => false      }
                  onChange={(e)=>handleImgChange(e)}
                >
                  {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : 
                  <div>
                    {loadImg ? <LoadingOutlined /> : <PlusOutlined />}
                    <div style={{ marginTop: 8 }}>Tải ảnh</div>
                  </div>
                }
                </Upload>
            </Row>
            <Form
              name="addItemForm"
              form={form}
              onFinish={handleUpdateMenu}
            >
              <Row justify="center">
                <Col>
                  <Form.Item
                    name="name"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền tên thực đơn",
                      },
                    ]}
                    initialValue={menu?.name}
                  >
                    <Input
                      style={{
                        border: "none",
                        textAlign: "center",
                        fontSize: 18,
                        fontWeight: 400,
                      }}
                      value={formValue.name || ""}
                      placeholder="Tên thực đơn"
                      onChange={(e) =>
                        setFormValue({ ...formValue, name: e.target.value })
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row justify="center">
                <Col span={24}>
                  <Form.Item
                    name="description"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền mô tả",
                      },
                    ]}
                    initialValue={menu?.description}
                  >
                    <Input.TextArea
                      autoSize={{ minRows: 3, maxRows: 3 }}
                      value={formValue.description || ""}
                      placeholder="Mô tả"
                      onChange={(e) =>
                        setFormValue({
                          ...formValue,
                          description: e.target.value,
                        })
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={24}>
                <Typography.Title level={5}>Các món thêm </Typography.Title>
              </Row>
              <Row gutter={24} className="mt-2 mb-5">
                <Col span={24}>
                  <AutoComplete
                    value={autoComplete}
                    onSelect={() => setAutoComplete("")}
                    onChange={(value) => setAutoComplete(value)}
                    allowClear
                    style={{ width: "100%" }}
                    placeholder="Món ăn"
                    filterOption={(inputValue, option) =>
                      option!.value
                        .toUpperCase()
                        .indexOf(inputValue.toUpperCase()) !== -1
                    }
                  >
                    {itemList.map((item: any) => (
                      <AutoComplete.Option key={item.id} value={item.name}>
                        <Row
                          key={item.id}
                          onClick={() => handleSelectItem(item)}
                        >
                          {item.name}
                        </Row>
                      </AutoComplete.Option>
                    ))}
                  </AutoComplete>
                </Col>
              </Row>
              <Row justify="center">
                <Col style={{ width: "100%" }}>
                  <Timeline>
                    {items?.map((item: any) => (
                      <Timeline.Item key={item.id}>
                        <Row
                          justify="space-between"
                          className="mb-3"
                          gutter={24}
                        >
                          <Col span={20}>
                            <Input value={item.name} disabled />
                          </Col>
                          <Col span={4}>
                            <Button
                              onClick={() => {
                                handleRemoveItem(item);
                              }}
                              type="text"
                            >
                              <CIcon name="cil-delete"></CIcon>
                            </Button>
                          </Col>
                        </Row>
                      </Timeline.Item>
                    ))}
                  </Timeline>
                </Col>
              </Row>

              <Row justify="center">
                <Form.Item>
                  <Button
                    loading={loading}
                    type="primary"
                    htmlType="submit"
                  >
                    Lưu
                  </Button>
                </Form.Item>
              </Row>
            </Form>
          </Col>
        </Row>
      </Scrollbars>
    </Drawer>
  );
};

export default DetailMenuModal;
