import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AppThunk } from "src/store";
import { Role } from "@custom-types";
import {
  getRoleList
} from "@apis";
import swal from 'sweetalert';
// Types and state declaration
type RoleManagementState = {
  roleList: Role[];
};

const initialState: RoleManagementState = {
  roleList: []
};

// Slice
const RoleManagementSlice = createSlice({
  name: "roleManagement",
  initialState,
  reducers: {
    fetchRoleListSuccess: (state, action: PayloadAction<Role[]>) => {
      state.roleList = action.payload;
      return state;
    },
    fetchRoleListFailed: (state) => {
      return state;
    }
  }
});

// Thunk for async actions
export const fetchRoleListAsync = (): AppThunk => async (dispatch) => {
  try {
    let roleList: Role[];
    await getRoleList((data: Role[]) => {
      roleList = data;
      roleList.sort((item1: Role, item2: Role) => {
        return item1.id - item2.id;
      });
      dispatch(fetchRoleListSuccess(roleList));
    });
  } catch (e) {
    swal("Tải danh sách quyền không thành công!", "Vui lòng thử lại!", "error");
    dispatch(fetchRoleListFailed());
  }
};

// Export part
export const {
  fetchRoleListSuccess,
  fetchRoleListFailed,
} = RoleManagementSlice.actions;

export default RoleManagementSlice.reducer;
