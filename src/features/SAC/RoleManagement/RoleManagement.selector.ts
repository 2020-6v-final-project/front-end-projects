import { RootState } from "src/store";
import { createSelector } from "reselect";

export const roleListSelector = createSelector(
  [(state: RootState) => state.role.roleList],
  (roleList) => {
    return roleList;
  }
);
