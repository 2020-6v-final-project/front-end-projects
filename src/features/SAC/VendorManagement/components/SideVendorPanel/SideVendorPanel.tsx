import { Vendor } from "@shared";
import {
  Col,
  Divider,
  Empty,
  Input,
  List,
  Row,
  Spin,
} from "antd";
import React, { useEffect, useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import VendorFromList from "../VendorFromList/VendorFromList";
import "./SideVendorPanel.scss";

type VendorSidePanelProps = {
  vendors: Array<Vendor>;
  currentVendor?: Vendor;
};

type VendorSidePanelDispatch = {
  handleVendorSelect: Function;
};

type VendorSidePanelDefaultProps = VendorSidePanelProps & VendorSidePanelDispatch;

const SidePanel: React.FC<VendorSidePanelDefaultProps> = React.memo((props) => {
  const { vendors, currentVendor } = props;
  const { handleVendorSelect } = props;
  const [loading, setLoading] = useState(true);
  const [filtered, setFiltered] = useState(vendors);
  const [shouldRender, setShouldRender] = useState(false);

  useEffect(() => {
    if (vendors.length < 1) {
      setLoading(false);
      setShouldRender(false);
    } else {
      setLoading(false);
      setFiltered(vendors);
      setShouldRender(true);
    }
  }, [vendors]);

  const handleSearch = (value: string) => {
    if (!value) {
      setFiltered(vendors);
    } else {
      const temp = vendors.filter(
        (vendor) =>
          vendor.name.toLocaleLowerCase().includes(value.toLowerCase())
      );
      setFiltered(temp);
    }
  };

  return (
    <Col span={7} style={{ height: "90vh" }} className="side-menu">
      <Row justify="space-between" align="middle">
        <Input
          onChange={(e) => {
            handleSearch(e.target.value);
          }}
          className="mb-3"
          placeholder="Tìm kiếm nhà cung cấp"
        ></Input>
      </Row>

        <Row style={{ height: "65vh", padding: "0px" }} justify="center">
          <Col span={23}>
            {loading ? (
              <Spin
                size="large"
                style={{ textAlign: "center", margin: "auto" }}
                spinning={loading}
              />
            ) : (
              <>
                {shouldRender ? (
                  <>
                    <Scrollbars>
                      <List
                        className="list-vendor"
                        style={{ marginBottom: "176px" }}
                      >
                        {filtered.map((item, index) => (
                            <List.Item
                              className={
                                currentVendor?.id === item.id
                                  ? "active-vendor-from-list"
                                  : ""
                              }
                              onClick={() => handleVendorSelect(item)}
                            >
                              <VendorFromList vendor={item} />
                            </List.Item>
                        ))}
                      </List>
                    </Scrollbars>
                  </>
                ) : (
                  <Col span={24}>
                    <Empty
                      style={{ marginTop: "30vh" }}
                      description="Hiện chưa có nhà cung cấp nào!"
                    />
                  </Col>
                )}
              </>
            )}
          </Col>
          <Col span={1}>
            <Divider type="vertical" style={{ height: "100%" }} />
          </Col>
        </Row>
    </Col>
  );
});

export default SidePanel;
