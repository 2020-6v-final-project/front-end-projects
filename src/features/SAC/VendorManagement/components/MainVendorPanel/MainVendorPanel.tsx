import { Ingredient, Vendor } from "@custom-types";
import {
  Avatar,
  Button,
  Col,
  Descriptions,
  Empty,
  Row,
  Typography,
  Tag,
} from "antd";
import React from "react";
import { Scrollbars } from "react-custom-scrollbars";
import "./MainVendorPanel.scss";
import { Link } from "react-router-dom";

type MainVendorPanelProps = {
  vendor: Vendor | undefined;
  ingredientList: Array<Ingredient>;
  handleEditButtonClick: Function;
  handleDeactivateOrReactivate: Function;
};

const MainVendorPanel: React.FC<MainVendorPanelProps> = React.memo((props) => {
  const {
    vendor,
    handleEditButtonClick,
  } = props;
  return (
    <Col span={17} className="main-vendor-panel">
      {!vendor ? (
        <Row justify="center">
          <Empty
            style={{ marginTop: "36vh" }}
            description="Tạo hoặc chọn nhà cung cấp để xem thông tin!"
          />
        </Row>
      ) : (
        <Scrollbars
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              className="track-horizontal"
              style={{ display: "none" }}
            />
          )}
          renderThumbHorizontal={(props) => (
            <div
              {...props}
              className="thumb-horizontal"
              style={{ display: "none" }}
            />
          )}
          autoHide
        >
          <Row justify="end" gutter={16}>
            {/* <Col>
              <Button
                danger={vendor?.statuscode === "AVAILABLE" ? true : false}
                type={vendor?.statuscode === "AVAILABLE" ? undefined : "primary"}
                loading={loading}
                onClick={async () => {
                  setLoading(true);
                  await handleDeactivateOrReactivate(vendor?.id, vendor?.statuscode);
                  setLoading(false);
                }}
                shape="round"
              >
                {vendor?.statuscode === "AVAILABLE"
                  ? "Khóa nhà cung cấp"
                  : "Mở khóa nhà cung cấp"}
              </Button>
            </Col> */}
            <Col>
              <Button onClick={() => handleEditButtonClick()} shape="round">
                Sửa
              </Button>
            </Col>
          </Row>
          <Row className="mb-5" justify="center" align="middle">
            <Col span={12}>
              <Row justify="center">
                <Avatar
                  src="/icon-vendor.png" 
                  className="vendor-cover-image"
                />
              </Row>
            </Col>
            <Col span={12}>
              <Typography.Title level={1}>{vendor?.name}</Typography.Title>
              <Typography.Title level={5}>SĐT: {vendor?.phonenumber}</Typography.Title>
              <Typography.Paragraph>Địa chỉ: {vendor?.address}</Typography.Paragraph>
            </Col>
          </Row>

          <Row
            justify="space-between"
            gutter={24}
            style={{ paddingBottom: 24 }}
          >
            <Col span={24}>
              <Row className="mb-3" justify="center">
                <Typography.Title level={4}>Nguyên liệu</Typography.Title>
              </Row>
              <Row justify="space-between">
                <Col span={24}>
                  {vendor?.ingredients?.length > 0 ? (
                    <Descriptions
                      className="mb-5"
                      layout="horizontal"
                      bordered
                      column={2}
                    >
                      {vendor?.ingredients?.map(
                        (ingredient: any, index: number) => {
                          return (
                            <>
                              <Descriptions.Item
                                label="Nguyên liệu"
                                key={index * 2}
                              >
                                <Link
                                  to={
                                    "/ingredient-management/" + ingredient?.id
                                  }
                                >
                                  <Tag>{ingredient?.name}</Tag>
                                </Link>
                              </Descriptions.Item>
                              <Descriptions.Item
                                label="Đơn vị"
                                key={index * 2 + 2}
                              >
                                {ingredient?.unit}
                              </Descriptions.Item>
                            </>
                          );
                        }
                      )}
                    </Descriptions>
                  ) : (
                    <Empty className="mb-5" description="Chưa có dữ liệu" />
                  )}
                </Col>
              </Row>
            </Col>
          </Row>
        </Scrollbars>
      )}
    </Col>
  );
});

export default MainVendorPanel;
