import React, { CSSProperties } from "react";
import { Col, Row, Typography, Avatar } from "antd";
import "./VendorFromList.scss";
import { Vendor } from "@custom-types";

type VendorFromListProps = {
  vendor: Vendor;
};

const CSSStyle: CSSProperties = {
  textOverflow: "ellipsis",
  overflow: "hidden",
  whiteSpace: "nowrap",
};

const VendorFromList: React.FC<VendorFromListProps> = React.memo((props) => {
  const { vendor } = props;
  return (
    <Row
      className="vendor-from-list"
      justify="start"
      gutter={24}
      align="middle"
      style={{
        width: "100%",
      }}
      wrap={false}
    >
      <Col>
          <Avatar  style={{ width: 64, height: 64 }}  src="/icon-vendor.png"/>
        </Col>
      <Col  style={{ ...CSSStyle }}>
        <Typography.Title style={{ ...CSSStyle }} level={3}>
          {vendor?.name}
        </Typography.Title>
      </Col>
    </Row>
  );
});

export default VendorFromList;
