import { SubHeaderText } from "@components";
import CIcon from "@coreui/icons-react";
import { Ingredient, Vendor } from "@custom-types";
import {
  AutoComplete,
  Button,
  Col,
  Drawer,
  Form,
  Input,
  Row,
  Timeline,
  Typography,
} from "antd";
import React, {  useEffect, useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import "./VendorDetailsModal.scss";

type VendorDetailsModalProps = {
  vendor: Vendor | undefined;
  ingredientList: Array<Ingredient>;
  show: boolean;
  onFinish: Function;
  toggleModal: Function;
};

const VendorDetailsModal: React.FC<VendorDetailsModalProps> = (props) => {
  const { show, toggleModal, onFinish, ingredientList, vendor } = props;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [ingredients, setIngredients] = useState<any>(vendor?.ingredients);
  const [autoCompleteText, setAutoCompleteText] = useState("");
  const [formValue, setFormValue] = useState<any>({
    id: vendor?.id,
    name: "",
    phonenumber: "",
    address: "",
  });

  useEffect(() => {
    if (vendor) {
      setFormValue({
        name: vendor.name,
        phonenumber: vendor.phonenumber,
        address: vendor.address,
      });
      form?.setFieldsValue({
        name: vendor?.name,
        phonenumber: vendor?.phonenumber,
        address: vendor?.address,
      });
      setIngredients(vendor?.ingredients?.map((ingre:any) =>{
        let castIngre = {
          ingredientid: ingre.id
        }
        return castIngre;
      }));
    }
  }, [vendor, form, show]);

  const handleSelectIngredient = (item: any) => {
    let newIngredient = {};
    const index = ingredients.findIndex(
      (ingredient: any) => item.id === ingredient.ingredientid
    );
    if (index > -1) {
      newIngredient = {
        ingredientid: ingredients[index].ingredientid
      };
      let temp = [] as any;
      Object.assign(temp, ingredients);
      temp.splice(index, 1, newIngredient);
      setIngredients(temp);
    } else {
      newIngredient = {
        ingredientid: item.id
      };
      setIngredients(ingredients.concat(newIngredient));
    }
  };

  const handleRemoveIngredient = (item: any) => {
    setIngredients(
      ingredients.filter(
        (ingredient: any) => ingredient.ingredientid !== item.ingredientid
      )
    );
  };

  const handleUpdateVendor = async () => {
    const sendData = {
      ...formValue,
      id: vendor?.id,
      ingredients: ingredients,
    };
    setLoading(true);
    await onFinish(sendData);
    setLoading(false);
    setFormValue({
      name: "",
      phonenumber: "",
      adress: "",
    });
    form.resetFields();
    setIngredients([]);
    toggleModal();
  };

  const setInitialFormData = () => {
    form.setFieldsValue({
      name: vendor?.name,
      phonenumber: vendor?.phonenumber,
      address: vendor?.address,
    });
    setIngredients(vendor?.ingredients);
  };



  return (
    <Drawer
      visible={show}
      width="500"
      onClose={() => {
        toggleModal();
        setInitialFormData();
      }}
      zIndex={1}
      className="add-vendor-modal"
    >
      <Scrollbars autoHide>
        <Row style={{ margin: 20 }} justify="center" align="middle">
          <Col>
            <SubHeaderText style={{ textAlign: "center" }} className="mb-4">
              Chỉnh sửa nhà cung cấp
            </SubHeaderText>
            <Form
              form={form}
              name="addVendorForm"
              onFinish={handleUpdateVendor}
            >
              <Row justify="center">
                <Col>
                  <Form.Item
                    name="name"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền tên nhà cung cấp",
                      },
                    ]}
                    initialValue={vendor?.name}
                  >
                    <Input
                      style={{
                        border: "none",
                        textAlign: "center",
                        fontSize: 18,
                        fontWeight: 400,
                      }}
                      placeholder="Tên món"
                      onChange={(e) =>
                        setFormValue({ ...formValue, name: e.target.value })
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={24} justify="space-between">
                <Col span={24}>
                  <Form.Item
                    name="phonenumber"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền số điện thoại",
                      },
                    ]}
                    initialValue={vendor?.phonenumber}
                  >
                    <Input
                      type="number"
                      placeholder="Số điện thoại"
                      value={formValue.phonenumber || ""}
                      onChange={(e) =>
                        setFormValue({
                          ...formValue,
                          phonenumber: e.target.value,
                        })
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row justify="center">
                <Col span={24}>
                  <Form.Item
                    name="address"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền địa chỉ",
                      },
                    ]}
                    initialValue={vendor?.address}
                  >
                    <Input.TextArea
                      showCount
                      maxLength={100}
                      autoSize={{ minRows: 3, maxRows: 3 }}
                      placeholder="Địa chỉ"
                      value={formValue.address || ""}
                      onChange={(e) =>
                        setFormValue({
                          ...formValue,
                          address: e.target.value,
                        })
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Typography.Title level={5}>Nguyên liệu</Typography.Title>
              </Row>
              <Row gutter={24} className="mt-2 mb-5">
                <Col span={24}>
                  <AutoComplete
                    allowClear
                    value={autoCompleteText}
                    onChange={(value) => setAutoCompleteText(value)}
                    onSelect={() => setAutoCompleteText("")}
                    style={{ width: "100%" }}
                    placeholder="Nguyên liệu"
                    filterOption={(inputValue, option) =>
                      option!.value
                        .toUpperCase()
                        .indexOf(inputValue.toUpperCase()) !== -1
                    }
                  >
                    {ingredientList.map((vendor: any) => (
                      <AutoComplete.Option key={vendor.id} value={vendor.name}>
                        <Row
                          key={vendor.id}
                          onClick={() => handleSelectIngredient(vendor)}
                        >
                          {vendor.name}
                        </Row>
                      </AutoComplete.Option>
                    ))}
                  </AutoComplete>
                </Col>
              </Row>
              <Row justify="center">
                <Col>
                  <Timeline>
                    {ingredients?.map((item: any, index: number) => (
                      <Timeline.Item key={index}>
                        <Row
                          justify="space-between"
                          className="mb-3"
                          gutter={24}
                        >
                          <Col span={8}>
                            <Input value={ingredientList.find(ingre =>ingre.id === item?.ingredientid)?.name} disabled />
                          </Col>
                          <Col span={6}>
                            <Input value={ingredientList.find(ingre =>ingre.id === item?.ingredientid)?.unit} disabled />
                          </Col>
                          <Col span={4}>
                            <Button
                              onClick={() => {
                                handleRemoveIngredient(item);
                              }}
                              type="text"
                            >
                              <CIcon name="cil-delete"></CIcon>
                            </Button>
                          </Col>
                        </Row>
                      </Timeline.Item>
                    ))}
                  </Timeline>
                </Col>
              </Row>
              <Row justify="center">
                <Form.Item>
                  <Button loading={loading} type="primary" htmlType="submit">
                    Lưu
                  </Button>
                </Form.Item>
              </Row>
            </Form>
          </Col>
        </Row>
      </Scrollbars>
    </Drawer>
  );
};

export default VendorDetailsModal;
