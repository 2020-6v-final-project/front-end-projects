import { SubHeaderText } from "@components";
import CIcon from "@coreui/icons-react";
import { Ingredient } from "@custom-types";
import {
  AutoComplete,
  Button,
  Col,
  Drawer,
  Form,
  Input,
  Row,
  Timeline,
  Typography,
} from "antd";
import React, { useState, useEffect } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import "./AddVendorModal.scss";

type AddVendorModalProps = {
  ingredientList: Array<Ingredient>;
  show: boolean;
  onFinish: Function;
  toggleModal: Function;
};

const AddVendorModal: React.FC<AddVendorModalProps> = (props) => {
  const { show, toggleModal, onFinish, ingredientList } = props;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [ingredients, setIngredients] = useState<any>([]);
  const [autoCompleteText, setAutoCompleteText] = useState("");
  const [formValue, setFormValue] = useState<any>({
    name: "",
    phonenumber: "",
    adress: "",
  });

  useEffect(() => {
    if (!show) {
      setFormValue({
        name: "",
        phonenumber: "",
        address: "",
      });
      form.resetFields();
      setIngredients([]);
    }
  }, [show, form]);

  const handleSelectIngredient = (item: any) => {
    let newIngredient = {};
    const index = ingredients.findIndex(
      (ingredient: any) => item.id === ingredient.ingredientid
    );
    if (index > -1) {
      newIngredient = {
        ingredientid: ingredients[index].ingredientid
      };
      let temp = [] as any;
      Object.assign(temp, ingredients);
      temp.splice(index, 1, newIngredient);
      setIngredients(temp);
    } else {
      newIngredient = {
        ingredientid: item.id
      };
      setIngredients(ingredients.concat(newIngredient));
    }
  };

  const handleRemoveIngredient = (item: any) => {
    setIngredients(
      ingredients.filter(
        (ingredient: any) => ingredient.ingredientid !== item.ingredientid
      )
    );
  };

  const handleAddVendor = async () => {
    const sendData = {
      ...formValue,
      ingredients: ingredients,
    };
    setLoading(true);
    await onFinish(sendData);
    setLoading(false);
    setFormValue({
      name: "",
      phonenumber: "",
      adress: "",
    });
    form.resetFields();
    setIngredients([]);
    toggleModal();
  };

  
  return (
    <Drawer
      visible={show}
      width="500"
      onClose={() => {
        form.resetFields();
        toggleModal();
        setIngredients([]);
      }}
      zIndex={1}
      className="add-vendor-modal"
    >
      <Scrollbars autoHide>
        <Row style={{ margin: 20 }} justify="center" align="middle">
          <Col span={24}>
            <SubHeaderText style={{ textAlign: "center" }} className="mb-4">
              Thêm nhà cung cấp
            </SubHeaderText>
            <Form
              form={form}
              name="addVendorForm"
              onFinish={handleAddVendor}
            >
              <Row justify="center">
                <Col span={24}>
                  <Form.Item
                    name="name"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền tên nhà cung cấp",
                      },
                    ]}
                  >
                    <Input
                      style={{
                        border: "none",
                        textAlign: "center",
                        fontSize: 18,
                        fontWeight: 400,
                        width:"100%"
                      }}
                      placeholder="Tên nhà cung cấp"
                      value={formValue.name || ""}
                      onChange={(e) =>
                        setFormValue({ ...formValue, name: e.target.value })
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row justify="center">
                <Col span={24}>
                  <Form.Item
                    name="phonenumber"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền số điện thoại",
                      },
                    ]}
                  >
                    <Input
                      placeholder="Số điện thoại"
                      value={formValue.phonenumber || ""}
                      type="number"
                      onChange={(e) =>
                        setFormValue({
                          ...formValue,
                          phonenumber: e.target.value,
                        })
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row justify="center">
                <Col span={24}>
                  <Form.Item
                    name="address"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền địa chỉ",
                      },
                    ]}
                  >
                    <Input.TextArea
                      autoSize={{ minRows: 3, maxRows: 3 }}
                      placeholder="Địa chỉ"
                      value={formValue.address || ""}
                      onChange={(e) =>
                        setFormValue({ ...formValue, address: e.target.value })
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Typography.Title level={5}>Nguyên liệu</Typography.Title>
              </Row>
              <Row gutter={24} className="mt-2 mb-5">
                <Col span={24}>
                  <AutoComplete
                    allowClear
                    onSelect={() => setAutoCompleteText("")}
                    onChange={(value) => {
                      setAutoCompleteText(value);
                    }}
                    value={autoCompleteText}
                    style={{ width: "100%" }}
                    placeholder="Nguyên liệu"
                    filterOption={(inputValue, option) =>
                      option!.value
                        .toUpperCase()
                        .indexOf(inputValue.toUpperCase()) !== -1
                    }
                  >
                    {ingredientList
                      .filter(
                        (ingredient) => ingredient.statuscode === "AVAILABLE"
                      )
                      .map((ingredient) => (
                        <AutoComplete.Option
                          key={ingredient.id}
                          value={ingredient.name}
                        >
                          <Row
                            key={ingredient.id}
                            onClick={(e) => handleSelectIngredient(ingredient)}
                          >
                            {ingredient.name}
                          </Row>
                        </AutoComplete.Option>
                      ))}
                  </AutoComplete>
                </Col>
              </Row>
              <Row justify="center">
                <Col>
                  <Timeline>
                    {ingredients.map((item: any, index: number) => (
                      <Timeline.Item key={index}>
                        <Row
                          justify="space-between"
                          className="mb-3"
                          gutter={24}
                        >
                          <Col span={8}>
                            <Input value={ingredientList.find(ingre =>ingre.id === item?.ingredientid)?.name} disabled />
                          </Col>
                          <Col span={6}>
                            <Input value={ingredientList.find(ingre =>ingre.id === item?.ingredientid)?.unit} disabled />
                          </Col>
                          <Col span={4}>
                            <Button
                              onClick={() => {
                                handleRemoveIngredient(item);
                              }}
                              type="text"
                            >
                              <CIcon name="cil-delete"></CIcon>
                            </Button>
                          </Col>
                        </Row>
                      </Timeline.Item>
                    ))}
                  </Timeline>
                </Col>
              </Row>
              <Row justify="center">
                <Form.Item>
                  <Button loading={loading} type="primary" htmlType="submit">
                    Thêm
                  </Button>
                </Form.Item>
              </Row>
            </Form>
          </Col>
        </Row>
      </Scrollbars> 
    </Drawer>
  );
};

export default AddVendorModal;
