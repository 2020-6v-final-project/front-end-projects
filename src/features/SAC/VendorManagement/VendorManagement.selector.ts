import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectVendorList = createSelector(
    [(state: RootState) => state.vendor.vendorList],
    (vendorList) => vendorList
);