import { FAB } from "@components";
import { CFade } from "@coreui/react";
import { Ingredient, Vendor } from "@shared";
import { Row, message } from "antd";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { RootState } from "src/store";
import { selectIngredientList } from "../IngredientManagement/IngredientManagement.selector";
import { getIngredientAsync } from "../IngredientManagement/IngredientManagement.slice";
import AddVendorModal from "./components/AddVendorModal/AddVendorModal";
import VendorDetailsModal from "./components/VendorDetailsModal/VendorDetailsModal";
import MainVendorPanel from "./components/MainVendorPanel/MainVendorPanel";
import SideVendorPanel from "./components/SideVendorPanel/SideVendorPanel";
import "./VendorManagement.selector";
import { selectVendorList } from "./VendorManagement.selector";
import {
  addVendorAsync,
  deactivateVendorAsync,
  getVendorAsync,
  reactivateVendorAsync,
  updateVendorAsync,
} from "./VendorManagement.slice";
import { history } from "@shared/components";

type VendorManagementState = {
  vendorList: Array<Vendor>;
  ingredientList: Array<Ingredient>;
  match: any;
};

type VendorManagementDispatch = {
  getVendorAsync: Function;
  updateVendorAsync: Function;
  addVendorAsync: Function;
  reactivateVendorAsync: Function;
  deactivateVendorAsync: Function;
  getIngredientAsync: Function;
};

type VendorManagementDefaultProps = VendorManagementState & VendorManagementDispatch;

const VendorManagementContainer: React.FC<VendorManagementDefaultProps> = React.memo(
  (props) => {
    const { vendorList, ingredientList, match } = props;
    const {
      getVendorAsync,
      addVendorAsync,
      deactivateVendorAsync,
      reactivateVendorAsync,
      getIngredientAsync,
      updateVendorAsync,
    } = props;

    const [currentVendor, setcurrentVendor] = useState<Vendor | undefined>(
      vendorList[0] || undefined
    );
    const [addVendorModalIsOpen, toggleAddVendorModal] = useState(false);
    const [vendorDetailsModalIsOpen, toggleVendorDetailsModal] = useState(false);

    useEffect(() => {
      const fetchData = async () => {
        await getIngredientAsync();
        await getVendorAsync();
      }
      fetchData();
    }, [getVendorAsync, getIngredientAsync]);

    useEffect(() => {
      if (vendorList.length > 0 && currentVendor) {
        setcurrentVendor(vendorList.find((vendor) => vendor.id === currentVendor.id));
      }
    }, [vendorList, currentVendor]);

    useEffect(() => {
      if (vendorList.length > 0) {
        if (!match.params.id) {
          setcurrentVendor(vendorList[0]);
          history.push("/vendor-management");
          return;
        }
        const target = vendorList.find(
          (vendor) => vendor.id === Number(match.params.id)
        );
        if (target) {
          setcurrentVendor(target);
        } else {
          history.push("/vendor-management");
          message.error("Không tìm thấy nhà cung cấp!");
        }
      }
    }, [match.params.id, vendorList]);

    const handleVendorSelect = (vendor: Vendor) => {
      setcurrentVendor(vendor);
      history.push("/vendor-management/" + vendor.id);
    };

    const handleAddVendor = async (value: any) => {
      await addVendorAsync(value);
    };

    const handleUpdateVendor = async (value: any) => {
      await updateVendorAsync(value);
    };

    const handleDeactivateOrReactivateVendor = async (
      vendorId: number,
      statusCode: string
    ) => {
      if (statusCode === "AVAILABLE") await deactivateVendorAsync(vendorId);
      else await reactivateVendorAsync(vendorId);
    };

    return (
      <CFade>
        <div style={{ overflow: "hidden" }}>
          <Row gutter={48}>
            <SideVendorPanel
              vendors={vendorList}
              currentVendor={currentVendor}
              handleVendorSelect={handleVendorSelect}
            />
            <MainVendorPanel
              vendor={currentVendor}
              ingredientList={ingredientList}
              handleEditButtonClick={() =>
                toggleVendorDetailsModal(!vendorDetailsModalIsOpen)
              }
              handleDeactivateOrReactivate={handleDeactivateOrReactivateVendor}
            />
          </Row>
        </div>

        <FAB callback={() => toggleAddVendorModal(!addVendorModalIsOpen)} />
        <AddVendorModal
          show={addVendorModalIsOpen}
          onFinish={handleAddVendor}
          toggleModal={toggleAddVendorModal}
          ingredientList={ingredientList}
        ></AddVendorModal>
        <VendorDetailsModal
          show={vendorDetailsModalIsOpen}
          onFinish={handleUpdateVendor}
          toggleModal={toggleVendorDetailsModal}
          vendor={currentVendor}
          ingredientList={ingredientList}
        />
      </CFade>
    );
  }
);

const mapState = (state: RootState) => {
  return {
    vendorList: selectVendorList(state),
    ingredientList: selectIngredientList(state),
  };
};
const mapDispatch = {
  getVendorAsync,
  updateVendorAsync,
  addVendorAsync,
  reactivateVendorAsync,
  deactivateVendorAsync,
  getIngredientAsync,
};

export default connect(mapState, mapDispatch)(VendorManagementContainer);
