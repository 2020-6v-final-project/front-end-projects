import { 
    getVendorList, 
    updateVendor, 
    addVendor, 
    reactivateVendor, 
    deactivateVendor,
} from '@apis';
import { Vendor } from '@custom-types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { orderBy } from 'lodash';
import { AppThunk } from 'src/store';
import swal from 'sweetalert';

type VendorManagementInitialState = {
    vendorList: Array<Vendor>,
}

const initialState: VendorManagementInitialState = {
    vendorList: [],
}

const VendorManagementSlice = createSlice({
    name: 'vendorManagement',
    initialState,
    reducers: {
        getVendorsSuccess: (state, action: PayloadAction<Vendor[]>) => {
            state.vendorList = action.payload;
            state.vendorList = orderBy(state.vendorList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        getVendorsFailed: (state) => {
            return state;
        },
        addVendorSuccess: (state, action: PayloadAction<Vendor[]>) => {
            state.vendorList = action.payload;
            state.vendorList = orderBy(state.vendorList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        addVendorFailed: (state) => {
            return state;
        },
        updateVendorSuccess: (state, action: PayloadAction<Vendor[]>) => {
            state.vendorList = action.payload;
            state.vendorList = orderBy(state.vendorList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        updateVendorFailed: (state) => {
            return state;
        },
        removeVendorSuccess: (state, action: PayloadAction<number>) => {
            state.vendorList = state.vendorList.filter((vendor: Vendor) => vendor.id !== action.payload);
            return state;
        },
        removeVendorFailed: (state) => {
            return state;
        },
        deactivateVendorSuccess: (state, action: PayloadAction<Vendor>) => {
            const index = state.vendorList?.findIndex(vendor => vendor.id === action.payload.id);
            if (index > -1) {
                state.vendorList?.splice(index, 1, action.payload);
            }
            return state;
        },
        deactivateVendorFailed: (state) => {
            return state;
        },
        reactivateVendorSuccess: (state, action: PayloadAction<Vendor>) => {
            const index = state.vendorList?.findIndex(vendor => vendor.id === action.payload.id);
            if (index > -1) {
                state.vendorList?.splice(index, 1, action.payload);
            }
            return state;
        },
        reactivateVendorFailed: (state) => {
            return state;
        },
    }

})

export const getVendorAsync = (): AppThunk => async (dispatch) => {
    try {
        await getVendorList((data: Vendor[]) => {
            dispatch(getVendorsSuccess(data));
        });
    } catch (e) {
        swal("Lấy danh sách nhà cung cấp thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(getVendorsFailed());
    }
}

export const updateVendorAsync = (vendor: any): AppThunk => async (dispatch) => {
    try {
        await updateVendor(vendor, (data: Vendor[]) => {
            swal("Thành công", "Nhà cung cấp đã được cập nhật!", "success");
            dispatch(updateVendorSuccess(data));
        });
    } catch (e) {
        swal("Cập nhập nhà cung cấp thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(updateVendorFailed());
    }
}

export const addVendorAsync = (vendor: any): AppThunk => async (dispatch) => {
    try {
        await addVendor(vendor, (data: Vendor[]) => {
            swal("Thành công", "Nhà cung cấp đã được thêm!", "success");
            dispatch(addVendorSuccess(data));
        });
    } catch (e) {
        swal("Thêm nhà cung cấp thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(addVendorFailed());
    }
}

export const deactivateVendorAsync = (vendorId: number): AppThunk => async (dispatch) => {
    try {
        await deactivateVendor(vendorId, (data: Vendor) => {
            swal("Thành công", "Nhà cung cấp đã bị khóa", "success");
            dispatch(deactivateVendorSuccess(data));
        })
    } catch (e) {
        swal("Khóa nhà cung cấp thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(deactivateVendorFailed());
    }
}

export const reactivateVendorAsync = (vendorId: number): AppThunk => async (dispatch) => {
    try {
        await reactivateVendor(vendorId, (data: Vendor) => {
            swal("Thành công", "Nhà cung cấp đã được mở khóa", "success");
            dispatch(reactivateVendorSuccess(data));
        })
    } catch (e) {
        swal("Mở khóa nhà cung cấp thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(reactivateVendorFailed());
    }
}

const {
    getVendorsSuccess,
    getVendorsFailed,
    addVendorSuccess,
    addVendorFailed,
    updateVendorSuccess,
    updateVendorFailed,
    // removeVendorSuccess,
    // removeVendorFailed,
    deactivateVendorSuccess,
    deactivateVendorFailed,
    reactivateVendorSuccess,
    reactivateVendorFailed,
} = VendorManagementSlice.actions;

export default VendorManagementSlice.reducer;
