import { SubHeaderText } from '@components';
import { CFade } from '@coreui/react';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { RootState } from 'src/store';
import { Scrollbars } from 'react-custom-scrollbars'
import { Request,Branch,RequestType } from '@custom-types';
import CompanyRequestsTable from './components/CompanyRequestsTable/CompanyRequestsTable';
import { selectCompanyRequestsList } from './CompanyRequestManagement.selector';
import { branchListSelector } from '../BranchManagement/BranchManagement.selector';
import { fetchBranchListAsync } from '../BranchManagement/BranchManagement.slice';
import { selectCompanyRequestTypesList } from "./CompanyRequestManagement.selector";
import { getCompanyRequestTypesAsync } from "./CompanyRequestManagement.slice";
import { approveRequestAsync, rejectRequestAsync, addCompanyRequestAsync, getCompanyRequestsListAsync, updateCompanyRequestAsync,removeCompanyRequestAsync } from './CompanyRequestManagement.slice';
import { Divider } from 'antd';

type CompanyRequestManagementState = {
    companyRequestsList: Array<Request>,
    branchList: Array<Branch>,
    companyRequestTypesList: Array<RequestType>,
}

type CompanyRequestManagementDispatch = {
    getCompanyRequestsListAsync: Function,
    addCompanyRequestAsync: Function,
    updateCompanyRequestAsync: Function,
    removeCompanyRequestAsync: Function,
    fetchBranchListAsync: Function,
    getCompanyRequestTypesAsync: Function,
    approveRequestAsync: Function,
    rejectRequestAsync: Function,
}

type CompanyRequestManagementDefaultProps = CompanyRequestManagementState & CompanyRequestManagementDispatch;

const CompanyRequestManagementContainer: React.FC<CompanyRequestManagementDefaultProps> = React.memo((props) => {
    const { approveRequestAsync, rejectRequestAsync, companyRequestsList, branchList, fetchBranchListAsync,  
        getCompanyRequestsListAsync, getCompanyRequestTypesAsync } = props;
    useEffect(() =>{
        const fetchData = async () => {
            await getCompanyRequestsListAsync();
            await fetchBranchListAsync();
            await getCompanyRequestTypesAsync();
        }
        fetchData();
    }, [getCompanyRequestsListAsync, fetchBranchListAsync, getCompanyRequestTypesAsync]);

    const handleApproveRequest = async (CompanyRequest: Request) => {
        await approveRequestAsync(CompanyRequest);
    }

    const handleRejectRequest = async (CompanyRequest: Request) => {
        await rejectRequestAsync(CompanyRequest);
    }

    return (
        <CFade>
            <Scrollbars
                autoHeightMin={0}
                autoHeightMax={600}
                autoHeight
                >
            <Divider>
                <SubHeaderText>Các yêu cầu chờ duyệt</SubHeaderText>
            </Divider>
            <CompanyRequestsTable 
            branches={branchList}
            isActionable={true}
            data={companyRequestsList.filter(req => req.statuscode === "PENDINGAPPROVED")} 
            handleApproveRequest={handleApproveRequest}
            handleRejectRequest={handleRejectRequest} />

            <Divider>
                <SubHeaderText style={{marginTop:"20px"}}>Tất cả các yêu cầu</SubHeaderText>
            </Divider>
            <CompanyRequestsTable 
            isActionable={false}
            branches={branchList}
            data={companyRequestsList} 
            handleApproveRequest={handleApproveRequest}
            handleRejectRequest={handleRejectRequest} />
            </Scrollbars>
        </CFade>
    )
});

const mapState = (state: RootState) => {
    return {
        companyRequestsList: selectCompanyRequestsList(state),
        branchList: branchListSelector(state),
        companyRequestTypesList: selectCompanyRequestTypesList(state),
    }
};

const mapDispatch = {rejectRequestAsync, approveRequestAsync, getCompanyRequestTypesAsync, fetchBranchListAsync, removeCompanyRequestAsync, updateCompanyRequestAsync, addCompanyRequestAsync, getCompanyRequestsListAsync };

export default connect(mapState,mapDispatch)(CompanyRequestManagementContainer);