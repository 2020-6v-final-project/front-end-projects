import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectCompanyRequestsList = createSelector(
    [(state: RootState) => state.companyRequest.companyRequestsList],
    (companyRequestsList) => companyRequestsList
);

export const selectCompanyRequestTypesList = createSelector(
    [(state: RootState) => state.companyRequest.companyRequestTypesList],
    (companyRequestTypesList) => companyRequestTypesList
);