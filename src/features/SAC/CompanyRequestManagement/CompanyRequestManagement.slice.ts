import { rejectRequest, approveRequest, removeRequest, getRequestsList, updateRequest, addRequest, getRequestTypesList} from '@apis';
import { Request, RequestType } from '@custom-types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { orderBy } from 'lodash';
import { AppThunk } from 'src/store';
import swal from 'sweetalert';

type CompanyRequestManagementInitialState = {
    companyRequestsList: Array<Request>,
    companyRequestTypesList: Array<RequestType>,

}

const initialState: CompanyRequestManagementInitialState = {
    companyRequestsList: [],
    companyRequestTypesList: [],
}

const CompanyRequestManagementSlice = createSlice({
    name: 'CompanyRequestManagement',
    initialState,
    reducers: {
        getCompanyRequestTypesSuccess: (state, action: PayloadAction<RequestType[]>) => {
            state.companyRequestTypesList = action.payload;
            state.companyRequestTypesList = orderBy(state.companyRequestTypesList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        getCompanyRequestTypesFailed: (state) => {
            return state;
        },
        getCompanyRequestsListSuccess: (state, action: PayloadAction<Request[]>) => {
            state.companyRequestsList = action.payload;
            state.companyRequestsList = orderBy(state.companyRequestsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        getCompanyRequestsListFailed: (state) => {
            return state;
        },
        addCompanyRequestSuccess: (state, action: PayloadAction<Request[]>) => {
            state.companyRequestsList = action.payload;
            state.companyRequestsList = orderBy(state.companyRequestsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        addCompanyRequestFailed: (state) => {
            return state;
        },
        removeCompanyRequestSuccess: (state, action: PayloadAction<Request[]>) => {
            state.companyRequestsList = action.payload;
            state.companyRequestsList = orderBy(state.companyRequestsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        removeCompanyRequestFailed: (state) => {
            return state;
        },
        updateCompanyRequestSuccess: (state, action: PayloadAction<Request>) => {
            const index = state.companyRequestsList?.findIndex(CompanyRequest => CompanyRequest.id === action.payload.id);
            if (index > -1) {
                state.companyRequestsList?.splice(index, 1, action.payload);
            }
        },
        updateCompanyRequestFailed: (state) => {
            return state;
        },
        deactivateCompanyRequestSuccess: (state, action: PayloadAction<Request[]>) => {
            state.companyRequestsList = action.payload;
            state.companyRequestsList = orderBy(state.companyRequestsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        deactivateCompanyRequestFailed: (state) => {
            return state;
        },
        reactivateCompanyRequestSuccess: (state, action: PayloadAction<Request[]>) => {
            state.companyRequestsList = action.payload;
            state.companyRequestsList = orderBy(state.companyRequestsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        reactivateCompanyRequestFailed: (state) => {
            return state;
        },
        approvedRequestSuccess: (state, action: PayloadAction<Request[]>) => {
            state.companyRequestsList = action.payload;
            state.companyRequestsList = orderBy(state.companyRequestsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        approvedRequestFailed: (state) => {
            return state;
        },
        rejectedRequestSuccess: (state, action: PayloadAction<Request[]>) => {
            state.companyRequestsList = action.payload;
            state.companyRequestsList = orderBy(state.companyRequestsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        rejectedRequestFailed: (state) => {
            return state;
        },
    }

})

export const getCompanyRequestsListAsync = (): AppThunk => async (dispatch) => {
    try {
        await getRequestsList((data: Request[]) => {
            dispatch(getCompanyRequestsListSuccess(data));
        });
    } catch (e) {
        dispatch(getCompanyRequestsListFailed());
    }
}

export const updateCompanyRequestAsync = (CompanyRequest: any): AppThunk => async (dispatch) => {
    try {
        await updateRequest(CompanyRequest, (data: Request) => {
            swal("Thành công", "Yêu cầu đã được cập nhật!", "success");
            dispatch(updateCompanyRequestSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(updateCompanyRequestFailed());
    }
}

export const addCompanyRequestAsync = (CompanyRequest: any): AppThunk => async (dispatch) => {
    try {
        await addRequest(CompanyRequest, (data: Request[]) => {
            swal("Thành công", "Yêu cầu đã được gửi!", "success");
            dispatch(addCompanyRequestSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(addCompanyRequestFailed());
    }
}

export const removeCompanyRequestAsync = (CompanyRequest: any): AppThunk => async (dispatch) => {
    try {
        await removeRequest(CompanyRequest, (data: Request[]) => {
            swal("Thành công", "Yêu cầu đã được hủy", "success");
            dispatch(removeCompanyRequestSuccess(data));
        })
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(removeCompanyRequestFailed());
    }
}

export const getCompanyRequestTypesAsync = (): AppThunk => async (dispatch) => {
    try {
        await getRequestTypesList((data: RequestType[]) => {
            dispatch(getCompanyRequestTypesSuccess(data));
        });
    } catch (e) {
        dispatch(getCompanyRequestTypesFailed());
    }
}

export const approveRequestAsync = (req:any): AppThunk => async (dispatch) => {
    try {
        await approveRequest(req,(data: Request[]) => {
            swal("Chấp nhận yêu cầu", "Yêu cầu đã được duyệt", "success");
            dispatch(approvedRequestSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(approvedRequestFailed());
    }
}

export const rejectRequestAsync = (req:any): AppThunk => async (dispatch) => {
    try {
        await rejectRequest(req,(data: Request[]) => {
            swal("Từ chối yêu cầu", "Yêu cầu đã được từ chối", "success");
            dispatch(rejectedRequestSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(rejectedRequestFailed());
    }
}


const {
    getCompanyRequestsListSuccess,
    getCompanyRequestsListFailed,
    addCompanyRequestSuccess,
    addCompanyRequestFailed,
    removeCompanyRequestSuccess,
    removeCompanyRequestFailed,
    updateCompanyRequestSuccess,
    updateCompanyRequestFailed,
    getCompanyRequestTypesSuccess,
    getCompanyRequestTypesFailed,
    approvedRequestSuccess,
    approvedRequestFailed,
    rejectedRequestSuccess,
    rejectedRequestFailed,
} = CompanyRequestManagementSlice.actions;

export default CompanyRequestManagementSlice.reducer;
