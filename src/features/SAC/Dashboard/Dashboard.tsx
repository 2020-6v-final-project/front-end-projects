import { CFade } from "@coreui/react";
import React from "react";
import { Scrollbars } from "react-custom-scrollbars";

const Dashboard: React.FC = (props: any) => {
  return (
    <CFade>
      <Scrollbars autoHide></Scrollbars>
    </CFade>
  );
};

export default Dashboard;
