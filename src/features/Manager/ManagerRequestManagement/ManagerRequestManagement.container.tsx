import { SubHeaderText } from '@components';
import { CFade } from '@coreui/react';
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { RootState } from 'src/store';
import {Scrollbars} from 'react-custom-scrollbars'
import { Request,Branch } from '@custom-types';
import {Col, Divider, Row, Select } from "antd";
import ManagerRequestsTable from './components/ManagerRequestsTable/ManagerRequestsTable';
import { selectManagerRequestsList } from './ManagerRequestManagement.selector';
import { branchListSelector } from '../../SAC/BranchManagement/BranchManagement.selector';
import { approveRequestAsync, rejectRequestAsync, addManagerRequestAsync, getManagerRequestsListAsync } from './ManagerRequestManagement.slice';
import { selectBranchOfBranchManagementUserList } from "../../LoginPage/LoginPage.selector";

type ManagerRequestManagementState = {
    managerRequestsList: Array<Request>,
    branchListOfUser: Array<Branch>;

}

type ManagerRequestManagementDispatch = {
    getManagerRequestsListAsync: Function,
    addManagerRequestAsync: Function,
    approveRequestAsync: Function,
    rejectRequestAsync: Function,
}

type ManagerRequestManagementDefaultProps = ManagerRequestManagementState & ManagerRequestManagementDispatch;

const ManagerRequestManagementContainer: React.FC<ManagerRequestManagementDefaultProps> = React.memo((props) => {
    const { branchListOfUser, approveRequestAsync, rejectRequestAsync, managerRequestsList,  
         getManagerRequestsListAsync } = props;
    const [currentBranch, setCurrentBranch] = useState<Branch | undefined>(branchListOfUser[0] || undefined);

    useEffect(() =>{
        const fetchData = async () => {
            if(currentBranch?.id)
            {
                await getManagerRequestsListAsync(currentBranch?.id);
            }
        }
        fetchData();
    }, [currentBranch, getManagerRequestsListAsync]);

    const handleApproveRequest = async (ManagerRequest: Request,event:any) => {
        event.stopPropagation();
        event.preventDefault();
        await approveRequestAsync(ManagerRequest);
    }

    const handleRejectRequest = async (ManagerRequest: Request,event:any) => {
        event.stopPropagation();
        event.preventDefault();
        await rejectRequestAsync(ManagerRequest);
    }

    const handleChangeBranch = (value: number) => {
        setCurrentBranch(branchListOfUser?.find(branch => branch.id === value));
    }

    // const handleAddManagerRequest = async (ManagerRequest: Request) => {
    //     await addManagerRequestAsync(ManagerRequest);
    // }
    // const handleDeactivateOrReactivateManagerRequest = async (
    //     managerRequest: Request,
    //     statusCode: string
    //   ) => {
    //     if (statusCode === "AVAILABLE") await deactivateManagerRequestAsync(managerRequest);
    //     else await reactivateManagerRequestAsync(managerRequest);
    //   };
    return (
        <CFade>
            <Row>
            <Col style={{marginLeft:"25px", marginTop:"10px", marginBottom:"10px"}}>
                <Row>
                    <SubHeaderText style={{marginRight:'10px'}}>Chi nhánh: </SubHeaderText>
                    <Select 
                        defaultValue={branchListOfUser[0]?.id} 
                        style={{ width: 300 }} 
                        placeholder="Chọn chi nhánh"
                        onChange={handleChangeBranch}
                    >
                        {branchListOfUser?.map((branch, index) => (
                        <Select.Option key={index} value={branch.id}>{branch.code} | {branch.name}</Select.Option>
                        ))}
                    </Select>
                </Row>
            </Col>
            </Row>
            <Scrollbars
                autoHeightMin={0}
                autoHeightMax={600}
                autoHeight
                >
            <Divider>
                <SubHeaderText>Các yêu cầu chờ duyệt</SubHeaderText>
            </Divider>
            <ManagerRequestsTable 
            isActionable={true}
            data={managerRequestsList.filter(req => req.statuscode === "PENDINGAPPROVED")} 
            handleApproveRequest={handleApproveRequest}
            handleRejectRequest={handleRejectRequest} />
            <Divider>
                <SubHeaderText style={{marginTop:"20px"}}>Tất cả các yêu cầu</SubHeaderText>
            </Divider>
            <ManagerRequestsTable 
            isActionable={false}
            data={managerRequestsList} 
            handleApproveRequest={handleApproveRequest}
            handleRejectRequest={handleRejectRequest} />
            </Scrollbars>
        </CFade>
    )
});

const mapState = (state: RootState) => {
    return {
        managerRequestsList: selectManagerRequestsList(state),
        branchList: branchListSelector(state),
        branchListOfUser: selectBranchOfBranchManagementUserList(state),
    }
};

const mapDispatch = {rejectRequestAsync, approveRequestAsync, addManagerRequestAsync, getManagerRequestsListAsync };

export default connect(mapState, mapDispatch)(ManagerRequestManagementContainer);