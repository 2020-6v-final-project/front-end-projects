import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectManagerRequestsList = createSelector(
    [(state: RootState) => state.managerRequest.managerRequestsList],
    (managerRequestsList) => managerRequestsList
);

export const selectManagerRequestTypesList = createSelector(
    [(state: RootState) => state.managerRequest.managerRequestTypesList],
    (managerRequestTypesList) => managerRequestTypesList
);