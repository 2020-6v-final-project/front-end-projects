import { rejectBranchRequest, approveBranchRequest, getBranchRequestsList, addBranchRequest, getRequestTypesList} from '@apis';
import { Request,RequestType } from '@custom-types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { orderBy } from 'lodash';
import { AppThunk } from 'src/store';
import swal from 'sweetalert';

type ManagerRequestManagementInitialState = {
    managerRequestsList: Array<Request>,
    managerRequestTypesList: Array<RequestType>,

}

const initialState: ManagerRequestManagementInitialState = {
    managerRequestsList: [],
    managerRequestTypesList: [],
}

const ManagerRequestManagementSlice = createSlice({
    name: 'ManagerRequestManagement',
    initialState,
    reducers: {
        getManagerRequestTypesSuccess: (state, action: PayloadAction<RequestType[]>) => {
            state.managerRequestTypesList = action.payload;
            state.managerRequestTypesList = orderBy(state.managerRequestTypesList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        getManagerRequestTypesFailed: (state) => {
            return state;
        },
        getManagerRequestsListSuccess: (state, action: PayloadAction<Request[]>) => {
            state.managerRequestsList = action.payload;
            state.managerRequestsList = orderBy(state.managerRequestsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        getManagerRequestsListFailed: (state) => {
            return state;
        },
        addManagerRequestSuccess: (state, action: PayloadAction<Request[]>) => {
            state.managerRequestsList = action.payload;
            state.managerRequestsList = orderBy(state.managerRequestsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        addManagerRequestFailed: (state) => {
            return state;
        },
        approvedRequestSuccess: (state, action: PayloadAction<Request[]>) => {
            state.managerRequestsList = action.payload;
            state.managerRequestsList = orderBy(state.managerRequestsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        approvedRequestFailed: (state) => {
            return state;
        },
        rejectedRequestSuccess: (state, action: PayloadAction<Request[]>) => {
            state.managerRequestsList = action.payload;
            state.managerRequestsList = orderBy(state.managerRequestsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        rejectedRequestFailed: (state) => {
            return state;
        },
    }

})

export const getManagerRequestsListAsync = (branchid:number): AppThunk => async (dispatch) => {
    try {
        await getBranchRequestsList(branchid,(data: Request[]) => {
            dispatch(getManagerRequestsListSuccess(data));
        });
    } catch (e) {
        dispatch(getManagerRequestsListFailed());
    }
}


export const addManagerRequestAsync = (ManagerRequest: any): AppThunk => async (dispatch) => {
    try {
        await addBranchRequest(ManagerRequest, (data: Request[]) => {
            swal("Thành công", "Yêu cầu đã được gửi!", "success");
            dispatch(addManagerRequestSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(addManagerRequestFailed());
    }
}

export const getManagerRequestTypesAsync = (): AppThunk => async (dispatch) => {
    try {
        await getRequestTypesList((data: RequestType[]) => {
            dispatch(getManagerRequestTypesSuccess(data));
        });
    } catch (e) {
        dispatch(getManagerRequestTypesFailed());
    }
}

export const approveRequestAsync = (req:any): AppThunk => async (dispatch) => {
    try {
        await approveBranchRequest(req,(data: Request[]) => {
            swal("Chấp nhận yêu cầu", "Yêu cầu đã được duyệt", "success");
            dispatch(approvedRequestSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(approvedRequestFailed());
    }
}

export const rejectRequestAsync = (req:any): AppThunk => async (dispatch) => {
    try {
        await rejectBranchRequest(req,(data: Request[]) => {
            swal("Từ chối yêu cầu", "Yêu cầu đã được từ chối", "success");
            dispatch(rejectedRequestSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(rejectedRequestFailed());
    }
}

const {
    getManagerRequestsListSuccess,
    getManagerRequestsListFailed,
    addManagerRequestSuccess,
    addManagerRequestFailed,
    getManagerRequestTypesSuccess,
    getManagerRequestTypesFailed,
    approvedRequestSuccess,
    approvedRequestFailed,
    rejectedRequestSuccess,
    rejectedRequestFailed,
} = ManagerRequestManagementSlice.actions;

export default ManagerRequestManagementSlice.reducer;
