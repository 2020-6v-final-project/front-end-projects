import { Button, 
    Row, Space, Table, Tag ,Typography,
    } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import {
  shortDateFormat,
} from "@shared/utils/";
import React, { useState, useEffect } from 'react';
import './ManagerRequestsTable.scss';
import { history } from "@shared/components";

type ManagerRequestsManagerRequestProps = {
    data: Array<any>,
    handleApproveRequest: Function,
    handleRejectRequest: Function,
    isActionable:boolean,
}

const ManagerRequestsTable: React.FC<ManagerRequestsManagerRequestProps> = React.memo((props) => {
    const {data, handleApproveRequest, handleRejectRequest,isActionable
    } = props;
    const [filteredData, setFilteredData] = useState(data);
    useEffect(()=>{
        setFilteredData(data);
    },[data]);


    const actionableColumns: ColumnsType = [
        {
            align: "center",
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
            defaultSortOrder: 'ascend',
            sorter: (a: any, b: any) => a?.id - b?.id,
        },
        {
            align: "center",
            title: 'Loại yêu cầu',
            dataIndex: 'requesttypeid',
            key: 'requesttypeid',
            defaultSortOrder: 'ascend',
            sorter: (a: any, b: any) => a?.requesttypeid - b?.requesttypeid,
            render: (text: string) => 
            {
              if(Number(text) === 3)
              return  <Typography>Yêu cầu nhập kho</Typography>
              if(Number(text) === 2)
              return  <Typography>Yêu cầu xuất kho</Typography>
              if(Number(text) === 1)
              return  <Typography>Yêu cầu đổi Menu</Typography>
              return  <Typography>Yêu cầu duyệt chi</Typography>
            }
        },
        {
            align: 'center',
            title: 'Người tạo',
            dataIndex: 'createdby',
            key: 'createdby',
            // defaultSortOrder: 'ascend',
            // sorter: (a: any, b: any) => a.count - b.count,
        },
        {
            align: 'center',
            title: 'Ngày tạo',
            dataIndex: 'createdat',
            key: 'createdat',
            render: (text: Date) => <Typography>{shortDateFormat(text)}</Typography>,
            // defaultSortOrder: 'ascend',
            // sorter: (a: any, b: any) => a.count - b.count,
        },
        {
            align: 'center',
            title: 'Trạng thái',
            key: 'statuscode',
            dataIndex: 'statuscode',
            sorter: (a: any, b: any) => a?.statuscode.localeCompare(b?.statuscode),
            render: (text: string, record: any) => 
            {
              if(text === "PENDINGAPPROVED")
              return  <Tag color="geekblue">Đang chờ duyệt</Tag>
              if(text === "REJECTED")
              return  <Tag color="red">Bị từ chối</Tag>
              return  <Tag color="success">Đã duyệt</Tag>
            }
        },
        {
            align: 'center',
            title: 'Thao tác',
            key: 'action',
            render: (text: string, record: any) => (
                <>
                {record?.requesttypeid !== 1 ?
                <>
                <Row style={{justifyContent: "center"}}>
                <Space size="middle">
                    <Button onClick={(event) => handleApproveRequest(record,event)}
                     type="primary" size="small" disabled={record?.statuscode !== "PENDINGAPPROVED" ? true : false}>Duyệt</Button>
                    <Button onClick={(event) => handleRejectRequest(record,event)} danger size="small" disabled={record?.statuscode !== "PENDINGAPPROVED" ? true : false}>Từ chối</Button>
                </Space>
                </Row>
                </>
                :
                <></>
                }
                </>
            ),
        },
    ];

    const nonActionableColumns: ColumnsType = [
        {
            align: "center",
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
            defaultSortOrder: 'ascend',
            sorter: (a: any, b: any) => a?.id - b?.id,
        },
        {
            align: "center",
            title: 'Loại yêu cầu',
            dataIndex: 'requesttypeid',
            key: 'requesttypeid',
            defaultSortOrder: 'ascend',
            sorter: (a: any, b: any) => a?.requesttypeid - b?.requesttypeid,
            render: (text: string) => 
            {
              if(Number(text) === 3)
              return  <Typography>Yêu cầu nhập kho</Typography>
              if(Number(text) === 2)
              return  <Typography>Yêu cầu xuất kho</Typography>
              if(Number(text) === 1)
              return  <Typography>Yêu cầu đổi Menu</Typography>
              return  <Typography>Yêu cầu duyệt chi</Typography>
            }
        },
        {
            align: 'center',
            title: 'Người tạo',
            dataIndex: 'createdby',
            key: 'createdby',
            // defaultSortOrder: 'ascend',
            // sorter: (a: any, b: any) => a.count - b.count,
        },
        {
            align: 'center',
            title: 'Ngày tạo',
            dataIndex: 'createdat',
            key: 'createdat',
            render: (text: Date) => <Typography>{shortDateFormat(text)}</Typography>,
            // defaultSortOrder: 'ascend',
            // sorter: (a: any, b: any) => a.count - b.count,
        },
        {
            align: 'center',
            title: 'Ngày duyệt',
            dataIndex: 'approvedat',
            key: 'approvedat',
            render: (text: Date) => <Typography>{shortDateFormat(text)}</Typography>,
            // defaultSortOrder: 'ascend',
            // sorter: (a: any, b: any) => a.count - b.count,
        },
        {
            align: 'center',
            title: 'Người duyệt',
            dataIndex: 'approvedby',
            key: 'approvedby',
            // defaultSortOrder: 'ascend',
            // sorter: (a: any, b: any) => a.count - b.count,
        },
        {
            align: 'center',
            title: 'Trạng thái',
            key: 'statuscode',
            dataIndex: 'statuscode',
            sorter: (a: any, b: any) => a?.statuscode.localeCompare(b?.statuscode),
            render: (text: string, record: any) => 
            {
              if(text === "PENDINGAPPROVED")
              return  <Tag color="geekblue">Đang chờ duyệt</Tag>
              if(text === "REJECTED")
              return  <Tag color="red">Bị từ chối</Tag>
              return  <Tag color="success">Đã duyệt</Tag>
            }
        },
    ];

    return (
        <>
            <Table
                bordered
                size="small"
                className="tables-table"
                columns={isActionable === true ? actionableColumns : nonActionableColumns}
                pagination={{ position: ['bottomCenter'] }}
                dataSource={filteredData}
                onRow={(record:any) =>  { 
                    return {
                    onClick: event => 
                    { 
                        if(record.requesttypeid === 2 || record.requesttypeid === 3)
                            history.push(`/receipt-detail/branch/${record.branchid}/receipt/${record.referenceid}`)
                        if(record.requesttypeid === 1)
                            history.push(`/menu-branch-management/${record.referenceid}`);
                    }, 
                    };
                }}
                scroll={{ y: '55vh' }} />
        </>
    )
})

export default ManagerRequestsTable;