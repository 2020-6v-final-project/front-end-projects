import { FAB, SubHeaderText } from '@components';
import { CFade } from '@coreui/react';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { RootState } from 'src/store';
import { Scrollbars } from "react-custom-scrollbars";
import { RepositoryIngredient,Branch } from '@custom-types';
import {Col, Row, message, Select, Tabs } from "antd";
import AddReceiptModal from './components/AddReceiptModal/AddReceiptModal';
import IngredientsTable from './components/IngredientsTable/IngredientsTable';
import ReceiptsTable from './components/ReceiptsTable/ReceiptsTable';
import { selectRepositoryIngredientsList } from './StorageManagement.selector';
import { addReceiptAsync } from './ReceiptManagement/ReceiptManagement.slice';
import { getRepositoryIngredientsAsync } from './StorageManagement.slice';
import { getIngredientAsync } from "../../SAC/IngredientManagement/IngredientManagement.slice";
import { selectIngredientList } from "../../SAC/IngredientManagement/IngredientManagement.selector";
import { selectRepositoriesList } from "../../SAC/RepositoryManagement/RepositoryManagement.selector";
import { getRepositoryByIdAsync } from "../../SAC/RepositoryManagement/RepositoryManagement.slice";
import { selectReceiptsList } from "./ReceiptManagement/ReceiptManagement.selector";
import { updateReceiptAsync, addExportReceiptIngredientAsync,receiveReceiptIngredientsAsync, getReceiptsAsync, addReceiptIngredientAsync, removeReceiptAsync, removeRepositoryIngredientAsync,addReceiptRequestAsync } from "./ReceiptManagement/ReceiptManagement.slice";
import ReceiptInDetailModal from "./components/ReceiptInDetailModal/ReceiptInDetailModal";
import ReceiptOutDetailModal from "./components/ReceiptOutDetailModal/ReceiptOutDetailModal";
import AddRequestModal from "./components/AddRequestModal/AddRequestModal";
import AddIngreModal from "./components/AddIngreModal/AddIngreModal";
import { selectReceiptTypesList } from "./ReceiptManagement/ReceiptManagement.selector";
import { getReceiptTypesAsync } from "./ReceiptManagement/ReceiptManagement.slice";
import { selectVendorList } from "../../SAC/VendorManagement/VendorManagement.selector";
import {  getVendorAsync } from "../../SAC/VendorManagement/VendorManagement.slice";
import { selectBranchOfUserForStorageList } from "../../LoginPage/LoginPage.selector";
import { history } from "@shared/components";

type StorageManagementState = {
    match: any;
    repositoryIngredientsList: Array<RepositoryIngredient>,
    ingredientList: Array<any>;
    repositoriesList: Array<any>;
    receiptsList: Array<any>;
    receiptTypesList: Array<any>;
    vendorList: Array<any>;
    branchListOfUser: Array<Branch>;
}

type StorageManagementDispatch = {
    getRepositoryIngredientsAsync: Function,
    getIngredientAsync: Function,
    getRepositoryByIdAsync: Function,
    getReceiptsAsync: Function,
    getReceiptTypesAsync: Function,
    addReceiptAsync: Function,
    addReceiptRequestAsync: Function,
    addReceiptIngredientAsync: Function,
    removeRepositoryIngredientAsync: Function,
    removeReceiptAsync: Function,
    getVendorAsync: Function,
    receiveReceiptIngredientsAsync: Function,
    addExportReceiptIngredientAsync: Function,
    updateReceiptAsync: Function,
}

type StorageManagementDefaultProps = StorageManagementState & StorageManagementDispatch;

const StorageManagementContainer: React.FC<StorageManagementDefaultProps> = React.memo((props) => {
    const { updateReceiptAsync, addExportReceiptIngredientAsync, receiveReceiptIngredientsAsync, branchListOfUser, getVendorAsync, vendorList, removeReceiptAsync, removeRepositoryIngredientAsync, addReceiptIngredientAsync, addReceiptAsync, receiptTypesList, receiptsList ,getReceiptsAsync, repositoriesList,getRepositoryByIdAsync,ingredientList, repositoryIngredientsList,  
        getIngredientAsync, getRepositoryIngredientsAsync, getReceiptTypesAsync, addReceiptRequestAsync, match } = props;
    
    const [currentBranch, setCurrentBranch] = useState<Branch | undefined>(branchListOfUser[0] || undefined);
    const [addModalVisible, toggleAddModal] = useState(false);
    const [addRequestModalVisible, toggleAddRequestModal] = useState(false);
    const [addIngreModalVisible, toggleAddIngreModal] = useState(false);

    const [openReceiptInDetailModal, setOpenReceiptInDetailModal] = useState<boolean>(
        false
      );

    const [openReceiptOutDetailModal, setOpenReceiptOutDetailModal] = useState<boolean>(
        false
    );
    const [receiptDetail, setReceiptDetail] = useState<any>(null);

    useEffect(() =>{
        const fetchData = async () => {
            await getIngredientAsync();
            if(currentBranch?.repositoryid) 
            {
                await getRepositoryIngredientsAsync(currentBranch.repositoryid);
                await getRepositoryByIdAsync(currentBranch.repositoryid);
                await getReceiptsAsync(currentBranch.id);
            }
            await getReceiptTypesAsync();
            await getVendorAsync();
        }
        fetchData();
    },[getVendorAsync, getReceiptTypesAsync, getIngredientAsync,getRepositoryIngredientsAsync,getRepositoryByIdAsync,getReceiptsAsync,currentBranch]);
    
    useEffect(() => {
        if (match.params.branchid && match.params.branchid !== currentBranch?.id) {
            setCurrentBranch(branchListOfUser?.find(branch => branch.id === Number(match.params.branchid)));
        }
        else if (receiptsList.length > 0 && match.params.receiptid) {
            const target = receiptsList.find(
                (recepit) => recepit.id === match.params.receiptid
            );
            if (target) {
                setReceiptDetail(target);
                if(target.receipttypeid === 1)
                setOpenReceiptInDetailModal(true);
                else
                setOpenReceiptOutDetailModal(true);
            } else {
                history.push("/storage-management/");
                message.error("Không tìm thấy phiếu nào!");
            }
        }
        if (receiptsList.length > 0 && receiptDetail) {
            setReceiptDetail(receiptsList.find((receipt) => receipt.id === receiptDetail.id));
        }
    }, [receiptsList, receiptDetail,match.params.branchid,branchListOfUser,currentBranch,match.params.receiptid]);

    const handleToggleAddModal = () => {
        toggleAddModal(!addModalVisible);
    }

    const handleToggleAddRequestModal = () => {
        toggleAddRequestModal(!addRequestModalVisible);
    }

    const handleUpdateIngredientQuantity = async (value: any) => {
        // await updateRepositoryIngredientAsync(e);
    }

    const handleDeleteReceiptIngredient = async (e: any) => {
        await removeRepositoryIngredientAsync(e,currentBranch?.id);
    }

    const handleReceiveIngredientFromReceipt = async (e: any) => {
        await receiveReceiptIngredientsAsync(e);
    }

    const handleAddReceipt = async (e: any) => {
        await addReceiptAsync(e);
    }
    
    const handleRemoveReceipt = async (e: any,event:any) => {
        event.stopPropagation();
        event.preventDefault();
        await removeReceiptAsync(e);
    }

    const handleAddReceiptIngredient = async (e:any) =>{
        await addReceiptIngredientAsync(e, currentBranch?.id);
        // await getRepositoryIngredientsAsync(currentBranch?.repositoryid);
    }

    const handleAddReceiptOutIngredient = async (e:any, receiptid:number) =>{
        await addExportReceiptIngredientAsync(e, currentBranch?.id, receiptid);
    }

    const handleDeactivateOrReactivateIngredient = async (
        repoIngredient: RepositoryIngredient,
        statusCode: string
      ) => {
        // if (statusCode === "AVAILABLE") await deactivateRepositoryIngredientAsync(repoIngredient);
        // else await reactivateRepositoryIngredientAsync(repoIngredient);
      };

    const showDetailReceiptIn = (record:any) =>{
        if(record === undefined) history.push("/storage-management/");
        setReceiptDetail(record);
        setOpenReceiptInDetailModal(!openReceiptInDetailModal);
    };

    const showDetailReceiptOut = (record:any) =>{
        if(record === undefined) history.push("/storage-management/");
        setReceiptDetail(record);
        setOpenReceiptOutDetailModal(!openReceiptOutDetailModal);
    };

    
    const showAddRequestModal = (record:any,event:any) =>{
        event?.stopPropagation();
        event?.preventDefault();
        setReceiptDetail(record);
        toggleAddRequestModal(!addRequestModalVisible);
    };
    const showAddIngreModal = (record:any,event:any) =>{
        event?.stopPropagation();
        event?.preventDefault();
        setReceiptDetail(record);
        toggleAddIngreModal(!addIngreModalVisible);
    };

    const handleAddRequest = async (request: Request) => {
        await addReceiptRequestAsync(request);
    }

    const handleUpdateReceipt = async (receipt: any,receiptid:number) => {
        await updateReceiptAsync(receipt, currentBranch?.id, receiptid);
    }

    const handleChangeBranch = (value: number) => {
        setCurrentBranch(branchListOfUser?.find(branch => branch.id === value));
      }
    return (
        <CFade>
            <Row gutter={24}>
            <Col style={{marginLeft:"25px", marginTop:"10px", marginBottom:"10px"}}>
                <Row>
                    <SubHeaderText style={{marginRight:'10px'}}>Chi nhánh: </SubHeaderText>
                    <Select 
                        defaultValue={branchListOfUser[0]?.id} 
                        style={{ width: 300 }} 
                        placeholder="Chọn chi nhánh"
                        onChange={handleChangeBranch}
                        value={currentBranch?.id}
                    >
                        {branchListOfUser?.map((branch, index) => (
                        <Select.Option key={index} value={branch.id}>{branch.code} | {branch.name}</Select.Option>
                        ))}
                    </Select>
                </Row>
            </Col>
            </Row>
            <Scrollbars
                autoHeightMin={0}
                autoHeightMax={400}
                autoHeight
                autoHide
                >
                <Tabs size="large"centered defaultActiveKey="1" style={{padding: "0px" }}>
                    <Tabs.TabPane tab="Danh sách nguyên liệu" key="1">
                    <IngredientsTable 
                        ingredients={ingredientList} 
                        repositories={repositoriesList} 
                        branches={branchListOfUser}
                        data={repositoryIngredientsList}
                        handleDeactivateOrReactivateIngredient={handleDeactivateOrReactivateIngredient} 
                        handleUpdateIngredientQuantity={handleUpdateIngredientQuantity}  />
                    </Tabs.TabPane>
                    
                    <Tabs.TabPane tab="Danh sách phiếu nhập/xuất" key="2">
                    <Tabs centered defaultActiveKey="1" style={{ padding: "0px" }}>
                        <Tabs.TabPane tab="Phiếu nhập" key="1">
                            <ReceiptsTable 
                                showAddIngreModal={showAddIngreModal}
                                ingredients={ingredientList} 
                                repositories={repositoriesList} 
                                branches={branchListOfUser} 
                                data={receiptsList.filter(receipt => receipt.receipttypeid === 1)}          
                                handleRemoveReceipt={handleRemoveReceipt}
                                handleDeactivateOrReactivateIngredient={handleDeactivateOrReactivateIngredient} 
                                handleUpdateIngredientQuantity={handleUpdateIngredientQuantity} 
                                // handleDeleteIngredient={handleDeleteIngredient} 
                                showDetailReceipt={showDetailReceiptIn} 
                                showAddRequestModal={showAddRequestModal} 
                            />
                        </Tabs.TabPane>
                        <Tabs.TabPane tab="Phiếu xuất" key="2">
                            <ReceiptsTable 
                                showAddIngreModal={showAddIngreModal}
                                ingredients={ingredientList} 
                                repositories={repositoriesList} 
                                branches={branchListOfUser} 
                                data={receiptsList.filter(receipt=>receipt.receipttypeid === 2)} 
                                handleRemoveReceipt={handleRemoveReceipt}
                                handleDeactivateOrReactivateIngredient={handleDeactivateOrReactivateIngredient} 
                                handleUpdateIngredientQuantity={handleUpdateIngredientQuantity} 
                                // handleDeleteIngredient={handleDeleteIngredient} 
                                showDetailReceipt={showDetailReceiptOut} 
                                showAddRequestModal={showAddRequestModal} 
                            />
                        </Tabs.TabPane>
                    </Tabs>
                    </Tabs.TabPane>
                    </Tabs>
                    <FAB callback={handleToggleAddModal} />
                    <ReceiptInDetailModal
                        vendorList={vendorList}
                        handleDeleteReceiptIngredient={handleDeleteReceiptIngredient}
                        receiptTypes={receiptTypesList}
                        ingredients={ingredientList} 
                        repositories={repositoriesList} 
                        branches={branchListOfUser}
                        isShow={openReceiptInDetailModal}
                        onClose={showDetailReceiptIn}
                        data={receiptDetail}
                        handleAddReceiptIngredient={handleAddReceiptIngredient}
                        // updateUserAsync={updateUserAsync}
                    />
                    <ReceiptOutDetailModal
                        repositoryIngredientsList={repositoryIngredientsList}
                        vendorList={vendorList}
                        handleDeleteReceiptIngredient={handleDeleteReceiptIngredient}
                        receiptTypes={receiptTypesList}
                        ingredients={ingredientList} 
                        repositories={repositoriesList} 
                        branches={branchListOfUser}
                        isShow={openReceiptOutDetailModal}
                        onClose={showDetailReceiptOut}
                        data={receiptDetail}
                        handleAddReceiptOutIngredient={handleAddReceiptOutIngredient}
                        handleUpdateReceipt={handleUpdateReceipt}
                    />
                    <AddReceiptModal 
                        currentBranch={currentBranch?.id}
                        receiptTypes={receiptTypesList}
                        repositories={repositoriesList} 
                        show={addModalVisible} 
                        toggleModal={handleToggleAddModal} 
                        onFinish={handleAddReceipt} 
                    />

                    <AddRequestModal
                        show={addRequestModalVisible}
                        toggleModal={handleToggleAddRequestModal}
                        onFinish={handleAddRequest}
                        data={receiptDetail}
                    />
                    <AddIngreModal
                        handleReceiveIngredientFromReceipt={handleReceiveIngredientFromReceipt}
                        handleDeleteReceiptIngredient={handleDeleteReceiptIngredient}
                        receiptTypes={receiptTypesList}
                        ingredients={ingredientList} 
                        repositories={repositoriesList} 
                        branches={branchListOfUser}
                        isShow={addIngreModalVisible}
                        onClose={showAddIngreModal}
                        data={receiptDetail}
                        handleAddReceiptIngredient={handleAddReceiptIngredient}
                        // updateUserAsync={updateUserAsync}
                    />
            </Scrollbars>
        </CFade>
    )
});

const mapState = (state: RootState) => {
    return {
        repositoryIngredientsList: selectRepositoryIngredientsList(state),
        ingredientList: selectIngredientList(state),
        repositoriesList: selectRepositoriesList(state),
        receiptsList: selectReceiptsList(state),
        receiptTypesList: selectReceiptTypesList(state),
        vendorList: selectVendorList(state),
        branchListOfUser: selectBranchOfUserForStorageList(state),
    }
};

const mapDispatch = { 
    getIngredientAsync, 
    getRepositoryIngredientsAsync,
    getRepositoryByIdAsync,
    getReceiptsAsync,
    getReceiptTypesAsync,
    addReceiptAsync,
    addReceiptRequestAsync,
    addReceiptIngredientAsync,
    removeRepositoryIngredientAsync,
    removeReceiptAsync,
    getVendorAsync,
    receiveReceiptIngredientsAsync,
    addExportReceiptIngredientAsync,
    updateReceiptAsync,
};

export default connect(mapState, mapDispatch)(StorageManagementContainer);