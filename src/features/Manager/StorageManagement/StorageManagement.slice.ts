import {  reactivateRepoIngredient, deactivateRepoIngredient, updateRepoIngredients, getRepoIngredientsList } from '@apis';
import { RepositoryIngredient } from '@custom-types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { orderBy } from 'lodash';
import { AppThunk } from 'src/store';
import swal from 'sweetalert';

type RepositoryIngredientsManagementInitialState = {
    repositoryIngredientsList: Array<RepositoryIngredient>,
}

const initialState:RepositoryIngredientsManagementInitialState = {
    repositoryIngredientsList: [],
}

const RepositoryIngredientsManagementSlice = createSlice({
    name: 'RepositoryIngredientsManagement',
    initialState,
    reducers: {
        getRepositoryIngredientsSuccess: (state, action: PayloadAction<RepositoryIngredient[]>) => {
            state.repositoryIngredientsList = action.payload;
            state.repositoryIngredientsList = orderBy(state.repositoryIngredientsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        getRepositoryIngredientsFailed: (state) => {
            return state;
        },
        updateRepositoryIngredientSuccess: (state, action: PayloadAction<RepositoryIngredient>) => {
            const index = state.repositoryIngredientsList?.findIndex(repoIngredient => repoIngredient.repositoryid === action.payload.repositoryid);
            if (index > -1) {
                state.repositoryIngredientsList?.splice(index, 1, action.payload);
            }
        },
        updateRepositoryIngredientFailed: (state) => {
            return state;
        },
        deactivateRepositoryIngredientSuccess: (state, action: PayloadAction<RepositoryIngredient[]>) => {
            state.repositoryIngredientsList = action.payload;
            state.repositoryIngredientsList = orderBy(state.repositoryIngredientsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        deactivateRepositoryIngredientFailed: (state) => {
            return state;
        },
        reactivateRepositoryIngredientSuccess: (state, action: PayloadAction<RepositoryIngredient[]>) => {
            state.repositoryIngredientsList = action.payload;
            state.repositoryIngredientsList = orderBy(state.repositoryIngredientsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        reactivateRepositoryIngredientFailed: (state) => {
            return state;
        },
    }

})

export const getRepositoryIngredientsAsync = (repoId:number): AppThunk => async (dispatch) => {
    try {
        await getRepoIngredientsList(repoId,(data: RepositoryIngredient[]) => {
            dispatch(getRepositoryIngredientsSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Không thể tải danh sách nguyên liệu của kho!", "error");
        dispatch(getRepositoryIngredientsFailed());
    }
}

export const updateRepositoryIngredientAsync = (table: any): AppThunk => async (dispatch) => {
    try {
        await updateRepoIngredients(table, (data: RepositoryIngredient) => {
            swal("Thành công", "Nguyên liệu đã được cập nhật!", "success");
            dispatch(updateRepositoryIngredientSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(updateRepositoryIngredientFailed());
    }
}


export const deactivateRepositoryIngredientAsync = (RepositoryIngredient: any): AppThunk => async (dispatch) => {
    try {
        await deactivateRepoIngredient(RepositoryIngredient, (data: RepositoryIngredient[]) => {
            swal("Thành công", "Nguyên liệu đã bị khóa", "success");
            dispatch(deactivateRepositoryIngredientSuccess(data));
        })
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(deactivateRepositoryIngredientFailed());
    }
}

export const reactivateRepositoryIngredientAsync = (RepositoryIngredient: any): AppThunk => async (dispatch) => {
    try {
        await reactivateRepoIngredient(RepositoryIngredient, (data: RepositoryIngredient[]) => {
            swal("Thành công", "Nguyên liệu đã được mở khóa", "success");
            dispatch(reactivateRepositoryIngredientSuccess(data));
        })
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(reactivateRepositoryIngredientFailed());
    }
}




const {
    getRepositoryIngredientsSuccess,
    getRepositoryIngredientsFailed,
    updateRepositoryIngredientSuccess,
    updateRepositoryIngredientFailed,
    deactivateRepositoryIngredientSuccess,
    deactivateRepositoryIngredientFailed,
    reactivateRepositoryIngredientSuccess,
    reactivateRepositoryIngredientFailed,
} = RepositoryIngredientsManagementSlice.actions;

export {
    getRepositoryIngredientsSuccess,
};

export default RepositoryIngredientsManagementSlice.reducer;
