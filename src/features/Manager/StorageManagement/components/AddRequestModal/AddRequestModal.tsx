import { Modal, Form, Typography, Button, Col, Input, Row } from 'antd';
import React, { useState, useEffect } from 'react';
import './AddRequestModal.scss';
const { TextArea } = Input;

type AddManagerRequestModalProps = {
    show: boolean,
    onFinish: Function,
    toggleModal: Function,
    data:any,
}

const AddManagerRequestModal: React.FC<AddManagerRequestModalProps> = (props) => {
    const { onFinish, show, toggleModal,data } = props;
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();
    const [formValue, setFormValue] = useState<any>({
        referenceid:data?.id,
        branchid: data?.branchid,
        note: "",
      });
    const [requestType, setRequestType] = useState<Number>(3);
    useEffect(() => {
        if (!show) {
          form.resetFields();
          setRequestType(3);
        }
        if(data?.receipttypeid === 1) setRequestType(3);
        if(data?.receipttypeid === 2) setRequestType(2);

      }, [show, form,data]);

    
    const handleAddManagerRequest = async () => {
        const sendData = {
            ...formValue,
            branchid: data?.branchid,
            referenceid:data?.id,
            requesttypeid: requestType,
        };
        setLoading(true);
        await onFinish(sendData);
        setLoading(false);
        form.resetFields();
        setRequestType(3);
        toggleModal();
    };

    return (
        <Modal visible={show} centered footer={false} onCancel={(e) => toggleModal()} zIndex={1} className="add-storage-item-modal">
            <Row justify="center">
                <Typography.Title level={4}>Thêm yêu cầu</Typography.Title>
            </Row>
            <Form className="mt-3 px-5" form={form}
              name="addManagerRequestForm"
              onFinish={ handleAddManagerRequest} >
                <Row gutter={24}>
                    <Col span={24}>
                        <Form.Item name="note" rules={[
                            {
                                required: false,
                            }
                        ]
                        }>
                            <TextArea  style={{ width: '100%' }} name="note" placeholder="Ghi chú"
                            onChange={(e) =>
                                setFormValue({ ...formValue, note: e.target.value })
                            }/>
                        </Form.Item>
                    </Col>
                </Row>
                <Row justify="center">
                    <Button loading={loading}
                     htmlType="submit" type="primary">Thêm</Button>
                </Row>
            </Form>
        </Modal>
    )
}

export default AddManagerRequestModal;
