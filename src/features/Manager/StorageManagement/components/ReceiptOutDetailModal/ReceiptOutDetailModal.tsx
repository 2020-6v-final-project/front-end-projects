import {
  CModal
} from '@coreui/react';
import React, { useEffect, useState } from 'react';
import {
  currencyFormat,
  shortDateFormat,
} from "@shared/utils/";
import { ColumnsType } from "antd/lib/table";
import { Row,Col,Form, Space, Table, Tag, Typography, Button, InputNumber, Radio} from 'antd';
import './ReceiptOutDetailModal.scss';

type ReceiptOutDetailModalState = {
  isShow: boolean,
  data: any,
  ingredients: any[],
  repositories: any[],
  branches: any[],
  receiptTypes: any[],
  vendorList: any[],
  repositoryIngredientsList: any[],
} 

type ReceiptOutDetailModalDispatch = {
  onClose: Function,
  handleAddReceiptOutIngredient: Function,
  handleDeleteReceiptIngredient: Function,
  handleUpdateReceipt: Function,
}

type ReceiptOutDetailModalDefaultProps = ReceiptOutDetailModalState & ReceiptOutDetailModalDispatch;

// { isShow, onClose, data ,props}
const ReceiptOutDetailModal: React.FC<ReceiptOutDetailModalDefaultProps> = React.memo((props) => {
  const { handleUpdateReceipt, repositoryIngredientsList, vendorList, receiptTypes, ingredients, branches, repositories, isShow, onClose, data, handleAddReceiptOutIngredient, handleDeleteReceiptIngredient } = props;
  const [filteredData, setFilteredData] = useState<any[]>(data?.listIngredients||[]);
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);
  const [addIngredient, setAddIngredient] = useState<boolean>(false);
  const [selectedRowKeys,setSelectedRowKey] = useState<any>([]);
  const [formValue, setFormValue] = useState<any[]>([]);
  const [ exportQuantityList, setExportQuantityList] = useState<any[]>([]);
  const [ isDeposit, setIsDeposit ] = useState<boolean>(data?.newdeposit as boolean);
  useEffect(()=>{
    setFilteredData(data?.listIngredients);
    setIsDeposit(data?.newdeposit);
    if(!addIngredient )
    {
      form.resetFields();
      setSelectedRowKey([]);
      setExportQuantityList([]);
      setFormValue([]);
    }
    else {
      let exportIngresList = [] as any[];
      repositoryIngredientsList.forEach((ingre:any) => {
        let exportIngre = {"key":`${ingre.receiptid}+${ingre.ingredientid}+${ingre.vendorid}+${ingre.repositoryid}`,quantity:1}
        exportIngresList.push(exportIngre);
      });
      setExportQuantityList(exportIngresList);
    }
    if(!isShow){
      setAddIngredient(false);
    }
  },[data, form, addIngredient, isShow, onClose, repositoryIngredientsList]);

  const onSelectChange = (selectRowKeys:any) => {
    let selectIngres = [] as any[];
    selectRowKeys.forEach((key:string) => {
      let keySplitted = key.split('+');
      selectIngres.push({"receiptid":keySplitted[0],"ingredientid":keySplitted[1],"vendorid":keySplitted[2],"repositoryid":keySplitted[3],"exportquantity":exportQuantityList.find((quantity:any) => quantity.key===key)?.quantity});
    });
    setSelectedRowKey(selectRowKeys);
    setFormValue(selectIngres);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange:onSelectChange,
  };

  const openAddIngredient = () =>{
    setAddIngredient(!addIngredient);
  };

  const onIsDepositChange = (e:any) =>{
    setIsDeposit(e.target.value);
  };

  const handleAddIngredient = async () => {
    const sendData = {
      ingredients:[...formValue], 
    };
    setLoading(true);
    await handleAddReceiptOutIngredient(sendData,data?.id);
    setLoading(false);
    form.resetFields();
    setAddIngredient(false);
  };
  
  const handleUpdateReceiptDeposit = async () => {
    const sendData = {
      newdeposit:isDeposit, 
    };
    setLoading(true);
    await handleUpdateReceipt(sendData,data?.id);
    setLoading(false);
  };

  const handleDeleteIngredient = (e:any) => {
    const sendData = e;
    handleDeleteReceiptIngredient(sendData,data?.id);
  };

  const handleChangeExportQuantity = (
    e: any,
    record: any
  ) => {
    const indexQuantity = exportQuantityList.findIndex((value: any) => 
      value.key === `${record.receiptid}+${record.ingredientid}+${record.vendorid}+${record.repositoryid}`
    );
    let changedExport;
    if (indexQuantity > -1) {
      changedExport = exportQuantityList[indexQuantity];
      changedExport.quantity = e;
      const tempQuantityValue = [...exportQuantityList];
      tempQuantityValue.splice(indexQuantity, 1, changedExport);
      setExportQuantityList(tempQuantityValue);
    }    
    const index = formValue.findIndex((value: any) => 
      Number(value.receiptid) === Number(record.receiptid) && Number(value.vendorid) === Number(record.vendorid) && Number(value.ingredientid) === Number(record.ingredientid) && Number(value.repositoryid) === Number(record.repositoryid)
    );
    let changedIngre;
    if (index > -1) {
      changedIngre = formValue[index];
      changedIngre.exportquantity = e;
      const tempFormValue = [...formValue];
      tempFormValue.splice(index, 1, changedIngre);
      setFormValue(tempFormValue);
    }
  };
  const columns: ColumnsType = [
    {
      align: "center",
      title: "Tên nguyên liệu",
      dataIndex: "ingredientid",
      key: "ingredientid",
      render: (text: string) => <Typography>{ingredients.find((ingre)=>ingre.id===Number(text))?.name}</Typography>,
    },
    {
      align: "center",
      title: 'Nhà cung cấp',
      dataIndex: 'vendorid',
      key: 'vendorid',
      render: (text: string) => <Typography>{vendorList?.find(vendor => vendor.id === text)?.name}</Typography>,
    },
    {
      align: "center",
      title: "Giá",
      dataIndex: "price",
      key: "price",
      render: (text: string) => <Typography>{currencyFormat(Number(text))}</Typography>,
    },
    {
      align: "center",
      title: "Số lượng",
      dataIndex: "quantity",
      key: "quantity",
    },
    {
      align: "center",
      title: "Tổng tiền",
      dataIndex: "total",
      key: "total",
      render: (text: string) => <Typography>{currencyFormat(Number(text))}</Typography>,
    },
    {
      align: "center",
      title: "Ghi chú",
      dataIndex: "note",
      key: "note",
      render: (text:string) => <Typography.Text editable={data?.statuscode !== "EDITING" ? false : true} > {text} </Typography.Text>
    },
    {
      align: "center",
      title: "Thao tác",
      key: "action",
      render: (text: string, record: any) => (
        <Space size="middle">
          <Button
          onClick={() => {
            setLoading(true);
            handleDeleteIngredient(record);
            setLoading(false);}} 
            danger
            size="small" disabled={data?.statuscode !== "EDITING" ? true : false}
          >
            Xóa
          </Button>
        </Space>
      ),
    },
  ];

  const renderSwitchTag = (param:any) => {
    switch(param) {
      case "EDITING":
        return <Tag color="magenta">Chỉnh sửa</Tag>;
      case "PENDINGAPPROVED":
        return <Tag color="gold">Đang chờ duyệt</Tag>;
      case "REJECTED":
        return <Tag color="red">Bị từ chối</Tag>;
      case "APPROVED":
        return <Tag color="lime">Đã duyệt</Tag>;
      case "PENDINGPAYMENT":
        return <Tag color="geekblue">Đang chờ chi</Tag>;
      case "PENDINGRECEIVED":
        return <Tag color="cyan">Đang nhận hàng</Tag>;
      default:
        return <Tag color="success">Hoàn thành</Tag>;
    }
  }

  const addIngreColumns:ColumnsType = [
    {
      align: "center",
      title: "Tên nguyên liệu",
      dataIndex: "ingredientid",
      key: "ingredientid",
      render: (text: string) => <Typography>{ingredients?.find(ingre => ingre.id === text)?.name}</Typography>,
    },
    {
      align: "center",
      title: "Nhà cung cấp",
      dataIndex: "vendorid",
      key: "vendorid",
      render: (text: string) => <Typography>{vendorList?.find(vendor => vendor.id === text)?.name}</Typography>,
    },
    {
      align: "center",
      title: "Nhập từ phiếu",
      dataIndex: "receiptid",
      key: "receiptid",
    },
    {
      align: "center",
      title: "Hạn sử dụng",
      dataIndex: "expiredat",
      key: "expiredat",
      render: (text: Date) => <Typography>{shortDateFormat(text)}</Typography>,
    },
    {
      align: "center",
      title: "SL còn",
      dataIndex: "currentquantity",
      key: "currentquantity",
    },
    {
      align: "center",
      title: "SL xuất",
      key: "exportquantity",
      render: (text: string, record: any) => (
        <Space size="middle">
          <InputNumber
          onChange={(e:any)=>handleChangeExportQuantity(e,record)} 
          disabled={selectedRowKeys?.findIndex((rowKey:any) => rowKey === `${record.receiptid}+${record.ingredientid}+${record.vendorid}+${record.repositoryid}`) < 0 ? true : false}
          value={exportQuantityList.find((quantity:any) => quantity.key === `${record.receiptid}+${record.ingredientid}+${record.vendorid}+${record.repositoryid}`)?.quantity}          
          min={1} 
          max = {record.currentquantity}
          />
        </Space>
      ),
    },
  ];

  return (
    <CModal
      centered
      backdrop
      size="xl"
      show={isShow}
      onClose={() => { onClose() }}
      className="ReceiptOutDetailModal-styles">
      <div style={{padding:"30px"}}>
        <Row justify="center" gutter={24}>
          <Col span={6} className="ReceiptOutDetailModal-row-title">
            <Typography.Text >Chi tiết đơn hàng</Typography.Text>
          </Col>
        </Row>
        <Row  justify="center" gutter={24}>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Chi Nhánh</h6>
            <h5>{branches.find((branch) => data?.branchid === branch.id)?.name}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Kho</h6>
            <h5>{repositories.find((repo) => data?.repositoryid === repo.id)?.name}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Ngày tạo</h6>
            <h5>{shortDateFormat(data?.createdat)}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Loại phiếu</h6>
            <h5>{receiptTypes.find((rec)=>rec.id===data?.receipttypeid)?.name}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Trạng thái</h6>
            {renderSwitchTag(data?.statuscode)}  
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="ReceiptOutDetailModal-row-sub-title">
            <Typography.Text >Phiếu thu:</Typography.Text>
          </Col>
          <Col span={16} className="ReceiptOutDetailModal-row-text">
            <Radio.Group onChange={onIsDepositChange} name="isnewdeposit" value={isDeposit}>
              <Radio value={true}>Đính kèm phiếu thu</Radio>  
              <Radio value={false}>Không kèm phiếu thu</Radio>
            </Radio.Group>
          </Col>
          <Col span={4} className="ReceiptOutDetailModal-row-text">
              <Button onClick={handleUpdateReceiptDeposit} 
              disabled={isDeposit !== data?.newdeposit ? false :true} 
              loading={loading} 
              type="primary" > 
              Lưu
                </Button>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="ReceiptOutDetailModal-row-sub-title">
            <Typography.Text >Ghi chú:</Typography.Text>
          </Col>
          <Col span={18} className="ReceiptOutDetailModal-row-text">
            <Typography.Text editable={data?.statuscode !== "EDITING" ? false : true}>{data?.note}</Typography.Text>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={24} className="ReceiptOutDetailModal-row-title">
            <Typography.Text >Danh sách nguyên liệu:</Typography.Text>
            <Button className="ReceiptOutDetailModal-button" onClick={()=>{openAddIngredient()}} disabled={data?.statuscode !== "EDITING" ? true : false}>
              {addIngredient === false ? "Thêm" : "Hủy"}
              </Button>
          </Col>
        </Row>
        {addIngredient === true ?
          <>
          <Row gutter={24} justify="center">
            <Table
              bordered
              size="small"
              className="ReceiptOutDetailModal-table"
              columns={addIngreColumns}
              pagination={{ position: ["bottomCenter"] }}
              dataSource={repositoryIngredientsList?.filter(data => data?.currentquantity >0)}
              scroll={{ y: "55vh" }}
              rowSelection={rowSelection}
              rowKey={record => `${record.receiptid}+${record.ingredientid}+${record.vendorid}+${record.repositoryid}`}
            />
          </Row>
          <Row gutter={24} justify="center">
            <Button onClick={handleAddIngredient} 
                loading={loading} type="primary" htmlType="submit">
                Xác nhận
            </Button>
          </Row>
          </>
          : null
        }
        <Row>
          <Table
            bordered
            size="small"
            className="ReceiptOutDetailModal-table"
            columns={columns}
            pagination={{ position: ["bottomCenter"] }}
            dataSource={filteredData}
            scroll={{ y: "55vh" }}
          />
      </Row>
      </div>
    </CModal>
  );
});


export default ReceiptOutDetailModal;
