import {
  CModal
} from '@coreui/react';
import React, { useEffect, useState } from 'react';
import {
  shortDateFormat,
} from "@shared/utils/";
import { Timeline, Row,Col,Form, DatePicker , Tag, Typography, Button, Input} from 'antd';
import './AddIngreModal.scss';


type ReceiptDetailModalState = {
  isShow: boolean,
  data: any,
  ingredients: any[],
  repositories: any[],
  branches: any[],
  receiptTypes: any[]
} 

type ReceiptDetailModalDispatch = {
  onClose: Function,
  handleAddReceiptIngredient: Function,
  handleDeleteReceiptIngredient: Function,
  handleReceiveIngredientFromReceipt: Function,
}

type ReceiptDetailModalDefaultProps = ReceiptDetailModalState & ReceiptDetailModalDispatch;

const formatIngreList = (ingredients: any[]) => {
  return ingredients.map((ingre) => {
    return {
      id: ingre.id,
      vendorid:ingre.vendorid,
      quantity: ingre.quantity,
      price:ingre.price,
      factorydate:null,
      expiredat:null,
    };
  });
};

// { isShow, onClose, data ,props}
const AddIngreModal: React.FC<ReceiptDetailModalDefaultProps> = React.memo((props) => {
  const { handleReceiveIngredientFromReceipt, receiptTypes, ingredients, branches, repositories, isShow, onClose, data } = props;
  const [formValue, setFormValue] = useState<any[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [newIngredients, setNewIngredients] = useState<any>([]);
  const [form] = Form.useForm();


  useEffect(() => {
    const loadIngres = () => {
      let ingres = [] as any[];
      if (data?.listIngredients?.length > 0) {
        data.listIngredients.forEach((receiptIngre: any) => {
          let rowIngre: any = {
            ...ingredients.find((value) => value.id === receiptIngre.ingredientid),
          } as any;
          if (rowIngre !== undefined) {
            // rowItem.options = [ ...options ];
            ingres.push(rowIngre);
            ingres[ingres.length - 1] = {
              ...ingres[ingres.length - 1],
              quantity:receiptIngre.quantity,
              vendorid:receiptIngre.vendorid,
              price:receiptIngre.price,
            };
          }
        });
      }
      setNewIngredients(ingres);
      setFormValue(formatIngreList(ingres));
    };
    loadIngres();
  }, [data, form, ingredients, isShow]);
  
  const handleChangeFactoryDate = (
    e: any,
    ingre: any
  ) => {
    const index = formValue.findIndex((value: any) => value.id === ingre.id);
    let changedIngre;
    if (index > -1) {
      changedIngre = formValue[index];
      changedIngre.factorydate = e?.toDate();
      const tempFormValue = [...formValue];
      tempFormValue.splice(index, 1, changedIngre);
      setFormValue(tempFormValue);
    }
  };

  const handleChangeExpiredAt = (
    e: any,
    ingre: any
  ) => {
    const index = formValue.findIndex((value: any) => value.id === ingre.id);
    let changedIngre;
    if (index > -1) {
      changedIngre = formValue[index];
      changedIngre.expiredat = e?.toDate();
      const tempFormValue = [...formValue];
      tempFormValue.splice(index, 1, changedIngre);
      setFormValue(tempFormValue);
    }
  };

  const handleReceiveIngredient = async () => {
    const sendData = {
      repoIngredients:[...formValue], 
      receiptid:data?.id,
      branchid: data?.branchid,
      repositoryid:data?.repositoryid,
    };
    setLoading(true);
    await handleReceiveIngredientFromReceipt(sendData);
    setLoading(false);
    form.resetFields();
    setNewIngredients([]);
    onClose();
  };

  
  const renderSwitchTag = (param:any) => {
    switch(param) {
      case "EDITING":
        return <Tag color="magenta">{data?.statuscode}</Tag>;
      case "PENDINGAPPROVED":
        return <Tag color="gold">{data?.statuscode}</Tag>;
      case "REJECTED":
        return <Tag color="red">{data?.statuscode}</Tag>;
      case "APPROVED":
        return <Tag color="lime">{data?.statuscode}</Tag>;
      case "PENDINGPAYMENT":
        return <Tag color="geekblue">{data?.statuscode}</Tag>;
      case "PENDINGRECEIVED":
        return <Tag color="cyan">{data?.statuscode}</Tag>;
      default:
        return <Tag color="success">{data?.statuscode}</Tag>;
    }
  }
  return (
    <CModal
      centered
      backdrop
      size="xl"
      show={isShow}
      onClose={() => { onClose() }}
      className="ReceiptDetailModal-styles">
      <div style={{padding:"30px"}}>
        <Row justify="center" gutter={24}>
          <Col span={6} className="ReceiptDetailModal-row-title">
            <Typography.Text >Chi tiết đơn hàng</Typography.Text>
          </Col>
        </Row>
        <Row  justify="center" gutter={24}>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Chi Nhánh</h6>
            <h5>{branches.find((branch) => data?.branchid === branch.id)?.name}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Kho</h6>
            <h5>{repositories.find((repo) => data?.repositoryid === repo.id)?.name}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Ngày tạo</h6>
            <h5>{shortDateFormat(data?.createdat)}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Loại phiếu</h6>
            <h5>{receiptTypes.find((rec)=>rec.id===data?.receipttypeid)?.name}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Trạng thái</h6>
            {renderSwitchTag(data?.statuscode)}  
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="ReceiptDetailModal-row-sub-title">
            <Typography.Text >Ghi chú:</Typography.Text>
          </Col>
          <Col span={18} className="ReceiptDetailModal-row-text">
            <Typography.Text editable={data?.statuscode !== "EDITING" ? false : true}>{data?.note}</Typography.Text>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={24} className="ReceiptDetailModal-row-title">
            <Typography.Text >Danh sách nguyên liệu:</Typography.Text>
          </Col>
        </Row>
        <Form
              name="changePriceForm"
              form={form}
              onFinish={handleReceiveIngredient}
            >
              <Row justify="center" className="mt-3">
                <Col style={{ width: "100%" }}>
                  <Timeline>
                    {newIngredients.map((item:any, itemIndex:number) => (
                      <Timeline.Item key={itemIndex}>
                        <Row justify="space-between" gutter={24}>
                          <Col span={6}>
                            <Input value={item.name} disabled />
                          </Col>
                          <Col span={2}>
                            <Input value={item.quantity} disabled />
                          </Col>
                          <Col span={8}>
                            <DatePicker 
                              style={{borderRadius: 20,width: '100%'}}
                              placeholder="Ngày sản xuất"
                              onChange={(e) => handleChangeFactoryDate(e, item)}
                              />
                          </Col>
                          <Col span={8}>
                            <DatePicker 
                              style={{borderRadius: 20,width: '100%'}}
                              placeholder="Ngày hết hạn"
                              onChange={(e) => handleChangeExpiredAt(e, item)}                              
                              />
                          </Col>
                        </Row>
                        </Timeline.Item>
                      ))}
                  </Timeline>
                </Col>
              </Row>
              <Row justify="center">
                <Form.Item>
                  <Button loading={loading} type="primary" htmlType="submit">
                    Nhập hàng
                  </Button>
                </Form.Item>
              </Row>
          </Form>
      </div>
    </CModal>
  );
});

export default AddIngreModal;
