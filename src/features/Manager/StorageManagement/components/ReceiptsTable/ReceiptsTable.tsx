import {
  Button,
  Space,
  Table,
  Tag,Typography
} from "antd";
import {
  shortDateFormat,
} from "@shared/utils/";
import { ColumnsType } from "antd/lib/table";
import React, { useState, useEffect } from "react";
import "./ReceiptsTable.scss";


type IngredientsTableProps = {
  data: Array<any>,
  branches: Array<any>,
  repositories: Array<any>,
  ingredients:Array<any>,
  handleDeactivateOrReactivateIngredient: Function,
  handleUpdateIngredientQuantity: Function,
  showDetailReceipt: Function,
  showAddRequestModal: Function,
  showAddIngreModal: Function,
  handleRemoveReceipt: Function,
};

const ReceiptsTable: React.FC<IngredientsTableProps> = React.memo((props) => {
  const {repositories, data, showDetailReceipt, showAddRequestModal, handleRemoveReceipt, showAddIngreModal
    // branches, handleUpdateIngredientQuantity,handleDeactivateOrReactivateIngredient 
  } = props;
  const [filteredData, setFilteredData] = useState(data);
  // const [autoComplete, setAutoComplete] = useState<string>("");
  useEffect(()=>{
      setFilteredData(data);
  },[data]);

  const columns: ColumnsType = [
    {
      align: "center",
      title: "Id",
      dataIndex: "id",
      key: "id",
      defaultSortOrder: 'ascend',
    },
    {
      align: "center",
      title: 'Kho',
      dataIndex: 'repositoryid',
      key: 'repositoryid',
      sorter: (a: any, b: any) => a?.repositoryid - b?.repositoryid,
      render: (text: string) => <Typography>{repositories.find((repo)=>repo?.id===Number(text))?.name}</Typography>,
    },
    {
      align: "center",
      title: "Ngày tạo",
      dataIndex: "createdat",
      key: "createdat",
      render: (text: Date) => <Typography>{shortDateFormat(text)}</Typography>,
    },
    {
      align: "center",
      title: "Trạng thái",
      dataIndex: "statuscode",
      key: "statuscode",
      sorter: (a: any, b: any) => a?.statuscode.localeCompare(b?.statuscode),
      render: (text: string, record: any) => 
      {
        if(text === "EDITING")
        return  <Tag color="magenta">Chỉnh sửa</Tag>
        if(text === "PENDINGAPPROVED")
        return  <Tag color="gold">Đang chờ duyệt</Tag>
        if(text === "REJECTED")
        return  <Tag color="red">Bị từ chối</Tag>
        if(text === "APPROVED")
        return  <Tag color="lime">Đã duyệt</Tag>
        if(text === "PENDINGPAYMENT")
        return  <Tag color="geekblue">Đang chờ chi</Tag>
        if(text === "PENDINGRECEIVED")
        return  <Tag color="cyan">Đang nhận hàng</Tag>
        return  <Tag color="success">Hoàn thành</Tag>
      }
    },
    {
      align: "center",
      title: "Thao tác",
      key: "action",
      render: (text: string, record: any) => (
        <Space size="middle">
          <Button
            onClick={(e) => handleRemoveReceipt(record,e)}
            danger
            size="small"
            disabled = {record?.statuscode === "EDITING" ? false : true}
          >
            Xóa
          </Button>
          {record?.statuscode === "EDITING" ? 
            <Button
              onClick={(e) => showAddRequestModal(record,e)}
              type="dashed"
              block
              size="small"
              disabled = {record?.statuscode === "EDITING" ? false : true}
            >Gửi yêu cầu
            </Button>
            : 
            <Button
              onClick={(e) => showAddIngreModal(record,e)}
              type="primary"
              block
              size="small"
              disabled = {record?.statuscode === "PENDINGRECEIVED" ? false : true}
            >Nhập hàng
            </Button>
          }
        </Space>
      ),
    },
  ];

  return (
    <>
      <Table
        bordered
        size="small"
        className="ingredients-table"
        columns={columns}
        pagination={{ position: ["bottomCenter"] }}
        dataSource={filteredData}
        rowKey={record => record.id}
        onRow={(record:any) =>  { 
          return {
          onClick: event =>  showDetailReceipt(record)
          };
      }}
      />
    </>
  );
}
);

export default ReceiptsTable;
