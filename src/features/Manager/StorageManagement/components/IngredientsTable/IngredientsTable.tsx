import {
  Avatar,
  Table,
  Tag,Typography
} from "antd";
import {
  shortDateFormat,
} from "@shared/utils/";
import { ColumnsType } from "antd/lib/table";
import React, { useState, useEffect } from "react";
import "./IngredientsTable.scss";


type IngredientsTableProps = {
  data: Array<any>,
  branches: Array<any>,
  handleDeactivateOrReactivateIngredient: Function,
  handleUpdateIngredientQuantity: Function,
  repositories: Array<any>,
  ingredients:Array<any>,
};

const IngredientsTable: React.FC<IngredientsTableProps> = React.memo((props) => {
  const {ingredients,repositories, data,
    // branches, handleUpdateIngredientQuantity,handleDeactivateOrReactivateIngredient 
  } = props;
  const [filteredData, setFilteredData] = useState(data);
  useEffect(()=>{
      setFilteredData(data);
  },[data]);

  const columns: ColumnsType = [
    {
      align: "center",
      title: "Id phiếu nhập",
      dataIndex: "receiptid",
      key: "receiptid",
    },
    {
      align: "center",
      title: "Id nguyên liệu",
      dataIndex: "ingredientid",
      key: "ingredientid",
    },
    {
      align: "center",
      title: "Hình ảnh",
      dataIndex: "ingredientid",
      key: "ingredientid",
      render: (text: string) => <Avatar src={ingredients.find((ingre)=>ingre?.id===Number(text))?.imgpath === null|| ingredients.find((ingre)=>ingre.id===Number(text))?.imgpath === "" ? "/item-sample.svg" : ingredients.find((ingre)=>ingre.id===Number(text))?.imgpath} size={64} />,
    },
    {
      align: "center",
      title: 'Kho',
      dataIndex: 'repositoryid',
      key: 'ingredientid',
      render: (text: string) => <Typography>{repositories.find((repo)=>repo?.id===Number(text))?.name}</Typography>,
    },
    {
      title: "Tên nguyên liệu",
      dataIndex: "ingredientid",
      key: "ingredientid",
      render: (text: string) => <Typography>{ingredients.find((ingre)=>ingre?.id===Number(text))?.name}</Typography>,
    },
    {
      align: "center",
      title: "Ngày nhập",
      dataIndex: "receivedat",
      key: "ingredientid",
      render: (text: Date) => <Typography>{shortDateFormat(text)}</Typography>,
    },
    {
      align: "center",
      title: "Ngày sản xuất",
      dataIndex: "factorydate",
      key: "ingredientid",
      render: (text: Date) => <Typography>{shortDateFormat(text)}</Typography>,
    },
    {
      align: "center",
      title: "Hạn sử dụng",
      dataIndex: "expiredat",
      key: "ingredientid",
      render: (text: Date) => <Typography>{shortDateFormat(text)}</Typography>,
    },
    {
      align: "center",
      title: "Số lượng ban đầu",
      dataIndex: "startquantity",
      key: "ingredientid",
    },
    {
      align: "center",
      title: "Số lượng hiện tại",
      dataIndex: "currentquantity",
      key: "ingredientid",
      // render: (text: string) => (
      //   <InputNumber
      //     onPressEnter={()=>handleUpdateIngredientQuantity(Number(text))}
      //     style={{ width: 64 }}
      //     min={0}
      //     type="number"
      //     defaultValue={Number(text)}
      //   />
      // ),
      defaultSortOrder: "ascend",
      sorter: (a: any, b: any) => a?.currentquantity - b?.currentquantity,
    },
    {
      align: "center",
      title: "Trạng thái",
      key: "ingredientid",
      render: (text: string, record: any) =>
        record.currentquantity > 0 ? (
          <Tag color="success">Còn hàng</Tag>
        ) : (
          <Tag color="error">Hết hàng</Tag>
        ),
      filters: [
        {
          text: "Còn hàng",
          value: "Còn hàng",
        },
        {
          text: "Hết hàng",
          value: "Hết hàng",
        },
      ],
      onFilter: (value, record: any) => {
        if (value === "Còn hàng") {
          return record.currentquantity > 0;
        } else {
          return record.currentquantity <= 0;
        }
      },
    },
  ];

  return (
    <>
      <Table
        bordered
        size="small"
        className="ingredients-table"
        columns={columns}
        pagination={{ position: ["bottomCenter"] }}
        dataSource={filteredData}
      />
    </>
  );
}
);

export default IngredientsTable;
