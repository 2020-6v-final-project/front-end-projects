import {
  CModal
} from '@coreui/react';
import React, { useEffect, useState } from 'react';
import {
  currencyFormat,
  shortDateFormat,
} from "@shared/utils/";
import { ColumnsType } from "antd/lib/table";
import { AutoComplete, Row,Col,Form, Space, Table, Tag, Typography, Button, Input} from 'antd';
import './ReceiptInDetailModal.scss';

const { TextArea } = Input;


type ReceiptInDetailModalState = {
  isShow: boolean,
  data: any,
  ingredients: any[],
  repositories: any[],
  branches: any[],
  receiptTypes: any[]
  vendorList: any[]
} 

type ReceiptInDetailModalDispatch = {
  onClose: Function,
  handleAddReceiptIngredient: Function,
  handleDeleteReceiptIngredient: Function,
}

type ReceiptInDetailModalDefaultProps = ReceiptInDetailModalState & ReceiptInDetailModalDispatch;

// { isShow, onClose, data ,props}
const ReceiptInDetailModal: React.FC<ReceiptInDetailModalDefaultProps> = React.memo((props) => {
  const { vendorList, receiptTypes, ingredients, branches, repositories, isShow, onClose, data, handleAddReceiptIngredient, handleDeleteReceiptIngredient } = props;
  const [filteredData, setFilteredData] = useState<any[]>(data?.listIngredients||[]);
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);
  const [vendorIngredients, setVendorIngredients] = useState<any>([]);
  const [addIngredient, setAddIngredient] = useState<boolean>(false);
  const [newIngredient, setNewIngredient] = useState<any>("");
  const [newVendor, setNewVendor] = useState<any>("");
  const [autoCompleteIngre, setAutoCompleteIngre] = useState("");
  const [autoCompleteVendor, setAutoCompleteVendor] = useState("");

  const [formValue, setFormValue] = useState<any>({
    receiptid:data?.id,
    quantity: 1,
    price:0,
    note:"",
  });

  useEffect(()=>{
      setFilteredData(data?.listIngredients);
      if(!addIngredient )
      {
        form.resetFields();
        setNewIngredient("");
        setAutoCompleteIngre("");
        setAutoCompleteVendor("");
        setNewVendor("");
      }
      if(!isShow){
        setAddIngredient(false);
      }
  },[data,form,addIngredient,isShow,onClose]);

  const handleSelectVendor= (item:any) =>{
    setAutoCompleteVendor(item.name);
    setNewVendor(item.id);
    setVendorIngredients(item.ingredients);
  }

  const handleSelectIngredient = (item:any) =>{
    setAutoCompleteIngre(item.name);
    setNewIngredient(item.id);
  }

  const openAddIngredient = () =>{
    setAddIngredient(!addIngredient);
  };

  const handleAddIngredient = () => {

    const sendData = {
      ...formValue, 
      vendorid:newVendor,
      receiptid:data?.id,
      ingredientid:newIngredient,
    };
    handleAddReceiptIngredient(sendData);
    form.resetFields();
    setAddIngredient(false);
  };

  const handleDeleteIngredient = (e:any) => {
    const sendData = e;
    handleDeleteReceiptIngredient(sendData);
  };

  const columns: ColumnsType = [
    {
      align: "center",
      title: "Tên nguyên liệu",
      dataIndex: "ingredientid",
      key: "ingredientid",
      render: (text: string) => <Typography>{ingredients.find((ingre)=>ingre.id===Number(text))?.name}</Typography>,
    },
    {
      align: "center",
      title: 'Nhà cung cấp',
      dataIndex: 'vendorid',
      key: 'vendorid',
      render: (text: string) => <Typography>{vendorList?.find(vendor => vendor.id === text)?.name}</Typography>,
    },
    {
      align: "center",
      title: "Giá",
      dataIndex: "price",
      key: "price",
      render: (text: string) => <Typography>{currencyFormat(Number(text))}</Typography>,
    },
    {
      align: "center",
      title: "Số lượng",
      dataIndex: "quantity",
      key: "quantity",
    },
    {
      align: "center",
      title: "Tổng tiền",
      dataIndex: "total",
      key: "total",
      render: (text: string) => <Typography>{currencyFormat(Number(text))}</Typography>,
    },
    {
      align: "center",
      title: "Ghi chú",
      dataIndex: "note",
      key: "note",
      render: (text:string) => <Typography.Text editable={data?.statuscode !== "EDITING" ? false : true} > {text} </Typography.Text>
    },
    {
      align: "center",
      title: "Thao tác",
      key: "action",
      render: (text: string, record: any) => (
        <Space size="middle">
          <Button
          onClick={() => {
            setLoading(true);
            handleDeleteIngredient(record);
            setLoading(false);}} 
            danger
            size="small" disabled={data?.statuscode !== "EDITING" ? true : false}
          >
            Xóa
          </Button>
        </Space>
      ),
    },
  ];

  const renderSwitchTag = (param:any) => {
    switch(param) {
      case "EDITING":
        return <Tag color="magenta">Chỉnh sửa</Tag>;
      case "PENDINGAPPROVED":
        return <Tag color="gold">Đang chờ duyệt</Tag>;
      case "REJECTED":
        return <Tag color="red">Bị từ chối</Tag>;
      case "APPROVED":
        return <Tag color="lime">Đã duyệt</Tag>;
      case "PENDINGPAYMENT":
        return <Tag color="geekblue">Đang chờ chi</Tag>;
      case "PENDINGRECEIVED":
        return <Tag color="cyan">Đang nhận hàng</Tag>;
      default:
        return <Tag color="success">Hoàn thành</Tag>;
    }
  }


  return (
    <CModal
      centered
      backdrop
      size="xl"
      show={isShow}
      onClose={() => { onClose() }}
      className="ReceiptInDetailModal-styles">
      <div style={{padding:"30px"}}>
        <Row justify="center" gutter={24}>
          <Col span={6} className="ReceiptInDetailModal-row-title">
            <Typography.Text >Chi tiết đơn hàng</Typography.Text>
          </Col>
        </Row>
        <Row  justify="center" gutter={24}>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Chi Nhánh</h6>
            <h5>{branches.find((branch) => data?.branchid === branch.id)?.name}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Kho</h6>
            <h5>{repositories.find((repo) => data?.repositoryid === repo.id)?.name}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Ngày tạo</h6>
            <h5>{shortDateFormat(data?.createdat)}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Loại phiếu</h6>
            <h5>{receiptTypes.find((rec)=>rec.id===data?.receipttypeid)?.name}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Trạng thái</h6>
            {renderSwitchTag(data?.statuscode)}  
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="ReceiptInDetailModal-row-sub-title">
            <Typography.Text >Ghi chú:</Typography.Text>
          </Col>
          <Col span={18} className="ReceiptInDetailModal-row-text">
            <Typography.Text editable={data?.statuscode !== "EDITING" ? false : true}>{data?.note}</Typography.Text>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={24} className="ReceiptInDetailModal-row-title">
            <Typography.Text >Danh sách nguyên liệu:</Typography.Text>
            <Button className="ReceiptInDetailModal-button" onClick={()=>{openAddIngredient()}} disabled={data?.statuscode !== "EDITING" ? true : false}>
              {addIngredient === false ? "Thêm" : "Hủy"}
              </Button>
          </Col>
        </Row>
        {addIngredient === true ?
          <div>
            <Form
              form={form}
              name="addIngredientForm"
              onFinish={() => {
                setLoading(true);
                handleAddIngredient();
                setLoading(false);
              }}
            >
              <Row justify="center" style={{marginBottom:"20px"}}>
                <Col span={24}>
                <AutoComplete
                    allowClear
                    onChange={(value) => {
                      setAutoCompleteVendor(value);
                    }}
                    value={autoCompleteVendor}
                    style={{ width: "100%" }}
                    placeholder="Nhà cung cấp"
                    filterOption={(inputValue, option) =>
                      option!.value
                        .toUpperCase()
                        .indexOf(inputValue.toUpperCase()) !== -1
                    }
                  >
                    {vendorList
                      .map((vendor) => (
                        <AutoComplete.Option
                          key={vendor.id}
                          value={vendor.name}
                        >
                          <Row
                            key={vendor.id}
                            onClick={(e) => handleSelectVendor(vendor)}
                          >
                            {vendor.name}
                          </Row>
                        </AutoComplete.Option>
                      ))}
                  </AutoComplete>
                </Col>
              </Row> 
              {newVendor ? <>
              <Row justify="center" style={{marginBottom:"20px"}}>
                <Col span={24}>
                <AutoComplete
                    allowClear
                    onChange={(value) => {
                      setAutoCompleteIngre(value);
                    }}
                    value={autoCompleteIngre}
                    style={{ width: "100%" }}
                    placeholder="Nguyên liệu"
                    filterOption={(inputValue, option) =>
                      option!.value
                        .toUpperCase()
                        .indexOf(inputValue.toUpperCase()) !== -1
                    }
                  >
                    {vendorIngredients
                      .filter(
                        (ingredient:any) => ingredient.statuscode === "AVAILABLE"
                      )
                      .map((ingredient:any) => (
                        <AutoComplete.Option
                          key={ingredient.id}
                          value={ingredient.name}
                        >
                          <Row
                            key={ingredient.id}
                            onClick={(e) => handleSelectIngredient(ingredient)}
                          >
                            {ingredient.name}
                          </Row>
                        </AutoComplete.Option>
                      ))}
                  </AutoComplete>
                </Col>
              </Row>
              <Row gutter={24} justify="space-between">
              <Col span={12}>
                  <Form.Item
                    name="quantity"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền Số lượng",
                      },
                    ]}
                  >
                    <Input
                      type="number"
                      placeholder="Số lượng"
                      value={formValue.quantity || ""}
                      onChange={(e) =>
                        setFormValue({
                          ...formValue,
                          quantity: e.target.value,
                        })
                      }
                    />
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    name="price"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền giá tiền",
                      },
                    ]}
                  >
                    <Input
                      type="number"
                      placeholder="Giá tiền"
                      value={formValue.price || ""}
                      onChange={(e) =>
                        setFormValue({
                          ...formValue,
                          price: e.target.value,
                        })
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>
              </> : null}
              <Row gutter={24} justify="space-between">
                <Col span={24}>
                  <Form.Item
                    name="note"
                    rules={[
                      {
                        required: false,
                      },
                    ]}
                  >
                    <TextArea  style={{ width: '100%' }} name="note" placeholder="Ghi chú"
                            onChange={(e) =>
                                setFormValue({ ...formValue, note: e.target.value })
                            }/>
                  </Form.Item>
                </Col>
              </Row>
              <Row justify="center">
                <Form.Item>
                  <Button onClick={() => {
                      setLoading(true);
                      handleAddIngredient();
                      setLoading(false);}} 
                      loading={loading} type="primary" htmlType="submit">
                      Xác nhận
                  </Button>
                </Form.Item>
              </Row>
            </Form>
          </div>
          : null
        }
        <Row>
          <Table
            bordered
            size="small"
            className="ReceiptInDetailModal-table"
            columns={columns}
            pagination={{ position: ["bottomCenter"] }}
            dataSource={filteredData}
            scroll={{ y: "55vh" }}
          />
      </Row>
      </div>
    </CModal>
  );
});

export default ReceiptInDetailModal;
