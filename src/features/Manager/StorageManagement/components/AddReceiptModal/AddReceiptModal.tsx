import { Modal, AutoComplete,  Form, Typography, Button, Row, Col, Input } from 'antd';
import React, { useState, useEffect } from 'react';
import './AddReceiptModal.scss';

const { TextArea } = Input;

type AddReceiptModalProps = {
    show: boolean,
    onFinish: Function,
    toggleModal: Function,
    repositories: Array<any>,
    receiptTypes: Array<any>,
    currentBranch: number | undefined,
}

const AddReceiptModal: React.FC<AddReceiptModalProps> = (props) => {
    const { receiptTypes, currentBranch, onFinish, show, toggleModal } = props;
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();
    const [formValue, setFormValue] = useState<any>({
        note:"",
      });
    const [ receiptType, setReceiptType] = useState<Number>(0);

    useEffect(() => {
        if (!show) {
          form.resetFields();
          setReceiptType(0);
        }
      }, [show, form]);
    

    const handleReceiptTypeSelect = (item: any) => {
        setReceiptType(item);
    };
    

    const handleAddIngredient = async () => {
        const sendData = {
            ...formValue,
            branchid: currentBranch,
            receipttypeid: receiptType,
        };
        setLoading(true);
        await onFinish(sendData);
        setLoading(false);
        form.resetFields();
        toggleModal();
    };

    // const handleIngredientSelect = () => { }

    return (
        <Modal visible={show} centered footer={false} onCancel={(e) => toggleModal()} zIndex={1} className="add-storage-item-modal">
            <Row justify="center">
                <Typography.Title level={4}>Thêm phiếu xuất/nhập kho</Typography.Title>
            </Row>
            <Form className="mt-3 px-5" form={form}
              name="addTableForm"
              onFinish={handleAddIngredient} >
                <Row gutter={24}>
                    <Col span={24}>
                        <Form.Item name="receipttype" rules={[
                            {
                                required: true,
                                message: 'Vui lòng chọn loại phiếu!'
                            }
                        ]
                        }>
                            <AutoComplete placeholder="Chọn loại phiếu" >
                                {receiptTypes?.map((item: any) =>
                                    <AutoComplete.Option  key={item.id} value={item.name}>
                                       <Row key={item.id}
                                            onClick={() => handleReceiptTypeSelect(item.id)}
                                            >
                                            {item.name}
                                        </Row>
                                    </AutoComplete.Option>)}
                            </AutoComplete>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={24}>
                    <Col span={24}>
                        <Form.Item name="note" rules={[
                            {
                                required: false,
                            }
                        ]
                        }>
                            <TextArea  style={{ width: '100%' }} name="note" placeholder="Ghi chú"
                            onChange={(e) =>
                                setFormValue({ ...formValue, note: e.target.value })
                            }/>
                        </Form.Item>
                    </Col>
                </Row>
                <Row justify="center">
                    <Button  
                      loading={loading} 
                      htmlType="submit" type="primary">Thêm</Button>
                </Row>
            </Form>
        </Modal>
    )
}

export default AddReceiptModal;
