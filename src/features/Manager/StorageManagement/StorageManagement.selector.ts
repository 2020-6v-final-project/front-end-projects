import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectRepositoryIngredientsList = createSelector(
    [(state: RootState) => state.repositoryIngredients.repositoryIngredientsList],
    (repositoryIngredientsList) => repositoryIngredientsList
)