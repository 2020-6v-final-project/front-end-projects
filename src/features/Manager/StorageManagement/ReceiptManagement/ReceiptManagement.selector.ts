import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectReceiptsList = createSelector(
    [(state: RootState) => state.receipt.receiptsList],
    (receiptsList) => receiptsList
);

export const selectReceiptTypesList = createSelector(
    [(state: RootState) => state.receipt.receiptTypesList],
    (receiptTypesList) => receiptTypesList
);