import { getReceiptById, addIngredientToExportReceipt,getRepoIngredientsList,receiveReceipt, createReceiptRequest, removeIngredientFromReceipt, addIngredientToReceipt, getReceiptTypesList, removeReceipt, reactivateReceipt, deactivateReceipt, updateReceipt, addReceipt, getReceiptsList } from '@apis';
import { Receipt, ReceiptType, RepositoryIngredient } from '@custom-types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { getRepositoryIngredientsSuccess } from "../StorageManagement.slice";
import { orderBy } from 'lodash';
import { AppThunk } from 'src/store';
import swal from 'sweetalert';

type ReceiptManagementInitialState = {
    receiptsList: Array<Receipt>,
    receiptTypesList: Array<ReceiptType>,
}

const initialState:ReceiptManagementInitialState = {
    receiptsList: [],
    receiptTypesList:[],
}

const ReceiptsManagementSlice = createSlice({
    name: ' ReceiptManagement',
    initialState,
    reducers: {
        getReceiptTypesSuccess: (state, action: PayloadAction<ReceiptType[]>) => {
            state.receiptTypesList = action.payload;
            state.receiptTypesList = orderBy(state.receiptTypesList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        getReceiptTypesFailed: (state) => {
            return state;
        },
        getReceiptsSuccess: (state, action: PayloadAction<Receipt[]>) => {
            state.receiptsList = action.payload;
            state.receiptsList = orderBy(state.receiptsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        getReceiptsFailed: (state) => {
            return state;
        },
        addReceiptSuccess: (state, action: PayloadAction<Receipt[]>) => {
            state.receiptsList = action.payload;
            state.receiptsList = orderBy(state.receiptsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        addReceiptFailed: (state) => {
            return state;
        },
        removeReceiptSuccess: (state, action: PayloadAction<Receipt[]>) => {
            state.receiptsList = action.payload;
            state.receiptsList = orderBy(state.receiptsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        removeReceiptFailed: (state) => {
            return state;
        },
        updateReceiptSuccess: (state, action: PayloadAction<Receipt[]>) => {
            state.receiptsList = action.payload;
            state.receiptsList = orderBy(state.receiptsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        updateReceiptFailed: (state) => {
            return state;
        },
        deactivateReceiptSuccess: (state, action: PayloadAction<Receipt[]>) => {
            state.receiptsList = action.payload;
            state.receiptsList = orderBy(state.receiptsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        deactivateReceiptFailed: (state) => {
            return state;
        },
        reactivateReceiptSuccess: (state, action: PayloadAction<Receipt[]>) => {
            state.receiptsList = action.payload;
            state.receiptsList = orderBy(state.receiptsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        reactivateReceiptFailed: (state) => {
            return state;
        },
        addReceiptIngredientSuccess: (state, action: PayloadAction<Receipt[]>) => {
            state.receiptsList = action.payload;
            state.receiptsList = orderBy(state.receiptsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        addReceiptIngredientFailed: (state) => {
            return state;
        },

        removeRepositoryIngredientSuccess: (state, action: PayloadAction<Receipt[]>) => {
            state.receiptsList = action.payload;
            state.receiptsList = orderBy(state.receiptsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        removeRepositoryIngredientFailed: (state) => {
            return state;
        },
        addRequestForReceiptSuccess: (state, action: PayloadAction<Receipt[]>) => {
            state.receiptsList = action.payload;
            state.receiptsList = orderBy(state.receiptsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        addRequestForReceiptFailed: (state) => {
            return state;
        },
        receiveReceiptIngredientsSuccess: (state, action: PayloadAction<Receipt[]>) => {
            state.receiptsList = action.payload;
            state.receiptsList = orderBy(state.receiptsList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        receiveReceiptIngredientsFailed: (state) => {
            return state;
        },
    }

})

export const getReceiptsAsync = (branchid:number): AppThunk => async (dispatch) => {
    try {
        await getReceiptsList(branchid,(data: Receipt[]) => {
            dispatch(getReceiptsSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Không thể tải danh sách phiếu nhập/xuất!", "error");
        dispatch(getReceiptsFailed());
    }
}

export const getReceiptByIdAsync = (branchid:number,receiptid:number): AppThunk => async (dispatch) => {
    try {
        await getReceiptById(branchid,receiptid,(data: Receipt[]) => {
            dispatch(getReceiptsSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Không thể tải danh sách phiếu nhập/xuất!", "error");
        dispatch(getReceiptsFailed());
    }
}

export const updateReceiptAsync = (receipt: any,branchid:number,receiptid:number): AppThunk => async (dispatch) => {
    try {
        await updateReceipt(receipt, branchid, receiptid, (data: Receipt[]) => {
            swal("Thành công", "Phiếu nhập/xuất kho đã được cập nhật!", "success");
            dispatch(updateReceiptSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(updateReceiptFailed());
    }
}

export const addReceiptAsync = (RepositoryIngredient: any): AppThunk => async (dispatch) => {
    try {
        await addReceipt(RepositoryIngredient, (data: Receipt[]) => {
            swal("Thành công", "Phiếu nhập/xuất kho đã được thêm!", "success");
            dispatch(addReceiptSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(addReceiptFailed());
    }
}

export const deactivateReceiptAsync = (RepositoryIngredient: any): AppThunk => async (dispatch) => {
    try {
        await deactivateReceipt(RepositoryIngredient, (data: Receipt[]) => {
            swal("Thành công", "Phiếu nhập/xuất kho đã bị khóa", "success");
            dispatch(deactivateReceiptSuccess(data));
        })
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(deactivateReceiptFailed());
    }
}

export const reactivateReceiptAsync = (RepositoryIngredient: any): AppThunk => async (dispatch) => {
    try {
        await reactivateReceipt(RepositoryIngredient, (data: Receipt[]) => {
            swal("Thành công", "Phiếu nhập/xuất kho đã được mở khóa", "success");
            dispatch(reactivateReceiptSuccess(data));
        })
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(reactivateReceiptFailed());
    }
}


export const removeReceiptAsync = (receipt: any): AppThunk => async (dispatch) => {
    try {
        await removeReceipt(receipt, (data: Receipt[]) => {
            swal("Thành công", "Phiếu nhập/xuất kho đã được xóa", "success");
            dispatch(removeReceiptSuccess(data));
        })
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(removeReceiptFailed());
    }
}

export const getReceiptTypesAsync = (): AppThunk => async (dispatch) => {
    try {
        await getReceiptTypesList((data: ReceiptType[]) => {
            dispatch(getReceiptTypesSuccess(data));
        });
    } catch (e) {
        dispatch(getReceiptTypesFailed());
    }
}

export const addReceiptIngredientAsync = (RecIngredient: any,branchid:number): AppThunk => async (dispatch) => {
    try {
        await addIngredientToReceipt(RecIngredient,branchid, (data: Receipt[]) => {
            swal("Thành công", "Nguyên liệu đã được thêm vào phiếu!", "success");
            dispatch(addReceiptIngredientSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", e.response?.data?.message === `duplicate key value violates unique constraint "receipts_ingredients_pkey"` ? "Nguyên liệu từ nhà cung cấp này đã được nhập" : "Vui lòng thử lại!", "error");
        dispatch(addReceiptIngredientFailed());
    }
}

export const addExportReceiptIngredientAsync = (RecIngredient: any,branchid:number,receiptid:number): AppThunk => async (dispatch) => {
    try {
        await addIngredientToExportReceipt(RecIngredient,branchid,receiptid, (data: Receipt[]) => {
            swal("Thành công", "Nguyên liệu đã được thêm vào phiếu!", "success");
            dispatch(addReceiptIngredientSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", e.response?.data?.message , "error");
        dispatch(addReceiptIngredientFailed());
    }
}

export const removeRepositoryIngredientAsync = (RepositoryIngredient: any,branchid:number): AppThunk => async (dispatch) => {
    try {
        await removeIngredientFromReceipt(RepositoryIngredient,branchid, (data: Receipt[]) => {
            swal("Thành công", "Nguyên liệu đã được xóa khỏi phiếu", "success");
            dispatch(removeRepositoryIngredientSuccess(data));
        })
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(removeRepositoryIngredientFailed());
    }
}

export const addReceiptRequestAsync = (req: any): AppThunk => async (dispatch) => {
    try {
        await createReceiptRequest(req, (data: Receipt[]) => {
            swal("Thành công", "Yêu cầu đã được gửi!", "success");
            dispatch(receiveReceiptIngredientsSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(receiveReceiptIngredientsFailed());
    }
}

export const receiveReceiptIngredientsAsync = (req: any): AppThunk => async (dispatch) => {
    try {
        await receiveReceipt(req, async (data: Receipt[]) => {
            await getRepoIngredientsList(req.repositoryid,(data: RepositoryIngredient[]) => {
                dispatch(getRepositoryIngredientsSuccess(data));
            });
            swal("Thành công", "Nguyên liệu đã được nhập vào kho!", "success");
            dispatch(addRequestForReceiptSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(addRequestForReceiptFailed());
    }
}

const {
    getReceiptsSuccess,
    getReceiptsFailed,
    addReceiptSuccess,
    addReceiptFailed,
    removeReceiptSuccess,
    removeReceiptFailed,
    updateReceiptSuccess,
    updateReceiptFailed,
    deactivateReceiptSuccess,
    deactivateReceiptFailed,
    reactivateReceiptSuccess,
    reactivateReceiptFailed,
    getReceiptTypesSuccess,
    getReceiptTypesFailed,
    addReceiptIngredientSuccess,
    addReceiptIngredientFailed,
    removeRepositoryIngredientSuccess,
    removeRepositoryIngredientFailed,
    addRequestForReceiptSuccess,
    addRequestForReceiptFailed,
    receiveReceiptIngredientsSuccess,
    receiveReceiptIngredientsFailed,
} = ReceiptsManagementSlice.actions;

export {
    getReceiptsSuccess,
    getReceiptsFailed,
};


export default ReceiptsManagementSlice.reducer;
