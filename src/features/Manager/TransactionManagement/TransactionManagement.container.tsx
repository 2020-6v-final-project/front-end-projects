import { FAB, SubHeaderText } from '@components';
import { CFade } from '@coreui/react';
import React, { useState, useEffect } from 'react';
import { message, Col, Select, Row, Tabs} from 'antd';
import { Branch, Check, Finance } from "@shared";
import { connect } from 'react-redux';
import { RootState } from 'src/store';
import { addCheckRequestAsync, addCheckAsync, getChecksAsync, updateCheckAsync, } from './TransactionManagement.slice';
import { selectChecksList } from "./TransactionManagement.selector";
import ChecksTable from './components/ChecksTable/ChecksTable';
import { history } from "@shared/components";
import CheckDetailModal from './components/CheckDetailModal/CheckDetailModal';
import AddRequestModal from "./components/AddRequestModal/AddRequestModal";
import { selectBranchOfBranchManagementUserList } from "../../LoginPage/LoginPage.selector";
import { getBranchFinancesAsync } from "../FinanceManagement/FinanceManagement.slice";
import { selectFinanceList } from "../FinanceManagement/FinanceManagement.selector";

type TransactionManagementState = {
    financeList: Array<Finance>;
    match: any;
    checksList: Array<Check>;
    branchListOfUser: Array<Branch>;
}

type TransactionManagementDispatch = {
    getChecksAsync: Function,
    addCheckAsync: Function,
    updateCheckAsync: Function,
    getBranchFinancesAsync: Function,
    addCheckRequestAsync: Function,
}

type TransactionManagementDefaultProps = TransactionManagementState & TransactionManagementDispatch;

const TransactionManagementContainer: React.FC<TransactionManagementDefaultProps> = React.memo((props) => {
    const { addCheckRequestAsync, financeList, branchListOfUser, checksList,getChecksAsync, updateCheckAsync, match, getBranchFinancesAsync
        //  updateRepositoryAsync 
        } = props;
    const [addModalVisible, toggleAddModal] = useState(false);
    const [openCheckDetailModal, setOpenCheckDetailModal] = useState<boolean>(
        false
      );
    const [checkDetail, setCheckDetail] = useState<any>(null);
    const [addRequestModalVisible, toggleAddRequestModal] = useState(false);
    const [currentBranch, setCurrentBranch] = useState<Branch | undefined>(branchListOfUser[0] || undefined);

    useEffect(() =>{
        const fetchData = async () => {
            if(currentBranch?.repositoryid) 
            {
            await getChecksAsync(currentBranch.id);
            await getBranchFinancesAsync(currentBranch.id);
            }
          }
          fetchData();
    },[getChecksAsync,currentBranch,getBranchFinancesAsync]);

    useEffect(() => {
        if (checksList.length > 0 && match.params.id) {
            const target = checksList.find(
                (recepit) => recepit.id === match.params.id
            );
            if (target) {
                setCheckDetail(target);
                setOpenCheckDetailModal(true);
            } else {
                history.push("/transaction-management/");
                message.error("Không tìm thấy phiếu nào!");
            }
            
        }
        if (checksList.length > 0 && checkDetail) {
            setCheckDetail(checksList.find((check) => check.id === checkDetail.id));
        }
    }, [checksList, checkDetail,match.params.id]);

    const handleToggleAddRequestModal = () => {
        toggleAddRequestModal(!addRequestModalVisible);
    }

    const handleToggleAddModal = () => {
        toggleAddModal(!addModalVisible);
    }

    const handleRemoveCheck = async (e: any) => {
        // await removeCheckAsync(e);
    }

    const handleUpdateCheckFinanceAccount = async (e: any) => {
        await updateCheckAsync(currentBranch?.id,e);
    }

    const showDetailCheck = (record:any) =>{
        if(record === undefined) history.push("/transaction-management/");
        setCheckDetail(record);
        setOpenCheckDetailModal(!openCheckDetailModal);
    };
    
    const showAddRequestModal = (record:any) =>{
        setCheckDetail(record);
        toggleAddRequestModal(!addRequestModalVisible);
    };
    
    const handleAddRequest = async (request: Request) => {
        await addCheckRequestAsync(request);
    }
    const handleChangeBranch = (value: number) => {
        setCurrentBranch(branchListOfUser?.find(branch => branch.id === value));
      }
    return (
        <CFade>
            <Row>
            <Col style={{marginLeft:"25px", marginTop:"10px", marginBottom:"10px"}}>
                <Row>
                    <SubHeaderText style={{marginRight:'10px'}}>Chi nhánh: </SubHeaderText>
                    <Select 
                        defaultValue={branchListOfUser[0]?.id} 
                        style={{ width: 300 }} 
                        placeholder="Chọn chi nhánh"
                        onChange={handleChangeBranch}
                    >
                        {branchListOfUser?.map((branch, index) => (
                        <Select.Option key={index} value={branch.id}>{branch.code} | {branch.name}</Select.Option>
                        ))}
                    </Select>
                </Row>
            </Col>
            </Row>
            <Tabs size="large"centered defaultActiveKey="1" style={{ height: "65vh", width: "100%", padding: "0px" }}>
                <Tabs.TabPane tab="Danh sách phiếu chi" key="1">
                    <ChecksTable 
                    financeList={financeList}
                    handleRemoveCheck={handleRemoveCheck}
                    showAddRequestModal={showAddRequestModal} 
                    data={checksList} 
                    showDetailCheck={showDetailCheck}     
                    />
                </Tabs.TabPane>
                <Tabs.TabPane tab="Danh sách phiếu thu" key="2">
                    
                </Tabs.TabPane>
            </Tabs>
               
            <CheckDetailModal
                financeList={financeList}
                isShow={openCheckDetailModal}
                onClose={showDetailCheck}
                data={checkDetail}
                handleUpdateCheckFinanceAccount={handleUpdateCheckFinanceAccount}
                // updateUserAsync={updateUserAsync}
            />
            <FAB callback={handleToggleAddModal} />
            <AddRequestModal
                show={addRequestModalVisible}
                toggleModal={handleToggleAddRequestModal}
                onFinish={handleAddRequest}
                data={checkDetail}
            />
            {/* <AddRepositoryItemModal show={addModalVisible} toggleModal={handleToggleAddModal} onFinish={handleAddRepository} /> */}
        </CFade>
    )
});

const mapState = (state: RootState) => {
    return {
        checksList: selectChecksList(state),
        branchListOfUser: selectBranchOfBranchManagementUserList(state),
        financeList: selectFinanceList(state),
    }
};

const mapDispatch = {  
    updateCheckAsync, 
    addCheckAsync, 
    getChecksAsync,
    getBranchFinancesAsync,
    addCheckRequestAsync,
};

export default connect(mapState, mapDispatch)(TransactionManagementContainer);