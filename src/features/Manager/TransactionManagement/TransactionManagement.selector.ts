import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectChecksList = createSelector(
    [(state: RootState) => state.transaction.checksList],
    (checksList) => checksList
)