import { createCheckRequest, updateBranchCheck, addBranchCheck, getBranchChecksList } from '@apis';
import { Check } from '@custom-types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { orderBy } from 'lodash';
import { AppThunk } from 'src/store';
import swal from 'sweetalert';

type TransactionManagementInitialState = {
    checksList: Array<Check>,
}

const initialState: TransactionManagementInitialState = {
    checksList: [],
}

const TransactionManagementSlice = createSlice({
    name: 'TransactionManagement',
    initialState,
    reducers: {
        getChecksSuccess: (state, action: PayloadAction<Check[]>) => {
            state.checksList = action.payload;
            state.checksList = orderBy(state.checksList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        getChecksFailed: (state) => {
            return state;
        },
        addCheckSuccess: (state, action: PayloadAction<Check[]>) => {
            state.checksList = action.payload;
            state.checksList = orderBy(state.checksList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        addCheckFailed: (state) => {
            return state;
        },
        removeCheckSuccess: (state, action: PayloadAction<Check[]>) => {
            state.checksList = action.payload;
            state.checksList = orderBy(state.checksList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        removeCheckFailed: (state) => {
            return state;
        },
        updateCheckSuccess: (state, action: PayloadAction<Check>) => {
            const index = state.checksList?.findIndex(repo => repo.id === action.payload.id);
            if (index > -1) {
                state.checksList?.splice(index, 1, action.payload);
            }
        },
        updateCheckFailed: (state) => {
            return state;
        },
        deactivateCheckSuccess: (state, action: PayloadAction<Check[]>) => {
            state.checksList = action.payload;
            state.checksList = orderBy(state.checksList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        deactivateCheckFailed: (state) => {
            return state;
        },
        reactivateCheckSuccess: (state, action: PayloadAction<Check[]>) => {
            state.checksList = action.payload;
            state.checksList = orderBy(state.checksList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        reactivateCheckFailed: (state) => {
            return state;
        },
        addRequestForCheckSuccess: (state, action: PayloadAction<Check[]>) => {
            state.checksList = action.payload;
            state.checksList = orderBy(state.checksList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        addRequestForCheckFailed: (state) => {
            return state;
        },
    }

})

export const getChecksAsync = (branchid:number): AppThunk => async (dispatch) => {
    try {
        await getBranchChecksList(branchid,(data: Check[]) => {
            dispatch(getChecksSuccess(data));
        });
    } catch (e) {
        dispatch(getChecksFailed());
    }
}

export const updateCheckAsync = (branchid:number,check: any): AppThunk => async (dispatch) => {
    try {
        await updateBranchCheck(branchid,check, (data: Check) => {
            swal("Thành công", "Phiếu chi đã được cập nhật!", "success");
            dispatch(updateCheckSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(updateCheckFailed());
    }
}

export const addCheckAsync = (branchid:number,Check: any): AppThunk => async (dispatch) => {
    try {
        await addBranchCheck(branchid,Check, (data: Check[]) => {
            swal("Thành công", "Phiếu chi đã được thêm!", "success");
            dispatch(addCheckSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(addCheckFailed());
    }
}

export const addCheckRequestAsync = (req: any): AppThunk => async (dispatch) => {
    try {
        await createCheckRequest(req, (data: Check[]) => {
            swal("Thành công", "Yêu cầu đã được gửi!", "success");
            dispatch(addRequestForCheckSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(addRequestForCheckFailed());
    }
}

const {
    addRequestForCheckSuccess,
    addRequestForCheckFailed,
    getChecksSuccess,
    getChecksFailed,
    addCheckSuccess,
    addCheckFailed,
    updateCheckSuccess,
    updateCheckFailed,
} = TransactionManagementSlice.actions;

export {
    getChecksSuccess,
    getChecksFailed,
    addCheckSuccess,
    addCheckFailed,
    updateCheckSuccess,
    updateCheckFailed,
};

export default TransactionManagementSlice.reducer;
