import { Modal, AutoComplete,  Form, Typography, Button, Row, Col, Input } from 'antd';
import React, { useState, useEffect } from 'react';
import './AddTransactionModal.scss';

const { TextArea } = Input;

type AddTransactionModalProps = {
    show: boolean,
    onFinish: Function,
    toggleModal: Function,
    branches: Array<any>,
    repositories: Array<any>,
    checkTypes: Array<any>,
}

const AddCheckModal: React.FC<AddTransactionModalProps> = (props) => {
    const { checkTypes, branches, onFinish, show, toggleModal } = props;
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();
    const [formValue, setFormValue] = useState<any>({
        note:"",
      });
    const [ repoBranch, setRepoBranch ] = useState("") as any;
    const [ checkType, setCheckType] = useState<Number>(0);

    useEffect(() => {
        if (!show) {
          form.resetFields();
          setRepoBranch("");
          setCheckType(0);
        }
      }, [show, form]);
    
    const handleBranchSelect = (item: any) => {
        setRepoBranch(item.id);
     };

    const handleCheckTypeSelect = (item: any) => {
        setCheckType(item);
    };
    

    const handleAddIngredient = async () => {
        const sendData = {
            ...formValue,
            branchid: repoBranch,
            checktypeid: checkType,
        };
        setLoading(true);
        await onFinish(sendData);
        setLoading(false);
        form.resetFields();
        setRepoBranch("");
        toggleModal();
    };

    // const handleIngredientSelect = () => { }

    return (
        <Modal visible={show} centered footer={false} onCancel={(e) => toggleModal()} zIndex={1} className="add-storage-item-modal">
            <Row justify="center">
                <Typography.Title level={4}>Thêm phiếu xuất/nhập kho</Typography.Title>
            </Row>
            <Form className="mt-3 px-5" form={form}
              name="addTableForm"
              onFinish={handleAddIngredient} >
                  <Row gutter={24}>
                    <Col span={24}>
                        <Form.Item name="branch" rules={[
                            {
                                required: true,
                                message: 'Vui lòng chọn chi nhánh!'
                            }
                        ]
                        }>
                            <AutoComplete placeholder="Chọn chi nhánh" >
                                {branches?.map((item: any) =>
                                    <AutoComplete.Option  key={item.id} value={item.name}>
                                       <Row key={item.id}
                                            onClick={() => handleBranchSelect(item)}
                                            >
                                            {item.name}
                                        </Row>
                                    </AutoComplete.Option>)}
                            </AutoComplete>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={24}>
                    <Col span={24}>
                        <Form.Item name="checktype" rules={[
                            {
                                required: true,
                                message: 'Vui lòng chọn loại phiếu!'
                            }
                        ]
                        }>
                            <AutoComplete placeholder="Chọn loại phiếu" >
                                {checkTypes?.map((item: any) =>
                                    <AutoComplete.Option  key={item.id} value={item.name}>
                                       <Row key={item.id}
                                            onClick={() => handleCheckTypeSelect(item.id)}
                                            >
                                            {item.name}
                                        </Row>
                                    </AutoComplete.Option>)}
                            </AutoComplete>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={24}>
                    <Col span={24}>
                        <Form.Item name="note" rules={[
                            {
                                required: false,
                            }
                        ]
                        }>
                            <TextArea  style={{ width: '100%' }} name="note" placeholder="Ghi chú"
                            onChange={(e) =>
                                setFormValue({ ...formValue, note: e.target.value })
                            }/>
                        </Form.Item>
                    </Col>
                </Row>
                <Row justify="center">
                    <Button  
                      loading={loading} 
                      htmlType="submit" type="primary">Thêm</Button>
                </Row>
            </Form>
        </Modal>
    )
}

export default AddCheckModal;
