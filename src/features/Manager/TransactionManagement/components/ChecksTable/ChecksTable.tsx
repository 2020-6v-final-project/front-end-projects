import {
  Button,
  Space,
  Table,
  Tag,Typography
} from "antd";
import {
  currencyFormat,
  shortDateFormat,
} from "@shared/utils/";
import {Finance} from '@custom-types';
import { ColumnsType } from "antd/lib/table";
import React, { useState, useEffect } from "react";
import "./ChecksTable.scss";


type ChecksTableProps = {
  data: Array<any>,
  handleRemoveCheck: Function,
  showAddRequestModal: Function,
  showDetailCheck: Function,
  financeList: Array<Finance>
};

const ChecksTable: React.FC<ChecksTableProps> = React.memo((props) => {
  const { financeList, data, showAddRequestModal, handleRemoveCheck, showDetailCheck } = props;
  const [filteredData, setFilteredData] = useState(data);
  useEffect(()=>{
      setFilteredData(data);
  },[data]);

  const columns: ColumnsType = [
    {
      align: "center",
      title: "Id",
      dataIndex: "id",
      key: "id",
      defaultSortOrder: 'ascend',
    },
    {
      align: "center",
      title: 'Tài khoản chi',
      dataIndex: 'accountid',
      key: 'accountid',
      render: (text: string) => <Typography>{financeList.find(finAcc => finAcc.id === Number(text))?.bankname} | {financeList.find(finAcc => finAcc.id === Number(text))?.accountnumber}</Typography>,
    },
    {
      align: "center",
      title: "Tổng chi",
      dataIndex: "debitamount",
      key: "debitamount",
      render: (text: number) => <Typography>{currencyFormat(text)}</Typography>,
    },
    {
      align: "center",
      title: "Ngày tạo",
      dataIndex: "createdat",
      key: "createdat",
      render: (text: Date) => <Typography>{shortDateFormat(text)}</Typography>,
    },
    {
      align: "center",
      title: "Người tạo",
      dataIndex: "createdby",
      key: "createdby",
    },
    {
      align: 'center',
      title: 'Trạng thái',
      key: 'statuscode',
      dataIndex: 'statuscode',
      render: (text: string) => 
      {
        if(text === "EDITING")
        return  <Tag color="magenta">Đang chỉnh sửa</Tag>
        if(text === "PENDINGAPPROVED")
        return  <Tag color="geekblue">Đang chờ duyệt</Tag>
        if(text === "REJECTED")
        return  <Tag color="red">Bị từ chối</Tag>
        return  <Tag color="success">Hoàn thành</Tag>
      }
    },
    {
      align: "center",
      title: "Thao tác",
      key: "action",
      render: (text: string, record: any) => (
        <Space size="middle">
          <Button
            onClick={(e) => {
              e.stopPropagation();
              e.preventDefault();
              handleRemoveCheck(record);
            }}
            danger
            size="small"
            disabled = {record?.statuscode === "EDITING" ? false : true}
          >
            Xóa
          </Button>
          <Button
            onClick={(e) => {
              e.stopPropagation();
              e.preventDefault();
              showAddRequestModal(record);
            }}
            type="primary"
            block
            size="small"
            disabled = {record?.statuscode === "EDITING" && record?.accountid ? false : true}
          >Gửi yêu cầu
          </Button>
        </Space>
      ),
    },
  ];

  return (
    <>
      <Table
        bordered
        size="small"
        className="ingredients-table"
        columns={columns}
        pagination={{ position: ["bottomCenter"] }}
        dataSource={filteredData}
        scroll={{ y: "55vh" }}
        onRow={(record:any) =>  { 
          return {
          onClick: event =>  showDetailCheck(record)
          };
        }}
      />
    </>
  );
}
);

export default ChecksTable;
