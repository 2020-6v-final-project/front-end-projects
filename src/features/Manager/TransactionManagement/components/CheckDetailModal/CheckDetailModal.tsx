import {
  CModal
} from '@coreui/react';
import React, { useEffect, useState } from 'react';
import {
  currencyFormat,
  shortDateFormat,
} from "@shared/utils/";
import { Row,Col,Form, Tag, Typography, Button, Input, Select} from 'antd';
import { Finance } from '@custom-types';
import './CheckDetailModal.scss';
import { Link } from "react-router-dom";

const { TextArea } = Input;


type CheckDetailModalState = {
  isShow: boolean,
  data: any,
  financeList: Array<Finance>
} 

type CheckDetailModalDispatch = {
  onClose: Function,
  handleUpdateCheckFinanceAccount: Function;
}

type CheckDetailModalDefaultProps = CheckDetailModalState & CheckDetailModalDispatch;

// { isShow, onClose, data ,props}
const CheckDetailModal: React.FC<CheckDetailModalDefaultProps> = React.memo((props) => {
  const {financeList, isShow, onClose, data, handleUpdateCheckFinanceAccount } = props;
  const [updateFinanceAccount, setUpdateFinanceAccount] = useState<boolean>(false);
  const [currentFinanceAccount, setCurrentFianceAccount] = useState<any>("");
  const [loading, setLoading] = useState<boolean>(false);
  const [form] = Form.useForm();
  const [formValue, setFormValue] = useState<any>({
    id:data?.id,
    note:"",
    fee:0,
  });

  useEffect(()=>{
    if(!updateFinanceAccount )
    {
      form.resetFields();
      setCurrentFianceAccount("");
    }
    if(!isShow){
      setUpdateFinanceAccount(false);
    }
  },[data,form,updateFinanceAccount,isShow,onClose]);

  const openAddFinanceAccount = () =>{
    setUpdateFinanceAccount(!updateFinanceAccount);
  };

  const handleChangeFinanceAccount = (value: number) => {
    setCurrentFianceAccount(financeList?.find(finance => finance.id === value));
  }

  const handleUpdateFinanceAccount = async () => {
    let sendData = {
      ...formValue, 
      id:data?.id,
      accountid:currentFinanceAccount.id,
    };
    if(sendData.fee === "") sendData.fee = 0;
    setLoading(true);
    await handleUpdateCheckFinanceAccount(sendData);
    setLoading(false);
    form.resetFields();
    setUpdateFinanceAccount(false);
  };

  const renderSwitchTag = (param:any) => {
    switch(param) {
      case "EDITING":
        return <Tag color="magenta">Đang chỉnh sửa</Tag>;
      case "PENDINGAPPROVED":
        return <Tag color="geekblue">Đang chờ duyệt</Tag>;
      case "REJECTED":
        return <Tag color="red">Bị từ chối</Tag>;
      default:
        return <Tag color="success">Hoàn thành</Tag>;
    }
  }


  return (
    <CModal
      centered
      backdrop
      size="xl"
      show={isShow}
      onClose={() => { onClose() }}
      className="CheckDetailModal-styles">
      <div style={{padding:"30px"}}>
        <Row justify="center" gutter={24}>
          <Col span={6} className="CheckDetailModal-row-title">
            <Typography.Text >Chi tiết phiếu chi</Typography.Text>
          </Col>
        </Row>
        <Row  justify="center" gutter={24}>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Người tạo</h6>
            <h5>{data?.createdby}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Ngày tạo</h6>
            <h5>{shortDateFormat(data?.createdat)}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Tổng chi phí</h6>
            <h5>{currencyFormat(data?.debitamount)}</h5>
          </Col>
          <Col span={4} className="BranchDetailModal-statistics-container">
            <h6 className="BranchDetailModal-light-text">Trạng thái</h6>
            {renderSwitchTag(data?.statuscode)}  
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={24} className="CheckDetailModal-row-title">
            <Typography.Text >Chỉnh sửa phiếu chi:</Typography.Text>
            <Button className="CheckDetailModal-button" onClick={()=>{openAddFinanceAccount()}} disabled={data?.statuscode !== "EDITING" ? true : false}>
              {updateFinanceAccount === false ? "Sửa" : "Hủy"}
              </Button>
          </Col>
        </Row>
        {updateFinanceAccount === true ?
          <div>
            <Form
              form={form}
              name="addFinanceAccountForm"
              onFinish={handleUpdateFinanceAccount}
            >
              <Row justify="center" style={{marginBottom:"20px"}}>
                      <Select 
                          style={{ width: "100%" }} 
                          placeholder="Chọn tài khoản chi"
                          onChange={handleChangeFinanceAccount}
                      >
                          {financeList?.map((fiance, index) => (
                          <Select.Option key={index} value={fiance.id}>{fiance.bankname} | {fiance.accountnumber}</Select.Option>
                          ))}
                      </Select> 
              </Row> 
              <Row gutter={24} justify="space-between">
                <Col span={24}>
                  <Form.Item
                    name="fee"
                    rules={[
                      {
                        required: false,
                      },
                    ]}
                  >
                    <Input type="number"  style={{ width: '100%' }} name="fee" placeholder="Phí"
                      onChange={(e) =>
                          setFormValue({ ...formValue, fee: e.target.value })
                      }/>
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={24} justify="space-between">
                <Col span={24}>
                  <Form.Item
                    name="note"
                    rules={[
                      {
                        required: false,
                      },
                    ]}
                  >
                    <TextArea  style={{ width: '100%' }} name="note" placeholder="Ghi chú"
                            onChange={(e) =>
                                setFormValue({ ...formValue, note: e.target.value })
                            }/>
                  </Form.Item>
                </Col>
              </Row>
              <Row justify="center">
                <Form.Item>
                  <Button 
                      loading={loading} type="primary" htmlType="submit">
                      Xác nhận
                  </Button>
                </Form.Item>
              </Row>
            </Form>
          </div>
          : null
        }
        <Row gutter={24}>
          <Col span={6} className="CheckDetailModal-row-sub-title">
            <Typography.Text >Tài khoản chi:</Typography.Text>
          </Col>
          <Col span={18} className="CheckDetailModal-row-text">
            <Typography.Text >{!data?.accountid ? "Chưa đặt khoản chi" :  (financeList.find(finAcc => finAcc.id === data?.accountid)?.bankname + " | " + financeList.find(finAcc => finAcc.id === data?.accountid)?.accountnumber)}</Typography.Text>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={6} className="CheckDetailModal-row-sub-title">
            <Typography.Text >Số dư trước chi:</Typography.Text>
          </Col>
          <Col span={18} className="CheckDetailModal-row-text">
            <Typography.Text >{data?.oldbalance ? currencyFormat(data.oldbalance) : "Phiếu chi chưa được duyệt"}</Typography.Text>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={6} className="CheckDetailModal-row-sub-title">
            <Typography.Text >Số dư sau chi:</Typography.Text>
          </Col>
          <Col span={18} className="CheckDetailModal-row-text">
            <Typography.Text >{data?.newbalance ? currencyFormat(data.newbalance) : "Phiếu chi chưa được duyệt"}</Typography.Text>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={6} className="CheckDetailModal-row-sub-title">
            <Typography.Text >Chi phí phát sinh:</Typography.Text>
          </Col>
          <Col span={18} className="CheckDetailModal-row-text">
            <Typography.Text >{data?.fee ? currencyFormat(data.fee) : "Chưa có phí phát sinh"}</Typography.Text>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={6} className="CheckDetailModal-row-sub-title">
            <Typography.Text >Ghi chú:</Typography.Text>
          </Col>
          <Col span={18} className="CheckDetailModal-row-text">
            <Typography.Text >{data?.note ? data?.note : "Chưa có ghi chú"}</Typography.Text>
          </Col>
        </Row>
        <Row gutter={24} style={{justifyContent:"center"}}>
          <Link to={`/receipt-detail/branch/${data?.branchid}/receipt/${data?.receiptid}`} >Xem chi tiết khoảng chi</Link>
        </Row>
      </div>
    </CModal>
  );
});

export default CheckDetailModal;
