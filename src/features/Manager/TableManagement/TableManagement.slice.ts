import { removeTable, getTableList, updateTable, addTable, reactivateTable, deactivateTable } from '@apis';
import { Table } from '@custom-types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { orderBy } from 'lodash';
import { AppThunk } from 'src/store';
import swal from 'sweetalert';

type TableManagementInitialState = {
    tableList: Array<Table>,
}

const initialState: TableManagementInitialState = {
    tableList: [],
}

const TableManagementSlice = createSlice({
    name: 'TableManagement',
    initialState,
    reducers: {
        getTablesSuccess: (state, action: PayloadAction<Table[]>) => {
            state.tableList = action.payload;
            state.tableList = orderBy(state.tableList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        getTablesFailed: (state) => {
            return state;
        },
        addTableSuccess: (state, action: PayloadAction<Table[]>) => {
            state.tableList = action.payload;
            state.tableList = orderBy(state.tableList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        addTableFailed: (state) => {
            return state;
        },
        removeTableSuccess: (state, action: PayloadAction<Table[]>) => {
            state.tableList = action.payload;
            state.tableList = orderBy(state.tableList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        removeTableFailed: (state) => {
            return state;
        },
        updateTableSuccess: (state, action: PayloadAction<Table>) => {
            const index = state.tableList?.findIndex(table => table.id === action.payload.id);
            if (index > -1) {
                state.tableList?.splice(index, 1, action.payload);
            }
        },
        updateTableFailed: (state) => {
            return state;
        },
        deactivateTableSuccess: (state, action: PayloadAction<Table[]>) => {
            state.tableList = action.payload;
            state.tableList = orderBy(state.tableList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        deactivateTableFailed: (state) => {
            return state;
        },
        reactivateTableSuccess: (state, action: PayloadAction<Table[]>) => {
            state.tableList = action.payload;
            state.tableList = orderBy(state.tableList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        reactivateTableFailed: (state) => {
            return state;
        },
    }

})

export const getTableAsync = (): AppThunk => async (dispatch) => {
    try {
        await getTableList((data: Table[]) => {
            dispatch(getTablesSuccess(data));
        });
    } catch (e) {
        dispatch(getTablesFailed());
    }
}

export const updateTableAsync = (table: any): AppThunk => async (dispatch) => {
    try {
        await updateTable(table, (data: Table) => {
            swal("Thành công", "Bàn đã được cập nhật!", "success");
            dispatch(updateTableSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(updateTableFailed());
    }
}

export const addTableAsync = (table: any): AppThunk => async (dispatch) => {
    try {
        await addTable(table, (data: Table[]) => {
            swal("Thành công", "Bàn đã được thêm!", "success");
            dispatch(addTableSuccess(data));
        });
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(addTableFailed());
    }
}

export const deactivateTableAsync = (table: any): AppThunk => async (dispatch) => {
    try {
        await deactivateTable(table, (data: Table[]) => {
            swal("Thành công", "Bàn đã bị khóa", "success");
            dispatch(deactivateTableSuccess(data));
        })
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(deactivateTableFailed());
    }
}

export const reactivateTableAsync = (table: any): AppThunk => async (dispatch) => {
    try {
        await reactivateTable(table, (data: Table[]) => {
            swal("Thành công", "Bàn đã được mở khóa", "success");
            dispatch(reactivateTableSuccess(data));
        })
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(reactivateTableFailed());
    }
}


export const removeTableAsync = (table: any): AppThunk => async (dispatch) => {
    try {
        await removeTable(table, (data: Table[]) => {
            swal("Thành công", "Bàn đã được xóa", "success");
            dispatch(removeTableSuccess(data));
        })
    } catch (e) {
        swal("Thất bại", "Vui lòng thử lại!", "error");
        dispatch(removeTableFailed());
    }
}

const {
    getTablesSuccess,
    getTablesFailed,
    addTableSuccess,
    addTableFailed,
    removeTableSuccess,
    removeTableFailed,
    updateTableSuccess,
    updateTableFailed,
    deactivateTableSuccess,
    deactivateTableFailed,
    reactivateTableSuccess,
    reactivateTableFailed,
} = TableManagementSlice.actions;

export {
    getTablesSuccess,
    getTablesFailed,
    addTableSuccess,
    addTableFailed,
    removeTableSuccess,
    removeTableFailed,
    updateTableSuccess,
    updateTableFailed,
    deactivateTableSuccess,
    deactivateTableFailed,
    reactivateTableSuccess,
    reactivateTableFailed,
};

export default TableManagementSlice.reducer;
