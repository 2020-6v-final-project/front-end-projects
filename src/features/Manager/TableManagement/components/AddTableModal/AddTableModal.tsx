import { Modal, AutoComplete, Form, Typography, Button, Row, Col, Input } from 'antd';
import React, { useState, useEffect } from 'react';
import './AddTableModal.scss';

type AddTableModalProps = {
    show: boolean,
    onFinish: Function,
    toggleModal: Function,
    branches: Array<any>,
}

const AddTableModal: React.FC<AddTableModalProps> = (props) => {
    const { branches, onFinish, show, toggleModal } = props;
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();
    const [formValue, setFormValue] = useState<any>({
        slots: 0,
        floor: "",
      });
    const [ tableBranch, setTableBranch ] = useState("") as any;
    const [ numberTableAdd, setNumberTableAdd ] = useState(0);
    useEffect(() => {
        if (!show) {
          form.resetFields();
          setTableBranch("");
        }
      }, [show, form]);

    const handleBranchSelect = (item: any) => {
        setTableBranch(item.id);
     };

    const handleAddTable = () => {
        const sendData = {
            ...formValue,
            branchid: tableBranch,
            countTable: numberTableAdd,
        };
        onFinish(sendData);
        form.resetFields();
        setTableBranch("");
        toggleModal();
    };

    return (
        <Modal visible={show} centered footer={false} onCancel={(e) => toggleModal()} zIndex={1} className="add-storage-item-modal">
            <Row justify="center">
                <Typography.Title level={4}>Thêm Bàn</Typography.Title>
            </Row>
            <Form className="mt-3 px-5" form={form}
              name="addTableForm"
              onFinish={() => {
                setLoading(true);
                handleAddTable();
                setLoading(false);
              }} >
                <Row gutter={24}>
                    <Col span={24}>
                    <Form.Item rules={[
                            {
                                required: true,
                                message: 'Vui lòng điền số lượng bàn!'
                            }
                        ]
                        }>
                            <Input style={{ width: '100%' }} name="count" placeholder="Số lượng bàn" min={0} type="number"
                            onChange={(e) =>
                                setNumberTableAdd(parseInt(e.target.value))
                            }/>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={24}>
                    <Col span={24}>
                        <Form.Item name="" rules={[
                            {
                                required: true,
                                message: 'Vui lòng chọn chi nhánh!'
                            }
                        ]
                        }>
                            <AutoComplete placeholder="Chọn chi nhánh" >
                                {branches?.map((item: any) =>
                                    <AutoComplete.Option  key={item.id} value={item.name}>
                                       <Row key={item.id}
                                            onClick={() => handleBranchSelect(item)}
                                            >
                                            {item.name}
                                        </Row>
                                    </AutoComplete.Option>)}
                            </AutoComplete>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={24} justify="center">
                    <Col span={12}>
                        <Form.Item rules={[
                            {
                                required: true,
                                message: 'Vui lòng điền số lượng chỗ!'
                            }
                        ]
                        }>
                            <Input style={{ width: '100%' }} name="count" placeholder="Số lượng chỗ" min={0} type="number"
                            value={formValue.slots ||""}
                            onChange={(e) =>
                                setFormValue({ ...formValue, slots: parseInt(e.target.value) })
                            }/>
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item rules={[
                            {
                                required: false,
                            }
                        ]
                        }>
                            <Input name="floor"  placeholder="Khu vực bàn" value={formValue.floor || ""}
                            onChange={(e) =>
                                setFormValue({ ...formValue, floor: e.target.value })
                            }/>
                        </Form.Item>
                    </Col>
                </Row>
                <Row justify="center">
                    <Button loading={loading}
                    onClick={() => {
                      setLoading(true);
                      onFinish();
                      setLoading(false);
                    }} htmlType="submit" type="primary">Thêm</Button>
                </Row>
            </Form>
        </Modal>
    )
}

export default AddTableModal;
