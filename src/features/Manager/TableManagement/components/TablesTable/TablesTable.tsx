import {
  Button,
  Col,
  // InputNumber,
  Row,
  Space,
  Table,
  Tag,
  Typography,
  AutoComplete,
  Input,
} from "antd";
import { ColumnsType } from "antd/lib/table";
import React, { useState, useEffect } from "react";
// import { Table as tb}  from '@custom-types';
import "./TablesTable.scss";

type TablesTableProps = {
  data: Array<any>;
  branches: Array<any>;
  handleUpdateTable: Function;
  handleDeleteTable: Function;
  handleDeactivateOrReactivateTable: Function;
};

const TablesTable: React.FC<TablesTableProps> = React.memo((props) => {
  const {
    branches,
    data,
    handleDeleteTable,
    // handleUpdateTable,
    handleDeactivateOrReactivateTable,
  } = props;
  const [filteredData, setFilteredData] = useState(data);
  const [autoComplete, setAutoComplete] = useState<string>("");

  useEffect(() => {
    setFilteredData(data);
  }, [data]);

  const handleBranchFilter = (value?: any) => {
    if (!value) {
      setFilteredData(data);
    } else {
      let temp = data.filter((item: any) => item?.branchid === value.id);
      setFilteredData(temp);
    }
  };

  const columns: ColumnsType = [
    {
      align: "center",
      title: "ID bàn",
      dataIndex: "id",
      key: "id",
      sorter: (a: any, b: any) => a.id - b.id,
      defaultSortOrder: "ascend",
    },
    {
      align: "center",
      title: "Chi nhánh",
      dataIndex: "branchid",
      key: "branchid",
      defaultSortOrder: "ascend",
      sorter: (a: any, b: any) => a.branchid - b.branchid,
      render: (text: string) => (
        <Typography>
          {branches.find((branch) => branch.id === Number(text))?.name}
        </Typography>
      ),
    },
    {
      align: "center",
      title: "Tên bàn",
      dataIndex: "name",
      key: "name",
      render: (text: string) => (
        <Typography.Text style={{ width: 64 }} editable>
          {" "}
          {text}{" "}
        </Typography.Text>
      ),
      // sorter: (a: any, b: any) => a.name.localeCompare(b.name),
    },
    {
      align: "center",
      title: "Số lượng chỗ",
      dataIndex: "slots",
      key: "slots",
      // render: (text: number) => <InputNumber onPressEnter={(e) => handleUpdateTable(e)} style={{ width: 64 }} min={0} type="number" value={text} />,
      // defaultSortOrder: 'ascend',
      // sorter: (a: any, b: any) => a.count - b.count,
    },
    {
      align: "center",
      title: "Khuc vực",
      dataIndex: "floor",
      key: "floor",
      render: (text: string) => <Typography>Tầng {text}</Typography>,
      // defaultSortOrder: 'ascend',
      // sorter: (a: any, b: any) => a.count - b.count,
    },
    {
      align: "center",
      title: "Trạng thái",
      key: "statuscode",
      dataIndex: "statuscode",
      render: (text: string) => (
        <Tag color={text === "AVAILABLE" ? "success" : "error"}>
          {text === "AVAILABLE" ? "Hoạt động" : "Vô hiệu"}
        </Tag>
      ),
    },
    {
      align: "center",
      title: "Thao tác",
      key: "action",
      render: (text: string, record: any) => (
        <Space size="middle">
          <Button
            onClick={(e) => handleDeleteTable(record)}
            danger
            size="small"
          >
            Xóa
          </Button>
          <Button
            onClick={(e) => {
              handleDeactivateOrReactivateTable(record, record.statuscode);
            }}
            type="primary"
            danger={record?.statuscode === "AVAILABLE" ? true : false}
            size="small"
          >
            {record?.statuscode === "AVAILABLE" ? "Khóa" : "Mở khóa"}
          </Button>
        </Space>
      ),
    },
  ];

  return (
    <>
      <Row>
        <Col span="10">
          <Row
            className="mb-4"
            justify="space-between"
            align="middle"
            gutter={16}
          >
            <Col span={12}>
              <Input
                style={{ width: "100%" }}
                placeholder="Tìm kiếm nguyên liệu"
              />
            </Col>
            <Col span={12}>
              <AutoComplete
                defaultValue="Tất cả"
                value={autoComplete}
                onChange={(value) => {
                  setAutoComplete(value);
                }}
                allowClear
                style={{ width: "100%" }}
                placeholder="Tất cả các kho của bạn"
                filterOption={(inputValue, option) =>
                  option!.value
                    .toUpperCase()
                    .indexOf(inputValue.toUpperCase()) !== -1
                }
              >
                <AutoComplete.Option value="Tất cả">
                  <Row onClick={() => handleBranchFilter()}>Tất cả</Row>
                </AutoComplete.Option>
                {branches?.map((item: any) => (
                  <AutoComplete.Option key={item.id} value={item.name}>
                    <Row key={item.id} onClick={() => handleBranchFilter(item)}>
                      {item.name}
                    </Row>
                  </AutoComplete.Option>
                ))}
              </AutoComplete>
            </Col>
          </Row>
        </Col>
      </Row>
      <Table
        bordered
        size="small"
        className="tables-table"
        columns={columns}
        pagination={{ position: ["bottomCenter"] }}
        dataSource={filteredData}
        scroll={{ y: "55vh" }}
      />
    </>
  );
});

export default TablesTable;
