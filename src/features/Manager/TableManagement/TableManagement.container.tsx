import { FAB } from "@components";
import { CFade } from "@coreui/react";
import { Branch, Table } from "@custom-types";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { RootState } from "src/store";
import { branchListSelector } from "../../SAC/BranchManagement/BranchManagement.selector";
import { fetchBranchListAsync } from "../../SAC/BranchManagement/BranchManagement.slice";
import AddTableModal from "./components/AddTableModal/AddTableModal";
import TablesTable from "./components/TablesTable/TablesTable";
import { selectTableList } from "./TableManagement.selector";
import {
  addTableAsync,
  deactivateTableAsync,
  getTableAsync,
  reactivateTableAsync,
  removeTableAsync,
  updateTableAsync,
} from "./TableManagement.slice";

type TableManagementState = {
  tableList: Array<Table>;
  branchList: Array<Branch>;
};

type TableManagementDispatch = {
  getTableAsync: Function;
  addTableAsync: Function;
  updateTableAsync: Function;
  reactivateTableAsync: Function;
  deactivateTableAsync: Function;
  removeTableAsync: Function;
  fetchBranchListAsync: Function;
};

type TableManagementDefaultProps = TableManagementState &
  TableManagementDispatch;

const TableManagementContainer: React.FC<TableManagementDefaultProps> = React.memo(
  (props) => {
    const {
      tableList,
      branchList,
      fetchBranchListAsync,
      addTableAsync,
      deactivateTableAsync,
      reactivateTableAsync,
      getTableAsync,
      // updateTableAsync,
      removeTableAsync,
    } = props;
    const [addModalVisible, toggleAddModal] = useState(false);
    useEffect(() => {
      getTableAsync();
      fetchBranchListAsync();
    }, [getTableAsync, fetchBranchListAsync]);

    const handleToggleAddModal = () => {
      toggleAddModal(!addModalVisible);
    };

    const handleUpdateTable = async (
      e: any,
      tableid: number,
      branchid: number
    ) => {
      // await updateTableAsync({"id":tableid, "slots":e});
    };

    const handleDeleteTable = async (table: Table) => {
      await removeTableAsync(table);
    };

    const handleAddTable = async (table: Table) => {
      await addTableAsync(table);
    };
    const handleDeactivateOrReactivateTable = async (
      table: Table,
      statusCode: string
    ) => {
      if (statusCode === "AVAILABLE") await deactivateTableAsync(table);
      else await reactivateTableAsync(table);
    };
    return (
      <CFade>
        <TablesTable
          branches={branchList}
          data={tableList}
          handleDeactivateOrReactivateTable={handleDeactivateOrReactivateTable}
          handleUpdateTable={handleUpdateTable}
          handleDeleteTable={handleDeleteTable}
        />
        <FAB callback={handleToggleAddModal} />
        <AddTableModal
          branches={branchList}
          show={addModalVisible}
          toggleModal={handleToggleAddModal}
          onFinish={handleAddTable}
        />
      </CFade>
    );
  }
);

const mapState = (state: RootState) => {
  return {
    tableList: selectTableList(state),
    branchList: branchListSelector(state),
  };
};

const mapDispatch = {
  fetchBranchListAsync,
  removeTableAsync,
  updateTableAsync,
  addTableAsync,
  reactivateTableAsync,
  deactivateTableAsync,
  getTableAsync,
};

export default connect(mapState, mapDispatch)(TableManagementContainer);
