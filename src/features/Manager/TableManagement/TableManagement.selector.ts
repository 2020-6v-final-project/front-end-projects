import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectTableList = createSelector(
    [(state: RootState) => state.table.tableList],
    (tableList) => tableList
)