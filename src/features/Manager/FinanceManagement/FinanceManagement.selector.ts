import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectFinanceList = createSelector(
    [(state: RootState) => state.finance.financeList],
    (financeList) => financeList
);