import { FAB } from "@components";
import { CFade } from "@coreui/react";
import { Branch, Check, Finance } from "@shared";
import { history } from "@shared/components";
import { Col, message, Row, Select, Tabs, Typography } from "antd";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { RootState } from "src/store";
import { selectBranchOfBranchManagementUserList } from "../../LoginPage/LoginPage.selector";
import { selectChecksList } from "../TransactionManagement/TransactionManagement.selector";
import { getChecksAsync } from "../TransactionManagement/TransactionManagement.slice";
import BranchFinanceAccountCarousel from "./components/BranchFinanceAccountCarousel/BranchFinanceAccountCarousel";
import { selectFinanceList } from "./FinanceManagement.selector";
import { getBranchFinancesAsync } from "./FinanceManagement.slice";
import FinanceStatistics from "./components/FinanceStatistics/FinanceStatistics";
import UpcomingCarousel from "./components/UpcomingCarousel/UpcomingCarousel";
import FinancePieCharts from "./components/FinancePieCharts/FinancePieCharts";
import FinanceLineChart from "./components/FinanceLineChart/FinanceLineChart";
import { Scrollbars } from "react-custom-scrollbars";

type FinanceManagementState = {
  financeList: Array<Finance>;
  match: any;
  branchListOfUser: Array<Branch>;
  checksList: Array<Check>;
};

type FinanceManagementDispatch = {
  getBranchFinancesAsync: Function;
  getChecksAsync: Function;
};

type FinanceManagementDefaultProps = FinanceManagementState &
  FinanceManagementDispatch;

const FinanceManagementContainer: React.FC<FinanceManagementDefaultProps> = React.memo(
  (props) => {
    const { branchListOfUser, financeList, match, checksList } = props;
    const { getBranchFinancesAsync, getChecksAsync } = props;
    const [currentBranch, setCurrentBranch] = useState<Branch | undefined>(
      branchListOfUser[0] || undefined
    );

    const [currentFinance, setcurrentFinance] = useState<Finance | undefined>(
      financeList[0] || undefined
    );
    const [addFinanceModalIsOpen, toggleAddFinanceModal] = useState(false);
    // const [financeDetailsModalIsOpen, toggleFinanceDetailsModal] = useState(false);
    console.log(checksList);
    useEffect(() => {
      const fetchData = async () => {
        if (currentBranch?.id) {
          await getBranchFinancesAsync(currentBranch.id);
          await getChecksAsync(currentBranch.id);
        }
      };
      fetchData();
    }, [getBranchFinancesAsync, currentBranch, getChecksAsync]);

    useEffect(() => {
      if (financeList.length > 0 && currentFinance) {
        setcurrentFinance(
          financeList.find((finance) => finance.id === currentFinance.id)
        );
      }
    }, [financeList, currentFinance]);

    useEffect(() => {
      if (financeList.length > 0) {
        if (!match.params.id) {
          setcurrentFinance(financeList[0]);
          history.push("/finance-management");
          return;
        }
        const target = financeList.find(
          (finance) => finance.id === Number(match.params.id)
        );
        if (target) {
          setcurrentFinance(target);
        } else {
          history.push("/finance-management");
          message.error("Không tìm thấy tài khoản!");
        }
      }
    }, [match.params.id, financeList]);

    // const handleFinanceSelect = (finance: Finance) => {
    //   setcurrentFinance(finance);
    //   history.push("/finance-management/" + finance.id);
    // };

    const handleChangeBranch = (value: number) => {
      setCurrentBranch(branchListOfUser?.find((branch) => branch.id === value));
    };
    return (
      <CFade>
        <Scrollbars autoHeightMin={0} autoHeightMax="90vh" autoHeight autoHide>
          <Row>
            <Select
              defaultValue={branchListOfUser[0]?.id}
              placeholder="Chọn chi nhánh"
              onChange={handleChangeBranch}
            >
              {branchListOfUser?.map((branch, index) => (
                <Select.Option key={index} value={branch.id}>
                  {branch.code} | {branch.name}
                </Select.Option>
              ))}
            </Select>
          </Row>
          <Row justify="center" align="middle">
            <Col>
              <BranchFinanceAccountCarousel />
            </Col>
          </Row>
          <Row className="my-3" justify="center" align="middle">
            <Col>
              <Tabs centered defaultActiveKey="1">
                <Tabs.TabPane key="1" tab="Tổng quan">
                  <FinanceStatistics />

                  <Row>
                    <Typography.Title level={4}>Sắp đến</Typography.Title>
                  </Row>

                  <Row justify="center" align="middle">
                    <Col>
                      <UpcomingCarousel />
                    </Col>
                  </Row>

                  <Row className="my-4">
                    <Col span={12}>
                      <Row justify="center">
                        <Col span={16}>
                          <Typography.Title
                            style={{ textAlign: "center" }}
                            level={4}
                          >
                            Thu hôm nay
                          </Typography.Title>
                          <FinancePieCharts />
                        </Col>
                      </Row>
                    </Col>
                    <Col span={12}>
                      <Row justify="center">
                        <Col span={16}>
                          <Typography.Title
                            style={{ textAlign: "center" }}
                            level={4}
                          >
                            Chi hôm nay
                          </Typography.Title>
                          <FinancePieCharts />
                        </Col>
                      </Row>
                    </Col>
                  </Row>

                  <Row justify="center">
                    <Col span={24}>
                      <Typography.Title
                        style={{ textAlign: "center" }}
                        level={4}
                      >
                        Thống kê thu chi
                      </Typography.Title>
                      <Row justify="center">
                        <Col span={22}>
                          <Tabs className="my-2" centered defaultActiveKey="1">
                            <Tabs.TabPane key="1" tab="Tuần">
                              <FinanceLineChart />
                            </Tabs.TabPane>
                            <Tabs.TabPane key="2" tab="Tháng">
                              <FinanceLineChart />
                            </Tabs.TabPane>
                            <Tabs.TabPane key="3" tab="Quý">
                              <FinanceLineChart />
                            </Tabs.TabPane>
                            <Tabs.TabPane key="4" tab="Năm">
                              <FinanceLineChart />
                            </Tabs.TabPane>
                          </Tabs>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Tabs.TabPane>
                <Tabs.TabPane key="2" tab="Chi tiết"></Tabs.TabPane>
              </Tabs>
            </Col>
          </Row>
        </Scrollbars>

        <FAB callback={() => toggleAddFinanceModal(!addFinanceModalIsOpen)} />
      </CFade>
    );
  }
);

const mapState = (state: RootState) => {
  return {
    financeList: selectFinanceList(state),
    branchListOfUser: selectBranchOfBranchManagementUserList(state),
    checksList: selectChecksList(state),
  };
};
const mapDispatch = {
  getBranchFinancesAsync,
  getChecksAsync,
};

export default connect(mapState, mapDispatch)(FinanceManagementContainer);
