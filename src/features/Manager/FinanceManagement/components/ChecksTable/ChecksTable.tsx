import {
  Table,
  Tag,Typography
} from "antd";
import {
  currencyFormat,
  shortDateFormat,
} from "@shared/utils/";
import {Finance} from '@custom-types';
import { ColumnsType } from "antd/lib/table";
import React, { useState, useEffect } from "react";
import "./ChecksTable.scss";
import { history } from "@shared/components";


type ChecksTableProps = {
  data: Array<any>,
  finance: Finance,
};

const ChecksTable: React.FC<ChecksTableProps> = React.memo((props) => {
  const {data} = props;
  const [filteredData, setFilteredData] = useState(data);
  useEffect(()=>{
      setFilteredData(data);
  },[data]);
  const columns: ColumnsType = [
    {
      align: "center",
      title: "Id",
      dataIndex: "id",
      key: "id",
      defaultSortOrder: 'ascend',
    },
    {
      align: "center",
      title: "Tổng chi",
      dataIndex: "debitamount",
      key: "debitamount",
      render: (text: number) => <Typography>{currencyFormat(text)}</Typography>,
    },
    {
      align: "center",
      title: "Số dư trước chi",
      dataIndex: "oldbalance",
      key: "oldbalance",
      render: (text: number) => <Typography>{currencyFormat(text)}</Typography>,
    },
    {
      align: "center",
      title: "Số dư sau chi",
      dataIndex: "newbalance",
      key: "newbalance",
      render: (text: number) => <Typography>{currencyFormat(text)}</Typography>,
    },
    {
      align: "center",
      title: "Ngày tạo",
      dataIndex: "createdat",
      key: "createdat",
      render: (text: Date) => <Typography>{shortDateFormat(text)}</Typography>,
    },
    {
      align: "center",
      title: "Người tạo",
      dataIndex: "createdby",
      key: "createdby",
    },
    {
      align: 'center',
      title: 'Trạng thái',
      key: 'statuscode',
      dataIndex: 'statuscode',
      render: (text: string) => 
      {
        if(text === "EDITING")
        return  <Tag color="magenta">Đang chỉnh sửa</Tag>
        if(text === "PENDINGAPPROVED")
        return  <Tag color="geekblue">Đang chờ duyệt</Tag>
        if(text === "REJECTED")
        return  <Tag color="red">Bị từ chối</Tag>
        return  <Tag color="success">Hoàn thành</Tag>
      }
    },
  ];

  return (
    <>
      <Table
        bordered
        size="small"
        className="ingredients-table"
        columns={columns}
        pagination={{ position: ["bottomCenter"] }}
        dataSource={filteredData}
        onRow={(record:any) =>  { 
          return {
          onClick: event =>  history.push( `/transaction-management/${record.id}`)
          };
        }}
        scroll={{ y: "55vh" }}
      />
    </>
  );
}
);
export default ChecksTable;
