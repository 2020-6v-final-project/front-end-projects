import React, { CSSProperties } from "react";
import { Col, Row, Typography, Avatar } from "antd";
import "./FinanceFromList.scss";
import { Finance } from "@custom-types";

type FinanceFromListProps = {
  finance: Finance;
};

const CSSStyle: CSSProperties = {
  textOverflow: "ellipsis",
  overflow: "hidden",
  whiteSpace: "nowrap",
};

const FinanceFromList: React.FC<FinanceFromListProps> = React.memo((props) => {
  const { finance } = props;
  return (
    <Row
      className="finance-from-list"
      justify="start"
      gutter={24}
      align="middle"
      style={{
        width: "100%",
      }}
      wrap={false}
    >
      <Col>
          <Avatar shape="square" style={{ width: 64, height: 64 }}  
          src={!finance?.bankname ? "../bank.jpg" : (finance.bankname === "CASH" ? "../cash.jpg" : `../${finance.bankname.toLocaleLowerCase()}.jpg`)}/>
        </Col>
      <Col  style={{ ...CSSStyle }}>
        <Typography.Title style={{ ...CSSStyle }} level={3}>
          {finance?.bankname}
        </Typography.Title>
      </Col>
    </Row>
  );
});

export default FinanceFromList;
