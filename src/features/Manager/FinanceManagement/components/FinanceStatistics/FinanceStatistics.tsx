import { ChartBarSimple, ChartLineSimple } from "@components";
import { CFade, CWidgetSimple } from "@coreui/react";
import { Col, Row } from "antd";
import React from "react";
import "./FinanceStatistics.scss";

const FinanceStatistics: React.FC = (props) => {
  return (
    <CFade>
      <Row className="my-3" gutter={24} justify="space-between" align="middle">
        <Col span={4}>
          <CWidgetSimple header="title" text="1,123">
            <ChartLineSimple
              className="finance-management-small-card"
              borderColor="danger"
            />
          </CWidgetSimple>
        </Col>
        <Col span={4}>
          <CWidgetSimple header="title" text="1,123">
            <ChartLineSimple
              className="finance-management-small-card"
              borderColor="primary"
            />
          </CWidgetSimple>
        </Col>
        <Col span={4}>
          <CWidgetSimple header="title" text="1,123">
            <ChartLineSimple
              className="finance-management-small-card"
              borderColor="success"
            />
          </CWidgetSimple>
        </Col>
        <Col span={4}>
          <CWidgetSimple header="title" text="1,123">
            <ChartBarSimple
              className="finance-management-small-card"
              backgroundColor="danger"
            />
          </CWidgetSimple>
        </Col>
        <Col span={4}>
          <CWidgetSimple header="title" text="1,123">
            <ChartBarSimple
              className="finance-management-small-card"
              backgroundColor="primary"
            />
          </CWidgetSimple>
        </Col>
        <Col span={4}>
          <CWidgetSimple header="title" text="1,123">
            <ChartBarSimple
              className="finance-management-small-card"
              backgroundColor="success"
            />
          </CWidgetSimple>
        </Col>
      </Row>
    </CFade>
  );
};

export default FinanceStatistics;
