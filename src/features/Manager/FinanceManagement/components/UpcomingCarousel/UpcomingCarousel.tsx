import React, { CSSProperties } from "react";
import { Carousel, Col, Row, Typography, Avatar } from "antd";
import { ArrowRightOutlined, ArrowLeftOutlined } from "@ant-design/icons";

const CardStyle: CSSProperties = {
  width: 380,
  height: 120,
  border: "1px solid #ececec",
  borderRadius: 20,
  margin: 0,
  marginRight: 310,
};

const UpcomingCarousel: React.FC = (props) => {
  return (
    <Carousel
      style={{ width: "90vw" }}
      slidesToScroll={1}
      slidesPerRow={3}
      arrows
      nextArrow={<ArrowRightOutlined />}
      prevArrow={<ArrowLeftOutlined />}
      className="my-4 py-0"
      swipe
      swipeToSlide
      dots
    >
      <div>
        <Row style={CardStyle} justify="center" align="middle" gutter={24}>
          <Col>
            <Avatar src="/item-sample-large.jpg" size={50} />
          </Col>
          <Col>
            <Typography.Title className="my-1" level={5}>
              Phí thuê mặt bằng
            </Typography.Title>
            <Typography.Title
              className="my-1"
              level={5}
              style={{ fontWeight: 300 }}
            >
              24 tháng 02, 2021
            </Typography.Title>
          </Col>
          <Col>
            <Row style={{ textAlign: "end" }} justify="end" align="middle">
              <Col>
                <Typography.Title
                  style={{ fontWeight: 400, color: "#dc3545" }}
                  level={5}
                >
                  25,000,000đ
                </Typography.Title>
                <Typography.Text>15:00</Typography.Text>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
      <div>
        <Row style={CardStyle} justify="center" align="middle" gutter={24}>
          <Col>
            <Avatar src="/item-sample-large.jpg" size={50} />
          </Col>
          <Col>
            <Typography.Title className="my-1" level={5}>
              Phí thuê mặt bằng
            </Typography.Title>
            <Typography.Title
              className="my-1"
              level={5}
              style={{ fontWeight: 300 }}
            >
              24 tháng 02, 2021
            </Typography.Title>
          </Col>
          <Col>
            <Row style={{ textAlign: "end" }} justify="end" align="middle">
              <Col>
                <Typography.Title
                  style={{ fontWeight: 400, color: "#dc3545" }}
                  level={5}
                >
                  25,000,000đ
                </Typography.Title>
                <Typography.Text>15:00</Typography.Text>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
      <div>
        <Row style={CardStyle} justify="center" align="middle" gutter={24}>
          <Col>
            <Avatar src="/item-sample-large.jpg" size={50} />
          </Col>
          <Col>
            <Typography.Title className="my-1" level={5}>
              Phí thuê mặt bằng
            </Typography.Title>
            <Typography.Title
              className="my-1"
              level={5}
              style={{ fontWeight: 300 }}
            >
              24 tháng 02, 2021
            </Typography.Title>
          </Col>
          <Col>
            <Row style={{ textAlign: "end" }} justify="end" align="middle">
              <Col>
                <Typography.Title
                  style={{ fontWeight: 400, color: "#dc3545" }}
                  level={5}
                >
                  25,000,000đ
                </Typography.Title>
                <Typography.Text>15:00</Typography.Text>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
      <div>
        <Row style={CardStyle} justify="center" align="middle" gutter={24}>
          <Col>
            <Avatar src="/item-sample-large.jpg" size={50} />
          </Col>
          <Col>
            <Typography.Title className="my-1" level={5}>
              Phí thuê mặt bằng
            </Typography.Title>
            <Typography.Title
              className="my-1"
              level={5}
              style={{ fontWeight: 300 }}
            >
              24 tháng 02, 2021
            </Typography.Title>
          </Col>
          <Col>
            <Row style={{ textAlign: "end" }} justify="end" align="middle">
              <Col>
                <Typography.Title
                  style={{ fontWeight: 400, color: "#dc3545" }}
                  level={5}
                >
                  25,000,000đ
                </Typography.Title>
                <Typography.Text>15:00</Typography.Text>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </Carousel>
  );
};

export default UpcomingCarousel;
