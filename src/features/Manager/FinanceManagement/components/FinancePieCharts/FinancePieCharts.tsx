import React from "react";
import { Pie } from "@ant-design/charts";
import { PieConfig } from "@ant-design/charts/es/pie";

const FinancePieCharts: React.FC = (props) => {
  var data = [
    {
      type: "Ví dụ 1",
      value: 27,
    },
    {
      type: "Ví dụ 2",
      value: 25,
    },
    {
      type: "Ví dụ 3",
      value: 18,
    },
    {
      type: "Ví dụ 4",
      value: 15,
    },
    {
      type: "Ví dụ 5",
      value: 10,
    },
    {
      type: "Ví dụ 6",
      value: 5,
    },
  ];
  const config: PieConfig = {
    appendPadding: 10,
    data: data,
    angleField: "value",
    colorField: "type",
    radius: 1,
    innerRadius: 0.5,
    meta: {
      value: {
        formatter: function formatter(v: any) {
          return "".concat(v, " %");
        },
      },
    },
    label: {
      type: "inner",
      offset: "-50%",
      style: { textAlign: "center" },
      autoRotate: false,
      content: "{value}",
    },
    interactions: [
      { type: "element-selected" },
      { type: "element-active" },
      { type: "pie-statistic-active" },
    ],
  };
  return <Pie {...config} />;
};

export default FinancePieCharts;
