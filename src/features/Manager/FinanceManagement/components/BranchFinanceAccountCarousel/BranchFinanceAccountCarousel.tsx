import React, { CSSProperties } from "react";
import { Carousel, Col, Row, Typography } from "antd";
import { ArrowRightOutlined, ArrowLeftOutlined } from "@ant-design/icons";
import Avatar from "antd/lib/avatar/avatar";

const CardStyle: CSSProperties = {
  width: 300,
  height: 150,
  border: "1px solid #ececec",
  borderRadius: 20,
  margin: 0,
  marginRight: 310,
};

const BranchFinanceAccountCarousel: React.FC = (props) => {
  return (
    <Carousel
      style={{ width: "90vw" }}
      slidesToScroll={1}
      slidesPerRow={4}
      arrows
      nextArrow={<ArrowRightOutlined />}
      prevArrow={<ArrowLeftOutlined />}
      className="my-4 py-2"
      variableWidth
      swipe
      swipeToSlide
      dots
    >
      <div>
        <Row style={CardStyle} justify="center" align="middle" gutter={24}>
          <Col>
            <Avatar src="/item-sample-large.jpg" size={64} />
          </Col>
          <Col>
            <Typography.Title className="my-1" level={5}>
              Account 1
            </Typography.Title>
            <Typography.Title
              className="my-1"
              level={5}
              style={{ fontWeight: 300 }}
            >
              25,000,000đ
            </Typography.Title>
          </Col>
        </Row>
      </div>
      <div>
        <Row style={CardStyle} justify="center" align="middle" gutter={24}>
          <Col>
            <Avatar src="/item-sample-large.jpg" size={64} />
          </Col>
          <Col>
            <Typography.Title className="my-1" level={5}>
              Account 1
            </Typography.Title>
            <Typography.Title
              className="my-1"
              level={5}
              style={{ fontWeight: 300 }}
            >
              25,000,000đ
            </Typography.Title>
          </Col>
        </Row>
      </div>
      <div>
        <Row style={CardStyle} justify="center" align="middle" gutter={24}>
          <Col>
            <Avatar src="/item-sample-large.jpg" size={64} />
          </Col>
          <Col>
            <Typography.Title className="my-1" level={5}>
              Account 1
            </Typography.Title>
            <Typography.Title
              className="my-1"
              level={5}
              style={{ fontWeight: 300 }}
            >
              25,000,000đ
            </Typography.Title>
          </Col>
        </Row>
      </div>
      <div>
        <Row style={CardStyle} justify="center" align="middle" gutter={24}>
          <Col>
            <Avatar src="/item-sample-large.jpg" size={64} />
          </Col>
          <Col>
            <Typography.Title className="my-1" level={5}>
              Account 1
            </Typography.Title>
            <Typography.Title
              className="my-1"
              level={5}
              style={{ fontWeight: 300 }}
            >
              25,000,000đ
            </Typography.Title>
          </Col>
        </Row>
      </div>
      <div>
        <Row style={CardStyle} justify="center" align="middle" gutter={24}>
          <Col>
            <Avatar src="/item-sample-large.jpg" size={64} />
          </Col>
          <Col>
            <Typography.Title className="my-1" level={5}>
              Account 1
            </Typography.Title>
            <Typography.Title
              className="my-1"
              level={5}
              style={{ fontWeight: 300 }}
            >
              25,000,000đ
            </Typography.Title>
          </Col>
        </Row>
      </div>
      <div>
        <Row style={CardStyle} justify="center" align="middle" gutter={24}>
          <Col>
            <Avatar src="/item-sample-large.jpg" size={64} />
          </Col>
          <Col>
            <Typography.Title className="my-1" level={5}>
              Account 1
            </Typography.Title>
            <Typography.Title
              className="my-1"
              level={5}
              style={{ fontWeight: 300 }}
            >
              25,000,000đ
            </Typography.Title>
          </Col>
        </Row>
      </div>
    </Carousel>
  );
};

export default BranchFinanceAccountCarousel;
