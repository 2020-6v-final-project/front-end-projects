import { Finance,Check } from "@custom-types";
import {
  Tabs,
  Avatar,
  Col,
  Statistic,
  Empty,
  Row,
  Typography,
  Divider
} from "antd";
import {
  currencyFormat,
} from "@shared/utils/";
import React from "react";
import { Scrollbars } from "react-custom-scrollbars";
import "./MainFinancePanel.scss";
import  ChecksTable from '../ChecksTable/ChecksTable'; 

type MainFinancePanelProps = {
  finance: Finance | undefined;
  checksList: Array<Check>;
};

const MainFinancePanel: React.FC<MainFinancePanelProps> = React.memo((props) => {
  const {
    finance, checksList
  } = props;
  return (
    <Col span={17} className="main-finance-panel">
      {!finance ? (
        <Row justify="center">
          <Empty
            style={{ marginTop: "36vh" }}
            description="Tạo hoặc chọn nhà cung cấp để xem thông tin!"
          />
        </Row>
      ) : (
        <Scrollbars
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              className="track-horizontal"
              style={{ display: "none" }}
            />
          )}
          renderThumbHorizontal={(props) => (
            <div
              {...props}
              className="thumb-horizontal"
              style={{ display: "none" }}
            />
          )}
          autoHide
        >
          <Row justify="end" gutter={16}>
          </Row>
          <Row className="mb-5" justify="center" align="middle">
            <Col span={12}>
              <Row justify="center">
                <Avatar shape="square"
                  src={!finance?.bankname ? "../bank.jpg" : (finance.bankname === "CASH" ? "../cash.jpg" : `../${finance.bankname.toLocaleLowerCase()}.jpg`)}
                  className="finance-cover-image"
                />
              </Row>
            </Col>
            <Col span={12}>
              <Typography.Title level={1}>{finance?.bankname}</Typography.Title>  
            </Col>
          </Row>
          <Row justify="space-between" className="mb-5" gutter={24}>
            <Col span={4}>
              <Statistic title="Số dư hiện tại" value={currencyFormat(finance?.currentbalance)} />
            </Col>
            <Col span={1}>
              <Divider style={{ height: "100%" }} type="vertical" />
            </Col>
            <Col span={4}>
              <Statistic title="Số tài khoản" value={finance?.accountnumber} />
            </Col>
            <Col span={1}>
              <Divider style={{ height: "100%" }} type="vertical" />
            </Col>
            <Col span={4}>
              <Statistic title="Loại tài khoản" value={finance?.isbankaccount === true ? "TK Ngân hàng" : "TK Tiền mặt"} />
            </Col>
            <Col span={1}>
              <Divider style={{ height: "100%" }} type="vertical" />
            </Col>
            <Col span={4}>
              <Statistic
                title="Trạng thái"
                value={
                  finance?.statuscode === "AVAILABLE" ? "Hoạt động" : "Vô hiệu"
                }
                valueStyle={
                  finance?.statuscode === "AVAILABLE"
                    ? { color: "#3f8600" }
                    : { color: "#cf1322" }
                }
              />
            </Col>
          </Row>
          <Tabs centered defaultActiveKey="1" style={{ height: "65vh", width: "100%", padding: "0px" }}>
            <Tabs.TabPane tab="Khoản chi" key="1">
            <ChecksTable 
                finance={finance}
                data={checksList.filter(check => check.accountid === finance.id)}      
            />
            </Tabs.TabPane>
            <Tabs.TabPane tab="Khoản thu" key="2">

            </Tabs.TabPane>
          </Tabs>
        </Scrollbars>
      )}
    </Col>
  );
});

export default MainFinancePanel;
