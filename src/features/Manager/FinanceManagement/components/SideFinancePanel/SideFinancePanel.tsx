import { Finance } from "@shared";
import {
  Col,
  Divider,
  Empty,
  List,
  Row,
  Spin,
} from "antd";
import React, { useEffect, useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import FinanceFromList from "../FinanceFromList/FinanceFromList";
import "./SideFinancePanel.scss";

type FinanceSidePanelProps = {
  finances: Array<Finance>;
  currentFinance?: Finance;
};

type FinanceSidePanelDispatch = {
  handleFinanceSelect: Function;
};

type FinanceSidePanelDefaultProps = FinanceSidePanelProps & FinanceSidePanelDispatch;

const SidePanel: React.FC<FinanceSidePanelDefaultProps> = React.memo((props) => {
  const { finances, currentFinance } = props;
  const { handleFinanceSelect } = props;
  const [loading, setLoading] = useState(true);
  const [filtered, setFiltered] = useState(finances);
  const [shouldRender, setShouldRender] = useState(false);

  useEffect(() => {
    if (finances.length < 1) {
      setLoading(false);
      setShouldRender(false);
    } else {
      setLoading(false);
      setFiltered(finances);
      setShouldRender(true);
    }
  }, [finances]);

  // const handleSearch = (value: string) => {
  //   if (!value) {
  //     setFiltered(finances);
  //   } else {
  //     const temp = finances.filter(
  //       (finance) =>
  //         finance.name.toLocaleLowerCase().includes(value.toLowerCase())
  //     );
  //     setFiltered(temp);
  //   }
  // };

  return (
    <Col span={7} style={{ height: "90vh" }} className="side-menu">
      {/* <Row justify="space-between" align="middle">
        <Input
          onChange={(e) => {
            handleSearch(e.target.value);
          }}
          className="mb-3"
          placeholder="Tìm kiếm nhà cung cấp"
        ></Input>
      </Row> */}

        <Row style={{ height: "65vh", padding: "0px" }} justify="center">
          <Col span={23}>
            {loading ? (
              <Spin
                size="large"
                style={{ textAlign: "center", margin: "auto" }}
                spinning={loading}
              />
            ) : (
              <>
                {shouldRender ? (
                  <>
                    <Scrollbars>
                      <List
                        className="list-finance"
                        style={{ marginBottom: "176px" }}
                      >
                        {filtered.map((item, index) => (
                            <List.Item
                              className={
                                currentFinance?.id === item.id
                                  ? "active-finance-from-list"
                                  : ""
                              }
                              onClick={() => handleFinanceSelect(item)}
                            >
                              <FinanceFromList finance={item} />
                            </List.Item>
                        ))}
                      </List>
                    </Scrollbars>
                  </>
                ) : (
                  <Col span={24}>
                    <Empty
                      style={{ marginTop: "30vh" }}
                      description="Hiện chưa có nhà cung cấp nào!"
                    />
                  </Col>
                )}
              </>
            )}
          </Col>
          <Col span={1}>
            <Divider type="vertical" style={{ height: "100%" }} />
          </Col>
        </Row>
    </Col>
  );
});

export default SidePanel;
