import { 
    getBranchFinancesList, 
} from '@apis';
import { Finance } from '@custom-types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { orderBy } from 'lodash';
import { AppThunk } from 'src/store';
import swal from 'sweetalert';

type FinanceManagementInitialState = {
    financeList: Array<Finance>,
}

const initialState: FinanceManagementInitialState = {
    financeList: [],
}

const FinanceManagementSlice = createSlice({
    name: 'financeManagement',
    initialState,
    reducers: {
        getBranchFinancesSuccess: (state, action: PayloadAction<Finance[]>) => {
            state.financeList = action.payload;
            state.financeList = orderBy(state.financeList, ["isprimary", "name"], ["desc", "asc"]);
            return state;
        },
        getBranchFinancesFailed: (state) => {
            return state;
        },
    }

})

export const getBranchFinancesAsync = (branchid:number): AppThunk => async (dispatch) => {
    try {
        await getBranchFinancesList(branchid,(data: Finance[]) => {
            dispatch(getBranchFinancesSuccess(data));
        });
    } catch (e) {
        swal("Lấy danh sách tài khoản tài chính thất bại", e.response?.data?.message ? e.response?.data?.message : "Vui lòng thử lại!", "error");
        dispatch(getBranchFinancesFailed());
    }
}

const {
    getBranchFinancesSuccess,
    getBranchFinancesFailed,
} = FinanceManagementSlice.actions;

export default FinanceManagementSlice.reducer;
