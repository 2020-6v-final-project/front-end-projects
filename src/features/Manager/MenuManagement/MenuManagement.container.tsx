import { history } from "@components";
import { CFade } from "@coreui/react";
import { Branch, Item, ItemType, Menu, Option } from "@shared";
import { Button, Col, message, Row, Select, Typography } from "antd";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { RootState } from "src/store";
import { selectBranchOfBranchManagementUserList } from "../../LoginPage/LoginPage.selector";
import { updateBranchAsync } from "../../SAC/BranchManagement/BranchManagement.slice";
import { selectItemTypeList } from "../../SAC/ItemIngredientTypeManagement/ItemIngredientTypeManagement.selector";
import { getItemTypeAsync } from "../../SAC/ItemIngredientTypeManagement/ItemIngredientTypeManagement.slice";
import {
  selectItemList,
  selectOptionList,
} from "../../SAC/ItemManagement/ItemManagement.selector";
import {
  getItemAsync,
  getOptionsAsync,
} from "../../SAC/ItemManagement/ItemManagement.slice";
import { selectMenuList } from "../../SAC/MenuManagement/MenuManagement.selector";
import {
  getMenuAsync,
  requestNewMenuAsyns,
} from "../../SAC/MenuManagement/MenuManagement.slice";
import MainMenuPanel from "./components/MainMenuPanel/MainMenuPanel";
import RequestNewMenuModal from "./components/RequestNewMenuModal/RequestNewMenuModal";
import SideMenuPanel from "./components/SideMenuPanel/SideMenuPanel";

type MenuManagementState = {
  menuList: Array<Menu>;
  itemList: Array<Item>;
  itemTypeList: Array<ItemType>;
  optionList: Array<Option>;
  branchListOfUser: Array<Branch>;
  match: any;
};

type MenuManagementDispatch = {
  getMenuAsync: Function;
  getItemAsync: Function;
  getItemTypeAsync: Function;
  updateBranchAsync: Function;
  requestNewMenuAsyns: Function;
  getOptionsAsync: Function;
};

type MenuManagementDefaultProps = MenuManagementState & MenuManagementDispatch;

const MenuManagementContainer: React.FC<MenuManagementDefaultProps> = React.memo(
  (props) => {
    const {
      itemTypeList,
      menuList,
      itemList,
      branchListOfUser,
      optionList,
      getItemAsync,
      getMenuAsync,
      getItemTypeAsync,
      updateBranchAsync,
      requestNewMenuAsyns,
      getOptionsAsync,
      match,
    } = props;

    const [currentMenu, setCurrentMenu] = useState<Menu | undefined>(
      menuList[0] || undefined
    );
    const [currentBranch, setCurrentBranch] = useState<Branch | undefined>(
      branchListOfUser[0] || undefined
    );

    const [requestNewMenuModalOpen, toggleMenuDetailsModal] = useState(false);

    useEffect(() => {
      const fetchData = async () => {
        await getItemTypeAsync();
        await getOptionsAsync();
        await getItemAsync();
        await getMenuAsync();
      };
      fetchData();
    }, [getMenuAsync, getItemAsync, getItemTypeAsync, getOptionsAsync]);

    useEffect(() => {
      if (menuList.length > 0 && currentMenu) {
        setCurrentMenu(menuList.find((item) => item.id === currentMenu.id));
      }
    }, [menuList, currentMenu]);

    useEffect(() => {
      if (branchListOfUser.length > 0) {
        setCurrentBranch(branchListOfUser[0]);
      }
    }, [branchListOfUser]);

    useEffect(() => {
      if (menuList.length > 0) {
        if (!match.params.id) {
          setCurrentMenu(menuList[0]);
          history.push("/menu-branch-management");
          return;
        }
        const target = menuList.find(
          (menu) => menu.id === Number(match.params.id)
        );
        if (target) {
          setCurrentMenu(target);
        } else {
          history.push("/menu-branch-management");
          message.error("Không tìm thấy thực đơn!");
        }
      }
    }, [match.params.id, menuList]);

    const handleMenuSelect = (menu: any) => {
      setCurrentMenu(menu);
      history.push("/menu-branch-management/" + menu.id);
    };

    const handleChangeBranch = (value: number) => {
      setCurrentBranch(branchListOfUser?.find((branch) => branch.id === value));
    };

    const handleSetMainMenu = async () => {
      await updateBranchAsync({
        id: currentBranch?.id,
        mainmenu: currentMenu?.id,
      });
    };

    return (
      <CFade>
        <div style={{ overflow: "hidden" }}>
          <Row gutter={24} justify="center" className="mb-3">
            <Col>
              <Row gutter={12}>
                <Col>
                  <Typography.Title level={4}>Chi nhánh</Typography.Title>
                </Col>
                <Col>
                  <Select
                    defaultValue={branchListOfUser[0]?.id}
                    style={{ width: 300 }}
                    placeholder="Chọn chi nhánh"
                    onChange={handleChangeBranch}
                  >
                    {branchListOfUser?.map((branch, index) => (
                      <Select.Option key={index} value={branch.id}>
                        {branch.code} | {branch.name}
                      </Select.Option>
                    ))}
                  </Select>
                </Col>
              </Row>
            </Col>
            <Col>
              <Link
                to={
                  "/menu-branch-management/" +
                  menuList?.find((menu) => menu.id === currentBranch?.mainmenu)
                    ?.id
                }
              >
                <Button>
                  Thực đơn chính:{" "}
                  {
                    menuList?.find(
                      (menu) => menu.id === currentBranch?.mainmenu
                    )?.name
                  }
                </Button>
              </Link>
            </Col>
          </Row>
          <Row gutter={48}>
            <SideMenuPanel
              currentMenu={currentMenu}
              menus={menuList}
              handleMenuSelect={handleMenuSelect}
            />
            <MainMenuPanel
              menu={currentMenu}
              itemList={itemList}
              itemTypeList={itemTypeList}
              currentBranch={currentBranch}
              handleRequestNewMenuButtonClick={() =>
                toggleMenuDetailsModal(!requestNewMenuModalOpen)
              }
              handleSetMainMenu={handleSetMainMenu}
            />
          </Row>
        </div>

        <RequestNewMenuModal
          show={requestNewMenuModalOpen}
          onFinish={requestNewMenuAsyns}
          toggleModal={() => toggleMenuDetailsModal(!requestNewMenuModalOpen)}
          menu={currentMenu}
          itemList={itemList}
          optionList={optionList}
          branchId={currentBranch?.id}
        />
      </CFade>
    );
  }
);

const mapState = (state: RootState) => {
  return {
    menuList: selectMenuList(state),
    itemList: selectItemList(state),
    itemTypeList: selectItemTypeList(state),
    branchListOfUser: selectBranchOfBranchManagementUserList(state),
    optionList: selectOptionList(state),
  };
};
const mapDispatch = {
  getItemAsync,
  getMenuAsync,
  getItemTypeAsync,
  updateBranchAsync,
  requestNewMenuAsyns,
  getOptionsAsync,
};

export default connect(mapState, mapDispatch)(MenuManagementContainer);
