import { Item, Menu, ItemType, Branch } from "@custom-types";
import {
  Avatar,
  Button,
  Col,
  Input,
  Row,
  Typography,
  Empty,
  Statistic,
  Divider,
  Select,
} from "antd";
import React, { useEffect, useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import ItemFromRow from "../ItemFromRow/ItemFromRow";
import "./MainMenuPanel.scss";

type MainMenuPanelProps = {
  menu: Menu | undefined;
  currentBranch: Branch | undefined;
  itemList: Array<Item>;
  itemTypeList: Array<ItemType>;
  handleRequestNewMenuButtonClick: Function;
  handleSetMainMenu: Function;
};

const countItemType = (itemTypeList: ItemType[], itemList: Item[]) => {
  const typeIdsOfItemList = itemList.map(item => item.typeid);
  return itemTypeList.filter(itemType => typeIdsOfItemList.includes(itemType.id));
}

const statusTextAndColor = (statusCode: string): {text: string, color: string} => {
  switch (statusCode) {
    case "AVAILABLE": 
      return {
        text: "Hoạt động",
        color: "#a5ca34"
      }
    case "DISABLED": 
      return {
        text: "Vô Hiệu",
        color: "#f581c4"
      }
    case "PENDINGAPPROVED": 
      return {
        text: "Chờ duyệt",
        color: "#fa9665"
      }
    default:
      return {
        text: "NaN",
        color: "#a5ca34"
      }
  }
}

const handleSetMainMenuButtonLogic = (statusCode: string, menu?: Menu, branch?: Branch) => {
  if (menu?.id === branch?.mainmenu) {
    return {
      isDisabled: true,
      text: "Menu chính của chi nhánh",
    }
  } else {
    if (statusCode === "AVAILABLE") return {
      isDisabled: false,
      text: "Đặt làm menu chính",
    }
    return {
      isDisabled: true,
      text: "Đặt làm menu chính",
    }
  }
}

const MainMenuPanel: React.FC<MainMenuPanelProps> = (props) => {
  const {
    menu,
    currentBranch,
    handleRequestNewMenuButtonClick,
    handleSetMainMenu,
    itemList,
    itemTypeList,
  } = props;

  const [loading, setLoading] = useState<{
    setMainMenuAction: boolean,
  }>({
    setMainMenuAction: false,
  });
  const [menuItems, setMenuItems] = useState<Item[]>([]);
  const [filtered, setFiltered] = useState(menuItems);

  const handleSearch = (value: string) => {
    if (!value) {
      setFiltered(menuItems);
    } else {
      let temp = menuItems?.filter((item) =>
        item.name.toLocaleLowerCase().includes(value.toLocaleLowerCase())
      );
      setFiltered(temp);
    }
  };

  useEffect(() => {
    const loadItemsOfMenu = () => {
      let items = [] as Item[];

      menu?.itemList.forEach(menuItem => {
        const rowItem: Item | undefined = itemList.find(
          (value) => value.id === menuItem.itemid
        );
        if (rowItem !== undefined) {
          items.push(rowItem);
          items[items.length - 1] = { ...items[items.length - 1], price: menuItem.price };
        }
      });

      setMenuItems(items);
      setFiltered(items);
    };
    loadItemsOfMenu();
  }, [menu, itemList]);

  return (
    <Col span={17} className="main-menu-panel">
      {!menu ? (
        <Row justify="center" style={{ marginTop: "36vh" }}>
          <Empty description="Tạo hoặc chọn thực đơn để xem thông tin!" />
        </Row>
      ) : (
        <Scrollbars
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              className="track-horizontal"
              style={{ display: "none" }}
            />
          )}
          renderThumbHorizontal={(props) => (
            <div
              {...props}
              className="thumb-horizontal"
              style={{ display: "none" }}
            />
          )}
          autoHide
        >
          <Row justify="end" gutter={16}>
            <Col>
              <Button
                type="dashed"
                onClick={async () => {
                  setLoading({ ...loading, setMainMenuAction: true });
                  await handleSetMainMenu();
                  setLoading({ ...loading, setMainMenuAction: false });
                }}
                shape="round"
                loading={loading.setMainMenuAction}
                disabled={handleSetMainMenuButtonLogic(menu?.statuscode, menu, currentBranch).isDisabled}
              >
                {handleSetMainMenuButtonLogic(menu?.statuscode, menu, currentBranch).text}
              </Button>
            </Col>
            <Col>
              <Button 
                onClick={() => handleRequestNewMenuButtonClick()} 
                shape="round" 
                type="primary"
                disabled={menu?.statuscode === "AVAILABLE" ? false : true}
              >
                Yêu cầu thay đổi giá
              </Button>
            </Col>
          </Row>
          <Row className="mb-5" justify="center" align="middle">
            <Col span={8}>
              <Row justify="center">
                <Avatar
                  src={(menu?.imgpath === null || menu?.imgpath ==="") ? "/item-sample-large.jpg" : menu?.imgpath}
                  className="menu-cover-image"
                />
              </Row>
            </Col>
            <Col span={16}>
              <Typography.Title level={1}>{menu?.name}</Typography.Title>
              <Typography.Paragraph>{menu?.description}</Typography.Paragraph>
              <Row justify="space-between" className="mt-5">
                <Col span={6}>
                  <Statistic title="Số món" value={menu.itemList?.length || 0} />
                </Col>
                <Col span={3}>
                  <Divider type="vertical" style={{ height: "100%" }} />
                </Col>
                <Col span={6}>
                  <Statistic
                    title="Số loại món"
                    value={countItemType(itemTypeList, menuItems).length || 0}
                  />
                </Col>
                <Col span={3}>
                  <Divider type="vertical" style={{ height: "100%" }} />
                </Col>
                <Col span={6}>
                  <Statistic
                    title="Trạng thái"
                    valueStyle={{ color: statusTextAndColor(menu?.statuscode).color }}
                    value={
                      statusTextAndColor(menu?.statuscode).text
                    }
                  />
                </Col>
              </Row>
            </Col>
          </Row>

          <Row gutter={24}>
            <Col>
              <Typography.Title level={4}>Các món ăn</Typography.Title>
            </Col>
            <Col>
              <Input
                onChange={(e) => {
                  handleSearch(e.target.value);
                }}
                className="mb-3"
                placeholder="Tìm kiếm món ăn"
              ></Input>
            </Col>
            <Col>
              <Select
                placeholder="Sắp xếp"
                defaultValue={"name_asc"}
                defaultActiveFirstOption
              >
                <Select.Option value="name_asc" key={1}>
                  Tên tăng dần
                </Select.Option>
                <Select.Option value="name_desc" key={2}>
                  Tên giảm dần
                </Select.Option>
                <Select.Option value="baseprice_asc" key={3}>
                  Giá tăng dần
                </Select.Option>
                <Select.Option value="baseprice_desc" key={4}>
                  Giá giảm dần
                </Select.Option>
              </Select>
            </Col>
          </Row>

          <Row
            className="mt-3 mb-5"
            justify={filtered?.length > 0 ? "start" : "center"}
            style={{ margin: 0 }}
            gutter={[24, 24]}
          >
            {countItemType(itemTypeList, filtered).map((itemType, index) => (
              <Col span={24} key={index}>
                <Row justify="start" className="mb-3">
                  <Typography.Title level={3}>{itemType.name}</Typography.Title>
                </Row>
                <Row gutter={[24, 24]}>
                  {filtered?.length > 0 ? (
                    filtered?.filter(item => item.typeid === itemType.id).map((menuItem: any, index: number) => (
                      <Col key={index}>
                        <ItemFromRow key={menuItem.id} item={menuItem} />
                      </Col>
                    ))
                  ) : (
                    <Col>
                      <Empty description="Không có món nào!" />
                    </Col>
                  )}
                </Row>
              </Col>
            ))}
          </Row>
        </Scrollbars>
      )}
    </Col>
  );
};

export default MainMenuPanel;
