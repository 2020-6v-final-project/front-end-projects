import React, { CSSProperties } from "react";
import { Avatar, Col, Row, Typography, Tag } from "antd";
import "./MenuFromList.scss";

type MenuFromListProps = {
  menu: any;
};

const styleProps: CSSProperties = {
  // overflowX: "hidden",
  // whiteSpace: "nowrap",
  // textOverflow: "ellipsis",
};

export const statusTextAndColor = (statusCode: string): {text: string, color: string} => {
  switch (statusCode) {
    case "AVAILABLE": 
      return {
        text: "Hoạt động",
        color: "success"
      }
    case "DISABLED": 
      return {
        text: "Vô Hiệu",
        color: "error"
      }
    case "PENDINGAPPROVED": 
      return {
        text: "Chờ duyệt",
        color: "warning"
      }
    default:
      return {
        text: "NaN",
        color: "success"
      }
  }
}

const MenuFromList: React.FC<MenuFromListProps> = React.memo((props) => {
  const { menu } = props;
  return (
    <Row
      wrap={false}
      className="item-from-list"
      justify="start"
      gutter={32}
      align="middle"
      style={{ width: "100%" }}
    >
      <Col>
        <Avatar style={{ width: 64, height: 64 }} src={(menu?.imgpath === null || menu?.imgpath ==="") ? "/item-sample.svg" : menu?.imgpath} />
      </Col>
      <Col style={styleProps}>
        <Typography.Title level={5}>{menu.name}</Typography.Title>
        <Row>
          <Tag color={statusTextAndColor(menu?.statuscode).color}>
            {statusTextAndColor(menu?.statuscode).text}
          </Tag>
        </Row>
      </Col>
    </Row>
  );
});

export default MenuFromList;
