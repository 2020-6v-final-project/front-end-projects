import { SubHeaderText } from "@components";
import CIcon from "@coreui/icons-react";
import { freeSet } from "@coreui/icons";
import {
  Button,
  Col,
  Drawer,
  Form,
  Input,
  Row,
  Timeline,
  Typography,
} from "antd";
import React, { useState, useEffect, ChangeEvent } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import { Item, Option } from "@custom-types";
import "./RequestNewMenuModal.scss";

type RequestNewMenuModalProps = {
  show: boolean;
  onFinish: Function;
  toggleModal: Function;
  itemList: Array<Item>;
  optionList: Array<Option>;
  menu: any | null;
  branchId: number | undefined;
};

const formatItemList = (itemList: Item[]) => {
  return itemList.map((item) => {
    const options = item.options?.map((option) => {
      return {
        id: option.optionid,
        price: option.price,
      };
    });
    return {
      id: item.id,
      price: item.price,
      options: options,
    };
  });
};

const RequestNewMenuModal: React.FC<RequestNewMenuModalProps> = (props) => {
  const {
    itemList,
    show,
    toggleModal,
    onFinish,
    menu,
    optionList,
    branchId,
  } = props;

  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState<Item[]>([]);
  const [formValue, setFormValue] = useState<any[]>([]);
  const [note, setNote] = useState<string>("");

  const handleRequestNewMenu = async () => {
    setLoading(true);
    await onFinish(menu?.id, {
      menu: formValue,
      note: note,
      branchId: branchId,
    });
    setLoading(false);
    form.resetFields();
    setItems([]);
    toggleModal();
  };

  useEffect(() => {
    const loadItemsOfMenu = () => {
      let items = [] as Item[];
      if (menu?.itemList?.length > 0) {
        menu.itemList.forEach((menuItem: any) => {
          let rowItem: Item = {
            ...itemList.find((value) => value.id === menuItem.itemid),
          } as Item;
          if (rowItem !== undefined) {
            rowItem.options = menuItem?.optionList?.map(
              (itemMenuOption: any) => {
                return {
                  ...itemMenuOption,
                  name: optionList.find(
                    (defaultOption) =>
                      defaultOption.id === itemMenuOption.optionid
                  )?.name,
                };
              }
            );
            // rowItem.options = [ ...options ];
            items.push(rowItem);
            items[items.length - 1] = {
              ...items[items.length - 1],
              price: menuItem.price,
            };
          }
        });
      }
      setItems(items);
      setFormValue(formatItemList(items));
    };
    loadItemsOfMenu();
  }, [menu, form, itemList, show, optionList]);

  const handleChangeItemPrice = (
    e: ChangeEvent<HTMLInputElement>,
    item: Item
  ) => {
    const index = formValue.findIndex((value: any) => value.id === item.id);
    let changedItem;
    if (index > -1) {
      changedItem = formValue[index];
      const newPrice = parseInt(e.target.value);
      if (newPrice < 0) changedItem.price = (-newPrice).toString();
      else changedItem.price = newPrice.toString();
      const tempFormValue = [...formValue];
      tempFormValue.splice(index, 1, changedItem);
      setFormValue(tempFormValue);
    }
  };

  const handleResetItemPrice = (item: Item) => {
    const index = formValue.findIndex((value: any) => value.id === item.id);
    let changedItem;
    if (index > -1) {
      changedItem = formValue[index];
      changedItem.price = item.price;
      const tempFormValue = [...formValue];
      tempFormValue.splice(index, 1, changedItem);
      setFormValue(tempFormValue);
    }
  };

  const handleChangeOptionPrice = (
    e: ChangeEvent<HTMLInputElement>,
    item: Item,
    optionId: number
  ) => {
    const indexItem = formValue.findIndex((value: any) => value.id === item.id);
    let changedItem;
    let changedOption;
    if (indexItem > -1) {
      changedItem = formValue[indexItem];
      const indexOption = changedItem.options.findIndex(
        (value: any) => value.id === optionId
      );
      if (indexOption > -1) {
        changedOption = changedItem.options[indexOption];
        const newPrice = parseInt(e.target.value);
        if (newPrice < 0) changedOption.price = (-newPrice).toString();
        else changedOption.price = newPrice.toString();
        const tempFormValue = [...formValue];
        changedItem.options.splice(indexOption, 1, changedOption);
        tempFormValue.splice(indexItem, 1, changedItem);
        setFormValue(tempFormValue);
      }
    }
  };

  const handleResetOptionPrice = (item: Item, optionId: number) => {
    const indexItem = formValue.findIndex((value: any) => value.id === item.id);
    let changedItem;
    let changedOption;
    if (indexItem > -1) {
      changedItem = formValue[indexItem];
      const indexOption = changedItem.options.findIndex(
        (value: any) => value.id === optionId
      );
      if (indexOption > -1) {
        changedOption = changedItem.options[indexOption];
        changedOption.price = item.options.find(
          (option) => option.optionid === optionId
        ).price;
        const tempFormValue = [...formValue];
        changedItem.options.splice(indexOption, 1, changedOption);
        tempFormValue.splice(indexItem, 1, changedItem);
        setFormValue(tempFormValue);
      }
    }
  };

  return (
    <Drawer
      visible={show}
      width="800"
      onClose={(e) => toggleModal()}
      zIndex={1}
      className="add-item-modal"
    >
      <Scrollbars autoHide>
        <Row style={{ margin: 20 }} justify="center" align="middle">
          <Col span={24}>
            <SubHeaderText style={{ textAlign: "center" }} className="mb-4">
              Yêu cầu đổi giá món ăn
            </SubHeaderText>
            <Form
              name="changePriceForm"
              form={form}
              onFinish={handleRequestNewMenu}
            >
              <Row justify="center">
                <Col>
                  <Form.Item
                    name="name"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền tên thực đơn",
                      },
                    ]}
                    initialValue={menu?.name}
                  >
                    <Input
                      style={{
                        border: "none",
                        textAlign: "center",
                        fontSize: 18,
                        fontWeight: 400,
                      }}
                      value={menu?.name || ""}
                      placeholder="Tên thực đơn"
                      disabled
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row justify="center">
                <Col span={24}>
                  <Form.Item
                    name="description"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng điền mô tả",
                      },
                    ]}
                  >
                    <Input.TextArea
                      showCount
                      maxLength={100}
                      autoSize={{ minRows: 3, maxRows: 3 }}
                      placeholder="Ghi chú"
                      value={note || ""}
                      onChange={(e) => setNote(e.target.value)}
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row justify="center" className="mt-4">
                <Typography.Title level={4}>Món ăn</Typography.Title>
              </Row>
              <Row justify="center" className="mt-3">
                <Col style={{ width: "100%" }}>
                  <Timeline>
                    {items?.map((item, itemIndex) => (
                      <Timeline.Item key={itemIndex}>
                        <Row justify="space-between" gutter={24}>
                          <Col span={14}>
                            <Input value={item.name} disabled />
                          </Col>
                          <Col span={6}>
                            <Input
                              type="number"
                              value={
                                formValue?.find(
                                  (value: any) => value.id === item.id
                                )?.price
                              }
                              onChange={(e) => handleChangeItemPrice(e, item)}
                            />
                          </Col>
                          <Col span={4}>
                            <Button
                              onClick={() => handleResetItemPrice(item)}
                              type="text"
                            >
                              <CIcon content={freeSet.cilSync}></CIcon>
                            </Button>
                          </Col>
                        </Row>
                        {item.options?.length > 0 ? (
                          <Row justify="start" className="mt-4">
                            <Typography.Title level={5}>
                              Tùy chọn
                            </Typography.Title>
                          </Row>
                        ) : (
                          <></>
                        )}
                        <Row justify="center" className="mt-2">
                          <Col style={{ width: "100%" }}>
                            <Timeline>
                              {item.options?.map((option, optionIndex) => (
                                <Timeline.Item key={optionIndex}>
                                  <Row
                                    justify="space-between"
                                    className="mb-3"
                                    gutter={24}
                                  >
                                    <Col span={13}>
                                      <Input value={option.name} disabled />
                                    </Col>
                                    <Col span={7}>
                                      <Input
                                        type="number"
                                        value={
                                          formValue
                                            ?.find(
                                              (value) => value.id === item.id
                                            )
                                            ?.options.find(
                                              (formOptionValue: any) =>
                                                formOptionValue.id ===
                                                option.optionid
                                            )?.price
                                        }
                                        onChange={(e) =>
                                          handleChangeOptionPrice(
                                            e,
                                            item,
                                            option.optionid
                                          )
                                        }
                                      />
                                    </Col>
                                    <Col span={4}>
                                      <Button
                                        onClick={() =>
                                          handleResetOptionPrice(
                                            item,
                                            option.optionid
                                          )
                                        }
                                        type="text"
                                      >
                                        <CIcon
                                          content={freeSet.cilSync}
                                        ></CIcon>
                                      </Button>
                                    </Col>
                                  </Row>
                                </Timeline.Item>
                              ))}
                            </Timeline>
                          </Col>
                        </Row>
                      </Timeline.Item>
                    ))}
                  </Timeline>
                </Col>
              </Row>

              <Row justify="center">
                <Form.Item>
                  <Button loading={loading} type="primary" htmlType="submit">
                    Gửi yêu cầu
                  </Button>
                </Form.Item>
              </Row>
            </Form>
          </Col>
        </Row>
      </Scrollbars>
    </Drawer>
  );
};

export default RequestNewMenuModal;
