import React from "react";
import { Row, Col, Card, Avatar, Typography } from "antd";
import "./ItemFromRow.scss";
import { Item } from "@custom-types";
import { currencyFormat } from "@utils";

type ItemFromRowProps = {
  key: number;
  item: Item | undefined;
};

const ItemFromRow: React.FC<ItemFromRowProps> = React.memo((props) => {
  const { item } = props;
  return (
    <Card
      hoverable
      size="default"
      style={{
        borderRadius: 20,
        padding: "16px",
        width: 224,
        overflow: "hidden",
        textOverflow: "ellipsis",
        whiteSpace: "nowrap",
      }}
    >
      <Row justify="center">
        <Avatar size={64}src={(item?.imgpath === null || item?.imgpath ==="") ? "/item-sample.svg" : item?.imgpath}></Avatar>
      </Row>
      <Row className="mt-3" justify="center">
        <Col>
          <Typography.Title level={5}>{item?.name}</Typography.Title>
        </Col>
      </Row>
      <Row justify="center">
        <Col>
          <Typography.Text>
            {currencyFormat(item?.price)} / {item?.unit}
          </Typography.Text>
        </Col>
      </Row>
    </Card>
  );
});

export default ItemFromRow;
