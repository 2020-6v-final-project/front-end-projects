import { Menu } from "@shared";
import {
  Col,
  Input,
  List,
  Popover,
  Row,
  Spin,
  Divider,
  Empty,
  Select,
  Tabs,
} from "antd";
import React, { useEffect, useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import MenuFromList from "../MenuFromList/MenuFromList";
import "./SideMenuPanel.scss";

type MenuSidePanelProps = {
  menus: Array<Menu>;
  currentMenu?: any;
};

type MenuSidePanelDispatch = {
  handleMenuSelect: Function;
};

type MenuSidePanelDefaultProps = MenuSidePanelProps & MenuSidePanelDispatch;

const SidePanel: React.FC<MenuSidePanelDefaultProps> = React.memo((props) => {
  const { menus, handleMenuSelect, currentMenu } = props;
  const [loading, setLoading] = useState(false);
  const [filtered, setFiltered] = useState(menus);
  const [shouldRender, setShouldRender] = useState(false);

  useEffect(() => {
    if (menus.length < 1) {
      setLoading(false);
      setShouldRender(false);
    } else {
      setLoading(false);
      setShouldRender(true);
      setFiltered(menus);
    }
  }, [menus]);

  const handleSearch = (value: string) => {
    if (!value) {
      setFiltered(menus);
    } else {
      const temp = menus.filter((menu) =>
        menu.name.toLocaleLowerCase().includes(value.toLocaleLowerCase())
      );
      setFiltered(temp);
    }
  };

  return (
    <Col span={7} style={{ height: "90vh" }}>
      <Row justify="space-between" gutter={16}>
        <Col span={14}>
          <Input
            style={{ width: "100%" }}
            onChange={(e) => {
              handleSearch(e.target.value);
            }}
            className="mb-2"
            placeholder="Tìm kiếm thực đơn"
          />
        </Col>
        <Col span={10}>
          <Select
            style={{ width: "100%" }}
            placeholder="Sắp xếp"
            defaultValue={"name_asc"}
            defaultActiveFirstOption
          >
            <Select.Option value="name_asc" key={1}>
              Tên tăng dần
            </Select.Option>
            <Select.Option value="name_desc" key={2}>
              Tên giảm dần
            </Select.Option>
            <Select.Option value="baseprice_asc" key={3}>
              Giá tăng dần
            </Select.Option>
            <Select.Option value="baseprice_desc" key={4}>
              Giá giảm dần
            </Select.Option>
          </Select>
        </Col>
      </Row>

      <Row justify="center">
        <Tabs
          centered
          defaultActiveKey="1"
          style={{ height: "65vh", width: "100%", padding: "0px" }}
        >
          <Tabs.TabPane tab="Tất cả" key="1">
            <Row style={{ height: "65vh", padding: "0px" }} justify="center">
              <Col span={23}>
                {loading ? (
                  <Spin
                    size="large"
                    style={{ textAlign: "center", margin: "auto" }}
                    spinning={loading}
                  />
                ) : (
                  <>
                    {shouldRender ? (
                      <>
                        <Scrollbars>
                          <List
                            className="list-ingredient"
                            style={{ marginBottom: "176px" }}
                          >
                            {filtered?.map((menu, index) => (
                              <Popover key={index} content={menu.description}>
                                <List.Item
                                  onClick={() => handleMenuSelect(menu)}
                                  className={
                                    currentMenu?.id === menu.id
                                      ? "active-menu-from-list"
                                      : ""
                                  }
                                >
                                  <MenuFromList menu={menu} />
                                </List.Item>
                              </Popover>
                            ))}
                          </List>
                        </Scrollbars>
                      </>
                    ) : (
                      <Empty
                        style={{ marginTop: "30vh" }}
                        description="Chưa có thực đơn!"
                      />
                    )}
                  </>
                )}
              </Col>

              <Col span={1}>
                <Divider type="vertical" style={{ height: "100%" }} />
              </Col>
            </Row>
          </Tabs.TabPane>
          <Tabs.TabPane tab="Đang hoạt động" key="2">
            <Row style={{ height: "65vh", padding: "0px" }} justify="center">
              <Col span={23}>
                {loading ? (
                  <Spin
                    size="large"
                    style={{ textAlign: "center", margin: "auto" }}
                    spinning={loading}
                  />
                ) : (
                  <>
                    {shouldRender ? (
                      <>
                        <Scrollbars>
                          <List
                            className="list-ingredient"
                            style={{ marginBottom: "176px" }}
                          >
                            {filtered
                              ?.filter(
                                (menu) => menu.statuscode === "AVAILABLE"
                              )
                              .map((menu, index) => (
                                <Popover key={index} content={menu.description}>
                                  <List.Item
                                    onClick={() => handleMenuSelect(menu)}
                                    className={
                                      currentMenu?.id === menu.id
                                        ? "active-menu-from-list"
                                        : ""
                                    }
                                  >
                                    <MenuFromList menu={menu} />
                                  </List.Item>
                                </Popover>
                              ))}
                          </List>
                        </Scrollbars>
                      </>
                    ) : (
                      <Empty
                        style={{ marginTop: "30vh" }}
                        description="Chưa có thực đơn!"
                      />
                    )}
                  </>
                )}
              </Col>

              <Col span={1}>
                <Divider type="vertical" style={{ height: "100%" }} />
              </Col>
            </Row>
          </Tabs.TabPane>
          <Tabs.TabPane tab="Bị vô hiệu" key="3">
            <Row style={{ height: "65vh", padding: "0px" }} justify="center">
              <Col span={23}>
                {loading ? (
                  <Spin
                    size="large"
                    style={{ textAlign: "center", margin: "auto" }}
                    spinning={loading}
                  />
                ) : (
                  <>
                    {shouldRender ? (
                      <>
                        <Scrollbars>
                          <List
                            className="list-ingredient"
                            style={{ marginBottom: "176px" }}
                          >
                            {filtered
                              ?.filter((menu) => menu.statuscode === "DISABLED")
                              .map((menu, index) => (
                                <Popover key={index} content={menu.description}>
                                  <List.Item
                                    onClick={() => handleMenuSelect(menu)}
                                    className={
                                      currentMenu?.id === menu.id
                                        ? "active-menu-from-list"
                                        : ""
                                    }
                                  >
                                    <MenuFromList menu={menu} />
                                  </List.Item>
                                </Popover>
                              ))}
                          </List>
                        </Scrollbars>
                      </>
                    ) : (
                      <Empty
                        style={{ marginTop: "30vh" }}
                        description="Chưa có thực đơn!"
                      />
                    )}
                  </>
                )}
              </Col>

              <Col span={1}>
                <Divider type="vertical" style={{ height: "100%" }} />
              </Col>
            </Row>
          </Tabs.TabPane>
          <Tabs.TabPane tab="Chờ duyệt" key="4">
            <Row style={{ height: "65vh", padding: "0px" }} justify="center">
              <Col span={23}>
                {loading ? (
                  <Spin
                    size="large"
                    style={{ textAlign: "center", margin: "auto" }}
                    spinning={loading}
                  />
                ) : (
                  <>
                    {shouldRender ? (
                      <>
                        <Scrollbars>
                          <List
                            className="list-ingredient"
                            style={{ marginBottom: "176px" }}
                          >
                            {filtered
                              ?.filter(
                                (menu) => menu.statuscode === "PENDINGAPPROVED"
                              )
                              .map((menu, index) => (
                                <Popover key={index} content={menu.description}>
                                  <List.Item
                                    onClick={() => handleMenuSelect(menu)}
                                    className={
                                      currentMenu?.id === menu.id
                                        ? "active-menu-from-list"
                                        : ""
                                    }
                                  >
                                    <MenuFromList menu={menu} />
                                  </List.Item>
                                </Popover>
                              ))}
                          </List>
                        </Scrollbars>
                      </>
                    ) : (
                      <Empty
                        style={{ marginTop: "30vh" }}
                        description="Chưa có thực đơn!"
                      />
                    )}
                  </>
                )}
              </Col>

              <Col span={1}>
                <Divider type="vertical" style={{ height: "100%" }} />
              </Col>
            </Row>
          </Tabs.TabPane>
        </Tabs>
      </Row>
    </Col>
  );
});

export default SidePanel;
