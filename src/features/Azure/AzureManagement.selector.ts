import { createSelector } from 'reselect';
import { RootState } from 'src/store';

export const selectAzureConfig = createSelector(
    [(state: RootState) => state.azure.azureKey],
    (azureKey) => azureKey
)