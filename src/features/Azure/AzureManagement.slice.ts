import { getAzureKey, } from '@apis';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppThunk } from 'src/store';
import swal from 'sweetalert';

type AzureManagementInitialState = {
    azureKey: string,
}

const initialState: AzureManagementInitialState = {
    azureKey: "",
}

const AzureManagementSlice = createSlice({
    name: 'AzureManagement',
    initialState,
    reducers: {
        getKeySuccess: (state, action: PayloadAction<string>) => {
            state.azureKey = action.payload;
            return state;
        },
        getKeyFailed: (state) => {
            return state;
        },
    }
})

export const getKeyAsync = (): AppThunk => async (dispatch) => {
    try {
        await getAzureKey((data: string) => {
            dispatch(getKeySuccess(data));
        });
    } catch (e) {
        swal("Thất bại!","Không thể tải Azure key!","error");
        dispatch(getKeyFailed());
    }
}


const {
    getKeySuccess,
    getKeyFailed,
} = AzureManagementSlice.actions;

export {
    getKeySuccess,
    getKeyFailed,
};

export default AzureManagementSlice.reducer;
