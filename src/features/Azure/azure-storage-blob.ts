import { BlobServiceClient, ContainerClient} from '@azure/storage-blob';
import { v4 as uuidv4 } from 'uuid';
// const sasToken = process.env.storagesastoken || ""; // Fill string with your SAS token
const containerName = `6v-container`;
const storageAccountName = process.env.storageresourcename || "cookit6v"; // Fill string with your Storage resource name
// </snippet_package>

// <snippet_isStorageConfigured>
// Feature flag - disable storage feature to app if not configured
// export const isStorageConfigured = () => {
//   return (!storageAccountName || !sasToken) ? false : true;
// }
// </snippet_isStorageConfigured>

// <snippet_getBlobsInContainer>
// return list of blobs in container to display
// const getBlobsInContainer = async (containerClient: ContainerClient) => {
//   const returnedBlobUrls: string[] = [];

//   // get list of blobs in container
//   // eslint-disable-next-line
//   for await (const blob of containerClient.listBlobsFlat()) {
//     // if image is public, just construct URL
//     returnedBlobUrls.push(
//       `https://${storageAccountName}.blob.core.windows.net/${containerName}/${blob.name}`
//     );
//   }

//   return returnedBlobUrls;
// }
// </snippet_getBlobsInContainer>

// <snippet_createBlobInContainer>
const createBlobInContainer = async (containerClient: ContainerClient, file: File) => {
  
  // create blobClient for container
  const blobClient = containerClient.getBlockBlobClient(uuidv4());

  // set mimetype as determined from browser with file upload control
  const options = { blobHTTPHeaders: { blobContentType: file.type } };

  // upload file
  await blobClient.uploadBrowserData(file, options);
  return blobClient;
}
// </snippet_createBlobInContainer>

// <snippet_uploadFileToBlob>
export const uploadFileToBlob = async (file: File | null, sasToken: string): Promise<any> => {
  if (!file) return [];

  // get BlobService = notice `?` is pulled out of sasToken - if created in Azure portal
  const blobService = new BlobServiceClient(
    `https://${storageAccountName}.blob.core.windows.net/?${sasToken}`
    // 'sv=2020-02-10&ss=b&srt=sco&sp=rwdlacx&se=2021-10-30T16:10:37Z&st=2021-03-20T08:10:37Z&spr=https,http&sig=ipcrEpc3gTf867iX8dJEqozsvLp8rPGoctnMDjhCoGk%3D`
  );

  // get Container - full public read access
  const containerClient: ContainerClient = blobService.getContainerClient(containerName);
  // await containerClient.createIfNotExists({
  //   access: 'container',
  // });

  // upload file
  const newFile = await createBlobInContainer(containerClient, file);
  return newFile;
  // get list of blobs in container
  // return getBlobsInContainer(containerClient);
};
// </snippet_uploadFileToBlob>

export const azureStorageBlobURL = `https://${storageAccountName}.blob.core.windows.net/?$`;

