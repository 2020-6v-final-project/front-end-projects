import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { RootState } from "src/store";
import { routesConstants } from "@constants/RoutesConstants";
import { checkPermission } from "@utils";
import { Role } from "@custom-types/Role";
import { useHistory } from "react-router-dom";

type AccessProviderProps = {
  roles: Role[];
};

const AccessProvider: React.FC<AccessProviderProps> = (props) => {
  const history = useHistory();
  const [shouldRender, setShouldRender] = useState(false);
  const { roles } = props;

  useEffect(() => {
    const targetPath =
      routesConstants.find((item) =>
        history.location.pathname.includes(item.path)
      ) || ([] as any);
    if (checkPermission(roles, targetPath.allowedPermissions)) {
      setShouldRender(true);
    } else {
      setShouldRender(false);
      history.push("/dashboard");
    }
  }, [roles, history.location, history]);

  return <>{shouldRender && props.children}</>;
};

const mapState = (state: RootState) => {
  return {
    roles: state.auth.roles,
  };
};

export default connect(mapState)(AccessProvider);
