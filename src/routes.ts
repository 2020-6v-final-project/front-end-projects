import React from "react";
const Dashboard = React.lazy(
  () => import("./features/SAC/Dashboard/Dashboard")
);
const UserManagement = React.lazy(
  () => import("./features/SAC/UserManagement/index")
);
const BranchManagement = React.lazy(
  () => import("./features/SAC/BranchManagement/")
);
const OrderManagement = React.lazy(
  () => import("./features/Receptionist/OrderManagement")
);
const ItemManagement = React.lazy(
  () => import("./features/SAC/ItemManagement")
);
const IngredientManagement = React.lazy(
  () => import("./features/SAC/IngredientManagement/")
);
const MenuManagement = React.lazy(
  () => import("./features/SAC/MenuManagement/")
);
const StorageManagement = React.lazy(
  () => import('./features/Manager/StorageManagement/')
);
const TableManagement = React.lazy(
  () => import('./features/Manager/TableManagement/')
);
const RepositoryManagement = React.lazy(
  () => import("./features/SAC/RepositoryManagement/")
);
const ItemIngredientTypeManagement = React.lazy(
  () => import('./features/SAC/ItemIngredientTypeManagement/')
);
const ManagerRequestManagement = React.lazy(
  () => import('./features/Manager/ManagerRequestManagement')
);
const CompanyRequestManagement = React.lazy(
  () => import('./features/SAC/CompanyRequestManagement')
);
const VendorManagement = React.lazy(
  () => import('./features/SAC/VendorManagement')
);
const TransactionManagement = React.lazy(
  () => import('./features/Manager/TransactionManagement')
);
const MenuBranchManagement = React.lazy(
  () => import('./features/Manager/MenuManagement')
);
const FinanceManagement = React.lazy(
  () => import('./features/Manager/FinanceManagement')
);
const PromotionManagement = React.lazy(
  () => import('./features/SAC/PromotionManagement')
);
const CampaignManagement = React.lazy(
  () => import('./features/SAC/CampaignManagement')
);
const ReceiptDetail = React.lazy(
  () => import('./features/ShareComponents/ReceiptDetailComponent')
);

const routes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  {
    path: "/user-management",
    exact: true,
    name: "User Management",
    component: UserManagement
  },
  {
    path: "/branch-management",
    exact: true,
    name: "Branch Management",
    component: BranchManagement
  },
  {
    path: "/order-management",
    exact: true,
    name: "Order Management",
    component: OrderManagement
  },
  {
    path: "/category",
    exact: true,
    name: "Category Management",
    component: ItemIngredientTypeManagement
  },
  {
    path: "/item-management",
    exact: true,
    name: "Item Management",
    component: ItemManagement,
  },
  {
    path: "/ingredient-management",
    exact: true,
    name: "Ingredient Management",
    component: IngredientManagement,
  },
  {
    path: "/menu-management",
    exact: true,
    name: "Menu Management",
    component: MenuManagement,
  },
  {
    path: "/menu-branch-management",
    exact: true,
    name: "Branch Menu Management",
    component: MenuBranchManagement,
  },{
    path: "/menu-branch-management/:id",
    exact: true,
    name: "Branch Menu Management",
    component: MenuBranchManagement,
  },
  {
    path: "/item-management/:id",
    exact: true,
    name: "Item Management",
    component: ItemManagement,
  },
  {
    path: "/ingredient-management/:id",
    exact: true,
    name: "Ingredient Management",
    component: IngredientManagement,
  },
  {
    path: "/menu-management/:id",
    exact: true,
    name: "Menu Management",
    component: MenuManagement,
  },
  {
    path: "/storage-management",
    exact: true,
    name: "Storage Management",
    component: StorageManagement,
  },
  {
    path: "/storage-management/receipt/:id",
    exact: true,
    name: "Storage Management",
    component: StorageManagement,
  },    
  {
    path: "/receipt-detail/branch/:branchid/receipt/:receiptid",
    exact: true,
    name: "Receipt Detail",
    component: ReceiptDetail,
  },
  {
    path: "/table-management",
    exact: true,
    name: "Table Management",
    component: TableManagement,
  },
  {
    path: "/repository-management",
    exact: true,
    name: "Repository Management",
    component: RepositoryManagement,
  },
  {
    path: "/manager-request-management",
    exact: true,
    name: "Manager Request Management",
    component: ManagerRequestManagement,
  },
  {
    path: "/company-request-management",
    exact: true,
    name: "Company Request Management",
    component: CompanyRequestManagement,
  },
  {
    path: "/vendor-management",
    exact: true,
    name: "Vendor Management",
    component: VendorManagement,
  },
  {
    path: "/vendor-management/:id",
    exact: true,
    name: "Vendor Management",
    component: VendorManagement,
  },
  {
    path: "/transaction-management/",
    exact: true,
    name: "Transaction Management",
    component: TransactionManagement,
  },
  {
    path: "/transaction-management/:id",
    exact: true,
    name: "Transaction Management",
    component: TransactionManagement,
  },
  {
    path: "/finance-management/",
    exact: true,
    name: "Finance Management",
    component: FinanceManagement,
  },
  {
    path: "/finance-management/:id",
    exact: true,
    name: "Finance Management",
    component: FinanceManagement,
  },
  {
    path: "/promotion-management",
    exact: true,
    name: "Promotion Management",
    component: PromotionManagement,
  },
  {
    path: "/campaign-management",
    exact: true,
    name: "Campaign Management",
    component: CampaignManagement,
  },
  {
    path: "/campaign-management/:id",
    exact: true,
    name: "Campaign Management",
    component: CampaignManagement,
  },
];

export default routes;
